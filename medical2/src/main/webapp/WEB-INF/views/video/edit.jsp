<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>

<jsp:include page="/WEB-INF/layout/begin-header.jsp"></jsp:include>

<link rel="stylesheet" href="/resources/css/jquery-ui.css"/>
<link rel="stylesheet" href="/resources/css/jquery-ui.support.css"/>
<link rel="stylesheet" href="/resources/css/visual.progress.css"/>

<%/*
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"/>
*/%>
<link rel="stylesheet" href="/resources/css/popup-support.css"/>
<link rel="stylesheet" href="/resources/css/edit.css"/>

<jsp:include page="/WEB-INF/items/video-edit.jsp"></jsp:include>

<script type="text/javascript" src="/resources/js/jquery.ui.1.11.4.js"></script>
<script type="text/javascript" src="/resources/js/jquery.ui.init.js"></script>

<script type="text/javascript" src="/resources/js/common/form.controller.js"></script>
<script type="text/javascript" src="/resources/js/common/visual.loader.js"></script>
<script type="text/javascript" src="/resources/js/common/visual.progress.js"></script>

<script type="text/javascript" src="/resources/js/views/video/label.editor.js"></script>
<script type="text/javascript" src="/resources/js/views/video/edit.js"></script>

<script>
$(function(){
	MMEditor.initialize();
	
	//-----------------------------------------------------------
	// 컨테이너가 스크롤 시 Date Picker 숨김 처리 
	
	$('#upload .info .upload-info-list>ul').scroll(function(){
		console.log('<<SCROLL>>');
		var e = $('input[name="pat_reg_date"]');
		e.datepicker('hide');
		e.blur();
	});
	
	//-----------------------------------------------------------
	// 인자값 처리
	
	var paramKey = $('#param-key').val();
	if (paramKey){
		MMEditor.open(paramKey);
	}
});
</script>

<jsp:include page="/WEB-INF/layout/end-header.jsp"></jsp:include>
<jsp:include page="/WEB-INF/layout/begin-body.jsp"></jsp:include>

<!-- 인자 전달 (시작) -->
<c:if test="${not empty key}">
	<input type="hidden" id="param-key" value="${key}"/>
</c:if>
<!-- 인자 전달 (끝) -->

<jsp:include page="/WEB-INF/layout/header.jsp"></jsp:include>
	
	<jsp:include page="/WEB-INF/debug/sample-datas.jsp"></jsp:include>

	<form id="form-screen" action="api/media-upload/edit" enctype="multipart/form-data" autocomplete="off" style="display: none;">
	<input type="hidden" name="u-type" value="screen"/>
	</form>

	<form id="form-snapshot" action="api/media-upload/edit" enctype="multipart/form-data" autocomplete="off" style="display: none;">
	<input type="hidden" name="u-type" value="snapshot"/>
	</form>

	<form id="form-removed-snapshot" action="api/media-upload/edit" enctype="multipart/form-data" autocomplete="off" style="display: none;">
	<input type="hidden" name="u-type" value="removed-snapshot"/>
	</form>

	<form id="form-meta" action="api/media-upload/edit" enctype="multipart/form-data" autocomplete="off" style="display: none;">
	<input type="hidden" name="u-type" value="meta"/>
	</form>

	<form id="form-image" action="api/media-upload/upload-modify-image" enctype="multipart/form-data" autocomplete="off" style="display: none;">
	</form>

  <section id="upload">
  	<input type="file" name="video" style="display: none;"/>
  	
    <div class="inner">
    
	<tag:video-edit-info script="MMEditor"/>
    
     <div mv-name="file-area" class="upload">
        <div class="upload-title">
          <input type="text" class="title-input" placeholder="제목" name="title"/>
          <a href="javascript:void(0)" class="v-regist-btn" mv-name="upload">수정</a>
        </div>
        <div mv-name="video-area">
        	<div>
				<tag:video-player id="vp" mode="edit"/>
				<tag:video-image-register id="vie" mode="edit"/>
				
				<!-- 디버그용 시작 -->
				<jsp:include page="/WEB-INF/debug/sample-buttons.jsp"></jsp:include>
				<!-- 디버그용 끝 -->
			</div>
        </div>
      </div>
    </div>
 </section>

<!-- 팝업: 스크린샷 시작 -->
<tag:popup-video-snapshot id="label-editor"/>
<!-- 팝업: 스크린샷 끝 -->

<!-- 팝업: 마스크 시작 -->
<tag:popup-video-mask id="mask-editor"/>
<!-- 팝업: 마스크 끝 -->

<!-- 서버 전송 메시지 시작 -->
<tag:visual-loader id="v-loader"/>
<!-- 서버 전송 메시지 끝 -->	

<!-- 서버 전송 메시지 시작 -->
<tag:visual-progress id="vprog-upload"/>
<!-- 서버 전송 메시지 끝 -->	

<jsp:include page="/WEB-INF/layout/end-body.jsp"></jsp:include>
