<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>

<jsp:include page="/WEB-INF/layout/begin-header.jsp"></jsp:include>

<link rel="stylesheet" href="/resources/css/jquery-ui.css"/>
<link rel="stylesheet" href="/resources/css/jquery-ui.support.css"/>
<link rel="stylesheet" href="/resources/css/visual.progress.css"/>

<%/*
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"/>
*/%>

<link rel="stylesheet" href="/resources/css/popup-support.css"/>
<link rel="stylesheet" href="/resources/css/upload.css"/>

<jsp:include page="/WEB-INF/items/video-edit.jsp"></jsp:include>

<script type="text/javascript" src="/resources/js/jquery.ui.1.11.4.js"></script>
<script type="text/javascript" src="/resources/js/jquery.ui.init.js"></script>

<script type="text/javascript" src="/resources/js/common/form.controller.js"></script>
<script type="text/javascript" src="/resources/js/common/visual.progress.js"></script>

<script type="text/javascript" src="/resources/js/views/video/label.editor.js"></script>
<script type="text/javascript" src="/resources/js/views/video/upload.js"></script>

<script>
$(function(){
	MMUploader.initialize();
	
	<%/*
	//MMUploader.openVideo('https://sqiengbucket.s3.ap-northeast-2.amazonaws.com/medi/7e5a5c63bad31fd2447d3ee5779ee96a?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAJLYUFXGSPXR335ZQ%2F20190419%2Fap-northeast-2%2Fs3%2Faws4_request&X-Amz-Date=20190419T104609Z&X-Amz-Expires=900&X-Amz-Signature=a6795191092bd13df9e604ee4e9370002bb9faf09c043939e7b3671b021105c3&X-Amz-SignedHeaders=host');
	//MMUploader.openVideo('/media/b');
	*/%>
	
	<%/*
	//var url = 'http://13.209.99.248/media/screen/D7F40B7791F54080A344B3F1EE09B596';
	var url = $('#sample_snapshot_image_1').attr('src');
	MMUploader.video.player.hide();
	MMUploader.image.register.show();
	
	MMUploader.mediaType = MMMediaType.IMAGE;
	MMUploader.data.image = url;
	MMUploader.openImage(url);
	*/%>

	
	//-----------------------------------------------------------
	// 체크용
	
	if (typeof sampleLinks == 'function') sampleLinks();
	
	//-----------------------------------------------------------
	// 컨테이너가 스크롤 시 Date Picker 숨김 처리 
	
	$('#upload .info .upload-info-list>ul').scroll(function(){
		console.log('<<SCROLL>>');
		var e = $('input[name="pat_reg_date"]');
		e.datepicker('hide');
		e.blur();
	});
});
</script>

<!-- 디버그용 시작 -->
<jsp:include page="/WEB-INF/debug/sample-script.jsp"></jsp:include>
<!-- 디버그용 끝 -->

<jsp:include page="/WEB-INF/layout/end-header.jsp"></jsp:include>
<jsp:include page="/WEB-INF/layout/begin-body.jsp"></jsp:include>

<jsp:include page="/WEB-INF/layout/header.jsp"></jsp:include>

	<jsp:include page="/WEB-INF/debug/sample-datas.jsp"></jsp:include>
	
	<form id="form-media" action="api/media-upload/upload" enctype="multipart/form-data" autocomplete="off" style="display: none;">
	<input type="hidden" name="u-type" value="media"/>
	</form>

	<form id="form-screen" action="api/media-upload/upload" enctype="multipart/form-data" autocomplete="off" style="display: none;">
	<input type="hidden" name="u-type" value="screen"/>
	</form>

	<form id="form-snapshot" action="api/media-upload/upload" enctype="multipart/form-data" autocomplete="off" style="display: none;">
	<input type="hidden" name="u-type" value="snapshot"/>
	</form>

	<form id="form-meta" action="api/media-upload/upload" enctype="multipart/form-data" autocomplete="off" style="display: none;">
	<input type="hidden" name="u-type" value="meta"/>
	</form>

	<form id="form-image" action="api/media-upload/upload-image" enctype="multipart/form-data" autocomplete="off" style="display: none;">
	</form>

  <section id="upload">
  	<input type="file" name="video" style="display: none;"/>
  	
    <div class="inner">
    
	<tag:video-edit-info script="MMUploader"/>
    
     <div mv-name="file-area" class="upload">
        <div class="upload-title">
          <input type="text" class="title-input" placeholder="제목" name="title"/>
          <a href="javascript:void(0)" class="v-regist-btn" mv-name="upload">등록</a>
        </div>
        <div mv-name="file-btn">
          <div class="img"><img src="/resources/img/icon/i-cloud.png" alt=""></div>
          <div class="txt">
            <p>Choose a video or drag it here</p>
            <p>선택하신 영상을 Drag 를 이곳에 올릴 시 사용하여 업로드가 가능합니다</p>
          </div>
        </div>
        <div mv-name="video-area" class="mv-hide">
        	<div>
			    <%/*
				<jsp:include page="/WEB-INF/items/video-player.jsp"></jsp:include>
				*/%>
				
				<tag:video-player id="vp" mode="edit"/>
				<tag:video-image-register id="vie" mode="edit"/>
				
				<!-- 디버그용 시작 -->
				<jsp:include page="/WEB-INF/debug/sample-buttons.jsp"></jsp:include>
				<!-- 디버그용 끝 -->
			
			</div>
        </div>
      </div>
    </div>
 </section>
 
<!-- 팝업: 스크린샷 시작 -->
<tag:popup-video-snapshot id="label-editor"/>
<!-- 팝업: 스크린샷 끝 -->

<!-- 팝업: 마스크 시작 -->
<tag:popup-video-mask id="mask-editor"/>
<!-- 팝업: 마스크 끝 -->

<!-- 서버 전송 메시지 시작 -->
<tag:visual-progress id="vprog-upload"/>
<!-- 서버 전송 메시지 끝 -->	

<jsp:include page="/WEB-INF/layout/end-body.jsp"></jsp:include>
