<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>

<jsp:include page="/WEB-INF/layout/begin-header.jsp"></jsp:include>

<link rel="stylesheet" href="/resources/css/jquery-ui.css"/>
<link rel="stylesheet" href="/resources/css/jquery-ui.support.css"/>
<link rel="stylesheet" href="/resources/css/visual.progress.css"/>

<%/*
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"/>
*/%>
<link rel="stylesheet" href="/resources/css/view.css"/>

<jsp:include page="/WEB-INF/items/video.jsp"></jsp:include>

<script type="text/javascript" src="/resources/js/jquery.ui.1.11.4.js"></script>
<script type="text/javascript" src="/resources/js/jquery.ui.init.js"></script>

<script type="text/javascript" src="/resources/js/common/form.controller.js"></script>

<script type="text/javascript" src="/resources/js/views/video/view.js"></script>

<script>
$(function(){
	MMViewer.initialize();
	
	//-----------------------------------------------------------

	$('.tab li').click(function(e){
		e.preventDefault();
		var idx = $(this).index();
		$(this).addClass('on').siblings().removeClass('on');
		$('.tab-info > div').eq(idx).addClass('on').siblings().removeClass('on');
	});
	
	//-----------------------------------------------------------
	// 인자값 처리
	
	var paramKey = $('#param-key').val();
	if (paramKey){
		MMViewer.open(paramKey);
	}
});
</script>

<jsp:include page="/WEB-INF/layout/end-header.jsp"></jsp:include>
<jsp:include page="/WEB-INF/layout/begin-body.jsp"></jsp:include>

<!-- 인자 전달 (시작) -->
<c:if test="${not empty key}">
	<input type="hidden" id="param-key" value="${key}"/>
</c:if>
<!-- 인자 전달 (끝) -->

<%/*
<jsp:include page="/WEB-INF/layout/header-nosearchbar.jsp"></jsp:include>
*/%>
<jsp:include page="/WEB-INF/layout/header.jsp"></jsp:include>

  <section id="view">
    <div class="inner">
      <div class="side-list">
        <div class="screen-search">
          <input mv-name="snapshot-search" type="text" placeholder="Search for Screenshot"/>
        </div>
        <div class="list">
          <ul mv-name="snapshot-list" class="snapshot-list">
          	<!--
            <li>
              <div class="thumbnail">
                <img src="" alt="">
                <span class="time">00:00</span>
              </div>
              <div class="descript">
                <ul>
                  <li>Rectums</li>
                  <li>6mm</li>
                  <li>Yamada type3</li>
                  <li>Hyperplastic potyp</li>
                </ul>
              </div>
            </li>
            -->
          </ul>
        </div>
      </div>
      <div class="video-container">
        <div class="player">
			<tag:video-player id="vp" mode="view"/>
			<div id="vie" class="image-viewer"></div>
        </div>

        <div class="subject">
          <span class="fl" name="title"></span>
          <span class="edit">
            <button class="modify" onclick="location.href='/video/edit/${key}?q=${q$enc}'"></button>
            <button mv-name="remove" class="del"></button>
          </span>
        </div>

        <div class="information">
          <div class="tab">
            <ul>
              <li class="on"><a href="">환자정보</a></li>
              <li><a href="">수술정보</a></li>
            </ul>
          </div>

          <div class="tab-info">
          <!-- 환자정보 -->
            <div class="section on" id="a1">
              <div>
                <strong>기본정보</strong>
                <ul>
                  <li><strong>환자번호</strong> <span name="pat_no"></span></li>
                  <li><strong>이니셜</strong> <span name="pat_nm"></span></li>
                </ul>
              </div>

              <div>
                <strong>개인정보</strong>
                <ul>
                  <li><strong>나이</strong> <span name="pat_age"></span></li>
                  <li><strong>성별</strong> <span name="pat_gen"></span></li>
                  <li><strong>신장</strong> <span name="pat_weight"></span></li>
                  <li><strong>체중</strong> <span name="pat_height"></li>
                </ul>
              </div>

              <div>
                <strong>이략</strong>
                <ul>
                  <li><strong>CFS Ix</strong> <span name="cfs_ix"></span></li>
                  <li><strong>수술력</strong> <span name="history"></span></li>
                  <li><strong>Exclusion</strong> <span name="exclusion"></span></li>
                  <li><strong>이전일자 내시결 시행여부</strong> <span name="endoscopy_stat"></span></li>
                </ul>
              </div>
            </div>
            <!-- 수술정보 -->
            <div class="section"  id="a2">
              <div>
                <strong>결과정보</strong>
                <ul>
                  <li><strong>명장도달</strong> <span name="appendix_reach"></span></li>
                  <li><strong>맹장도달시간</strong> <span name="appx_reach_tm"></span></li>
                  <li><strong>회수시간</strong> <span name="appx_remove_tm"></span></li>
                  <li><strong>전체검사시간</strong> <span name="appx_total_tm"></span></li>
                </ul>
              </div>

              <div>
                <strong>이상정보</strong>
                <ul>
                  <li><strong>장정결도 우측</strong> <span name="right_colon"></span></li>
                  <li><strong>장정결도 횡행</strong> <span name="trans_colon"></span></li>
                  <li><strong>장정결도 좌측</strong> <span name="left_colon"></span></li>
                  <li><strong>장정결도 합산</strong> <span name="bbps"></span></li>
                </ul>
              </div>

              <div>
                <strong>소견</strong>
                <ul>
                  <li><strong>CFS 소견</strong> <span name="cfs_opi"></span></li>
                  <li><strong>발견한 총 용종개수</strong> <span name="polyp_cnt"></span></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

 
<div id="video-view-templates" style="display: none;">
	<div t-template="snapshot">
		<li>
			<ul>
				<li onclick="MMViewer.seekSnapshot(this)">
					<div class="thumbnail">
						<div class="thumbnail-image">
							<img t-name="image">
						</div>
						<span t-name="time" class="time">00:00</span>
					</div>
				</li>
				<li>
					<ul template-name="shapes" class="snapshot-shape-list"></ul>
				</li>
			</ul>
		</li>
	</div>
	<div t-template="snapshot-shape">
		<li onclick="MMViewer.seekSnapshotLabel(this)">
			<div class="snapshot-label">
				<img t-name="image">
			</div>
			<div class="descript">
				<ul>
					<li t-name="polyp_pos"></li>
					<li><span t-name="polyp_size"></span>mm</li>
					<li t-name="polyp_shape"></li>
					<li t-name="polyp_bio"></li>
				</ul>
			</div>
		</li>
	</div>
</div>

<jsp:include page="/WEB-INF/layout/end-body.jsp"></jsp:include>
