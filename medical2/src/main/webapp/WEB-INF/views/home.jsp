<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>
<%@ page session="false" %>

<jsp:include page="/WEB-INF/layout/begin-header.jsp"></jsp:include>

<link rel="stylesheet" href="/resources/css/jquery-ui.css"/>
<link rel="stylesheet" href="/resources/css/jquery-ui.support.css"/>
<link rel="stylesheet" href="/resources/css/visual.progress.css"/>

<%/*
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"/>
*/%>
<link rel="stylesheet" href="/resources/css/list.css"/>

<jsp:include page="/WEB-INF/items/video.jsp"></jsp:include>

<script type="text/javascript" src="/resources/js/jquery.ui.1.11.4.js"></script>
<script type="text/javascript" src="/resources/js/jquery.ui.init.js"></script>

<script type="text/javascript" src="/resources/js/views/video/list.js"></script>

<script>
$(function(){
	MMListUp.initialize();
	
	//-----------------------------------------------------------
	// 컨테이너가 스크롤 시 Date Picker 숨김 처리 
	
});
</script>

<jsp:include page="/WEB-INF/layout/end-header.jsp"></jsp:include>
<jsp:include page="/WEB-INF/layout/begin-body.jsp"></jsp:include>

<jsp:include page="/WEB-INF/layout/header.jsp"></jsp:include>

<section id="list">
	<div class="inner">
		<ul>
			<c:if test="${not empty list}">
				<c:forEach var="item" items="${list}">
					<tag:media-list-item
						type="${item.mediaType}"
						link="video/${item.key}?q=${q$enc}"
						image="media/thumbnail/${item.key}"
						time="${item.duration}"
						date="${item.patRegDate}"
						title="${item.title}"/>
				</c:forEach>
			</c:if>
		</ul>
		<!--
		<div class="more tc">
			<button class="more-btn">더보기</button>
		</div>
		-->
	</div>
</section>

<jsp:include page="/WEB-INF/layout/end-body.jsp"></jsp:include>
