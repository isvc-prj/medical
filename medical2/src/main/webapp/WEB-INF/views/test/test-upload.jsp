<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>

<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Video 업로드 테스트</title>
</head>
<body>
	<form action="proc-upload" enctype="multipart/form-data" method="post">
		<div>
			<input type="file" name="files[]"/>
		</div>
		<div>&nbsp;</div>
		<div>
			<input type="submit" value="Submit"/>
		</div>
	</form>
</body>
</html>
