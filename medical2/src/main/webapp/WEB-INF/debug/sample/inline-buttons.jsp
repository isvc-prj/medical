<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="java.io.*" %>

<div>
	<input id="sample_mask1" type="button" value="Add Sample Mask #1" style="padding: 8px 10px; margin-right: 5px;"/>
	<input id="sample_mask2" type="button" value="Add Sample Mask #2" style="padding: 8px 10px; margin-right: 5px;"/>
	<input id="sample_labels" type="button" value="Add Sample Label" style="padding: 8px 10px; margin-right: 5px;"/>
	<input id="sample_image_label" type="button" value="Add Sample Image Label" style="padding: 8px 10px; margin-right: 5px;"/>
	<input id="sample_meta" type="button" value="Set Sample Meta" style="padding: 8px 10px; margin-right: 5px;"/>
</div>
