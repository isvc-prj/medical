<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="java.io.*" %>

<script>
function sampleLinks(){
	$('#sample_mask1').click(function(){
		var player = MMUploader.video.player;
		if (MMUploader.mediaType === MMMediaType.IMAGE)
			player = MMUploader.image.register;
		
		player.mask({
			invert: true,
			masks: [
				{
					height: 888.421052631579,
					width: 1020.3508771929825,
					x: 628.7719298245614,
					y: 5.175438596491228,
				},
			],
		});
	});
	
	$('#sample_mask2').click(function(){
		var player = MMUploader.video.player;
		if (MMUploader.mediaType === MMMediaType.IMAGE)
			player = MMUploader.image.register;
		
		player.mask({
			invert: false,
			masks: [
				{
					height: 695.5868544600938,
					width: 234.3661971830986,
					x: 247.88732394366195,
					y: 3.192488262910798,
				},
				{
					height: 180.28169014084506,
					width: 865.3521126760563,
					x: 688.075117370892,
					y: 891.0798122065728,
				},
			],
		});
	});	
	
	$('#sample_labels').click(function(){
		var player = MMUploader.video.player;
		
		VideoDataGenerator.label(
			9.697,
			$('#sample_snapshot_image_1').attr('src'),
			[
				{
					color: "white",
					height: 442.3963133640553,
					text: "",
					width: 460.0921658986175,
					x: 684.2396313364055,
					y: 47.465437788018434,
				},
				{
					color: "white",
					height: 454.19354838709677,
					text: "",
					width: 436.4976958525346,
					x: 1271.152073732719,
					y: 363.0414746543779,
				}
			],
			{
				polyp_bio: "3",
				polyp_pos: "5",
				polyp_shape: "2",
				polyp_size: "12",
			})
			.then(function(d){
				player.label(d, true);
				MMUploader.setSnapshot(d);
			});
		
		VideoDataGenerator.label(
				24.058,
				$('#sample_snapshot_image_2').attr('src'),
				[
					{
						x: 819.9078341013825,
						y: 79.90783410138249,
						width: 713.7327188940093,
						height: 663.594470046083,
						color: "white",
						text: "오마이갓"
					}
				],
				{
					polyp_bio: "6",
					polyp_pos: "1",
					polyp_shape: "1",
					polyp_size: "33",
				})
				.then(function(d){
					player.label(d, true);
					MMUploader.setSnapshot(d);
				});
		
		VideoDataGenerator.label(
				41.491887,
				$('#sample_snapshot_image_3').attr('src'),
				[
					{
						x: 872.9953917050692,
						y: 106.45161290322581,
						width: 421.7511520737327,
						height: 277.2350230414747,
						color: "white",
						text: "엄마야"
					}
				],
				{
					polyp_bio: "7",
					polyp_pos: "8",
					polyp_shape: "4",
					polyp_size: "12",
				})
				.then(function(d){
					player.label(d, true);
					MMUploader.setSnapshot(d);
				});
		
		VideoDataGenerator.label(
				54.261,
				$('#sample_snapshot_image_4').attr('src'),
				[
					{
						x: 1070.5990783410139,
						y: 121.19815668202766,
						width: 271.33640552995394,
						height: 318.5253456221198,
						color: "yellow",
						text: "Cancer",
					}
				],
				{
					polyp_bio: "5",
					polyp_pos: "4",
					polyp_shape: "4",
					polyp_size: "11",
				})
				.then(function(d){
					player.label(d, true);
					MMUploader.setSnapshot(d);
				});

	});
	
	$('#sample_image_label').click(function(){
		var player = MMUploader.image.register;
		
		//var imgUrl = $('#sample_snapshot_image_1').attr('src');
		//var imgUrl = player.data.screen.source;
		var imgUrl = player.command('capture').source;
		
		VpCommon.loadImage(imgUrl)
			.then(function(img){
				var labels = generateSampleLabels(img);
				
				for (var i=0; i < labels.length; i++)
					VpCommon.loadImage(labels[i].source);
				
				VideoDataGenerator.label(
					0,
					imgUrl,
					labels,
					{
						polyp_bio: "3",
						polyp_pos: "5",
						polyp_shape: "2",
						polyp_size: "12",
					})
					.then(function(d){
						player.label(d, true);
						MMUploader.setSnapshot(d);
					});
			});		
	});
	
	$('#sample_meta').click(function(){
		MMUploader.writeForm({
			title: '40번 환자 수술영상',
			
			patient: {
				regDate: '20181020',
				id: 'S-040',
				name: 'LOH',
				no: '851172',
				gender: 'W',
				age: 40,
				weight: 50,
				height: 165,
				cfxIx: 1,
				exclusion: 3,
				history: 1,
				endoscopyStat: 0,
			},
			
			operation: {
				appendixReach: 1,
				appendixReachTime: 420,
				appendixReachRemoveTime: 900,
				appendixTotalTime: 1320,
				rightColon: 3,
				transColon: 2,
				leftColon: 3,
				bbps: 8,
				bppsFitness: 1,
				cfxOpinion: 3,
				polypCnt: 2,
			},
		});
	});
	
	function generateSampleLabels(screenImage){
		var a = [
			new ShapePolygon({
				points: [ {x: 1299, y: 425}, {x: 1252, y: 415}, {x: 1225, y: 420}, {x: 1211, y: 428}, {x: 1187, y: 446}, {x: 1183, y: 462}, {x: 1189, y: 479}, {x: 1211, y: 487}, {x: 1243, y: 487}, {x: 1266, y: 474}, {x: 1292, y: 487}, {x: 1293, y: 501}, {x: 1299, y: 510}, {x: 1312, y: 517}, {x: 1326, y: 519}, {x: 1348, y: 501}, {x: 1357, y: 461}, {x: 1350, y: 444}, ],
			}),
			new ShapePolygon({
				points: [ {x: 929, y: 446}, {x: 914, y: 433}, {x: 873, y: 420}, {x: 851, y: 412}, {x: 803, y: 400}, {x: 774, y: 393}, {x: 753, y: 392}, {x: 733, y: 393}, {x: 733, y: 405}, {x: 753, y: 423}, {x: 759, y: 432}, {x: 773, y: 439}, {x: 781, y: 452}, {x: 763, y: 455}, {x: 742, y: 439}, {x: 724, y: 435}, {x: 700, y: 439}, {x: 694, y: 461}, {x: 722, y: 488}, {x: 776, y: 507}, {x: 858, y: 513}, {x: 940, y: 526}, {x: 966, y: 522}, {x: 974, y: 504}, {x: 978, y: 484}, {x: 969, y: 466}, {x: 956, y: 457}, {x: 946, y: 451},  ],
			}),
			new ShapePolygon({
				points: [ {x: 1544, y: 108}, {x: 1508, y: 95}, {x: 1467, y: 101}, {x: 1446, y: 126}, {x: 1445, y: 192}, {x: 1445, y: 296}, {x: 1455, y: 382}, {x: 1460, y: 507}, {x: 1464, y: 656}, {x: 1469, y: 743}, {x: 1476, y: 832}, {x: 1487, y: 903}, {x: 1496, y: 960}, {x: 1516, y: 961}, {x: 1535, y: 932}, {x: 1558, y: 798}, {x: 1551, y: 693},  ],
			}),
			new ShapePolygon({
				points: [ {x: 106, y: 30}, {x: 101, y: 46}, {x: 123, y: 76}, {x: 209, y: 104}, {x: 366, y: 123}, {x: 548, y: 133}, {x: 695, y: 136}, {x: 846, y: 141}, {x: 1066, y: 137}, {x: 1128, y: 129}, {x: 1283, y: 90}, {x: 1381, y: 72}, {x: 1527, y: 59}, {x: 1608, y: 96}, {x: 1635, y: 119}, {x: 1692, y: 145}, {x: 1740, y: 151}, {x: 1768, y: 133}, {x: 1772, y: 105}, {x: 1756, y: 64}, {x: 1731, y: 51}, {x: 1619, y: 45}, {x: 1516, y: 26}, {x: 1440, y: 26}, {x: 1334, y: 32},  ],
			}),			
		];
		
		var attrs = [
			{ polyp_bio: "3", polyp_pos: "5", polyp_shape: "2", polyp_size: "12", },
			{ polyp_bio: "1", polyp_pos: "2", polyp_shape: "4", polyp_size: "33", },
			{ polyp_bio: "0", polyp_pos: "1", polyp_shape: "2", polyp_size: "25", },
			{ polyp_bio: "5", polyp_pos: "4", polyp_shape: "3", polyp_size: "35", },
		];
		
		var r = [];
		for (var i=0; i < a.length; i++){
			var s = a[i];
			
			var b = s.bounds();
			var src = VideoDrawer.pickImage(screenImage, b, s.points);
			var attr = attrs[i];
			
			var label = {
				x: b.x,
				y: b.y,
				width: b.width,
				height: b.height,
				attrs: attr,
				points: s.points,
				source: src,
			};
			
			r.push(label);
		}
		
		return r;
	}
	
}
</script>
