<%@ tag language="java" body-content="empty" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags"%>

<%@ attribute name="id" type="java.lang.String"%>
<%@ attribute name="mode" type="java.lang.String"%>
<%@ attribute name="visible" type="java.lang.String"%>

<c:choose>
	<c:when test="${visible == 'true'}">
		<c:set var="hideCss" value=""/>
	</c:when>
	<c:otherwise>
		<c:set var="hideCss" value="visibility: hidden; z-index: -1; position: absolute; top: -10000000px"/>
	</c:otherwise>
</c:choose>

<!-- Video Image Register 시작 -->
<div id="${id}" class="vi-editor vp-player" style="${hideCss}">
	<div>
		<div class="vp-video">
			<img vp-name="image"/>
			<div vp-name="draws" class="vp-draws"></div>
		</div>
		<!-- Control Panel 시작 -->
		<div vp-name="panel" class="vp-panel nodrag">
			<div vp-name="panel-padding" class="vp-padding vp-hide">
				<div class="vp-padding-wrap">
					<div class="vp-padding-container-wrap">
						<div vp-name="snapshot" class="vp-padding-container">
							<!--
							<div class="snapshot" style="left: 30.5172%;">
								<div vp-name="img" class="vp-hide2 image">
									<img src="sample-img/sample.jpg"/>
								</div>
								<div class="label">
									<div vp-name="time" class="time">00:00</div>
								</div>
							</div>
							-->
						</div>
					</div>
				</div>
			</div>
			<div class="vp-progress">
				<div vp-name="progress" class="vp-progress-bar-list active2">
					<div vp-name="loading" class="vp-progress-loading-bar vp-hide2"></div>
					<div vp-name="current" class="vp-progress-current-bar vp-hide2"></div>
					<div vp-name="hover" class="vp-progress-hover-bar vp-hide2"></div>
					<div vp-name="snapshot-list" class="vp-snapshot-list">
						<!--
						<div class="snapshot" style="left: 10%;"></div>
						<div class="snapshot" style="left: 30%;"></div>
						-->
					</div>
				</div>
			</div>
			<!-- Control Button Bar 시작 -->
			<div vp-name="buttons" class="vp-icon-bar">
				<div class="vp-right">
					<c:if test="${mode eq 'edit'}">
					<div vp-notify="mask" class="vp-icon">
						<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><g><path fill="#ffffff" d="m3.232865,6c-0.11396,0 -0.20634,0.07363 -0.20634,0.16589l0,14.66822c0,0.09162 0.0919,0.16589 0.20634,0.16589l14.58732,0c0.11396,0 0.20634,-0.07363 0.20634,-0.16589l0,-14.66822c0,-0.09162 -0.0919,-0.16589 -0.20634,-0.16589l-14.58732,0l0,0zm3.79366,-1l12,0l0,12l3,0l0,-15l-15,0l0,3z"/></g></svg>
					</div>

					<div vp-notify="capture" class="vp-icon">
						<svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><g><path fill="#ffffff" d="m430.4,147l-67.5,0l-40.4,-40.8c0,0 -0.2,-0.2 -0.3,-0.2l-0.2,-0.2l0,0c-6,-6 -14.1,-9.8 -23.3,-9.8l-84,0c-9.8,0 -18.5,4.2 -24.6,10.9l0,0.1l-39.5,40l-69,0c-18.6,0 -33.6,14.6 -33.6,33.2l0,202.1c0,18.6 15,33.7 33.6,33.7l348.8,0c18.5,0 33.6,-15.1 33.6,-33.7l0,-202.1c0,-18.6 -15.1,-33.2 -33.6,-33.2zm-174.4,218.5c-50.9,0 -92.4,-41.6 -92.4,-92.6c0,-51.1 41.5,-92.6 92.4,-92.6c51,0 92.4,41.5 92.4,92.6c0,51 -41.4,92.6 -92.4,92.6zm168.1,-165c-7.7,0 -14,-6.3 -14,-14.1s6.3,-14.1 14,-14.1c7.7,0 14,6.3 14,14.1s-6.3,14.1 -14,14.1z"/><path fill="#ffffff" d="m256,202.9c-38.6,0 -69.8,31.3 -69.8,70c0,38.6 31.2,70 69.8,70c38.5,0 69.8,-31.3 69.8,-70c0,-38.7 -31.3,-70 -69.8,-70z"/></g></svg>
					</div>
					</c:if>
				</div>
			</div>
			<!-- Control Button Bar 끝 -->
		</div>
		<!-- Control Panel 끝 -->
		<!-- 템플릿 시작 -->
		<div vp-name="templates" style="display: none;">
			<div vp-template="snapshot">
				<div class="snapshot">
					<div vp-name="img" class="vp-hide image">
						<img src2="sample-img/sample.jpg"/>
					</div>
					<div class="label">
						<div vp-name="time" class="time">00:00</div>
					</div>
				</div>
			</div>
			<div vp-template="snapshot-button">
				<div vp-type="snapshot:button" class="snapshot"></div>
			</div>
		</div>
		<!-- 템플릿 끝 -->
	</div>
</div>
<!-- Video Image Register 끝 -->
