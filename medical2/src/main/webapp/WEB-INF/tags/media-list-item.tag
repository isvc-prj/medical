<%@ tag language="java" body-content="empty" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>

<%@ attribute name="type" type="java.lang.String"%>
<%@ attribute name="link" type="java.lang.String"%>
<%@ attribute name="image" type="java.lang.String"%>
<%@ attribute name="time" type="java.lang.Number"%>
<%@ attribute name="date" type="java.lang.String"%>
<%@ attribute name="title" type="java.lang.String"%>

<jsp:useBean id="DateUtil" class="com.sqisoft.medi.utils.DateUtil"/>
<jsp:useBean id="MediaUtil" class="com.sqisoft.medi.utils.MediaUtil"/>

<c:set var="linkOpen" value=""/>
<c:set var="linkClose" value=""/>

<c:if test="${not empty link}">
	<c:set var="linkOpen" value="<a href=&quot;${link}&quot;>"/>
	<c:set var="linkClose" value="</a>"/>
</c:if>

<li class="fll video-list">
	<c:out value="${linkOpen}" escapeXml="false"/>
	<div class="img tc" style="background-image:url(${image})">
		<div class="video-info dib vm tl">
			<c:if test="${type == 'MDMTYPE_VIDEO'}">
			<div class="time">
				<i class="dib vm"><img class="vm" src="/resources/img/icon/i-clock.png" alt=""></i>
				<span class="dib vm">${MediaUtil.toTimeSpanFormat(time, true)}</span>
			</div>
			</c:if>
			<div class="date">
				<i class="dib vm"><img class="vm" src="/resources/img/icon/i-calendar.png" alt=""></i>
				<span class="dib vm">${DateUtil.time(date, 'yyyyMMdd', 'yyyy.MM.dd')}</span>
			</div>
		</div>
	</div>
	<div class="title">
		<p>${title}</p>
	</div>
	<c:out value="${linkClose}" escapeXml="false"/>
</li>

