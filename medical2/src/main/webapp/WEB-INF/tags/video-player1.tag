<%@ tag language="java" body-content="empty" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags"%>

<%@ attribute name="id" type="java.lang.String"%>
<%@ attribute name="mode" type="java.lang.String"%>
<%@ attribute name="mediaKey" type="java.lang.String"%>
<%@ attribute name="visible" type="java.lang.String"%>

<c:choose>
	<c:when test="${visible == 'true'}">
		<c:set var="hideCss" value=""/>
	</c:when>
	<c:otherwise>
		<c:set var="hideCss" value="visibility: hidden; z-index: -1; position: absolute; top: -10000000px"/>
	</c:otherwise>
</c:choose>

<!-- Video Playuer 시작 -->
<div id="${id}" class="vp-player" style="width2: 1200px; margin-left2: 100px; ${hideCss}">
	<div>
		<div class="vp-video">
			<video class="vp-hide2" preload="auto" crossorigin="anonymous">
				<c:if test="${not empty mediaKey}">
					<source src="/media/${mediaKey}" type="video/mp4"/>
				</c:if>
			</video>
			<div vp-name="draws" class="vp-draws"></div>
		</div>
		<!-- Control Panel 시작 -->
		<div vp-name="panel" class="vp-panel nodrag">
			<div vp-name="panel-padding" class="vp-padding vp-hide">
				<div class="vp-padding-wrap">
					<div class="vp-padding-container-wrap">
						<div vp-name="snapshot" class="vp-padding-container">
							<!--
							<div class="snapshot" style="left: 30.5172%;">
								<div vp-name="img" class="vp-hide2 image">
									<img src="sample-img/sample.jpg"/>
								</div>
								<div class="label">
									<div vp-name="time" class="time">00:00</div>
								</div>
							</div>
							-->
						</div>
					</div>
				</div>
			</div>
			<div class="vp-progress">
				<div vp-name="progress" class="vp-progress-bar-list active2">
					<div vp-name="loading" class="vp-progress-loading-bar vp-hide2"></div>
					<div vp-name="current" class="vp-progress-current-bar vp-hide2"></div>
					<div vp-name="hover" class="vp-progress-hover-bar vp-hide2"></div>
					<div vp-name="snapshot-list" class="vp-snapshot-list">
						<!--
						<div class="snapshot" style="left: 10%;"></div>
						<div class="snapshot" style="left: 30%;"></div>
						-->
					</div>
				</div>
			</div>
			<!-- Control Button Bar 시작 -->
			<div vp-name="buttons" class="vp-icon-bar">
				<div vp-btn="play" class="vp-icon vp-play">
					<svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><path d="M256,512C114.625,512,0,397.375,0,256C0,114.609,114.625,0,256,0s256,114.609,256,256C512,397.375,397.375,512,256,512z   M256,64C149.969,64,64,149.969,64,256s85.969,192,192,192c106.03,0,192-85.969,192-192S362.031,64,256,64z M192,160l160,96l-160,96  V160z"/></svg>
				</div>
				<div vp-btn="stop" class="vp-icon vp-stop vp-hide">
					<svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><path d="M256,512C114.625,512,0,397.375,0,256C0,114.609,114.625,0,256,0s256,114.609,256,256C512,397.375,397.375,512,256,512z   M256,64C149.969,64,64,149.969,64,256s85.969,192,192,192c106.03,0,192-85.969,192-192S362.031,64,256,64z M192,192h128v128H192  V192z"/></svg>
				</div>
				<div vp-btn="pause" class="vp-icon vp-pause vp-hide">
					<svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="M256,0C114.625,0,0,114.609,0,256c0,141.375,114.625,256,256,256s256-114.625,256-256C512,114.609,397.375,0,256,0z     M256,448c-106.031,0-192-85.969-192-192S149.969,64,256,64c106.03,0,192,85.969,192,192S362.031,448,256,448z M160,320h64V192    h-64V320z M288,320h64V192h-64V320z"/></g></g></svg>
				</div>
				<div vp-btn="rewind" vp-value="5s" class="vp-icon vp-rewind">
					<svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="M256,0C114.625,0,0,114.625,0,256c0,141.391,114.625,256,256,256s256-114.609,256-256C512,114.625,397.375,0,256,0z     M256,448c-106.031,0-192-85.969-192-192S149.969,64,256,64c106.03,0,192,85.969,192,192S362.031,448,256,448z"/></g></g><polygon points="128,256 256,352 256,280 352,352 352,160 256,232 256,160 "/></svg>
				</div>
				<div vp-btn="forward" vp-value="5" class="vp-icon vp-forward">
					<svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="M256,0C114.625,0,0,114.609,0,256c0,141.375,114.625,256,256,256s256-114.625,256-256C512,114.609,397.375,0,256,0z     M256,448c-106.031,0-192-85.969-192-192S149.969,64,256,64c106.03,0,192,85.969,192,192S362.031,448,256,448z"/></g></g><polygon points="384,256 256,160 256,232 160,160 160,352 256,280 256,352 "/></svg>
				</div>
				<div vp-btn="sound" class="vp-icon vp-sound">
					<svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><path d="M319.967,503.497v-67.34C394.335,409.732,447.9,339.457,447.9,256c0-83.426-53.564-153.732-127.934-180.156V8.503  C430.254,36.988,511.867,136.812,511.867,256C511.867,375.188,430.254,475.043,319.967,503.497z M256,511.868L128.066,383.936  h-95.95c-17.679,0-31.983-14.307-31.983-31.983V160.05c0-17.679,14.306-31.983,31.983-31.983h95.95L256,0.133  c0,0,31.982-3.998,31.982,31.983c0,72.899,0,382.053,0,447.769C287.983,515.866,256,511.868,256,511.868z M224.017,128.066  l-63.967,63.967H64.099v127.934h95.951l63.967,63.969V128.066L224.017,128.066z M415.916,256  c0,59.532-40.854,109.132-95.949,123.404v-68.309c19.053-11.087,31.982-31.482,31.982-55.096c0-23.612-12.931-44.008-31.982-55.097  v-68.308C375.063,146.869,415.916,196.469,415.916,256z"/></svg>
				</div>
				<div vp-btn="mute" class="vp-icon vp-mute vp-hide">
					<svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><path d="M511.603,303.727l-47.694,47.726l-47.726-47.726l-47.726,47.726l-47.693-47.726L368.457,256l-47.693-47.725l47.693-47.726  l47.726,47.726l47.726-47.726l47.694,47.726L463.907,256L511.603,303.727z M256.266,511.868L128.332,383.936H32.381  c-17.678,0-31.983-14.307-31.983-31.983V160.05c0-17.679,14.307-31.983,31.983-31.983h95.951L256.266,0.133  c0,0,31.983-3.998,31.983,31.983c0,173.535,0,425.718,0,447.769C288.249,515.866,256.266,511.868,256.266,511.868z M224.282,128.066  l-63.968,63.967H64.364v127.934h95.951l63.968,63.969V128.066z"/></svg>
				</div>
				<div vp-btn="expand" class="vp-icon vp-expand vp-hide right">
					<svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="M178.375,287.812L73.094,393.094L32,352c-17.688,0-32,14.312-32,32v96c0,17.688,14.312,32,32,32h96    c17.688,0,32-14.312,32-32l-41.095-41.062l105.281-105.312L178.375,287.812z M480,0h-96c-17.688,0-32,14.328-32,32l41.094,41.094    L287.812,178.375l45.812,45.812l105.281-105.266L480,160c17.688,0,32-14.312,32-32V32C512,14.328,497.688,0,480,0z M480,352    l-41.095,41.094l-105.28-105.281l-45.812,45.812l105.281,105.312L352,480c0,17.688,14.312,32,32,32h96c17.688,0,32-14.312,32-32    v-96C512,366.312,497.688,352,480,352z M160,32c0-17.672-14.312-32-32-32H32C14.312,0,0,14.328,0,32v96c0,17.688,14.312,32,32,32    l41.094-41.078l105.281,105.266l45.812-45.812L118.906,73.094L160,32z"/></g></g></svg>
				</div>
				<div vp-name="volume" class="vp-volume">
					<div class="vp-volume-slider">
						<div vp-name="track" class="vp-volume-slider-track" style="width2: 30%">
							<div vp-name="handle" class="vp-volume-slider-handle"></div>
						</div>
					</div>
				</div>
				<div vp-name="duration" class="vp-duration">
					<div vp-name="current">00:00</div>
					<div>/</div>
					<div vp-name="duration">00:00</div>
				</div>
				<div class="vp-right">
					<c:if test="${mode eq 'edit'}">
					<div vp-notify="mask" class="vp-icon">
						<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><g><path fill="#ffffff" d="m3.232865,6c-0.11396,0 -0.20634,0.07363 -0.20634,0.16589l0,14.66822c0,0.09162 0.0919,0.16589 0.20634,0.16589l14.58732,0c0.11396,0 0.20634,-0.07363 0.20634,-0.16589l0,-14.66822c0,-0.09162 -0.0919,-0.16589 -0.20634,-0.16589l-14.58732,0l0,0zm3.79366,-1l12,0l0,12l3,0l0,-15l-15,0l0,3z"/></g></svg>
					</div>

					<div vp-notify="capture" class="vp-icon">
						<svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><g><path fill="#ffffff" d="m430.4,147l-67.5,0l-40.4,-40.8c0,0 -0.2,-0.2 -0.3,-0.2l-0.2,-0.2l0,0c-6,-6 -14.1,-9.8 -23.3,-9.8l-84,0c-9.8,0 -18.5,4.2 -24.6,10.9l0,0.1l-39.5,40l-69,0c-18.6,0 -33.6,14.6 -33.6,33.2l0,202.1c0,18.6 15,33.7 33.6,33.7l348.8,0c18.5,0 33.6,-15.1 33.6,-33.7l0,-202.1c0,-18.6 -15.1,-33.2 -33.6,-33.2zm-174.4,218.5c-50.9,0 -92.4,-41.6 -92.4,-92.6c0,-51.1 41.5,-92.6 92.4,-92.6c51,0 92.4,41.5 92.4,92.6c0,51 -41.4,92.6 -92.4,92.6zm168.1,-165c-7.7,0 -14,-6.3 -14,-14.1s6.3,-14.1 14,-14.1c7.7,0 14,6.3 14,14.1s-6.3,14.1 -14,14.1z"/><path fill="#ffffff" d="m256,202.9c-38.6,0 -69.8,31.3 -69.8,70c0,38.6 31.2,70 69.8,70c38.5,0 69.8,-31.3 69.8,-70c0,-38.7 -31.3,-70 -69.8,-70z"/></g></svg>
					</div>
					</c:if>
	
					<div class="gap"></div>

					<div vp-name="rate" class="vp-rate">x1</div>
					<div vp-name="rate-list" class="vp-rate-list vp-hide">
						<div vp-value="0.5">x0.5</div>
						<div vp-value="1">x1</div>
						<div vp-value="1.5">x1.5</div>
						<div vp-value="2">x2</div>
					</div>
				</div>
			</div>
			<!-- Control Button Bar 끝 -->
		</div>
		<!-- Control Panel 끝 -->
		<!-- 외부 지원 시작 -->
		<div vp-name="external" class="vp-external vp-hide">
			<!-- Video Label 화면 시작 -->
			<div id="label-editor" vp-name="label-editor" class="vp-video-label vp-hide">
				<div>
					<header>
						<h4>Video Label</h4>
					</header>
					<section>
						<article>
							<div>
								<img vp-name="image" src2="sample-img/CHK.jpg"/>
								<div vp-name="canvas" class="vp-canvas">
									<!--
									<div class="vp-label-box" style="left: 339px; top: 113.516px; width: 334px; height: 187px; line-height: 187px;">
										<div class="vp-edit-point vp-ep-LT" vp-cmd="LT"></div>
										<div class="vp-edit-point vp-ep-CT" vp-cmd="CT"></div>
										<div class="vp-edit-point vp-ep-RT" vp-cmd="RT"></div>
										<div class="vp-edit-point vp-ep-LC" vp-cmd="LC"></div>
										<div class="vp-edit-point vp-ep-RC" vp-cmd="RC"></div>
										<div class="vp-edit-point vp-ep-LB" vp-cmd="LB"></div>
										<div class="vp-edit-point vp-ep-CB" vp-cmd="CB"></div>
										<div class="vp-edit-point vp-ep-RB" vp-cmd="RB"></div>
									Label
									</div>
									-->
								</div>
							</div>
						</article>
						<aside>
							<div class="label-form">
								<div>
									<div class="label-form-wrap">
										<tag:video-label-form id="video-label-form"/>
									</div>
						    	</div>							
							</div>
							
							<div class="label-panel">
								<span vp-view="modify">
									<input vp-btn="remove" type="button" value="삭제"/>
								</span>
								<input vp-btn="cancel" type="button" value="취소"/>
								<input vp-btn="save" type="button" value="저장"/>
							</div>
					</aside>
					</section>
					<nav vp-name="label-list" class="label-list">
						<div>
							<span class="title">Label</span><input vp-name="label" type="text" disabled="disabled" value=""/>
						</div>
						<div>
							<select vp-name="label-color" disabled="disabled">
								<option value="black">Black</option>
								<option value="white">White</option>
								<option value="red">Red</option>
								<option value="blue">Blue</option>
								<option value="green">Green</option>
								<option value="yellow">Yellow</option>
								<option value="cyan">Cyan</option>
								<option value="gray">Gray</option>
							</select>
						</div>
					</nav>
				</div>
			</div>
			<!-- Video Label 화면 끝 -->

			<!-- Video Mask 화면 시작 -->
			<div id="mask-editor" vp-name="mask-editor" class="vp-video-mask vp-hide">
				<div>
					<header>
						<h4>Video Mask</h4>
					</header>
					<section>
						<article>
							<div>
								<img vp-name="image" src2="sample-img/CHK.jpg"/>
								<div vp-name="canvas" class="vp-canvas">
									<!--
									<div class="vp-mask-box vp-show-mask" style="left: 339px; top: 113.516px; width: 334px; height: 187px; line-height: 187px;">
										<div class="vp-edit-point vp-ep-LT" vp-cmd="LT"></div>
										<div class="vp-edit-point vp-ep-CT" vp-cmd="CT"></div>
										<div class="vp-edit-point vp-ep-RT" vp-cmd="RT"></div>
										<div class="vp-edit-point vp-ep-LC" vp-cmd="LC"></div>
										<div class="vp-edit-point vp-ep-RC" vp-cmd="RC"></div>
										<div class="vp-edit-point vp-ep-LB" vp-cmd="LB"></div>
										<div class="vp-edit-point vp-ep-CB" vp-cmd="CB"></div>
										<div class="vp-edit-point vp-ep-RB" vp-cmd="RB"></div>
									</div>
									<div class="vp-mask-box vp-hide-mask" style="left: 439px; top: 213.516px; width: 334px; height: 187px; line-height: 187px;">
										<div class="vp-edit-point vp-ep-LT" vp-cmd="LT"></div>
										<div class="vp-edit-point vp-ep-CT" vp-cmd="CT"></div>
										<div class="vp-edit-point vp-ep-RT" vp-cmd="RT"></div>
										<div class="vp-edit-point vp-ep-LC" vp-cmd="LC"></div>
										<div class="vp-edit-point vp-ep-RC" vp-cmd="RC"></div>
										<div class="vp-edit-point vp-ep-LB" vp-cmd="LB"></div>
										<div class="vp-edit-point vp-ep-CB" vp-cmd="CB"></div>
										<div class="vp-edit-point vp-ep-RB" vp-cmd="RB"></div>
									</div>
									-->
								</div>
							</div>
						</article>
						<aside>
							<nav>
								<div>
									<span vp-view="modify">
										<input vp-btn="remove" type="button" value="삭제"/>
									</span>
									<input vp-btn="cancel" type="button" value="취소"/>
									<input vp-btn="save" type="button" value="저장"/>
								</div>
								<div>
									<div class="chkbox round">
									<label>
										<input vp-btn="invert" type="checkbox"/>
										<span class="check"></span>
										<span class="name">Invert</span>
									</label>
									</div>
								</div>
								<div>&nbsp;</div>
								<div class="subtitle">
									<span>* Preview</span>
								</div>
								<div class="vp-mask-preview">
									<div vp-name="preview" class="vp-hide">
										<img vp-name="preview.image"/>
										<canvas vp-name="preview.canvas"></canvas>
									</div>
								</div>
								<!--
								<div>
									Creation Mask Type:
									<label>
										<input vp-btn="show" name="mask-type" type="radio" value="show"/>
										Show Mask
									</label>
									<label>
										<input vp-btn="hide" name="mask-type" type="radio" value="hide"/>
										Hide Mask
									</label>
								</div>
								-->
							</nav>
						</aside>
					</section>
					<!--
					<nav vp-name="label-list">
						Mask Type:
						<label>
							<input vp-btn="show" name="mask-type" type="radio" value="show"/>
							Show Mask
						</label>
						<label>
							<input vp-btn="hide" name="mask-type" type="radio" value="hide"/>
							Hide Mask
						</label>
					</nav>
					-->
					</div>
				</div>
				<!-- Video Mask 화면 끝 -->
			</div>
		<!-- 외부 지원 끝 -->
		<!-- 템플릿 시작 -->
		<div vp-name="templates" style="display: none;">
			<div vp-template="snapshot">
				<div class="snapshot">
					<div vp-name="img" class="vp-hide image">
						<img src2="sample-img/sample.jpg"/>
					</div>
					<div class="label">
						<div vp-name="time" class="time">00:00</div>
					</div>
				</div>
			</div>
			<div vp-template="snapshot-button">
				<div vp-type="snapshot:button" class="snapshot"></div>
			</div>
		</div>
		<!-- 템플릿 끝 -->
	</div>
</div>
<!-- Video Playuer 끝 -->
