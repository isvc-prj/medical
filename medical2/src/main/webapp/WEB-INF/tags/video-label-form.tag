<%@ tag language="java" body-content="empty" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags"%>

<%@ attribute name="id" type="java.lang.String"%>

<div id="${id}">
	<div class="group" vl-form="polyp_pos">
	  <div class="title etc"><span>용종 위치</span></div>
	  <div class="chkbox">
	    <label>
	      <input type="radio" name="polyp_pos" value="1"/>
	      <span class="check"></span>
	      <span class="name">Cecum</span>
	    </label>
	    <label>
	      <input type="radio" name="polyp_pos" value="2"/>
	      <span class="check"></span>
	      <span class="name">A colon</span>
	    </label>
	    <label>
	      <input type="radio" name="polyp_pos" value="3"/>
	      <span class="check"></span>
	      <span class="name">HF</span>
	    </label>
	    <label>
	      <input type="radio" name="polyp_pos" value="4"/>
	      <span class="check"></span>
	      <span class="name">T colon</span>
	    </label>
	    <label>
	      <input type="radio" name="polyp_pos" value="5"/>
	      <span class="check"></span>
	      <span class="name">SF</span>
	    </label>
	    <label>
	      <input type="radio" name="polyp_pos" value="6"/>
	      <span class="check"></span>
	      <span class="name">D colon</span>
	    </label>
	    <label>
	      <input type="radio" name="polyp_pos" value="7"/>
	      <span class="check"></span>
	      <span class="name">S colon</span>
	    </label>
	    <label>
	      <input type="radio" name="polyp_pos" value="8"/>
	      <span class="check"></span>
	      <span class="name">Rectum</span>
	    </label>
	  </div>
	</div>
	<div class="group" vl-form="polyp_size">
	  <div class="title etc"><span>용종 정보 크기</span></div>
	  <div class="upload-input"><input type="number" class="upload-info-input" placeholder="용정 정보를 입력해주시기 바랍니다."/></div>
	</div>
	<div class="group" vl-form="polyp_shape">
	  <div class="title etc"><span>용종정보 모양</span></div>
	  <div class="chkbox">
	    <label>
	      <input type="radio" name="polyp_shape" value="1"/>
	      <span class="check"></span>
	      <span class="name">Yamada type 1</span>
	    </label>
	    <label>
	      <input type="radio" name="polyp_shape" value="2"/>
	      <span class="check"></span>
	      <span class="name">Yamada type 2</span>
	    </label>
	    <label>
	      <input type="radio" name="polyp_shape" value="3"/>
	      <span class="check"></span>
	      <span class="name">Yamada type 3</span>
	    </label>
	    <label>
	      <input type="radio" name="polyp_shape" value="4"/>
	      <span class="check"></span>
	      <span class="name">Yamada type 4</span>
	    </label>
	  </div>
	</div>
	<div class="group" vl-form="polyp_bio">
	  <div class="title etc"><span>조직검사</span></div>
	  <div class="chkbox">
	    <label>
	      <input type="radio" name="polyp_bio" value="0"/>
	      <span class="check"></span>
	      <span class="name">Nonspecifc</span>
	    </label>
	    <label>
	      <input type="radio" name="polyp_bio" value="1"/>
	      <span class="check"></span>
	      <span class="name">Hyperplastic</span>
	    </label>
	    <label>
	      <input type="radio" name="polyp_bio" value="2"/>
	      <span class="check"></span>
	      <span class="name">TA LG</span>
	    </label>
	    <label>
	      <input type="radio" name="polyp_bio" value="3"/>
	      <span class="check"></span>
	      <span class="name">TA MG</span>
	    </label>
	    <label>
	      <input type="radio" name="polyp_bio" value="4"/>
	      <span class="check"></span>
	      <span class="name">TA HG</span>
	    </label>
	    <label>
	      <input type="radio" name="polyp_bio" value="5"/>
	      <span class="check"></span>
	      <span class="name">Cancer</span>
	    </label>
	    <label>
	      <input type="radio" name="polyp_bio" value="6"/>
	      <span class="check"></span>
	      <span class="name">Serrated polyp</span>
	    </label>
	    <label>
	      <input type="radio" name="polyp_bio" value="7"/>
	      <span class="check"></span>
	      <span class="name">other</span>
	    </label>
	  </div>
	</div>
</div>