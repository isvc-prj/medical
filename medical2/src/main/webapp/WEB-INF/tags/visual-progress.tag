<%@ tag language="java" body-content="empty" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>

<%@ attribute name="id" type="java.lang.String"%>

<div id="${id}" vprog-topic="ready" class="vprog-flybox">
	<ul>
		<li>
			<div class="vprog-window">
				<ul class="vprog-ready vprog-horiz vprog-center">
					<li><div class="loader"></div></li>
					<li class="text">전송할 정보들을 계산 중입니다...</li>
				</ul>
				<ul class="vprog-proc vprog-vert vprog-center pad10">
					<li><div class="loader"></div></li>
					<li>&nbsp;</li>
					<li class="fullwide vprog-current"><div class="progress"><div class="bar"></div></div></li>
					<li vprog-group="all">&nbsp;</li>
					<li vprog-group="all" class="fullwide vprog-all"><div class="progress"><div class="bar"></div></div></li>
					<li class="text">업로드중입니다...</li>
				</ul>
			</div>
		</li>
	</ul>
</div>
