<%@ tag language="java" body-content="empty" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>

<%@ attribute name="id" type="java.lang.String"%>

<div id="${id}" vprog-topic="ready" class="vprog-flybox">
	<ul>
		<li>
			<div class="vprog-window">
				<ul class="vprog-ready vprog-horiz vprog-center">
					<li><div class="loader"></div></li>
					<li class="text">로드 중입니다...</li>
				</ul>
			</div>
		</li>
	</ul>
</div>
