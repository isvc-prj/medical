<%@ tag language="java" body-content="empty" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags"%>

<%@ attribute name="id" type="java.lang.String"%>

<div id="${id}" class="popup video-mask">
	<div class="inner" vp-name="scroll-container">
		<a href="javascript:void(0)" vp-btn="cancel" class="popup-close"><img src="/resources/img/icon/i-close.png" alt=""></a>
		
		<section>
			<article>
				<div class="thumbnail">
					<img vp-name="image"/>
					<div vp-name="canvas" class="vp-canvas"></div>
				</div>
			</article>
			<aside>
				<div class="title"><span>Preview</span></div>
				<nav>
					<div>
						<div class="vp-mask-preview">
							<div vp-name="preview" class="vp-hide">
								<img vp-name="preview.image"/>
								<canvas vp-name="preview.canvas"></canvas>
							</div>
						</div>
					</div>
				</nav>
			</aside>
		</section>
		
		<nav>
			<div class="chkbox round">
			<label>
				<input vp-btn="invert" type="checkbox"/>
				<span class="check"></span>
				<span class="name">Invert</span>
			</label>
			</div>
		</nav>
		
		<div class="popup-btn" style="font-size: 12px;">
			<span vp-view="modify">
				<input vp-btn="remove" type="button" class="popup-save" value="삭제"/>
				&nbsp;
			</span>
			<input vp-btn="cancel" type="button" class="popup-save" value="취소"/>
			&nbsp;
			<input vp-btn="save" type="button" class="popup-save" value="저장"/>
		</div>
	</div>
</div>