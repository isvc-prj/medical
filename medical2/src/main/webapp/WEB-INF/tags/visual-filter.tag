<%@ tag language="java" body-content="empty" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags"%>

<%@ attribute name="id" type="java.lang.String"%>

  <div id="${id}" class="ab">

    <div class="inner re">
      <a href="javascript:void(0)" class="close-btn"><img src="/resources/img/icon/i-close.png" alt=""></a>
      <ul class="filter-tab">
        <li class="dib vm on"><a href="javascript:void(0)">Patient</a></li>
        <li class="dib vm"><a href="javascript:void(0)">Operation</a></li>
        <li class="dib vm"><a href="javascript:void(0)">Screenshot</a></li>
      </ul>
      <ul class="filter-list">
        <li class="on">
          <ul class="check-list">
            <li>
              <div class="group">
                <div class="title">
                  <b>Patient Name</b>
                </div>
                <input name="pat_nm" type="text" class="filter-input" placeholder="Patient Name to Search">
              </div>
              <div class="group">
                <div class="title">
                  <b>Patient ID</b>
                </div>
                <input name="pat_id" type="text" class="filter-input" placeholder="Patient ID to Search">
              </div>
              <div class="group">
                <div class="title">
                  <b>Patient Age</b>
                </div>
                <div class="range">
                  <div name="pat_age" class="slider-range age">
                    <span class="tooltip"></span>
                    <span class="tooltip"></span>
                  </div>
                </div>
              </div>
              <div class="group">
                <div class="title">
                  <b>Patient Weight</b>
                </div>
                <div class="range">
                  <div name="pat_weight" class="slider-range weight">
                    <span class="tooltip"></span>
                    <span class="tooltip"></span>
                  </div>
                </div>
              </div>
              <div class="group">
                <div class="title">
                  <b>Patient Height</b>
                </div>
                <div class="range">
                  <div name="pat_height" class="slider-range height">
                    <span class="tooltip"></span>
                    <span class="tooltip"></span>
                  </div>
                </div>
              </div>
            </li>
            <li>
              <div class="group">
                <div class="title etc">
                  <b>CFS Ix</b>
                </div>
                <div class="chkbox">
                  <label>
                    <input type="checkbox" name="cfx_ix" value="1"/>
                    <span class="check"></span>
                    <span class="name">screening</span>
                  </label>
                </div>
                <div class="chkbox">
                  <label>
                    <input type="checkbox" name="cfx_ix" value="2"/>
                    <span class="check"></span>
                    <span class="name">CRN f/u</span>
                  </label>
                </div>
                <div class="chkbox">
                  <label>
                    <input type="checkbox" name="cfx_ix" value="3"/>
                    <span class="check"></span>
                    <span class="name">bowel habit change</span>
                  </label>
                </div>
                <div class="chkbox">
                  <label>
                    <input type="checkbox" name="cfx_ix" value="4"/>
                    <span class="check"></span>
                    <span class="name">abdominal pain</span>
                  </label>
                </div>
                <div class="chkbox">
                  <label>
                    <input type="checkbox" name="cfx_ix" value="5"/>
                    <span class="check"></span>
                    <span class="name">blood in stool (M/H/FOB)</span>
                  </label>
                </div>
                <div class="chkbox">
                  <label>
                    <input type="checkbox" name="cfx_ix" value="6"/>
                    <span class="check"></span>
                    <span class="name">anemia</span>
                  </label>
                </div>
                <div class="chkbox">
                  <label>
                    <input type="checkbox" name="cfx_ix" value="7"/>
                    <span class="check"></span>
                    <span class="name">tenesmus</span>
                  </label>
                </div>
                <div class="chkbox">
                  <label>
                    <input type="checkbox" name="cfx_ix" value="8"/>
                    <span class="check"></span>
                    <span class="name">wt. loss</span>
                  </label>
                </div>
                <div class="chkbox">
                  <label>
                    <input type="checkbox" name="cfx_ix" value="9"/>
                    <span class="check"></span>
                    <span class="name">CRC FGx</span>
                  </label>
                </div>
                <div class="chkbox">
                  <label>
                    <input type="checkbox" name="cfx_ix" value="10"/>
                    <span class="check"></span>
                    <span class="name">CPP/EMR</span>
                  </label>
                </div>
                <div class="chkbox">
                  <label>
                    <input type="checkbox" name="cfx_ix" value="11"/>
                    <span class="check"></span>
                    <span class="name">others</span>
                  </label>
                </div>
              </div>
            </li>
            <li>
              <div class="group">
                <div class="title etc">
                  <b>Exculusion</b>
                </div>
                <div class="chkbox">
                  <label>
                    <input type="checkbox" name="exclusion" value="0"/>
                    <span class="check"></span>
                    <span class="name">없음</span>
                  </label>
                </div>
                <div class="chkbox">
                  <label>
                    <input type="checkbox" name="exclusion" value="1"/>
                    <span class="check"></span>
                    <span class="name">대장 절제술</span>
                  </label>
                </div>
                <div class="chkbox">
                  <label>
                    <input type="checkbox" name="exclusion" value="2"/>
                    <span class="check"></span>
                    <span class="name">이전에 염증성 장질환을 진단</span>
                  </label>
                </div>
                <div class="chkbox">
                  <label>
                    <input type="checkbox" name="exclusion" value="3"/>
                    <span class="check"></span>
                    <span class="name">용종증(polyposis syndrome)이나 유전적 비용종증 직장결장암의 과거력</span>
                  </label>
                </div>
                <div class="chkbox">
                  <label>
                    <input type="checkbox" name="exclusion" value="4"/>
                    <span class="check"></span>
                    <span class="name">동반된 심한 내과적 또는 외과적 질환</span>
                  </label>
                </div>
                <div class="chkbox">
                  <label>
                    <input type="checkbox" name="exclusion" value="5"/>
                    <span class="check"></span>
                    <span class="name">임신, 수유 중이거나 부인과 질환</span>
                  </label>
                </div>
                <div class="chkbox">
                  <label>
                    <input type="checkbox" name="exclusion" value="6"/>
                    <span class="check"></span>
                    <span class="name">기타 본 연구에 참여가 부적절하다고 연구자가 판단한 자</span>
                  </label>
                </div>
              </div>
            </li>
            <li>
              <div class="group">
                <div class="title etc">
                  <b>수술력</b>
                </div>
                <div class="chkbox">
                  <label>
                    <input type="checkbox" name="history" value="0"/>
                    <span class="check"></span>
                    <span class="name">없음</span>
                  </label>
                </div>
                <div class="chkbox">
                  <label>
                    <input type="checkbox" name="history" value="1"/>
                    <span class="check"></span>
                    <span class="name">저위험군</span>
                  </label>
                </div>
                <div class="chkbox">
                  <label>
                    <input type="checkbox" name="history" value="2"/>
                    <span class="check"></span>
                    <span class="name">고위험군</span>
                  </label>
                </div>
              </div>
              <div class="group">
                <div class="title etc">
                  <b>이전 일차 내시경 시행 여부</b>
                </div>
                <div class="chkbox">
                  <label>
                    <input type="checkbox" name="endoscopy_stat" value="0"/>
                    <span class="check"></span>
                    <span class="name">없음</span>
                  </label>
                </div>
                <div class="chkbox">
                  <label>
                    <input type="checkbox" name="endoscopy_stat" value="1"/>
                    <span class="check"></span>
                    <span class="name">있음</span>
                  </label>
                </div>
              </div>
            </li>
          </ul>
        </li>
        <li>
          <ul class="check-list">
            <li>
              <div class="group">
                <div class="title">
                  <b>맹장 도달</b>
                </div>
                <div class="chkbox">
                  <label>
                    <input type="checkbox" name="appendix_reach" value="1"/>
                    <span class="check"></span>
                    <span class="name">성공</span>
                  </label>
                </div>
                <div class="chkbox">
                  <label>
                    <input type="checkbox" name="appendix_reach" value="0"/>
                    <span class="check"></span>
                    <span class="name">실패</span>
                  </label>
                </div>
              </div>
              <div class="group">
                <div class="title">
                  <b>맹장 도달 시간</b>
                </div>
                <div class="range">
                  <div name="appx_reach_time" class="slider-range arrive">
                    <span class="tooltip"></span>
                    <span class="tooltip"></span>
                  </div>
                </div>
              </div>
              <div class="group">
                <div class="title">
                  <b>맹장 회수 시간</b>
                </div>
                <div class="range">
                  <div name="appx_remove_time" class="slider-range return">
                    <span class="tooltip"></span>
                    <span class="tooltip"></span>
                  </div>
                </div>
              </div>
              <div class="group">
                <div class="title">
                  <b>전체 검사 시간</b>
                </div>
                <div class="range">
                  <div name="appx_total_time" class="slider-range alltime">
                    <span class="tooltip"></span>
                    <span class="tooltip"></span>
                  </div>
                </div>
              </div>
            </li>
            <li>
              <div class="group">
                <div class="title etc">
                  <b>장정결도</b>
                </div>
                <div class="oper-tab">
                  <ul>
                    <li class="on">
                      <a href="javascript:void(0)">우측</a>
                      <div class="box">
                        <div class="chkbox">
                          <label>
                            <input type="checkbox" name="right_colon" value="-1"/>
                            <span class="check"></span>
                            <span class="name">n/a</span>
                          </label>
                        </div>
                        <div class="chkbox">
                          <label>
                            <input type="checkbox" name="right_colon" value="0"/>
                            <span class="check"></span>
                            <span class="name">0</span>
                          </label>
                        </div>
                        <div class="chkbox">
                          <label>
                            <input type="checkbox" name="right_colon" value="1"/>
                            <span class="check"></span>
                            <span class="name">1</span>
                          </label>
                        </div>
                        <div class="chkbox">
                          <label>
                            <input type="checkbox" name="right_colon" value="2"/>
                            <span class="check"></span>
                            <span class="name">2</span>
                          </label>
                        </div>
                        <div class="chkbox">
                          <label>
                            <input type="checkbox" name="right_colon" value="3"/>
                            <span class="check"></span>
                            <span class="name">3</span>
                          </label>
                        </div>
                      </div>
                    </li>
                    <li>
                      <a href="javascript:void(0)">횡측</a>
                      <div class="box">
                        <div class="chkbox">
                          <label>
                            <input type="checkbox" name="trans_colon" value="-1"/>
                            <span class="check"></span>
                            <span class="name">n/a</span>
                          </label>
                        </div>
                        <div class="chkbox">
                          <label>
                            <input type="checkbox" name="trans_colon" value="0"/>
                            <span class="check"></span>
                            <span class="name">0</span>
                          </label>
                        </div>
                        <div class="chkbox">
                          <label>
                            <input type="checkbox" name="trans_colon" value="1"/>
                            <span class="check"></span>
                            <span class="name">1</span>
                          </label>
                        </div>
                        <div class="chkbox">
                          <label>
                            <input type="checkbox" name="trans_colon" value="2"/>
                            <span class="check"></span>
                            <span class="name">2</span>
                          </label>
                        </div>
                        <div class="chkbox">
                          <label>
                            <input type="checkbox" name="trans_colon" value="3"/>
                            <span class="check"></span>
                            <span class="name">3</span>
                          </label>
                        </div>
                      </div>
                    </li>
                    <li>
                      <a href="javascript:void(0)">좌측</a>
                      <div class="box">
                        <div class="chkbox">
                          <label>
                            <input type="checkbox" name="left_colon" value="-1"/>
                            <span class="check"></span>
                            <span class="name">n/a</span>
                          </label>
                        </div>
                        <div class="chkbox">
                          <label>
                            <input type="checkbox" name="left_colon" value="0"/>
                            <span class="check"></span>
                            <span class="name">0</span>
                          </label>
                        </div>
                        <div class="chkbox">
                          <label>
                            <input type="checkbox" name="left_colon" value="1"/>
                            <span class="check"></span>
                            <span class="name">1</span>
                          </label>
                        </div>
                        <div class="chkbox">
                          <label>
                            <input type="checkbox" name="left_colon" value="2"/>
                            <span class="check"></span>
                            <span class="name">2</span>
                          </label>
                        </div>
                        <div class="chkbox">
                          <label>
                            <input type="checkbox" name="left_colon" value="3"/>
                            <span class="check"></span>
                            <span class="name">3</span>
                          </label>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="group">
                <div class="title etc">
                  <b>장정결도 적합여부</b>
                </div>
                <div class="chkbox">
                  <label>
                    <input type="checkbox" name="bbps_fitness" value="0"/>
                    <span class="check"></span>
                    <span class="name">적합</span>
                  </label>
                </div>
                <div class="chkbox">
                  <label>
                    <input type="checkbox" name="bbps_fitness" value="1"/>
                    <span class="check"></span>
                    <span class="name">부적합</span>
                  </label>
                </div>
              </div>
            </li>
            <li>
              <div class="group">
                <div class="title etc">
                  <b>CFS 소견</b>
                </div>
                <div class="chkbox">
                  <label>
                    <input type="checkbox" name="cfs_opi" value="1"/>
                    <span class="check"></span>
                    <span class="name">정상</span>
                  </label>
                </div>
                <div class="chkbox">
                  <label>
                    <input type="checkbox" name="cfs_opi" value="2"/>
                    <span class="check"></span>
                    <span class="name">과형 성용종</span>
                  </label>
                </div>
                <div class="chkbox">
                  <label>
                    <input type="checkbox" name="cfs_opi" value="3"/>
                    <span class="check"></span>
                    <span class="name">선종</span>
                  </label>
                </div>
                <div class="chkbox">
                  <label>
                    <input type="checkbox" name="cfs_opi" value="4"/>
                    <span class="check"></span>
                    <span class="name">대장암</span>
                  </label>
                </div>
                <div class="chkbox">
                  <label>
                    <input type="checkbox" name="cfs_opi" value="5"/>
                    <span class="check"></span>
                    <span class="name">게실</span>
                  </label>
                </div>
                <div class="chkbox">
                  <label>
                    <input type="checkbox" name="cfs_opi" value="6"/>
                    <span class="check"></span>
                    <span class="name">IBD</span>
                  </label>
                </div>
                <div class="chkbox">
                  <label>
                    <input type="checkbox" name="cfs_opi" value="7"/>
                    <span class="check"></span>
                    <span class="name">기타</span>
                  </label>
                </div>
              </div>
              <div class="group">
                <div class="title etc">
                  <b>용종발견 개수</b>
                </div>
                <div class="range">
                  <div name="polyp_cnt" class="slider-range num">
                    <span class="tooltip"></span>
                    <span class="tooltip"></span>
                  </div>
                </div>
              </div>
            </li>
          </ul>
        </li>
        <li>
          <ul class="check-list">
            <li>
              <div class="group">
                <div class="title">
                  <b>용종 위치</b>
                </div>
                <div class="chkbox">
                  <label>
                    <input type="checkbox" name="polyp_pos" value="1"/>
                    <span class="check"></span>
                    <span class="name">Cecum</span>
                  </label>
                </div>
                <div class="chkbox">
                  <label>
                    <input type="checkbox" name="polyp_pos" value="2"/>
                    <span class="check"></span>
                    <span class="name">A colon</span>
                  </label>
                </div>
                <div class="chkbox">
                  <label>
                    <input type="checkbox" name="polyp_pos" value="3"/>
                    <span class="check"></span>
                    <span class="name">HF</span>
                  </label>
                </div>
                <div class="chkbox">
                  <label>
                    <input type="checkbox" name="polyp_pos" value="4"/>
                    <span class="check"></span>
                    <span class="name">T colon</span>
                  </label>
                </div>
                <div class="chkbox">
                  <label>
                    <input type="checkbox" name="polyp_pos" value="5"/>
                    <span class="check"></span>
                    <span class="name">SF</span>
                  </label>
                </div>
                <div class="chkbox">
                  <label>
                    <input type="checkbox" name="polyp_pos" value="6"/>
                    <span class="check"></span>
                    <span class="name">D colon</span>
                  </label>
                </div>
                <div class="chkbox">
                  <label>
                    <input type="checkbox" name="polyp_pos" value="7"/>
                    <span class="check"></span>
                    <span class="name">S colon</span>
                  </label>
                </div>
                <div class="chkbox">
                  <label>
                    <input type="checkbox" name="polyp_pos" value="8"/>
                    <span class="check"></span>
                    <span class="name">Rectum</span>
                  </label>
                </div>
              </div>
            </li>
            <li>
              <div class="group">
                <div class="title etc">
                  <b>용종크기</b>
                </div>
                <div class="range">
                  <div name="polyp_size" class="slider-range status">
                    <span class="tooltip"></span>
                    <span class="tooltip"></span>
                  </div>
                </div>
              </div>
              <div class="group">
                <div class="title etc">
                  <b>용종 모양</b>
                </div>
                <div class="chkbox">
                  <label>
                    <input type="checkbox" name="polyp_shape" value="1"/>
                    <span class="check"></span>
                    <span class="name">Yamada type 1</span>
                  </label>
                </div>
                <div class="chkbox">
                  <label>
                    <input type="checkbox" name="polyp_shape" value="2"/>
                    <span class="check"></span>
                    <span class="name">Yamada type 2</span>
                  </label>
                </div>
                <div class="chkbox">
                  <label>
                    <input type="checkbox" name="polyp_shape" value="3"/>
                    <span class="check"></span>
                    <span class="name">Yamada type 3</span>
                  </label>
                </div>
                <div class="chkbox">
                  <label>
                    <input type="checkbox" name="polyp_shape" value="4"/>
                    <span class="check"></span>
                    <span class="name">Yamada type 4</span>
                  </label>
                </div>
              </div>
            </li>
            <li>
              <div class="group">
                <div class="title etc">
                  <b>조직검사</b>
                </div>
                <div class="chkbox">
                  <label>
                    <input type="checkbox" name="polyp_bio" value="0"/>
                    <span class="check"></span>
                    <span class="name">Nonspecific change</span>
                  </label>
                </div>
                <div class="chkbox">
                  <label>
                    <input type="checkbox" name="polyp_bio" value="1"/>
                    <span class="check"></span>
                    <span class="name">Hyperplastic polyp</span>
                  </label>
                </div>
                <div class="chkbox">
                  <label>
                    <input type="checkbox" name="polyp_bio" value="2"/>
                    <span class="check"></span>
                    <span class="name">TA LG</span>
                  </label>
                </div>
                <div class="chkbox">
                  <label>
                    <input type="checkbox" name="polyp_bio" value="3"/>
                    <span class="check"></span>
                    <span class="name">TA MG</span>
                  </label>
                </div>
                <div class="chkbox">
                  <label>
                    <input type="checkbox" name="polyp_bio" value="4"/>
                    <span class="check"></span>
                    <span class="name">TA HG</span>
                  </label>
                </div>
                <div class="chkbox">
                  <label>
                    <input type="checkbox" name="polyp_bio" value="5"/>
                    <span class="check"></span>
                    <span class="name">Cancer</span>
                  </label>
                </div>
                <div class="chkbox">
                  <label>
                    <input type="checkbox" name="polyp_bio" value="6"/>
                    <span class="check"></span>
                    <span class="name">Serrated polyp</span>
                  </label>
                </div>
                <div class="chkbox">
                  <label>
                    <input type="checkbox" name="polyp_bio" value="7"/>
                    <span class="check"></span>
                    <span class="name">other</span>
                  </label>
                </div>
              </div>
            </li>
          </ul>
        </li>
      </ul>
    </div>
    <div class="fold">
      <a href="javascript:void(0)"></a>
    </div>
  </div>
