<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="java.io.*" %>

<div class="info">
  <div class="upload-tab">
    <ul>
      <li class="on"><a href="javascript:void(0)">Patient</a></li>
      <li><a href="javascript:void(0)">Operation</a></li>
      <li><a href="javascript:void(0)">Screenshot</a></li>
    </ul>
  </div>
  <div class="upload-info-list">
    <ul>
      <li class="on">
        <div class="group">
          <div class="title">
            <span>환자정보</span>
          </div>
          <div class="upload-input"><input type="text" class="upload-info-input" placeholder="환자명" name="pat_nm"/></div>
          <div class="upload-input"><input type="text" class="upload-info-input" placeholder="ID" name="pat_id"/></div>
          <div class="upload-input"><input type="text" class="upload-info-input" placeholder="번호" name="pat_no"/></div>
          <div class="upload-input">
            <div class="half">
              <input type="number" class="upload-info-input" placeholder="나이" name="pat_age"/>
              <span class="unit">세</span>
            </div>
            <div class="half">
              <input type="number" class="upload-info-input" placeholder="체중" name="pat_weight"/>
              <span class="unit">kg</span>
            </div>
          </div>
          <div class="upload-input">
            <div class="half">
              <input type="number" class="upload-info-input" placeholder="신장" name="pat_height"/>
              <span class="unit">cm</span>
            </div>
            <div class="half">
              <div class="chkbox round">
                <label>
                  <input type="radio" name="pat_gen" value="M"/>
                  <span class="check"></span>
                  <span class="name">남자</span>
                </label>
                <label>
                  <input type="radio" name="pat_gen" value="W"/>
                  <span class="check"></span>
                  <span class="name">여자</span>
                </label>
              </div>
            </div>
          </div>
          <div class="upload-input">
          	<input type="text" class="upload-info-input" placeholder="연구등록일" name="pat_reg_date" autocomplete="off"/>
          </div>
        </div>
        <div class="group">
          <div class="title etc">
            <span>CFS Ix</span>
          </div>
          <div class="chkbox round">
            <label>
              <input type="radio" name="cfs_ix" value="1"/>
              <span class="check"></span>
              <span class="name">screening</span>
            </label>
          </div>
          <div class="chkbox round">
            <label>
              <input type="radio" name="cfs_ix" value="2"/>
              <span class="check"></span>
              <span class="name">CRN f/u</span>
            </label>
          </div>
          <div class="chkbox round">
            <label>
              <input type="radio" name="cfs_ix" value="3"/>
              <span class="check"></span>
              <span class="name">bowel habit change</span>
            </label>
          </div>
          <div class="chkbox round">
            <label>
              <input type="radio" name="cfs_ix" value="4"/>
              <span class="check"></span>
              <span class="name">abdominal pain</span>
            </label>
          </div>
          <div class="chkbox round">
            <label>
              <input type="radio" name="cfs_ix" value="5"/>
              <span class="check"></span>
              <span class="name">blood in stool (M/H/FOB)</span>
            </label>
          </div>
          <div class="chkbox round">
            <label>
              <input type="radio" name="cfs_ix" value="6"/>
              <span class="check"></span>
              <span class="name">anemia</span>
            </label>
          </div>
          <div class="chkbox round">
            <label for="cfs07">
              <input type="radio" name="cfs_ix" value="7"/>
              <span class="check"></span>
              <span class="name">tenesmus</span>
            </label>
          </div>
          <div class="chkbox round">
            <label>
              <input type="radio" name="cfs_ix" value="8"/>
              <span class="check"></span>
              <span class="name">wt. loss</span>
            </label>
          </div>
          <div class="chkbox round">
            <label>
              <input type="radio" name="cfs_ix" value="9"/>
              <span class="check"></span>
              <span class="name">CRC FGx</span>
            </label>
          </div>
          <div class="chkbox round">
            <label>
              <input type="radio" name="cfs_ix" value="10"/>
              <span class="check"></span>
              <span class="name">CPP/EMR</span>
            </label>
          </div>
          <div class="chkbox round">
            <label>
              <input type="radio" name="cfs_ix" value="11"/>
              <span class="check"></span>
              <span class="name">others</span>
            </label>
          </div>
        </div>
        <div class="group">
          <div class="title etc">
            <span>Exclusion</span>
          </div>
          <div class="chkbox round">
            <label>
              <input type="radio" name="exclusion" value="0"/>
              <span class="check"></span>
              <span class="name">없음</span>
            </label>
          </div>
          <div class="chkbox round">
            <label>
              <input type="radio" name="exclusion" value="1"/>
              <span class="check"></span>
              <span class="name">대장 절제술</span>
            </label>
          </div>
          <div class="chkbox round">
            <label>
              <input type="radio" name="exclusion" value="2"/>
              <span class="check"></span>
              <span class="name">이전에 염증성 장질환을 진단</span>
            </label>
          </div>
          <div class="chkbox round">
            <label>
              <input type="radio" name="exclusion" value="3"/>
              <span class="check"></span>
              <span class="name">용종증(polyposis syndrome)<br/>이나 유전적 비용종증 직장결장암의 과거력</span>
            </label>
          </div>
          <div class="chkbox round">
            <label>
              <input type="radio" name="exclusion" value="4"/>
              <span class="check"></span>
              <span class="name">동반된 심한 내과적 또는 외과적 질환</span>
            </label>
          </div>
          <div class="chkbox round">
            <label>
              <input type="radio" name="exclusion" value="5"/>
              <span class="check"></span>
              <span class="name">임신, 수유 중이거나 부인과 질환</span>
            </label>
          </div>
          <div class="chkbox round">
            <label>
              <input type="radio" name="exclusion" value="6"/>
              <span class="check"></span>
              <span class="name">기타 본 연구에 참여가 부적절하다고<br/>연구자가 판단한 자</span>
            </label>
          </div>
        </div>
        <div class="group">
          <div class="title etc">
            <span>수술력</span>
          </div>
          <div class="chkbox round">
            <label>
              <input type="radio" name="history" value="0"/>
              <span class="check"></span>
              <span class="name">없음</span>
            </label>
          </div>
          <div class="chkbox round">
            <label>
              <input type="radio" name="history" value="1"/>
              <span class="check"></span>
              <span class="name">저위험군</span>
            </label>
          </div>
          <div class="chkbox round">
            <label>
              <input type="radio" name="history" value="2"/>
              <span class="check"></span>
              <span class="name">고위험군</span>
            </label>
          </div>
        </div>
        <div class="group">
          <div class="title etc">
            <span>이전 일차 내시경 시행여부</span>
          </div>
          <div class="chkbox round">
            <label>
              <input type="radio" name="endoscopy_stat" value="1"/>
              <span class="check"></span>
              <span class="name">있음</span>
            </label>
            <label>
              <input type="radio" name="endoscopy_stat" value="0"/>
              <span class="check"></span>
              <span class="name">없음</span>
            </label>
          </div>
        </div>
      </li>
      <li>
        <div class="group">
          <div class="title etc"><span>조작정보</span></div>
          <div class="chkbox round">
            <span class="sub">맹장도달: </span>
            <label>
              <input type="radio" name="appendix_reach" value="1"/>
              <span class="check"></span>
              <span class="name">성공</span>
            </label>
            <label>
              <input type="radio" name="appendix_reach" value="0"/>
              <span class="check"></span>
              <span class="name">실패</span>
            </label>
          </div>
          <div class="upload-input">
            <div class="half">
              <span class="sub">맹장 도달 시간</span>
            </div>
            <div class="half">
              <div class="half">
              	<input type="number" class="upload-info-input" name="appx_reach_min" max="999" min="0"/>
              	<span class="unit">분</span>
              </div>
              <div class="half">
              	<input type="number" class="upload-info-input" name="appx_reach_sec" max="59" min="0"/>
              	<span class="unit">초</span>
              </div>
            </div>
          </div>
          <div class="upload-input">
            <div class="half">
              <span class="sub">맹장 회수 시간</span>
            </div>
            <div class="half">
              <div class="half">
              	<input type="number" class="upload-info-input" name="appx_remove_min" max="999" min="0"/>
              	<span class="unit">분</span>
              </div>
              <div class="half">
              	<input type="number" class="upload-info-input" name="appx_remove_sec" max="59" min="0"/>
              	<span class="unit">초</span>
              </div>
            </div>
          </div>
          <div class="upload-input">
            <div class="half">
              <span class="sub">맹장 검사 시간</span>
            </div>
            <div class="half">
              <div class="half">
              	<input type="number" class="upload-info-input" name="appx_total_min" max="999" min="0"/>
              	<span class="unit">분</span>
              </div>
              <div class="half">
              	<input type="number" class="upload-info-input" name="appx_total_sec" max="59" min="0"/>
              	<span class="unit">초</span>
              </div>
            </div>
          </div>
        </div>
        <div class="group">
          <div class="title etc"><span>장정결도 우측</span></div>
          <div class="chkbox round">
            <label>
              <input type="radio" name="right_colon" value="-1"/>
              <span class="check"></span>
              <span class="name">n/a</span>
            </label>
            <label>
              <input type="radio" name="right_colon" value="0"/>
              <span class="check"></span>
              <span class="name">0</span>
            </label>
            <label>
              <input type="radio" name="right_colon" value="1"/>
              <span class="check"></span>
              <span class="name">1</span>
            </label>
            <label>
              <input type="radio" name="right_colon" value="2"/>
              <span class="check"></span>
              <span class="name">2</span>
            </label>
            <label>
              <input type="radio" name="right_colon" value="3"/>
              <span class="check"></span>
              <span class="name">3</span>
            </label>
          </div>
        </div>
        <div class="group">
          <div class="title etc"><span>장정결도 횡행</span></div>
          <div class="chkbox round">
            <label>
              <input type="radio" name="trans_colon" value="-1"/>
              <span class="check"></span>
              <span class="name">n/a</span>
            </label>
            <label>
              <input type="radio" name="trans_colon" value="0"/>
              <span class="check"></span>
              <span class="name">0</span>
            </label>
            <label>
              <input type="radio" name="trans_colon" value="1"/>
              <span class="check"></span>
              <span class="name">1</span>
            </label>
            <label>
              <input type="radio" name="trans_colon" value="2"/>
              <span class="check"></span>
              <span class="name">2</span>
            </label>
            <label>
              <input type="radio" name="trans_colon" value="3"/>
              <span class="check"></span>
              <span class="name">3</span>
            </label>
          </div>
        </div>
        <div class="group">
          <div class="title etc"><span>장정결도 좌측</span></div>
          <div class="chkbox round">
            <label>
              <input type="radio" name="left_colon" value="-1"/>
              <span class="check"></span>
              <span class="name">n/a</span>
            </label>
            <label>
              <input type="radio" name="left_colon" value="0"/>
              <span class="check"></span>
              <span class="name">0</span>
            </label>
            <label>
              <input type="radio" name="left_colon" value="1"/>
              <span class="check"></span>
              <span class="name">1</span>
            </label>
            <label>
              <input type="radio" name="left_colon" value="2"/>
              <span class="check"></span>
              <span class="name">2</span>
            </label>
            <label>
              <input type="radio" name="left_colon" value="3"/>
              <span class="check"></span>
              <span class="name">3</span>
            </label>
          </div>
          
          <div class="title etc"><span>장정결도 합산</span></div>
          <div class="upload-input">
          	<input class="upload-info-input" type="number" name="bbps" readonly="readonly">
          	<input type="hidden" name="bbps_fitness"/>
          </div>        
        </div>
        <div class="group">
          <div class="title etc"><span>CFS 소견</span></div>
          <div class="chkbox round">
            <label>
              <input type="checkbox" name="cfs_opi" value="1"/>
              <span class="check"></span>
              <span class="name">정상</span>
            </label>
          </div>
          <div class="chkbox round">
            <label>
              <input type="checkbox" name="cfs_opi" value="2"/>
              <span class="check"></span>
              <span class="name">과형성용종</span>
            </label>
          </div>
          <div class="chkbox round">
            <label>
              <input type="checkbox" name="cfs_opi" value="3"/>
              <span class="check"></span>
              <span class="name">선종</span>
            </label>
          </div>
          <div class="chkbox round">
            <label>
              <input type="checkbox" name="cfs_opi" value="4"/>
              <span class="check"></span>
              <span class="name">대장암</span>
            </label>
          </div>
          <div class="chkbox round">
            <label>
              <input type="checkbox" name="cfs_opi" value="5"/>
              <span class="check"></span>
              <span class="name">게실</span>
            </label>
          </div>
          <div class="chkbox round">
            <label>
              <input type="checkbox" name="cfs_opi" value="6"/>
              <span class="check"></span>
              <span class="name">IBD</span>
            </label>
          </div>
           <div class="chkbox round">
            <label>
              <input type="checkbox" name="cfs_opi" value="7"/>
              <span class="check"></span>
              <span class="name">기타</span>
            </label>
          </div>
        </div>
        <div class="group">
          <div class="title etc"><span>CFS 소견 : 용종 발견 개수</span></div>
          <div class="upload-input">
          	<input type="number" class="upload-info-input" name="polyp_cnt"/>
          </div>
        </div>
      </li>
      <li>
        <div class="list">
          <ul mv-name="snapshot-list">
         	<!--
            <li>
              <div class="thumbnail">
                <img src="" alt="">
                <span class="time">00:00</span>
              </div>
              <div class="descript">
                <ul>
                  <li>Rectums</li>
                  <li>6mm</li>
                  <li>Yamada type3</li>
                  <li>Hyperplastic potyp</li>
                </ul>
              </div>
              <div class="mod-btn">
                <a href="javascript:void(0)" class="modify"><img src="/resources/img/icon/i-modify.png"/></a>
                <a href="javascript:void(0)" class="remove"><img src="/resources/img/icon/i-del.png"/></a>
              </div>
            </li>
          -->
          </ul>
        </div>
      </li>
    </ul>
  </div>
</div>
 
<div id="video-edit-templates" style="display: none;">
	<div t-template="snapshot">
		<li>
			<div class="thumbnail">
				<div class="thumbnail-image">
					<img t-name="image">
				</div>
				<span t-name="time" class="time">00:00</span>
			</div>
			<div class="descript">
				<ul>
					<li t-name="polyp_pos"></li>
					<li><span t-name="polyp_size"></span>mm</li>
					<li t-name="polyp_shape"></li>
					<li t-name="polyp_bio"></li>
				</ul>
			</div>
			<div class="mod-btn">
				<a t-name="btn-modify" href="javascript:void(0)" onclick="MMUploader.modifySnapshot(this)" class="modify"><img src="/resources/img/icon/i-modify.png"/></a>
				<a t-name="btn-remove" href="javascript:void(0)" onclick="MMUploader.removeSnapshot(this)" class="remove"><img src="/resources/img/icon/i-del.png"/></a>
			</div>
		</li>
	</div>
</div>
 