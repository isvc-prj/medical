<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="java.io.*" %>


<!-- 미디어 플레이어 스타일 (시작) -->
<link rel="stylesheet" type="text/css" href="/resources/css/video/common.css" media="all" />
<link rel="stylesheet" type="text/css" href="/resources/css/video/image.css" media="all" />
<link rel="stylesheet" type="text/css" href="/resources/css/video/video.css" media="all" />
<!-- 미디어 플레이어 스타일 (끝) -->

<!-- 미디어 플레이어 (시작) -->
<script type="text/javascript" src="/resources/js/vp/video.data.generator.js"></script>
<script type="text/javascript" src="/resources/js/vp/video.drawer.js"></script>
<script type="text/javascript" src="/resources/js/vp/video.image.register.js"></script>
<script type="text/javascript" src="/resources/js/vp/video.label.js"></script>
<script type="text/javascript" src="/resources/js/vp/video.mask.js"></script>
<script type="text/javascript" src="/resources/js/vp/video.player.js"></script>
<script type="text/javascript" src="/resources/js/vp/video.thumbnail.js"></script>
<script type="text/javascript" src="/resources/js/vp/vp.common.js"></script>
<!-- 미디어 플레이어 (끝) -->

<!-- 도형 편집 (시작) -->
<script type="text/javascript" src="/resources/js/se/graphics.js"></script>
<script type="text/javascript" src="/resources/js/se/shape.editor.js"></script>
<script type="text/javascript" src="/resources/js/se/shapes/polygon.js"></script>
<!-- 도형 편집 (끝) -->

