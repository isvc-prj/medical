<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="java.io.*" %>


<!-- 미디어 플레이어 스타일 (시작) -->
<link rel="stylesheet" type="text/css" href="/resources/css/video1/common.css" media="all" />
<link rel="stylesheet" type="text/css" href="/resources/css/video1/image.css" media="all" />
<link rel="stylesheet" type="text/css" href="/resources/css/video1/label.css" media="all" />
<link rel="stylesheet" type="text/css" href="/resources/css/video1/mask.css" media="all" />
<link rel="stylesheet" type="text/css" href="/resources/css/video1/video.css" media="all" />
<!-- 미디어 플레이어 스타일 (끝) -->

<!-- 미디어 플레이어 (시작) -->
<script type="text/javascript" src="/resources/js/vp1/video.data.generator.js"></script>
<script type="text/javascript" src="/resources/js/vp1/video.drawer.js"></script>
<script type="text/javascript" src="/resources/js/vp1/video.image.register.js"></script>
<script type="text/javascript" src="/resources/js/vp1/video.label.js"></script>
<script type="text/javascript" src="/resources/js/vp1/video.mask.js"></script>
<script type="text/javascript" src="/resources/js/vp1/video.player.js"></script>
<script type="text/javascript" src="/resources/js/vp1/video.thumbnail.js"></script>
<script type="text/javascript" src="/resources/js/vp1/vp.common.js"></script>
<!-- 미디어 플레이어 (끝) -->

