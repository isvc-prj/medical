<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>
<%@ page import="java.io.*" %>

 <header>
    <div class="logo dib vm bolder">
      <h1><a href="/">medical <span>videos</span></a></h1>
    </div>
    <div class="head-btn dib vm ab">
      <a href="/video/upload" class="upload-btn dib vm tc"><img class="vm" src="/resources/img/icon/i-upload.png"><span class="dib vm">Upload</span></a>
      <a href="#" class="signOut-btn dib vm tc"><img class="vm" src="/resources/img/icon/i-signOut.png"><span class ="dib vm">Sign out</span></a>
    </div>
  </header>

