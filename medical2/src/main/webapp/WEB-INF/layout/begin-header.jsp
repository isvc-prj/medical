<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="java.io.*" %>

<!doctype html>
<html lang="en">
<head class="noselection">
	<meta charset="UTF-8">
	<title>MEDICAL VIDEOS</title>
	<!-- <meta http-equiv="X-UA-Compatible" content="IE=edge"> -->

	<link rel="stylesheet" type="text/css" href="/resources/css/style.css" media="all" />
	<link rel="stylesheet" type="text/css" href="/resources/css/style-support.css" media="all" />

	<script type="text/javascript" src="/resources/js/es6-promise.auto.js"></script>
	<script type="text/javascript" src="/resources/js/jquery-3.3.1.min.js"></script>
	
	<%/*
	<script type="text/javascript" src="/resources/js/jquery.form.min.js"></script>
	*/%>
	
	<script type="text/javascript" src="/resources/js/script.js"></script>
	<script type="text/javascript" src="/resources/js/script-support.js"></script>
	<script type="text/javascript" src="/resources/js/common/types.js"></script>
	<script type="text/javascript" src="/resources/js/common/templates.js"></script>
	<script type="text/javascript" src="/resources/js/common/searcher.js"></script>

	<script>
	$(function(){
		MMSearcher.initialize();
		
		//-----------------------------------------------------------
		// 인자값 처리
		
		var paramSearchQ = $('#param-search-q').val();
		if (paramSearchQ){
			MMSearcher.read(paramSearchQ);
		}
	});
	</script>
	