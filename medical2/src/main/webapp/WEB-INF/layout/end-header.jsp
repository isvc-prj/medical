<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="java.io.*" %>
	
</head>

<!-- 인자 전달 (시작) -->
<c:if test="${not empty q}">
	<input type="hidden" id="param-search-q" value="<c:out value="${q}"/>"/>
</c:if>
<!-- 인자 전달 (끝) -->
