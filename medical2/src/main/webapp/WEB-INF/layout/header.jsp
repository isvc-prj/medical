<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>
<%@ page import="java.io.*" %>

 <header>
    <div class="logo dib vm bolder">
      <h1><a href="/">medical <span>videos</span></a></h1>
    </div>
    <div class="head-btn dib vm ab">
      <a href="/video/upload" class="upload-btn dib vm tc"><img class="vm" src="/resources/img/icon/i-upload.png"><span class="dib vm">Upload</span></a>
      <a href="#" class="signOut-btn dib vm tc"><img class="vm" src="/resources/img/icon/i-signOut.png"><span class ="dib vm">Sign out</span></a>
    </div>
    <div class="search re" id="visual-searcher">
    	<div class="search-box">
      <a vs-name="btn-search" href="javascript:void(0)" class="search-btn dib vm"><img src="/resources/img/icon/i-search.png" alt=""></a>
      <div class="filter-box dib vm" vs-name="filters">
      	<!--
        <div class="ft-box"><span>Screening</span> <img class="remove" src="/resources/img/icon/i-close.png" alt=""></div>
        -->
      	<!--
        <div class="ft-box"><span>Screening</span> <img class="remove" src="/resources/img/icon/i-close.png" alt=""></div>
        -->
      </div>
      <input vs-name="text-title" type="text" placeholder="Search for video" class="filter-search div vm"/>
      <a vs-name="btn-filter" class="filter-btn ab dib" href="javascript:void(0)"></a>
      	</div>
    </div>
  </header>


<div id="visual-searcher-templates" style="display: none;">
	<div t-template="box">
		<div class="ft-box"><span t-name="text"></span> <img t-name="close" class="remove" src="/resources/img/icon/i-close.png" alt=""></div>
	</div>
</div>


<tag:visual-filter id="filter"/>
