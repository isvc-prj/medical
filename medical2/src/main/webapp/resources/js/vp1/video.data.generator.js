var VideoDataGenerator = (function(){
	function defined(a, b){ return typeof a != 'undefined' ? a : b; }
	function definedBoolean(a, b){ return typeof a == 'boolean' ? a : b; }
	function definedNumber(a, b){ return typeof a == 'number' ? a : b; }
	function definedFunction(a, b){ return typeof a == 'function' ? a : b; }

	//-----------------------------------------------------------
	//-----------------------------------------------------------
	//-----------------------------------------------------------

	return {
		label: function(time, imageSource, labels, attrs){
			if (labels === true){
				labels = [
					{
						x: 739.7817086940158,
						y: 249.72713586751976,
						width: 271.52427549868275,
						height: 247.13586751975913,
						color: 'white',
						text: '용종 1',
					},
					{
						x: 1261.6936394429808,
						y: 344.02898005269105,
						width: 302.4162589386526,
						height: 229.25103500188183,
						color: 'red',
						text: '용종 2',
					},
				];
			}else if (!$.isArray(labels)){
				labels = [];
			}
			
			var data = {
				time: time,
				attrs: attrs || {},
				labels: labels,

				image: {
					width: 0,
					height: 0,
					source: imageSource,
					element: null,
					e: null,
				},

				thumbnail: {
					width: 0,
					height: 0,
					source: null,
					element: null,
					e: null,
				},
			};
			
			return new Promise(function (resolve, reject){
				VpCommon.loadImage(data.image.source)
					.then(function(img){
						data.image.width = img.width;
						data.image.height = img.height;
						data.image.element = img;
						data.image.e = $(img);

						var thumbData = VideoThumbnail.generate({
							source: img,
							width: img.width,
							height: img.height,
							draw: function(ctx, width, height, ratio){
								VideoDrawer.labels(ctx, data.labels, {
									ratio: ratio
								});
							}
						});

						data.thumbnail.width = thumbData.width;
						data.thumbnail.height = thumbData.height;
						data.thumbnail.source = thumbData.source;

						return VpCommon.loadImage(thumbData.source);
					}.bind(this))
					.then(function(thumb){
						data.thumbnail.element = thumb;
						data.thumbnail.e = $(thumb);

						resolve(data);

					}.bind(this))
					.catch(function(exception){
						console.log('<<<EXCEPTION::LABEL>>>', exception);
						alert('Label Exception: ' + exception);

						reject(exception);
					}.bind(this));
			});
			
		},
	};
})();