var VideoThumbnail = (function(){
	function defined(a, b){ return typeof a != 'undefined' ? a : b; }
	function definedBoolean(a, b){ return typeof a == 'boolean' ? a : b; }
	function definedNumber(a, b){ return typeof a == 'number' ? a : b; }
	function definedFunction(a, b){ return typeof a == 'function' ? a : b; }

	function generateThumbnail(image, imageWidth, imageHeight, limitWidth, limitHeight, canvas, callback){
		var ratioX = limitWidth / imageWidth;
		var ratioY = limitHeight / imageHeight;
	
		var ratio = ratioX > ratioY ? ratioY : ratioX;
	
		var thumbWidth = Math.round(imageWidth * ratio);
		var thumbHeight = Math.round(imageHeight * ratio);
	
		var ctx = null;
		if (canvas){
			ctx = canvas.get(0).getContext('2d');
			ctx.canvas.width = thumbWidth;
			ctx.canvas.height = thumbHeight;
		}else{
			canvas = $('<canvas width="'+thumbWidth+'" height="'+thumbHeight+'"/>');
			ctx = canvas.get(0).getContext('2d');
		}
	
		ctx.drawImage(image, 0, 0, thumbWidth, thumbHeight);

		if (typeof callback == 'function'){
			callback(ctx, thumbWidth, thumbHeight, ratio);
		}
	
		return {
			width: thumbWidth,
			height: thumbHeight,
			source: ctx.canvas.toDataURL('image/jpeg'),
		};
	}

	function generateOriginal(image, imageWidth, imageHeight, canvas, callback){
		var ctx = null;
		if (canvas){
			ctx = canvas.get(0).getContext('2d');
			ctx.canvas.width = imageWidth;
			ctx.canvas.height = imageHeight;
		}else{
			canvas = $('<canvas width="'+imageWidth+'" height="'+imageHeight+'"/>');
			ctx = canvas.get(0).getContext('2d');
		}
	
		ctx.drawImage(image, 0, 0, imageWidth, imageHeight);

		if (typeof callback == 'function'){
			callback(ctx, imageWidth, imageHeight, 1);
		}
	
		return {
			width: imageWidth,
			height: imageHeight,
			source: ctx.canvas.toDataURL('image/jpeg'),
		};
	}

	//-----------------------------------------------------------
	//-----------------------------------------------------------
	//-----------------------------------------------------------

	return {
		WIDTH: 220,
		HEIGHT: 220,

		generate: function(){
			if (!arguments.length)
				return null;

			var args = arguments[0];

			var limitWidth = definedNumber(args.limitWidth, this.WIDTH);
			var limitHeight = definedNumber(args.limitHeight, this.HEIGHT);
			var canvas = args.canvas;
			var drawCallback = definedFunction(args.draw, null);

			return generateThumbnail(
				args.source,
				args.width, args.height,
				limitWidth, limitHeight,
				canvas,
				drawCallback);
		},

		generateOriginal: function(){
			if (!arguments.length)
				return null;

			var args = arguments[0];

			var canvas = args.canvas;
			var drawCallback = definedFunction(args.draw, null);

			return generateOriginal(
				args.source,
				args.width, args.height,
				canvas,
				drawCallback);
		},
	};
})();