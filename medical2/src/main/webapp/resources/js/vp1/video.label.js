var VideoLabel = (function(){
	function defined(a, b){ return typeof a != 'undefined' ? a : b; }
	function definedBoolean(a, b){ return typeof a == 'boolean' ? a : b; }
	function definedFunction(a, b){ return typeof a == 'function' ? a : b; }

	function vp(e, name) { return e.find('[vp-name="' + name + '"]'); }
	function vpc(e, name) { return e.children('[vp-name="' + name + '"]'); }

	function eo(e) { return e.length ? e.get(0) : null; }
	function is(e, te) { return eo(e) == eo(te); }
	function has(e, te) {
		if (is(e, te)) return true;
		return e.has(te).length > 0;
	}

	function hasAttr(e, name){
		if (e.length){
			return e.get(0).hasAttribute(name);
		}
		return false;
	}

	//-----------------------------------------------------------
	//-----------------------------------------------------------
	//-----------------------------------------------------------

	function _VideoLabel (options){
		if (!options) options = {};

		//-----------------------------------------------------------

		this.selector = options.selector || '#label-editor';

		var e = $(this.selector);

		//-----------------------------------------------------------

		this.e = {
			body: e,

			image: vp(e, 'image'),
			canvas: vp(e, 'canvas'),
			labelList: vp(e, 'label-list'),
			textEditor: vp(e, 'label'),
			textColorSelector: vp(e, 'label-color'),
		};
	
		//-----------------------------------------------------------

		this.viewport = {
			resizing: false, // 윈도우 리사이징 여부
		};

		//-----------------------------------------------------------

		this.timeRes = {
			beginResized: null,
		};

		//-----------------------------------------------------------

		this.buttons = e.find('[vp-btn]');

		//-----------------------------------------------------------

		this.attrs = {};
		this.origin = null;
		// this.origin = {
		// 	time: 103.953017,
		// 	width: 1920,
		// 	height: 1080,
		// 	source: 'sample-img/CHK.jpg',
		// };
		this.labelList = [];
		this.creationIndex = 0;

		//-----------------------------------------------------------

		this.ratio = 1;
		this.selection = null;
		this.selectedItem = null;

		//-----------------------------------------------------------
		
		this.delegateMap = {};

		//-----------------------------------------------------------

		this.observers = [];

		//-----------------------------------------------------------

		this.initialize();
	}

	_VideoLabel.prototype = (function(){
		return {
			constructor: _VideoLabel,
		
			//-----------------------------------------------------------

			initialize: function(){
				displayView(this, 'modify', false);

				attachEvents(this);

				if (this.origin){
					this.set(this.origin);
				}
			},

			//-----------------------------------------------------------

			/**
			 * 옵저버 리스너를 등록합니다.
			 */
			observe: function(observer){ this.observers.push(observer); },
			/**
			 * 옵저버 리스너를 제거합니다.
			 */
			stopObserve: function(observer){
				var idx = this.observers.indexOf(observer);
				if (idx >= 0) this.observers.splice(idx, 1);
			},

			/**
			 * 등록된 옵저버에 Notify합니다.
			 * @param {String} topic 토픽
			 * @param {String} [value] 값
			 */
			notifyObservers: function(topic, value){
				var len = this.observers.length;
				for (var i=0; i < len; i++){
					var o = this.observers[i];

					if (typeof o == 'function')
						o(this, topic, value, this.shape);
					else if (typeof o.styleNotify == 'function')
						o.notifyPlayer(this, topic, value, this.shape);
				}
			},
			
			//-----------------------------------------------------------
			
			delegates: function(name, callback){
				if (!callback)
					delete this.delegateMap[name]
				else
					this.delegateMap[name] = callback;
			},
			
			execDelegates: function (name, cmd){
				var callback = this.delegateMap[name];
				if (callback){
					callback(this, cmd);
				}
			},
		
			//-----------------------------------------------------------

			reset: function(){
				releaseLabelBox(this);

				this.selectedItem = null;
				this.selection = null;

				this.e.canvas.empty();

				this.labelList.splice(0, this.labelList.length);
				this.creationIndex = 0;

				this.attrs = {};
			},

			set: function(origin){
				if (!validate(origin)) return;

				if (this.viewport.resizing){
					//console.log('<<<RESIZED>>> COMMAND');

					this.resized();
				}

				this.reset();

				this.origin = origin;
				changeImage(this, this.origin.source)
					.then(function(){
						this.attrs = {};
						
						if (this.origin.data){
							displayView(this, 'modify', true);

							var labels = this.origin.data.labels;
							var len = labels.length;
							for (var i=0; i < len; i++){
								var l = labels[i];

								this.creationIndex++;

								var box = createLabelBox(self);
								box.css({
									left: l.x * this.ratio,
									top: l.y * this.ratio,
									width: l.width * this.ratio,
									height: l.height * this.ratio,
									color: l.color,
								});
								box.attr('vp-cid', this.creationIndex);
								box.attr('vp-color', l.color);
								box.find('[vp-cmd="label"]').text(l.text);

								if (i + 1 == len){
									box.addClass('vp-select');
									this.selectedItem = box;

									selectLabelBox(this, box);
								}
				
								this.e.canvas.append(box);

								this.labelList.push(box);
							}
							
							for (var p in this.origin.data.attrs){
								this.attrs[p] = this.origin.data.attrs[p];
							}
						}else{
							displayView(this, 'modify', false);
						}
						
						this.execDelegates('attrs', 'set');
						
					}.bind(this));
			},
			
			//-----------------------------------------------------------

			command: function(cmd, value){
				var origin = this.origin;
				var attrs = this.attrs;
				var labelList = this.labelList;
				var numLabelList = labelList.length;
				var data = null;

				if (this.viewport.resizing){
					//console.log('<<<RESIZED>>> COMMAND');

					this.resized();
				}

				switch (cmd){
				case 'save':
					try {
						this.execDelegates('attrs', 'get');
					}catch(e){
						alert(e.message);
						return;
					}
					
					var labels = [];
					for (var i=0; i < numLabelList; i++){
						var label = labelList[i];
						var textLabel = label.find('[vp-cmd="label"]');
						var rect = getRectByVideo(this, label);

						labels.push({
							x: rect.x,
							y: rect.y,
							width: rect.width,
							height: rect.height,
							color: label.attr('vp-color'),
							text: textLabel.text(),
						});
					}
					
					var server = origin && origin.data && typeof origin.data.server == 'boolean' ? origin.data.server : false;

					data = {
						time: origin.time,
						server: server,
						
						attrs: attrs,
						labels: labels,

						image: {
							width: origin.width,
							height: origin.height,
							source: origin.source,
							element: null,
							e: null,
						},

						thumbnail: {
							width: 0,
							height: 0,
							source: null,
							element: null,
							e: null,
						},
					};

					VpCommon.loadImage(origin.source)
						.then(function(img){
							data.image.element = img;
							data.image.e = $(img);

							var thumbData = VideoThumbnail.generate({
								source: img,
								width: origin.width,
								height: origin.height,
								draw: function(ctx, width, height, ratio){
									VideoDrawer.labels(ctx, data.labels, {
										ratio: ratio
									});
								}
							});
		

							data.thumbnail.width = thumbData.width;
							data.thumbnail.height = thumbData.height;
							data.thumbnail.source = thumbData.source;

							return VpCommon.loadImage(thumbData.source);
						}.bind(this))
						.then(function(thumb){
							data.thumbnail.element = thumb;
							data.thumbnail.e = $(thumb);

							this.notifyObservers('save', data);
							this.notifyObservers('end');
		
						}.bind(this))
						.catch(function(exception){
							console.log('<<<EXCEPTION::LABEL>>>', exception);
							alert('Label Exception: ' + exception);
						}.bind(this));
					break;

				case 'cancel':
					this.notifyObservers('end');
					break;

				case 'remove':
					this.notifyObservers('remove', origin.time);
					break;
				}
			},


			//-----------------------------------------------------------

			beginResized: function (){

				if (this.timeRes.beginResized)
					clearTimeout(this.timeRes.beginResized);

				this.viewport.resizing = true;

				var origin = this.origin;
				if (!origin){
					return;
				}

				//-----------------------------------------------------------

				var originWidth = origin.width;
				var pImgWidth = originWidth * this.ratio;
				var bounds = getBounds(this.e.image);

				var viewRatio = bounds.width / pImgWidth;

				//-----------------------------------------------------------
		
				VpCommon.cssScale(this.e.canvas, viewRatio, viewRatio);
	
				this.timeRes.beginResized = setTimeout(function(){
					if (this.viewport.resizing){
						//console.log('<<<RESIZED>>> WINDOW');

						this.resized();
					}
				}.bind(this), 1000);
			},

			resized: function(){

				VpCommon.removeCssScale(this.e.canvas);

				this.viewport.resizing = false;

				if (!this.origin) return;

				//-----------------------------------------------------------

				var oldRatio = this.ratio;

				//-----------------------------------------------------------

				var origin = this.origin;
				var originWidth = origin.width;
				var pImgWidth = originWidth * oldRatio;

				var bounds = getBounds(this.e.image);
				var ratioX = bounds.width / origin.width;
				var ratioY = bounds.height / origin.height;

				this.ratio = ratioX > ratioY ? ratioY : ratioX;
				var viewRatio = bounds.width / pImgWidth;
				//var viewRatio = 1;

				//-----------------------------------------------------------

				var len = this.labelList.length;
				for (var i=0; i < len; i++){
					var box = this.labelList[i];

					var m = getRectByVideo(this, box);

					box.css({
						left: m.x * this.ratio * viewRatio,
						top: m.y * this.ratio * viewRatio,
						width: m.width * this.ratio * viewRatio,
						height: m.height * this.ratio * viewRatio
					});
				}

			},

		};

		//-----------------------------------------------------------

		function validate(d){
			return d
				&& typeof d.source != 'undefined'
				&& typeof d.width != 'undefined'
				&& typeof d.height != 'undefined'
				&& typeof d.time != 'undefined';
		}

		//-----------------------------------------------------------

		function getRectByVideo(controller, e){
			var editRatio = controller.ratio;

			var pos = e.position();
			var b = getBounds(e);

			return {
				x: pos.left / editRatio,
				y: pos.top / editRatio,
				width: b.width / editRatio,
				height: b.height / editRatio,
			};
		}

		//-----------------------------------------------------------

		function displayView(controller, view, visible){
			var a = controller.e.body.find('[vp-view="'+view+'"]');
			if (visible) a.show();
			else a.hide();
		}

		//-----------------------------------------------------------

		function changeImage(controller, source){
			return new Promise(function (resolve, reject){
				var cimg = controller.e.image;

				var onLoad = function(event){
					cimg.unbind('load', onLoad);
					cimg.unbind('error', onError);

					var origin = controller.origin;
					var bounds = getBounds(cimg);
					var ratioX = bounds.width / origin.width;
					var ratioY = bounds.height / origin.height;

					controller.ratio = ratioX > ratioY ? ratioY : ratioX;

					setTimeout(resolve.bind(null, this), 0);
				};
				var onError = function(event){
					cimg.unbind('load', onLoad);
					cimg.unbind('error', onError);
	
					setTimeout(reject.bind(null, new Error('It is not an image file')), 0);
				};

				cimg.bind('load', onLoad);
				cimg.bind('error', onError);
				cimg.attr('src', source);
			});

			// return VpCommon.loadImage(source)
			// 	.then(function(img){
			// 		var cimg = controller.e.image;

			// 		cimg.attr('src', source);
			// 	})
			// 	.catch(function(exception){
			// 		console.log('<<<EXCEPTION>>>', exception);
			// 		alert('Exception: ' + exception);
			// 	});
		}

		//-----------------------------------------------------------

		function getCanvasOffset(controller, event){
			var e = controller.e.canvas;
			var element = eo(e);

			var b = element.getBoundingClientRect();

			return {
				x: event.pageX - b.left,
				y: event.pageY - b.top
			};
		}

		//-----------------------------------------------------------

		function indexLabelOf(controller, e){
			var list = controller.labelList;

			var len = list.length;
			for (var i=0; i < len; i++){
				var be = list[i];

				if (has(be, e)){
					var sel = null;
					var eps = be.find('.vp-edit-point');
					var epsLen = eps.length;
					for (var j = 0; j < epsLen; j++){
						var ep = $(eps.get(j));

						if (has(ep, e)){
							sel = ep;
							break;
						}
					}

					return {
						index: i,
						editPoint: sel,
						label: be,
					};
				}
			}
			return null;
		}

		function hitTestTextEditor(x, y, e){
			if (e.attr('vp-mode') === 'text'){
				var editor = e.find('textarea');
				var b = getBounds(editor);

				if (hitTestRect(b.left, b.top, b.left + b.width, b.top + b.height, x, y)){
					return editor;
				}
			}
			return null;
		}

		function hitTestRect(x1, y1, x2, y2, x, y, dx, dy){
			if (arguments.length == 8)
				return ((((x1 <= x) && (dx2 <=x2)) && (y1 <= y)) && (dy2 <= y2));
			return ((((x1 <= x) && (x < x2)) && (y1 <= y)) && (y < y2));
		}

		//-----------------------------------------------------------

		function getBounds(e){
			return eo(e).getBoundingClientRect();
		}

		function getRect(l, t, r, b){
			var tmp = -1;
			if (l > r){
				tmp = l; l = r; r = tmp;
			}
			if (t > b) {
				tmp = t; t = b; b = tmp;
			}

			var cx = r - l;
			var cy = b - t;

			var x = l;
			var y = t;
			var w = Math.abs(cx);
			var h = Math.abs(cy);

			if (cx < 0) x = x - w;
			if (cy < 0) y = y - h;

			return {
				left: x,
				top: y,
				width: w,
				height: h
			};
		}

		//-----------------------------------------------------------

		function attachEvents(controller){
			var win = $(window);
			win.on('resize', controller, function(event){
				controller.beginResized();
			});

			//-----------------------------------------------------------

			var doc = $(document);

			doc.on('mousemove', controller, function(event){
				var self = event.data;
				var e = $(event.target);

				if (event.which === 1){
					mouseLeftMove(self, event);
				}
			});

			doc.on('mousedown', controller, function(event){
				var self = event.data;
				var e = $(event.target);

				if (has(self.e.canvas, e)){
					//var button = event.which == 1 ? 'left' : event.which == 2 ? 'middle' : event.which == 3 ? 'right' : null;

					if (event.which === 1){
						mouseLeftDown(self, event);
					}
				}
			});

			doc.on('mouseup', controller, function(event){
				var self = event.data;
				var e = $(event.target);

				if (self.selection){
					var box = self.selection.target;

					//box.removeClass('vp-select');

					if (self.selectedItem && !equalsLabelBox(self.selectedItem, box)){
						self.selectedItem.removeClass('vp-select');
						self.selectedItem = null;
					}

					self.selectedItem = box;

					if (self.selection.index < 0){
						self.labelList.push(box);
					}else{
					}
				}
				self.selection = null;
			});

			//-----------------------------------------------------------

			var canvas = controller.e.canvas;
			canvas.attr('tabindex', 0);

			canvas.on('keyup', controller, function(event){
				var self = event.data;

				if (event.which == 46){ // delete
					if (self.selectedItem){
						releaseLabelBox(self);

						var box = self.selectedItem;

						box.removeClass('vp-select');

						if (self.selection && equalsLabelBox(self.selection.target, self.selectedItem))
							self.selection = null;

						self.selectedItem = null;

						removeLabelBox(self, box);
					}
				}
			});

			//-----------------------------------------------------------

			var te = controller.e.textEditor;
			te.on('keyup', controller, function (event){
				var self = event.data;
				var e = $(this);

				updateLabelText(self);
			});

			te.on('change', controller, function (event){
				var self = event.data;
				var e = $(this);

				updateLabelText(self);
			});

			// te.on('focusout', controller, function (event){
			// 	var self = event.data;
			// 	var e = $(this);

			// 	updateLabelText(self);
			// });

			//-----------------------------------------------------------

			var ce = controller.e.textColorSelector;
			ce.on('change', controller, function (event){
				var self = event.data;
				var e = $(this);

				updateLabelColor(self);
			});

			//-----------------------------------------------------------

			controller.buttons.on('click', controller, function(event){
				var self = event.data;
				var e = $(this);

				var cmd = e.attr('vp-btn');
				var value = e.attr('vp-value');
	
				controller.command(cmd, value);
			});
		}

		function equalsLabelBox(a, b){
			return a && b && a.attr('vp-cid') === b.attr('vp-cid');
		}

		function removeLabelBox(controller, e){
			var cid = e.attr('vp-cid');

			var len = controller.labelList.length;
			for (var i=0; i < len; i++){
				var le = controller.labelList[i];
				var lcid = le.attr('vp-cid');

				if (lcid === cid){
					controller.labelList.splice(i, 1);
					le.remove();
					break;
				}
			}

			if (controller.labelList.length === 0){
				controller.creationIndex = 0;
			}
		}

		function mouseLeftDown(controller, event){
			var self = controller;
			var e = $(event.target);

			var offset = getCanvasOffset(self, event);

			var sel = indexLabelOf(self, e);
			if (sel){
				var textEditor = hitTestTextEditor(offset.x, offset.y, sel.label);

				if (!textEditor){
					sel.label.addClass('vp-select');

					var pos = sel.label.position();
					var bounds = getBounds(sel.label);

					self.selection = {
						x: offset.x,
						y: offset.y,
						left: pos.left,
						top: pos.top,
						right: pos.left + bounds.width,
						bottom: pos.top + bounds.height,
						cx: offset.x - pos.left,
						cy: offset.y - pos.top,
						index: sel.index,
						editPoint: sel.editPoint,
						target: sel.label,
					};
				}
			}else{
				var box = createLabelBox(self);
				box.css({
					left: offset.x,
					top: offset.y,
				});

				box.addClass('vp-select');

				self.e.canvas.append(box);

				self.selection = {
					x: offset.x,
					y: offset.y,
					index: -1,
					target: box,
				};	
			}

			if (self.selection){
				selectLabelBox(self, self.selection.target);
			}else{
				releaseLabelBox(self);
			}
		}

		function mouseLeftMove(controller, event){
			var self = controller;

			if (self.selection){
				var offset = getCanvasOffset(self, event);
				var box = self.selection.target;

				if (self.selection.index >= 0){
					if (self.selection.editPoint){
						var ep = self.selection.editPoint;
						var cmd = ep.attr('vp-cmd');

						var left = self.selection.left;
						var top = self.selection.top;
						var right = self.selection.right;
						var bottom = self.selection.bottom;

						var x = offset.x, y = offset.y;
						var rect = null;

						switch(cmd){
						case 'LT': rect = getRect(x, y, right, bottom); break;
						case 'CT': rect = getRect(left, y, right, bottom); break;
						case 'RT': rect = getRect(left, y, x, bottom); break;
						case 'LC': rect = getRect(x, top, right, bottom); break;
						case 'RC': rect = getRect(left, top, x, bottom); break;
						case 'LB': rect = getRect(x, top, right, y); break;
						case 'CB': rect = getRect(left, top, right, y); break;
						case 'RB': rect = getRect(left, top, x, y); break;
						}

						if (rect){
							box.css({
								left: rect.left,
								top: rect.top,
								width: rect.width,
								height: rect.height,
								lineHeight2: rect.height + 'px'
							});
						}

					}else{
						var cx = self.selection.cx;
						var cy = self.selection.cy;

						box.css({
							left: offset.x - cx,
							top: offset.y - cy,
						});	
					}
				}else{
					var x = self.selection.x;
					var y = self.selection.y;
					var cx = offset.x - x;
					var cy = offset.y - y;
					var w = Math.abs(cx);
					var h = Math.abs(cy);

					if (cx < 0) x = x - w;
					if (cy < 0) y = y - h;

					box.css({
						left: x,
						top: y,
						width: w,
						height: h,
						lineHeight2: h + 'px',
					});
				}

			}
		}

		function getLabelBox(controller, cid){
			var len = controller.labelList.length;
			for (var i=0; i < len; i++){
				var e = controller.labelList[i];

				if (e.attr('vp-cid') === cid){
					return {
						index: i,
						e: e,
					};
				}
			}
			return null;
		}

		function selectLabelBox(controller, e){
			var te = controller.e.textEditor;
			var ce = controller.e.textColorSelector;
			var cid = e.attr('vp-cid');
			var text = e.find('[vp-cmd="label"]').text();

			te.prop('disabled', false);
			te.attr('vp-cid', cid);
			te.val(text);

			//-----------------------------------------------------------

			var color = e.attr('vp-color');
			if (!color) color = 'white';

			ce.prop('disabled', false);
			ce.attr('vp-cid', cid);
			ce.val(color);
		}

		function updateLabelText(controller){
			var te = controller.e.textEditor;

			var cid = te.attr('vp-cid');
			if (cid){
				var d = getLabelBox(controller, cid);
				if (d){
					var text = te.val();
					d.e.find('[vp-cmd="label"]').text(text);
				}
			}
		}

		function updateLabelColor(controller){
			var ce = controller.e.textColorSelector;

			var cid = ce.attr('vp-cid');
			if (cid){
				var d = getLabelBox(controller, cid);
				if (d){
					var color = ce.val();

					d.e.attr('vp-color', color);
					d.e.css('color', color);
				}
			}
		}

		function releaseLabelBox(controller){
			var te = controller.e.textEditor;
			var ce = controller.e.textColorSelector;

			te.removeAttr('vp-cid');
			ce.removeAttr('vp-cid');

			te.val('');
			ce.val('');

			te.prop('disabled', true);
			ce.prop('disabled', true);
		}

		function createLabelBox(controller, text){
			text = defined(text, '');

			var e = $('<div class="vp-label-box" vp-color="white"></div>');

			controller.creationIndex++;
			e.attr('vp-cid', controller.creationIndex);
			
			addEditPoint(e, 'LT');
			addEditPoint(e, 'CT');
			addEditPoint(e, 'RT');
			addEditPoint(e, 'LC');
			addEditPoint(e, 'RC');
			addEditPoint(e, 'LB');
			addEditPoint(e, 'CB');
			addEditPoint(e, 'RB');

			addTextLabel(e, text);

			return e;
		}

		function addEditPoint(parent, cmd){
			var e = $('<div class="vp-edit-point vp-ep-'+cmd+'" vp-cmd="'+cmd+'"></div>');
			parent.append(e);

			return e;
		}

		function addTextLabel(parent, text){
			var e = $('<div class="vp-text-label" vp-cmd="label">'+text+'</div>');
			parent.append(e);

			return e;
		}

		function showEditPoints(e, visible){
			var eps = e.find('.vp-edit-point');
			var epsLen = eps.length;
			for (var j = 0; j < epsLen; j++){
				var ep = $(eps.get(j));

				if (visible){
					ep.show();
				}else{
					ep.hide();
				}
			}
		}
	})();

	//-----------------------------------------------------------
	//-----------------------------------------------------------
	//-----------------------------------------------------------

	return _VideoLabel;
})();