var VideoDrawer = (function(){
	function defined(a, b){ return typeof a != 'undefined' ? a : b; }
	function definedBoolean(a, b){ return typeof a == 'boolean' ? a : b; }
	function definedNumber(a, b){ return typeof a == 'number' ? a : b; }
	function definedFunction(a, b){ return typeof a == 'function' ? a : b; }

	function drawLabels(ctx, ratio, labels, fontSize, left, top){
		if (typeof fontSize == 'undefined') fontSize = 16;
		else if (typeof fontSize != 'number') fontSize = parseInt(fontSize);
		
		if (!left) left = 0;
		if (!top) top = 0;

		ctx.strokeStyle = '#99CCFF';
		ctx.font = 'Roboto,Arial,Helvetica,sans-serif';
		ctx.textBaseline = 'top';
		ctx.font = fontSize + 'px Roboto,Arial,Helvetica,sans-serif ';

		var len = labels ? labels.length : 0;
		for (var i=0; i < len; i++){
			var l = labels[i];

			var x = l.x * ratio + left;
			var y = l.y * ratio + top;
			var w = l.width * ratio;
			var h = l.height * ratio;

			ctx.strokeRect(Math.round(x) + 0.5, Math.round(y) + 0.5, Math.round(w), Math.round(h));

			var tw = ctx.measureText(l.text).width;
			var th = fontSize;

			ctx.save();

			ctx.fillStyle = l.color;
			ctx.scale(ratio, ratio);
			ctx.fillText(l.text, l.x + ((l.width - tw) >> 1), l.y + ((l.height - fontSize) >> 1));

			ctx.restore();
		}
	}

	//-----------------------------------------------------------
	//-----------------------------------------------------------
	//-----------------------------------------------------------

	return {
		labels: function(){
			if (arguments.length < 3)
				return;

			var ctx = arguments[0];
			if (!ctx) return;

			var labels = arguments[1];
			if (!labels) return;

			var numLabels = labels.length;
			if (!numLabels) return;

			var options = arguments[2];
			var fontOptions = options.font || {};

			var ratio = definedNumber(options.ratio,1);
			var lineColor = defined(options.lineColor, '#99CCFF');
			var fontSize = definedNumber(fontOptions.size, 20);
			var fontFamily = defined(fontOptions.family, 'Roboto,Arial,Helvetica,sans-serif');
	
			ctx.strokeStyle = lineColor;
			ctx.textBaseline = 'top';
			ctx.font = fontSize + 'px ' + fontFamily;
			
			var left = options.left || 0;
			var top = options.top || 0;
			
			var tleft = left / ratio;
			var ttop = top / ratio;

			for (var i=0; i < numLabels; i++){
				var l = labels[i];
	
				var x = l.x * ratio + left;
				var y = l.y * ratio + top;
				var w = l.width * ratio;
				var h = l.height * ratio;
	
				ctx.strokeRect(Math.round(x) + 0.5, Math.round(y) + 0.5, Math.round(w), Math.round(h));

				if (l.text){
					var tw = ctx.measureText(l.text).width;
					var th = fontSize;
		
					ctx.save();
		
					ctx.fillStyle = l.color;
					ctx.scale(ratio, ratio);
					ctx.fillText(l.text, l.x + tleft + ((l.width - tw) >> 1), l.y + ttop + ((l.height - fontSize) >> 1));
		
					ctx.restore();
				}
	
			}
	
		},
	};
})();