var ShapeEditor = (function(){
	function defined(a, b){ return typeof a != 'undefined' ? a : b; }
	function definedBoolean(a, b){ return typeof a == 'boolean' ? a : b; }
	function definedNumber(a, b){ return typeof a == 'number' ? a : b; }
	function definedString(a, b){ return typeof a == 'string' ? a : b; }
	function definedFunction(a, b){ return typeof a == 'function' ? a : b; }
	function isjQuery(obj){ return (obj && (obj instanceof jQuery || obj.constructor.prototype.jquery)); }

	function eo(e) { return e.length ? e.get(0) : null; }
	function is(e, te) { return eo(e) == eo(te); }
	function has(e, te) { if (is(e, te)) return true; return e.has(te).length > 0; }

	function getRootScrollOffset(e){ var a = e.parents(); var len = a ? a.length : 0; if (len){ var p = a.get(len - 1); return { left: typeof p.scrollLeft == 'number' ? p.scrollLeft : 0, top: typeof p.scrollTop == 'number' ? p.scrollTop : 0, }; } return { left: 0, top: 0 }; }
	function getBounds(e, original){ var r = eo(e).getBoundingClientRect(); if (original) return r; var s = getRootScrollOffset(e); return { left: r.left + s.left, top: r.top + s.top, width: r.width, height: r.height, }; }
	
	function getButton(event){ return event.which == 1 ? 'left' : event.which == 2 ? 'middle' : event.which == 3 ? 'right' : null; }

	//-----------------------------------------------------------
	//-----------------------------------------------------------
	//-----------------------------------------------------------

	function _ShapeEditor(options){
		if (!options) options = {};

		var ge = null;
		if (typeof options.ground == 'string'){
			ge = $(options.ground);
		}else if (isjQuery(options.ground)){
			ge = options.ground;
		}else if (options.ground){
			ge = $(options.ground);
		}

		var be = ge;
		if (options.benchmark){
			if (typeof options.benchmark == 'string'){
				be = $(options.benchmark);
			}else if (isjQuery(options.benchmark)){
				be = options.benchmark;
			}else{
				be = $(options.benchmark);
			}
		}

		var b = getBounds(be);

		this.width = b.width;
		this.height = b.height;
		this.ratio = 1;
		this.origin = options.origin;

		var ecanvas = $('<canvas width="'+this.width+'" height="'+this.height+'"/>');
		ecanvas.css('user-drag', 'none');
		ecanvas.css('user-select', 'none');
		ecanvas.css('-webkit-user-drag', 'none');
		//ecanvas.css('touch-action', 'none');
		ecanvas.css('-webkit-tap-highlight-color', 'rgba(0, 0, 0, 0);');
		ecanvas.css('outline', 'none');
		
		ecanvas.css('position', 'relative');
		
		ecanvas.attr('tabindex', '1');

		ge.append(ecanvas);

		this.e = {
			ground: ge,
			benchmark: be,

			canvas: ecanvas,
		};

		this.eo = {
			ground: eo(ge),
			benchmark: eo(be),

			canvas: eo(ecanvas),
		};


		var ecanvasBuf = $('<canvas width="'+this.width+'" height="'+this.height+'"/>');

		this.buffer = {
			canvas: ecanvasBuf,

			ctx: eo(ecanvasBuf).getContext('2d'),
		};

		this.ctx = this.eo.canvas.getContext('2d');

		this.ctx.translate(0.5, 0.5);
		//this.ctx.lineWidth = VpGraphics.point(1);

		this.creation = null;
		this.selectedShape = null;

		this.shapes = [];
		this.stores = [];

		this.enableNotifyObservers = true;

		//-----------------------------------------------------------

		this.observers = [];

		//-----------------------------------------------------------

		this.initialize();
	}

	//-----------------------------------------------------------
	//-----------------------------------------------------------
	//-----------------------------------------------------------

	_ShapeEditor.prototype = (function(){

		//-----------------------------------------------------------
		//-----------------------------------------------------------
		//-----------------------------------------------------------

		return {

			constructor: _ShapeEditor,
		
			//-----------------------------------------------------------

			initialize: function(){
				attachEvents(this);

				if (this.origin)
					this.resized();
			},

			//-----------------------------------------------------------

			/**
			 * 옵저버 리스너를 등록합니다.
			 */
			observe: function(observer){ this.observers.push(observer); },
			/**
			 * 옵저버 리스너를 제거합니다.
			 */
			stopObserve: function(observer){
				var idx = this.observers.indexOf(observer);
				if (idx >= 0) this.observers.splice(idx, 1);
			},

			/**
			 * 등록된 옵저버에 Notify합니다.
			 * @param {String} topic 토픽
			 * @param {String} [value] 값
			 */
			notifyObservers: function(topic, value){
				if (this.enableNotifyObservers !== true) return;

				var len = this.observers.length;
				for (var i=0; i < len; i++){
					var o = this.observers[i];

					if (typeof o == 'function')
						o(this, topic, value);
					else if (typeof o.notifyEditor == 'function')
						o.notifyEditor(this, topic, value);
				}
			},
		
			//-----------------------------------------------------------

			cursor: function(value){
				switch(value){
				case 'hand':
					this.e.canvas.css('cursor', 'pointer');
					break;

				case 'default':
					this.e.canvas.css('cursor', 'default');
					break;
				}
			},
			
			//-----------------------------------------------------------
			
			focus: function(){
				this.e.canvas.focus();
			},
		
			//-----------------------------------------------------------

			createShape: function(){
				return new ShapePolygon();
			},

			selectShape: function(s){
				if (typeof s == 'number'){
					if (s < 0 || s >= this.shapes.length) return;

					s = this.shapes[s];
				}

				if (s){
					if (this.selectedShape)
						this.selectedShape.selected = false;

					s.selected = true;

					this.selectedShape = s;
				}

				if (s)
					this.notifyObservers('shape:selected', s);
			},

			selectLastShape: function(){
				if (this.shapes.length){
					this.selectShape(this.shapes[this.shapes.length-1]);
				}
			},
		
			//-----------------------------------------------------------

			addShape: function (s){
				this.shapes.push(s);
				
				this.notifyObservers('shape:add', s);
				this.selectShape(s);
			},
		
			//-----------------------------------------------------------

			clear: function(){
				if (this.selectedShape)
					this.selectedShape.selected = false;

				this.shapes.splice(0, this.shapes.length);
				this.selectedShape = null;

				clearCanvas(this.buffer.ctx);
				clearCanvas(this.ctx);
			},
		
			//-----------------------------------------------------------

			contains: function(x, y){
				var len = this.shapes.length;
				for (var i=len-1; i >= 0; i--){
					var s = this.shapes[i];

					if (s.contains(x, y))
						return s;
				}
				return null;
			},

			indexOf: function(s){
				for (var i=this.shapes.length-1; i >= 0; i--){
					if (this.shapes[i] == s)
						return i;
				}
				return -1;
			},
		
			//-----------------------------------------------------------

			render: function(){
				task(this, function(){
					var ctx = this.buffer.ctx;

					clearCanvas(ctx);

					var g = {
						ratio: this.ratio,
					};

					var len = this.shapes.length;
					for (var i=0; i < len; i++){
						var s = this.shapes[i];
						s.draw(ctx, g);
					}

					task(this, function(){
						this.paint();
					});
				});
			},

			paintRect: function (x, y, w, h){
				//console.log('[PAINT-RECT] x' + x + ', y=' + y + ', w=' + w + ', h=' + h);
		
				if (x < 0){ w -= x; x = 0; }
				if (y < 0){ h -= y; y = 0; }
				if (w <= 0 || h <= 0) return;
		
				var ctx = this.ctx;
		
				ctx.clearRect(x, y, w, h);
				ctx.drawImage(this.buffer.ctx.canvas, x, y, w, h, x, y, w, h);
			},

			paint: function(){
				var ctx = this.ctx;

				clearCanvas(ctx);

				ctx.drawImage(this.buffer.ctx.canvas, 0, 0);
			},

			store: function(x, y, width, height){
				// position on canvas
				if (arguments.length === 1){
					this.stores.push(arguments[0]);
				}else{
					this.stores.push({
						x: x,
						y: y,
						width: width,
						height: height
					});
				}

				// this.ctx.save();
				// this.ctx.strokeStyle = 'red';

				// var r = this.stores[this.stores.length - 1];
				// this.ctx.strokeRect(r.x, r.y, r.width, r.height);

				// this.ctx.restore();
			},

			restore: function(){
				var ctx = this.ctx;
				ctx.save();

				var alpha = ctx.globalAlpha;
				ctx.globalAlpha = 1;

				for (var i=this.stores.length - 1; i >= 0; i--){
					var rect = this.stores[i];
					this.paintRect(rect.x, rect.y, rect.width, rect.height);
				}

				ctx.globalAlpha = alpha;
				ctx.restore();

				this.stores.splice(0, this.stores.length);
			},

			resizedGround: function(){
				var pos = this.e.benchmark.position();
				var b = getBounds(this.e.benchmark);
				this.resize(b.width, b.height);
				
				this.e.canvas.css({
					top: pos.top,
				});
			},

			resized: function(){
				this.ratio = VpGraphics.zoom(this.origin.width, this.origin.height, this.width, this.height);

				//console.log('<<<RATIO>>> ', this.ratio);
			},

			resize: function(w, h){
				this.width = w;
				this.height = h;

				this.ctx.canvas.width = w;
				this.ctx.canvas.height = h;

				this.buffer.ctx.canvas.width = w;
				this.buffer.ctx.canvas.height = h;

				this.resized();
			},

			set: function(origin){
				if (!validate(origin)) return;

				this.origin = origin;
				if (this.origin){
					this.resized();
				}
			},
		};

		//-----------------------------------------------------------
		//-----------------------------------------------------------
		//-----------------------------------------------------------

		function validate(d){
			return d
				&& typeof d.width != 'undefined'
				&& typeof d.height != 'undefined';
		}

		//-----------------------------------------------------------

		function clearCanvas(ctx){
			ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
		}

		function task(controller, func, delay){
			if (!delay) delay = 0;
			setTimeout(func.bind(controller), delay);
		}

		function notify(controller, topic, value){
			var len = controller.shapes.length;
			for (var i=0; i < len; i++){
				var s = controller.shapes[i];

				s.notify(topic, value);
			}
		}

		//-----------------------------------------------------------
		//-----------------------------------------------------------
		//-----------------------------------------------------------

		function attachEvents(controller){

			//-----------------------------------------------------------

			var doc = $(document);

			doc.on('mousedown', controller, function(event){
				var self = event.data;
				var e = $(event.target);

				if (has(self.e.canvas, e)){
					//var button = event.which == 1 ? 'left' : event.which == 2 ? 'middle' : event.which == 3 ? 'right' : null;

					mouseDown.bind(self)(event);
				}
			});

			doc.on('mousemove', controller, function(event){
				var self = event.data;

				mouseMove.bind(self)(event);
			});

			doc.on('mouseup', controller, function(event){
				var self = event.data;

				mouseUp.bind(self)(event);
			});

			//-----------------------------------------------------------

			var canvas = controller.e.canvas;
			canvas.attr('tabindex', 0);

			canvas.on('keyup', controller, function(event){
				var self = event.data;

				if (event.which == 46){ // delete
					deleteShape.bind(self)(event);
				}
			});

			canvas.on('contextmenu', function (e){
				e.preventDefault(); // 현재 이벤트의 기본 동작을 중단한다.
				e.stopPropagation(); // 현재 이벤트가 상위로 전파되지 않도록 중단한다.
				e.stopImmediatePropagation(); // 현재 이벤트가 상위뿐 아니라 현재 레벨에 걸린 다른 이벤트도 동작하지 않도록 중단한다.			
	
				if (e.originalEvent && e.originalEvent.hasOwnProperty('returnValue'))
					e.originalEvent.returnValue = false;
				
				return false;
			});
		}

		//-----------------------------------------------------------

		function getCanvasOffset(controller, event){
			var e = controller.e.canvas;
			var element = eo(e);

			var b = getBounds(e);

			return {
				x: event.pageX - b.left,
				y: event.pageY - b.top,
				ratio: controller.ratio,
			};
		}

		function cg(controller, data){
			return $.extend(data, {
				render: function(){
					if (arguments.length === 4){
						this.paintRect(arguments[0], arguments[1], arguments[2], arguments[3]);
					}else{
						this.render();
					}
				}.bind(controller),

				store: function(){
					this.store.apply(this, arguments);
				}.bind(controller),

				restore: function(){
					this.restore.apply(this, arguments);
				}.bind(controller),

				clear: function(){
					clearCanvas(this.ctx);
				}.bind(controller),

				complete: function(){
					var s = this.creation.shape;
					this.creation = null;

					this.addShape(s);
					this.render();

				}.bind(controller),

				cancel: function(){
					this.creation = null;

					this.render();

				}.bind(controller),
			});
		}

		//-----------------------------------------------------------

		function mouseDown(event){
			var button = getButton(event);
			var offset = getCanvasOffset(this, event);

			var g = cg(this, {
				x: Math.round(offset.x / this.ratio),
				y: Math.round(offset.y / this.ratio),
				ratio: this.ratio,
				ctx: this.buffer.ctx,
			});

			if (button === 'left'){
				if (this.creation){
					this.creation.shape.notify('create:add', g);

					if (this.creation != null){
						this.creation.x = g.x;
						this.creation.y = g.y;
					}
				}else{
					var s = this.contains(g.x, g.y);
					if (s){
						this.selectShape(s);
						this.render();
					}else{
						s = this.createShape();
						s.notify('create', g);
	
						this.creation = {
							x: g.x,
							y: g.y,
							shape: s,
						};
					}
				}

				//console.log('<<<MOUSEDOWN>>> ', this.creation);
			}else if (button === 'right'){
				if (this.creation){
					g.creation = this.creation;

					this.creation.shape.notify('create:end', g);
				}
			}
		}

		function mouseMove(event){
			var button = getButton(event);
			var offset = getCanvasOffset(this, event);

			var g = cg(this, {
				x: Math.round(offset.x / this.ratio),
				y: Math.round(offset.y / this.ratio),
				ratio: this.ratio,
				ctx: this.buffer.ctx,
			});

			if (this.creation){
				g.creation = this.creation;
				g.ctx = this.ctx;

				this.creation.shape.notify('create:mousemove', g);

				//console.log('<<<MOUSEMOVE>>>');
			}else{
				var s = this.contains(g.x, g.y);
				if (s){
					this.cursor('hand');
				}else{
					this.cursor('default');
				}
			}
		}

		function mouseUp(event){
			var button = getButton(event);
			var offset = getCanvasOffset(this, event);

			var g = cg(this, {
				x: Math.round(offset.x / this.ratio),
				y: Math.round(offset.y / this.ratio),
				ratio: this.ratio,
				ctx: this.buffer.ctx,
			});

			if (button === 'left'){
				if (this.creation){
					this.creation.shape.notify('create:mouseup', g);
				}
			}
		}

		function deleteShape(event){
			if (this.selectedShape){
				var idx = this.indexOf(this.selectedShape);
				this.selectedShape = null;
				
				this.shapes.splice(idx, 1);

				var len = this.shapes.length;
				if (len){
					idx--;
					if (idx < 0) idx = 0;
					if (idx >= len) idx = len - 1;

					this.selectShape(this.shapes[idx]);
				}else{
					this.notifyObservers('shape:clear');
				}

				this.render();
			}
		}
	})();

	//-----------------------------------------------------------
	//-----------------------------------------------------------
	//-----------------------------------------------------------

	return _ShapeEditor;
})();