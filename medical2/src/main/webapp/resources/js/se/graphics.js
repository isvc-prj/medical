var VpGraphics = {
	point: function (value) { return value - 0.5; },

	zoom: function (width, height, destWidth, destHeight){
		var ratioX = destWidth / width;
		var ratioY = destHeight / height;

		return ratioX > ratioY ? ratioY : ratioX;
	},

	/**
	 * 상자 관련 도구
	 * @memberof VpGraphics
	 * @namespace
	 */
	box: {
		/**
		 * 2개의 좌표로 Box 객체를 생성합니다.
		 * @memberof VpGraphics.box
		 * @param {Number} x1 좌표1의 X좌표
		 * @param {Number} y1 좌표1의 Y좌표
		 * @param {Number} x2 좌표2의 X좌표
		 * @param {Number} y2 좌표2의 Y좌표
		 * @return {Box} Box 객체
		 */
		rect: function(x1, y1, x2, y2){
			if (!x1) x1 = 0;
			if (!y1) y1 = 0;
			if (!x2) x2 = 0;
			if (!y2) y2 = 0;

			var tx1 = Math.min(x1, x2);
			var ty1 = Math.min(y1, y2);
			var tx2 = Math.max(x1, x2);
			var ty2 = Math.max(y1, y2);
	
			return { x: tx1, y: ty1, width: tx2 - tx1, height: ty2 - ty1 };
		},

		inflate: function (x, y, w, h){
			var left = 0, top = 0, right = 0, bottom = 0;
			if (arguments.length == 5){
				var o = arguments[4];
				if (typeof o == 'number'){
					left = top = right = bottom = o;
				}else if (o.width || o.height){
					left = right = o.width;
					top = bottom = o.height;	
				}else{
					top = o.top;
					right = o.right;
					bottom = o.bottom;
					left = o.left;
				}
			}else if (arguments.length == 6) {
				left = right = arguments[4];
				top = bottom = arguments[5];
			} else if (arguments.length == 8) {
				top = arguments[4];
				right = arguments[5];
				bottom = arguments[6];
				left = arguments[7];
			}

			return {
				x: x - left,
				y: y - top,
				width: w + (left + right),
				height: h + (top + bottom)
			};
		},

	},

	contains: {
		linePoint: function (x1, y1, x2, y2, x, y){
			var aX = x - x1, aY = y - y1;
			var bX = x2 - x1, bY = y2 - y1;
			var pX = (aX * bX + aY * bY) / (bX * bX + bY * bY) * bX;
			var pY = (aX * bX + aY * bY) / (bX * bX + bY * bY) * bY;
			var mX = aX - pX, mY = aY - pY;

			return {
				x: mX,
				y: mY,
				p : {
					x: pX,
					y: pY
				},
			};
		},

		lineCircle: function (x1, y1, x2, y2, x, y, radius){
			var lp = this.linePoint(x1, y1, x2, y2, x, y);
			var bX = x2 - x1, bY = y2 - y1;
			
			var d2 = Math.pow(x1 + lp.p.x - x, 2) + Math.pow(y1 + lp.p.y - y, 2);
			var plen2 = Math.pow(x1 + lp.p.x - x1, 2) + Math.pow(y1 + lp.p.y - y1, 2);
			var blen2 = bX * bX + bY * bY;
			var ptimd = lp.p.x * bX + lp.p.y * bY;

			var dist1 = Math.pow(x - x1, 2) + Math.pow(y - y1, 2);
			var dist2 = Math.pow(x - x2, 2) + Math.pow(y - y2, 2);
		   
			//console.log('[LINE] plen2=' + plen2 + ', blen2=' + blen2 + ', ptimd='+ptimd+', d2=' + d2 + ', dist1=' + dist1 + ', dist2=' + dist2);

			return d2 <= radius * radius && plen2 <= blen2 && ptimd >= 0 || dist1 <= radius || dist2 <= radius;
		},

		polygon: function(points, x, y, scaleX, scaleY, startX, startY){
			if (!scaleX) scaleX = 1;
			if (!scaleY) scaleY = 1;

			if (!startX) startX = 0;
			if (!startY) startY = 0;

			var len = points.length;
			if (len == 0) return false;
			if (len == 1 && startX + points[0].x * scaleX == x && startY + points[0].y * scaleY == y) return true;
			if (len == 2)
				return this.lineCircle(startX + points[0].x * scaleX, startY + points[0].y * scaleY, startX + points[1].x * scaleX, startY + points[1].y * scaleY, x, y, 1);

			var cnt = 0, inter = 0;
			var p2 = null, p2x = null, p2y = null;
			var p1x = startX + points[0].x * scaleX, p1y = startY + points[0].y * scaleY;
			for (var i=1; i <= len; i++){
				var p2 = points[i % len];
				p2x = startX + p2.x * scaleX;
				p2y = startY + p2.y * scaleY;

				if (y > Math.min(p1y, p2y)){
					if (y <= Math.max(p1y, p2y)){
						if (x <= Math.max(p1x, p2x)){
							if (p1y != p2y){
								inter = (y - p1y) * (p2x - p1x) / (p2y - p1y) + p1x;
								if (p1x == p2x || x <= inter)
									cnt++;
							}
						}
					}
				}
				p1x = p2x;
				p1y = p2y;
			}
			return cnt % 2 != 0;
		},
	},
};