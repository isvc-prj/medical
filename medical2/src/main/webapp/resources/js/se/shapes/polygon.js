var ShapePolygon = (function(){
	function defined(a, b){ return typeof a != 'undefined' ? a : b; }
	function definedBoolean(a, b){ return typeof a == 'boolean' ? a : b; }
	function definedNumber(a, b){ return typeof a == 'number' ? a : b; }
	function definedString(a, b){ return typeof a == 'string' ? a : b; }
	function definedFunction(a, b){ return typeof a == 'function' ? a : b; }
	function isjQuery(obj){ return (obj && (obj instanceof jQuery || obj.constructor.prototype.jquery)); }

	function eo(e) { return e.length ? e.get(0) : null; }
	function is(e, te) { return eo(e) == eo(te); }
	function has(e, te) { if (is(e, te)) return true; return e.has(te).length > 0; }

	function getBounds(e){ return eo(e).getBoundingClientRect(); }
	function getButton(event){ return event.which == 1 ? 'left' : event.which == 2 ? 'middle' : event.which == 3 ? 'right' : null; }

	//-----------------------------------------------------------
	//-----------------------------------------------------------
	//-----------------------------------------------------------

	function _ShapePolygon(options){
		if (!options) options = {};

		this.name = 'polygon';
		this.selected = false;
		this.attrs = options.attrs || {};

		this.points = options.points || [];
	}

	//-----------------------------------------------------------
	//-----------------------------------------------------------
	//-----------------------------------------------------------

	_ShapePolygon.prototype = (function(){

		//-----------------------------------------------------------
		//-----------------------------------------------------------
		//-----------------------------------------------------------

		return {

			constructor: _ShapePolygon,
	
			//-----------------------------------------------------------

			notify: function(topic, value){
				var g = value;

				switch(topic){
				case 'create':
					this.points.push({ x: g.x, y: g.y });
					break;
				case 'create:mousemove':
					var start = { x: g.creation.x * g.ratio, y: g.creation.y * g.ratio };
					var end = { x: g.x * g.ratio, y: g.y * g.ratio };

					var rect = VpGraphics.box.rect(start.x, start.y, end.x, end.y);
					rect = VpGraphics.box.inflate(rect.x, rect.y, rect.width, rect.height, 1); // add stroke width

					g.restore();
					g.store(rect);

					//draw(g.ctx, this.points, g.ratio);

					if (this.points.length){
						g.ctx.save();
						this.styling(g.ctx);
		
						drawSpread(g.ctx, this.points, g.ratio, end.x, end.y);

						g.ctx.restore();
					}
					break;
				case 'create:mouseup': break;
				case 'create:add':

					if (isEndPoint(this.points, g.x, g.y)){
						g.complete();
					}else{
						this.points.push({ x: g.x, y: g.y });

						g.ctx.save();
						this.styling(g.ctx);
		
						drawLast(g.ctx, this.points, g.ratio);

						g.ctx.restore();
					}
					break;
				case 'create:end':
					if (this.points.length < 2){
						g.cancel();						
					}else{
						g.complete();
					}
					break;
				}
			},
		
			//-----------------------------------------------------------

			styling: function(ctx, action){
				switch(action){
				case 'focus':
					ctx.strokeStyle = '#9D0D00';
					ctx.fillStyle = 'rgba(157,13,0,0.2)';
					break;
				default:
					ctx.strokeStyle = '#99CCFF';
					ctx.fillStyle = 'rgba(91,155,213,0.2)';
					break;
				}
			},
		
			//-----------------------------------------------------------

			contains: function (x, y){
				return VpGraphics.contains.polygon(this.points, x, y);
			},
			
			//-----------------------------------------------------------

			bounds: function(returnType){
				var minPointX = 0, minPointY = 0;
				var maxPointX = 0, maxPointY = 0;

				var len = this.points.length;
				for (var i=0; i < len; i++){
					var p = this.points[i];
		
					minPointX = i == 0 || p.x < minPointX ? p.x : minPointX;
					minPointY = i == 0 || p.y < minPointY ? p.y : minPointY;
					maxPointX = i == 0 || p.x > maxPointX ? p.x : maxPointX;
					maxPointY = i == 0 || p.y > maxPointY ? p.y : maxPointY;
				}

				if (returnType === 'point')
					return { x1: minPointX, y1: minPointY, x2: maxPointX, y2: maxPointY };

				return VpGraphics.box.rect(minPointX, minPointY, maxPointX, maxPointY);
			},
	
			//-----------------------------------------------------------

			draw: function (ctx, g){
				var ratio = definedNumber(g.ratio, 1);

				ctx.save();
				this.styling(ctx, this.selected === true ? 'focus' : '');
				//this.styling(ctx,'focus');
	
				ctx.lineJoin = 'round';
				ctx.lineCap = 'round';
				
				var len = this.points.length;
				if (len == 1){
					var p = this.points[0];

					ctx.arc(p.x * ratio, p.y * ratio, 1, 0, 360);
					ctx.fill();
				}else{
					ctx.beginPath();
					
					for (var i=0; i < len; i++){
						var p = this.points[i];
				
						if (i == 0) ctx.moveTo(p.x * ratio, p.y * ratio);
						else ctx.lineTo(p.x * ratio, p.y * ratio);
					}

					ctx.closePath();

					ctx.fill();
					ctx.stroke();
				}

				ctx.restore();
			},
		};

		//-----------------------------------------------------------
		//-----------------------------------------------------------
		//-----------------------------------------------------------
			
		/**
		 * 좌표 수집을 종료해야 하는 지 판단합니다.
		 * <p>* 시작점과 동일하면 수집 종료
		 * @private
		 * @param {Number} x X좌표
		 * @param {Number} y Y좌표
		 * @return {Boolean} 종료해야 하면 true
		 */
		function isEndPoint(points, x, y){
			if (!points.length) return false;

			var pt = points[0];
			return Math.abs(pt.x - x) < 3 && Math.abs(pt.y - y) < 3;
		}

		function drawLast(ctx, points, ratio){
			if (points.length < 2) return;

			var ratio = definedNumber(ratio, 1);

			ctx.beginPath();

			var sp = points[points.length - 2];
			var ep = points[points.length - 1];
	
			ctx.moveTo(sp.x * ratio, sp.y * ratio);
			ctx.lineTo(ep.x * ratio, ep.y * ratio);
			ctx.stroke();

			ctx.closePath();
		}

		function drawSpread(ctx, points, ratio, x, y){
			if (!points.length) return;

			var ratio = definedNumber(ratio, 1);

			ctx.beginPath();

			var sp = points[points.length - 1];

			ctx.moveTo(sp.x * ratio, sp.y * ratio);
			ctx.lineTo(x, y);
			ctx.stroke();

			ctx.closePath();
		}

	})();

	//-----------------------------------------------------------
	//-----------------------------------------------------------
	//-----------------------------------------------------------

	return _ShapePolygon;
})();