$(document).ready(function(){
  $('.filter-btn').on('click', function(){
    var isOn = $(this).is('.on');
    var filter = $('#filter');
    if(isOn){
      $(this).removeClass('on');
      filter.removeClass('on');
    }else{
      $(this).addClass('on');
      filter.addClass('on');
//      age();
//      weight();
//      height();
    }
  })

  $('.close-btn, .fold').on('click', function(){
    $(this).parents('#filter').removeClass('on');
    $('.filter-btn').removeClass('on');
  })

  $('.oper-tab a').on('click', function(){
    $(this).parent('li').addClass('on').siblings().removeClass('on');

  })

  $('.filter-tab li').on('click', function(){
    var idx = $(this).index();
    $(this).addClass('on').siblings().removeClass('on');
    $('.filter-list > li').eq(idx).addClass('on').siblings().removeClass('on');
//    if(idx == 0){
//      age();
//      weight();
//    }else if(idx == 1){
//      arrive();
//      rtime();
//      alltime();
//      num();
//    }else if(idx == 2){
//      status();
//    }
  })

  // 환자나이
  function age(){
    $( ".slider-range.age" ).slider({
      range: true,
      min: 0,
      max: 200,
      step: 1,
      values: [ 0, 200 ],
      slide: function( event, ui ) {
        var target = $(event.target);
        var handle = target.find('.ui-slider-handle');
        handle.each(function(i,e){
          var tooltip = target.find('.tooltip').eq(i);
          var left = $(e).position().left - 28;
          var val = $('.slider-range.age').slider('values',i);
          tooltip.css('left', left);
          tooltip.text(val);
        })
      },
      create: function(event, ui){
        var target = $(event.target);
        var handle = target.find('.ui-slider-handle');
        handle.each(function(i,e){
          var tooltip = target.find('.tooltip').eq(i);
          var left = $(e).position().left - 28;
          var val = $('.slider-range.age').slider('values',i);
          tooltip.css('left', left);
          tooltip.text(val);
        })
      }
    });
  }
  // 환자 체중
  function weight(){
    $( ".slider-range.weight" ).slider({
      range: true,
      min: 0,
      max: 500,
      step: 1,
      values: [ 0, 500 ],
      slide: function( event, ui ) {
        var target = $(event.target);
        var handle = target.find('.ui-slider-handle');
        handle.each(function(i,e){
          var tooltip = target.find('.tooltip').eq(i);
          var left = $(e).position().left - 28;
          var val = $('.slider-range.weight').slider('values',i);
          tooltip.css('left', left);
          tooltip.text(val);
        })
      },
      create: function(event, ui){
        var target = $(event.target);
        var handle = target.find('.ui-slider-handle');
        handle.each(function(i,e){
          var tooltip = target.find('.tooltip').eq(i);
          var left = $(e).position().left - 28;
          var val = $('.slider-range.weight').slider('values',i);
          tooltip.css('left', left);
          tooltip.text(val);
        })
      }
    });
  }
  // 환자 신장
  function height(){
    $( ".slider-range.height" ).slider({
      range: true,
      min: 0,
      max: 300,
      step: 1,
      values: [ 0, 300 ],
      slide: function( event, ui ) {
        var target = $(event.target);
        var handle = target.find('.ui-slider-handle');
        handle.each(function(i,e){
          var tooltip = target.find('.tooltip').eq(i);
          var left = $(e).position().left - 28;
          var val = $('.slider-range.height').slider('values',i);
          tooltip.css('left', left);
          tooltip.text(val);
        })
      },
      create: function(event, ui){
        var target = $(event.target);
        var handle = target.find('.ui-slider-handle');
        handle.each(function(i,e){
          var tooltip = target.find('.tooltip').eq(i);
          var left = $(e).position().left - 28;
          var val = $('.slider-range.height').slider('values',i);
          tooltip.css('left', left);
          tooltip.text(val);
        })
      }
    });
  }
  // 맹장 도달시간
  function arrive(){
    $( ".slider-range.arrive" ).slider({
      range: true,
      min: 0,
      max: 900,
      step: 1,
      values: [ 0, 900 ],
      slide: function( event, ui ) {
        var target = $(event.target);
        var handle = target.find('.ui-slider-handle');
        handle.each(function(i,e){
          var tooltip = target.find('.tooltip').eq(i);
          var left = $(e).position().left - 28;
          var val = $('.slider-range.arrive').slider('values',i);
          var timeSpanFormat = durationFormat(val);
          
          tooltip.css('left', left);
          tooltip.text(timeSpanFormat);
        })
      },
      create: function(event, ui){
        var target = $(event.target);
        var handle = target.find('.ui-slider-handle');
        handle.each(function(i,e){
          var tooltip = target.find('.tooltip').eq(i);
          var left = $(e).position().left - 28;
          var val = $('.slider-range.arrive').slider('values',i);
          var timeSpanFormat = durationFormat(val);
          tooltip.css('left', left);
          tooltip.text(timeSpanFormat);
        })
      }
    });
  }
  // 맹장 회수시간
  function rtime(){
    $( ".slider-range.return" ).slider({
      range: true,
      min: 0,
      max: 900,
      step: 1,
      values: [ 0, 900 ],
      slide: function( event, ui ) {
        var target = $(event.target);
        var handle = target.find('.ui-slider-handle');
        handle.each(function(i,e){
          var tooltip = target.find('.tooltip').eq(i);
          var left = $(e).position().left - 28;
          var val = $('.slider-range.return').slider('values',i);
          var timeSpanFormat = durationFormat(val);
          tooltip.css('left', left);
          tooltip.text(timeSpanFormat);
        })
      },
      create: function(event, ui){
        var target = $(event.target);
        var handle = target.find('.ui-slider-handle');
        handle.each(function(i,e){
          var tooltip = target.find('.tooltip').eq(i);
          var left = $(e).position().left - 28;
          var val = $('.slider-range.return').slider('values',i);
          var timeSpanFormat = durationFormat(val);
          tooltip.css('left', left);
          tooltip.text(timeSpanFormat);
        })
      }
    });
  }
  // 전체 검사 시간
  function alltime(){
    $( ".slider-range.alltime" ).slider({
      range: true,
      min: 0,
      max: 900,
      step: 1,
      values: [ 0, 900 ],
      slide: function( event, ui ) {
        var target = $(event.target);
        var handle = target.find('.ui-slider-handle');
        handle.each(function(i,e){
          var tooltip = target.find('.tooltip').eq(i);
          var left = $(e).position().left - 28;
          var val = $('.slider-range.alltime').slider('values',i);
          var timeSpanFormat = durationFormat(val);
          tooltip.css('left', left);
          tooltip.text(timeSpanFormat);
        })
      },
      create: function(event, ui){
        var target = $(event.target);
        var handle = target.find('.ui-slider-handle');
        handle.each(function(i,e){
          var tooltip = target.find('.tooltip').eq(i);
          var left = $(e).position().left - 28;
          var val = $('.slider-range.alltime').slider('values',i);
          var timeSpanFormat = durationFormat(val);
          tooltip.css('left', left);
          tooltip.text(timeSpanFormat);
        })
      }
    });
  }
  // 융종 발견 개수
  function num(){
    $( ".slider-range.num" ).slider({
      range: true,
      min: 0,
      max: 100,
      step: 1,
      values: [ 0, 100 ],
      slide: function( event, ui ) {
        var target = $(event.target);
        var handle = target.find('.ui-slider-handle');
        handle.each(function(i,e){
          var tooltip = target.find('.tooltip').eq(i);
          var left = $(e).position().left - 28;
          var val = $('.slider-range.num').slider('values',i);
          tooltip.css('left', left);
          tooltip.text(val);
        })
      },
      create: function(event, ui){
        var target = $(event.target);
        var handle = target.find('.ui-slider-handle');
        handle.each(function(i,e){
          var tooltip = target.find('.tooltip').eq(i);
          var left = $(e).position().left - 28;
          var val = $('.slider-range.num').slider('values',i);
          tooltip.css('left', left);
          tooltip.text(val);
        })
      }
    });
  }
  // screenshot 장정결도
  function status(){
    $( ".slider-range.status" ).slider({
      range: true,
      min: 0,
      max: 100,
      step: 1,
      values: [ 0, 100 ],
      slide: function( event, ui ) {
        var target = $(event.target);
        var handle = target.find('.ui-slider-handle');
        handle.each(function(i,e){
          var tooltip = target.find('.tooltip').eq(i);
          var left = $(e).position().left - 28;
          var val = $('.slider-range.status').slider('values',i);
          tooltip.css('left', left);
          tooltip.text(val);
        })
      },
      create: function(event, ui){
        var target = $(event.target);
        var handle = target.find('.ui-slider-handle');
        handle.each(function(i,e){
          var tooltip = target.find('.tooltip').eq(i);
          var left = $(e).position().left - 28;
          var val = $('.slider-range.status').slider('values',i);
          tooltip.css('left', left);
          tooltip.text(val);
        })
      }
    });
  }

  $('.upload-tab li').on('click', function(){
    var idx = $(this).index();

    $(this).addClass('on').siblings().removeClass('on');    
    $('.upload-info-list > ul > li').eq(idx).addClass('on').siblings().removeClass('on');

  })
//  $('.v-regist-btn').on('click', function(){
//    $('.popup.screenshot').addClass('on');
//  })

  $('.popup-close').on('click', function(){
    $(this).parents('.popup').removeClass('on');
  })

})
