var VideoLabel = (function(){
	function defined(a, b){ return typeof a != 'undefined' ? a : b; }
	function definedNumber(a, b){ return typeof a == 'number' ? a : b; }
	function definedBoolean(a, b){ return typeof a == 'boolean' ? a : b; }
	function definedFunction(a, b){ return typeof a == 'function' ? a : b; }

	function vp(e, name) { return e.find('[vp-name="' + name + '"]'); }
	function vpc(e, name) { return e.children('[vp-name="' + name + '"]'); }

	function eo(e) { return e.length ? e.get(0) : null; }
	function is(e, te) { return eo(e) == eo(te); }
	function has(e, te) {
		if (is(e, te)) return true;
		return e.has(te).length > 0;
	}

	function hasAttr(e, name){
		if (e.length){
			return e.get(0).hasAttribute(name);
		}
		return false;
	}

	//-----------------------------------------------------------
	//-----------------------------------------------------------
	//-----------------------------------------------------------

	function _VideoLabel (options){
		if (!options) options = {};

		//-----------------------------------------------------------

		this.selector = options.selector || '#label-editor';

		var e = $(this.selector);

		//-----------------------------------------------------------

		this.e = {
			body: e,
			
			scrollContainer: vp(e, 'scroll-container'),

			image: vp(e, 'image'),
			canvas: vp(e, 'canvas'),
			labelList: vp(e, 'label-list'),
			textEditor: vp(e, 'label'),
			textColorSelector: vp(e, 'label-color'),
		};

		//-----------------------------------------------------------

		this.buttons = e.find('[vp-btn]');

		//-----------------------------------------------------------

		this.origin = null;
		// this.origin = {
		// 	time: 103.953017,
		// 	width: 1920,
		// 	height: 1080,
		// 	source: 'sample-img/CHK.jpg',
		// };

		//-----------------------------------------------------------

		this.ratio = 1;
		this.selection = null;
		this.selectedItem = null;

		//-----------------------------------------------------------
		
		this.delegateMap = {};

		//-----------------------------------------------------------

		this.observers = [];

		//-----------------------------------------------------------
		
		this.editor = new ShapeEditor({
			ground: this.e.canvas,
			benchmark: this.e.image,
		});
		
		this.editor.observe(this);

		//-----------------------------------------------------------

		this.initialize();
	}

	_VideoLabel.prototype = (function(){
		return {
			constructor: _VideoLabel,
		
			//-----------------------------------------------------------

			initialize: function(){
				displayView(this, 'modify', false);
				
				attachEvents(this);

				if (this.origin){
					this.set(this.origin);
				}
			},

			//-----------------------------------------------------------

			/**
			 * 옵저버 리스너를 등록합니다.
			 */
			observe: function(observer){ this.observers.push(observer); },
			/**
			 * 옵저버 리스너를 제거합니다.
			 */
			stopObserve: function(observer){
				var idx = this.observers.indexOf(observer);
				if (idx >= 0) this.observers.splice(idx, 1);
			},

			/**
			 * 등록된 옵저버에 Notify합니다.
			 * @param {String} topic 토픽
			 * @param {String} [value] 값
			 */
			notifyObservers: function(topic, value){
				var len = this.observers.length;
				for (var i=0; i < len; i++){
					var o = this.observers[i];

					if (typeof o == 'function')
						o(this, topic, value, this.shape);
					else if (typeof o.styleNotify == 'function')
						o.notifyPlayer(this, topic, value, this.shape);
				}
			},
			
			//-----------------------------------------------------------
			
			delegates: function(name, callback){
				if (!callback)
					delete this.delegateMap[name];
				else
					this.delegateMap[name] = callback;
			},
			
			execDelegates: function (name, cmd, value){
				var callback = this.delegateMap[name];
				if (callback){
					callback(this, cmd, value);
				}
			},
			
			//-----------------------------------------------------------
			
			show: function(){
				this.e.body.show();
				this.e.scrollContainer.scrollTop(0);
				
				this.editor.focus();
			},
			
			hide: function(){
				this.e.body.hide();
			},
	
			//-----------------------------------------------------------

			reset: function(){
				this.selectedItem = null;
				this.selection = null;

				this.editor.clear();
			},

			set: function(origin, labelIndex){
				if (!validate(origin)) return;
				
				labelIndex = definedNumber(labelIndex, -1);

				this.reset();

				this.origin = origin;
				
				this.editor.clear();
				this.editor.set(this.origin);
				
				changeImage(this, this.origin.source)
					.then(function(){
						
						this.editor.resizedGround();
				
						if (this.origin.data){
							displayView(this, 'modify', true);
							
							this.editor.enableNotifyObservers = false;

							var labels = this.origin.data.labels;
							var len = labels.length;
							for (var i=0; i < len; i++){
								var l = labels[i];

								// shape regist
								
								this.editor.addShape(new ShapePolygon({
									attrs: l.attrs,
									points: l.points,
								}));
							}
							
							this.editor.enableNotifyObservers = true;
							
							if (labelIndex >= 0 && labelIndex < len)
								this.editor.selectShape(labelIndex);
							else
								this.editor.selectLastShape();
							
							this.editor.render();
						}else{
							displayView(this, 'modify', false);
						}
						
						this.execDelegates('attrs', 'prepare');
						
						if (this.editor.selectedShape)
							this.execDelegates('attrs', 'set', this.editor.selectedShape.attrs);
						else
							this.execDelegates('attrs', 'set', {});
					}.bind(this));
			},
			
			setAttr: function (name, value){
				this.editor.selectedShape.attrs[name] = value;
			},

			//-----------------------------------------------------------
			
			notifyEditor: function(sendor, topic, value){
				switch(topic){
				case 'shape:selected':
					this.execDelegates('attrs', 'enable');
					this.execDelegates('attrs', 'set', this.editor.selectedShape.attrs);					
					break;
					
				case 'shape:clear':
					this.execDelegates('attrs', 'disable');
					this.execDelegates('attrs', 'set', {});
					break;
				}
			},
			
			//-----------------------------------------------------------
			
			validate: function(){
				var len = this.editor.shapes.length;
				for (var i=0; i < len; i++){
					var s = this.editor.shapes[i];
					try
					{
						this.execDelegates('attrs', 'validate', s.attrs);
					}
					catch(e)
					{
						alert(e.message);
						return i;
					}
				}
				return -1;
			},
			
			//-----------------------------------------------------------

			command: function(cmd, value){
				var origin = this.origin;
				var shapes = this.editor.shapes;
				var data = null;

				switch (cmd){
				case 'save':
					var len = shapes.length;
					
					if (!len){
						alert('라벨을 설정하세요');
						return;
					}
					
					var idx = this.validate();
					if (idx >= 0){
						this.editor.selectShape(idx);
						this.editor.render();
						return;
					}
					
					var server = origin && origin.data && typeof origin.data.server == 'boolean' ? origin.data.server : false;

					data = {
						time: origin.time,
						server: server,
						
						labels: null,

						image: {
							width: origin.width,
							height: origin.height,
							source: origin.source,
							element: null,
							e: null,
						},

						thumbnail: {
							width: 0,
							height: 0,
							source: null,
							element: null,
							e: null,
						},
					};

					VpCommon.loadImage(origin.source)
						.then(function(img){
							data.image.element = img;
							data.image.e = $(img);
							
							var len = shapes.length;
							var labels = [];							
							for (var i=0; i < len; i++){
								var s = shapes[i];
								var bounds = s.bounds();
								
								var pImgSrc = VideoDrawer.pickImage(img, bounds, s.points);

								labels.push({
									x: bounds.x,
									y: bounds.y,
									width: bounds.width,
									height: bounds.height,
									attrs: s.attrs,
									points: s.points,
									source: pImgSrc,
								});
							}
							
							data.labels = labels;
							
							var thumbData = VideoThumbnail.generate({
								source: img,
								width: origin.width,
								height: origin.height,
								draw: function(ctx, width, height, ratio){
									VideoDrawer.labels(ctx, data.labels, {
										ratio: ratio
									});
								}
							});

							data.thumbnail.width = thumbData.width;
							data.thumbnail.height = thumbData.height;
							data.thumbnail.source = thumbData.source;

							return VpCommon.loadImage(thumbData.source);
						}.bind(this))
						.then(function(thumb){
							data.thumbnail.element = thumb;
							data.thumbnail.e = $(thumb);

							this.notifyObservers('save', data);
							this.notifyObservers('end');
		
						}.bind(this))
						.catch(function(exception){
							console.log('<<<EXCEPTION::LABEL>>>', exception);
							alert('Label Exception: ' + exception);
						}.bind(this));
					break;

				case 'cancel':
					this.notifyObservers('end');
					break;

				case 'remove':
					this.notifyObservers('remove', origin.time);
					break;
				}
			},

		};

		//-----------------------------------------------------------

		function validate(d){
			return d
				&& typeof d.source != 'undefined'
				&& typeof d.width != 'undefined'
				&& typeof d.height != 'undefined'
				&& typeof d.time != 'undefined';
		}

		//-----------------------------------------------------------

		function cloneAttrs(attrs){
			var r = {};
			for (var p in attrs) r[p] = attrs[p];
			return r;
		}

		//-----------------------------------------------------------

		function displayView(controller, view, visible){
			var a = controller.e.body.find('[vp-view="'+view+'"]');
			if (visible) a.show();
			else a.hide();
		}

		//-----------------------------------------------------------

		function changeImage(controller, source){
			return new Promise(function (resolve, reject){
				var cimg = controller.e.image;

				var onLoad = function(event){
					cimg.unbind('load', onLoad);
					cimg.unbind('error', onError);

					var origin = controller.origin;
					var bounds = getBounds(cimg);
					var ratioX = bounds.width / origin.width;
					var ratioY = bounds.height / origin.height;

					controller.ratio = ratioX > ratioY ? ratioY : ratioX;

					setTimeout(resolve.bind(null, this), 0);
				};
				var onError = function(event){
					cimg.unbind('load', onLoad);
					cimg.unbind('error', onError);
	
					setTimeout(reject.bind(null, new Error('It is not an image file')), 0);
				};

				cimg.bind('load', onLoad);
				cimg.bind('error', onError);
				cimg.attr('src', source);
			});

			// return VpCommon.loadImage(source)
			// 	.then(function(img){
			// 		var cimg = controller.e.image;

			// 		cimg.attr('src', source);
			// 	})
			// 	.catch(function(exception){
			// 		console.log('<<<EXCEPTION>>>', exception);
			// 		alert('Exception: ' + exception);
			// 	});
		}

		//-----------------------------------------------------------

		function getBounds(e){
			return eo(e).getBoundingClientRect();
		}

		//-----------------------------------------------------------

		function attachEvents(controller){
			controller.buttons.on('click', controller, function(event){
				var self = event.data;
				var e = $(this);

				var cmd = e.attr('vp-btn');
				var value = e.attr('vp-value');
	
				controller.command(cmd, value);
			});
		}
	})();

	//-----------------------------------------------------------
	//-----------------------------------------------------------
	//-----------------------------------------------------------

	return _VideoLabel;
})();