var VideoDrawer = (function(){
	function defined(a, b){ return typeof a != 'undefined' ? a : b; }
	function definedBoolean(a, b){ return typeof a == 'boolean' ? a : b; }
	function definedNumber(a, b){ return typeof a == 'number' ? a : b; }
	function definedFunction(a, b){ return typeof a == 'function' ? a : b; }
	function isjQuery(obj){ return (obj && (obj instanceof jQuery || obj.constructor.prototype.jquery)); }

	function eo(e) { return e.length ? e.get(0) : null; }
	
	//-----------------------------------------------------------
	//-----------------------------------------------------------
	//-----------------------------------------------------------

	return {
		labels: function(){
			if (arguments.length < 3)
				return;

			var ctx = arguments[0];
			if (!ctx) return;

			var labels = arguments[1];
			if (!labels) return;

			var numLabels = labels.length;
			if (!numLabels) return;

			var options = arguments[2];

			var ratio = definedNumber(options.ratio,1);
			var lineColor = defined(options.lineColor, '#99CCFF');
	
			ctx.save();
			ctx.translate(0.5, 0.5);
			
			ctx.strokeStyle = lineColor;
			ctx.lineJoin = 'round';
			ctx.lineCap = 'round';
			
			var left = options.left || 0;
			var top = options.top || 0;
			
			var tleft = left / ratio;
			var ttop = top / ratio;

			for (var ilabel=0; ilabel < numLabels; ilabel++){
				var l = labels[ilabel];
				
				var points = l.points;
				var len = points.length;
	
				if (len == 1){
					var p = points[0];

					ctx.arc(p.x * ratio, p.y * ratio, 1, 0, 360);
					ctx.fill();
				}else{
					ctx.beginPath();
					
					for (var i=0; i < len; i++){
						var p = points[i];
				
						if (i == 0) ctx.moveTo(p.x * ratio, p.y * ratio);
						else ctx.lineTo(p.x * ratio, p.y * ratio);
					}

					ctx.closePath();

					ctx.stroke();
				}
			}
	
			ctx.restore();
		},
		
		pickImage: function(image, bounds, points){
			var imgElement = null;
			if (isjQuery(image)) imgElement = eo(image);
			else imgElement = image;
			
			var ecanvas = $('<canvas width="'+bounds.width+'" height="'+bounds.height+'"/>');
			var ctx = ecanvas.get(0).getContext('2d');
			
			var hasPoints = points && $.isArray(points);
			
			if (hasPoints){
				var len = points.length;

				ctx.beginPath();
				
				var sx = bounds.x, sy = bounds.y;
				
				if (len == 1){
					var p = points[0];

					ctx.arc(p.x - sx, p.y - sy, 1, 0, 360);
				}else{
					for (var i=0; i < len; i++){
						var p = points[i];
				
						if (i == 0) ctx.moveTo(p.x - sx, p.y - sy);
						else ctx.lineTo(p.x - sx, p.y - sy);
					}
				}
				ctx.closePath();
				ctx.clip();
			}
			
			ctx.drawImage(imgElement, bounds.x, bounds.y, bounds.width, bounds.height, 0, 0, bounds.width, bounds.height);
			
			return ctx.canvas.toDataURL('image/png');
		},
	};
})();