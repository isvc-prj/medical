var VideoMask = (function(){
	function defined(a, b){ return typeof a != 'undefined' ? a : b; }
	function definedBoolean(a, b){ return typeof a == 'boolean' ? a : b; }
	function definedFunction(a, b){ return typeof a == 'function' ? a : b; }

	function vp(e, name) { return e.find('[vp-name="' + name + '"]'); }
	function vpc(e, name) { return e.children('[vp-name="' + name + '"]'); }
	function gete(e, attr, name) {
		var len = e.length;
		for (var i=0; i < len; i++){
			var ce = $(e.get(i));
			if (ce.attr(attr) === name)
				return ce;
		}
		return null;
	}

	function eo(e) { return e.length ? e.get(0) : null; }
	function is(e, te) { return eo(e) == eo(te); }
	function has(e, te) {
		if (is(e, te)) return true;
		return e.has(te).length > 0;
	}

	function hasAttr(e, name){
		if (e.length){
			return e.get(0).hasAttribute(name);
		}
		return false;
	}

	//-----------------------------------------------------------
	//-----------------------------------------------------------
	//-----------------------------------------------------------

	function _VideoMask (options){
		if (!options) options = {};

		//-----------------------------------------------------------

		this.selector = options.selector || '#mask-editor';

		var e = $(this.selector);
		var epreview = vp(e, 'preview');

		//-----------------------------------------------------------

		this.e = {
			body: e,
			
			scrollContainer: vp(e, 'scroll-container'),

			image: vp(e, 'image'),
			canvas: vp(e, 'canvas'),

			preview: {
				body: epreview,
				image: vp(epreview, 'preview.image'),
				canvas: vp(epreview, 'preview.canvas'),
			}
		};

		this.eo = {
			preview: {
				body: eo(this.e.preview.body),
				image: eo(this.e.preview.image),
				canvas: eo(this.e.preview.canvas),
			},
		};

		//-----------------------------------------------------------

		this.buttons = e.find('[vp-btn]');

		//-----------------------------------------------------------
		
		this.origin = null;
		// this.origin = {
		// 	time: 103.953017,
		// 	width: 1920,
		// 	height: 1080,
		// 	source: 'sample-img/CHK.jpg',
		// };
		this.maskList = [];
		this.creationIndex = 0;

		//-----------------------------------------------------------

		this.invert = null;

		this.ratio = 1;
		this.selection = null;
		this.selectedItem = null;

		//-----------------------------------------------------------

		this.observers = [];

		//-----------------------------------------------------------

		this.initialize();
	}

	_VideoMask.prototype = (function(){
		return {
			constructor: _VideoMask,
		
			//-----------------------------------------------------------

			initialize: function(){
				displayView(this, 'modify', false);

				attachEvents(this);

				if (this.origin){
					this.set(this.origin);
				}
			},

			//-----------------------------------------------------------

			/**
			 * 옵저버 리스너를 등록합니다.
			 */
			observe: function(observer){ this.observers.push(observer); },
			/**
			 * 옵저버 리스너를 제거합니다.
			 */
			stopObserve: function(observer){
				var idx = this.observers.indexOf(observer);
				if (idx >= 0) this.observers.splice(idx, 1);
			},

			/**
			 * 등록된 옵저버에 Notify합니다.
			 * @param {String} topic 토픽
			 * @param {String} [value] 값
			 */
			notifyObservers: function(topic, value){
				var len = this.observers.length;
				for (var i=0; i < len; i++){
					var o = this.observers[i];

					if (typeof o == 'function')
						o(this, topic, value, this.shape);
					else if (typeof o.styleNotify == 'function')
						o.notifyPlayer(this, topic, value, this.shape);
				}
			},
			
			//-----------------------------------------------------------
			
			show: function(){
				this.e.body.show();
			},
			
			hide: function(){
				this.e.body.hide();
			},
			
			//-----------------------------------------------------------

			reset: function(){
				releaseMask(this);

				this.selectedItem = null;
				this.selection = null;

				this.e.canvas.empty();

				this.maskList.splice(0, this.maskList.length);
				this.creationIndex = 0;

				this.attrs = {};
			},

			set: function(origin){
				if (!validate(origin)) return;

				this.reset();

				this.origin = origin;
				changeImage(this, this.origin.source)
					.then(function(img){
						if (this.origin.mask){
							var startOffset = getCanvasStartOffset(this);
							var md = this.origin.mask;

							displayView(this, 'modify', true);

							var masks = md.masks;

							this.invert = md.invert;
							var einvert = gete(this.buttons, 'vp-btn', 'invert');
							if (einvert) einvert.prop('checked', this.invert ? true : false);

							if (this.invert) this.e.canvas.addClass('vp-invert');
							else this.e.canvas.removeClass('vp-invert');

							var len = masks.length;
							for (var i=0; i < len; i++){
								var m = masks[i];

								var box = createMask(this);
								box.css({
									left: m.x * this.ratio + startOffset.x,
									top: m.y * this.ratio + startOffset.y,
									width: m.width * this.ratio,
									height: m.height * this.ratio
								});
				
								box.addClass('vp-select');
				
								this.e.canvas.append(box);
								this.maskList.push(box);
				
								if (i + 1 == len){
									box.addClass('vp-select');
									this.selectedItem = box;
								}
							}
						}else{
							displayView(this, 'modify', false);
						}

						preparePreview(this);
						updatePreview(this);
					}.bind(this))
			},
			
			//-----------------------------------------------------------

			command: function(cmd, value){
				var origin = this.origin;
				var maskList = this.maskList;
				var numMaskList = maskList.length;
				var data = null;

				switch (cmd){
				case 'save':
					if (!origin) return;

					var masks = [];
					for (var i=0; i < numMaskList; i++){
						var mask = maskList[i];
						var rect = getRectByVideo(this, mask);

						masks.push(rect);
					}

					data = {
						time: origin.time,
						invert: this.invert,
						masks: masks,
					};

					this.notifyObservers('save', data);
					this.notifyObservers('end');
					break;

				case 'cancel':
					this.notifyObservers('end');
					break;

				case 'remove':
					this.notifyObservers('remove');
					break;

				case 'invert':
					if (this.invert){
						this.invert = false;
						this.e.canvas.removeClass('vp-invert');
					}else{
						this.invert = true;
						this.e.canvas.addClass('vp-invert');
					}

					updatePreview(this);
					break;
				}
			},
		};

		//-----------------------------------------------------------

		function validate(d){
			return d
				&& typeof d.source != 'undefined'
				&& typeof d.width != 'undefined'
				&& typeof d.height != 'undefined'
				&& typeof d.time != 'undefined';
		}

		//-----------------------------------------------------------

		function getRectByVideo(controller, e){
			var startOffset = getCanvasStartOffset(controller);
			var editRatio = controller.ratio;

			var pos = e.position();
			var posLeft = pos.left - startOffset.x;
			var posTop = pos.top - startOffset.y;
			
			var b = getBounds(e);

			return {
				x: posLeft / editRatio,
				y: posTop / editRatio,
				width: b.width / editRatio,
				height: b.height / editRatio,
			};
		}

		//-----------------------------------------------------------

		function displayView(controller, view, visible){
			var a = controller.e.body.find('[vp-view="'+view+'"]');
			if (visible) a.show();
			else a.hide();
		}

		//-----------------------------------------------------------

		function changeImage(controller, source){
			return new Promise(function (resolve, reject){
				var cimg = controller.e.image;

				var onLoad = function(event){
					cimg.unbind('load', onLoad);
					cimg.unbind('error', onError);

					var origin = controller.origin;
					var bounds = getBounds(cimg);
					var ratioX = bounds.width / origin.width;
					var ratioY = bounds.height / origin.height;

					controller.ratio = ratioX > ratioY ? ratioY : ratioX;

					setTimeout(resolve.bind(null, this), 0);
				};
				var onError = function(event){
					cimg.unbind('load', onLoad);
					cimg.unbind('error', onError);
	
					setTimeout(reject.bind(null, new Error('It is not an image file')), 0);
				};

				cimg.bind('load', onLoad);
				cimg.bind('error', onError);
				cimg.attr('src', source);
			});

			// return VpCommon.loadImage(source)
			// 	.then(function(img){
			// 		var cimg = controller.e.image;

			// 		cimg.attr('src', source);
			// 	})
			// 	.catch(function(exception){
			// 		console.log('<<<EXCEPTION>>>', exception);
			// 		alert('Exception: ' + exception);
			// 	});
		}

		//-----------------------------------------------------------

		function getCanvasStartOffset(controller){
			var eimg = controller.e.image;
			var ecanvas = controller.e.canvas;

			var b = getBounds(ecanvas);
			var bimg = getBounds(eimg);

			var r = {
				x: bimg.left - b.left,
				y: bimg.top - b.top,
			};
			
			//console.log('<<CANVAS::OFFSET>> ', r);
			
			return r;
		}

		function getCanvasOffset(controller, event){
			var se = controller.e.scrollContainer;
			var eimg = controller.e.image;
			var ecanvas = controller.e.canvas;

			var bimg = getBounds(eimg);

			var r = {
				x: event.pageX - bimg.left,
				y: event.pageY - bimg.top,
			};
			
			//console.log('<<CANVAS::OFFSET>> ', r, sc, b, bimg);
			
			return r;
		}

		//-----------------------------------------------------------

		function indexLabelOf(controller, e){
			var list = controller.maskList;

			var len = list.length;
			for (var i=0; i < len; i++){
				var be = list[i];

				if (has(be, e)){
					var sel = null;
					var eps = be.find('.vp-edit-point');
					var epsLen = eps.length;
					for (var j = 0; j < epsLen; j++){
						var ep = $(eps.get(j));

						if (has(ep, e)){
							sel = ep;
							break;
						}
					}

					return {
						index: i,
						editPoint: sel,
						label: be,
					};
				}
			}
			return null;
		}

		function hitTestTextEditor(x, y, e){
			if (e.attr('vp-mode') === 'text'){
				var editor = e.find('textarea');
				var b = getBounds(editor);

				if (hitTestRect(b.left, b.top, b.left + b.width, b.top + b.height, x, y)){
					return editor;
				}
			}
			return null;
		}

		function hitTestRect(x1, y1, x2, y2, x, y, dx, dy){
			if (arguments.length == 8)
				return ((((x1 <= x) && (dx2 <=x2)) && (y1 <= y)) && (dy2 <= y2));
			return ((((x1 <= x) && (x < x2)) && (y1 <= y)) && (y < y2));
		}

		//-----------------------------------------------------------

		function getRootScrollOffset(e){ var a = e.parents(); var len = a ? a.length : 0; if (len){ var p = a.get(len - 1); return { left: typeof p.scrollLeft == 'number' ? p.scrollLeft : 0, top: typeof p.scrollTop == 'number' ? p.scrollTop : 0, }; } return { left: 0, top: 0 }; }
		function getBounds(e, original){ var r = eo(e).getBoundingClientRect(); if (original) return r; var s = getRootScrollOffset(e); return { left: r.left + s.left, top: r.top + s.top, width: r.width, height: r.height, }; }

		function getRect(l, t, r, b){
			var tmp = -1;
			if (l > r){
				tmp = l; l = r; r = tmp;
			}
			if (t > b) {
				tmp = t; t = b; b = tmp;
			}

			var cx = r - l;
			var cy = b - t;

			var x = l;
			var y = t;
			var w = Math.abs(cx);
			var h = Math.abs(cy);

			if (cx < 0) x = x - w;
			if (cy < 0) y = y - h;

			return {
				left: x,
				top: y,
				width: w,
				height: h
			};
		}

		//-----------------------------------------------------------

		function attachEvents(controller){
//			var win = $(window);
//			win.on('resize', controller, function(event){
//				controller.beginResized();
//			});

			//-----------------------------------------------------------

			var doc = $(document);

			doc.on('mousemove', controller, function(event){
				var self = event.data;
				var e = $(event.target);

				if (event.which === 1){
					mouseLeftMove(self, event);
				}
			});

			doc.on('mousedown', controller, function(event){
				var self = event.data;
				var e = $(event.target);

				if (has(self.e.canvas, e)){
					//var button = event.which == 1 ? 'left' : event.which == 2 ? 'middle' : event.which == 3 ? 'right' : null;

					if (event.which === 1){
						mouseLeftDown(self, event);
					}
				}
			});

			doc.on('mouseup', controller, function(event){
				var self = event.data;
				var e = $(event.target);

				if (self.selection){
					var box = self.selection.target;

					//box.removeClass('vp-select');

					if (self.selectedItem && !equalsMask(self.selectedItem, box)){
						self.selectedItem.removeClass('vp-select');
						self.selectedItem = null;
					}

					self.selectedItem = box;

					if (self.selection.index < 0){
						self.maskList.push(box);
					}else{
					}

					updatePreview(self);
				}
				self.selection = null;
			});

			//-----------------------------------------------------------

			var canvas = controller.e.canvas;
			canvas.attr('tabindex', 0);

			canvas.on('keyup', controller, function(event){
				var self = event.data;

				if (event.which == 46){ // delete
					if (self.selectedItem){
						releaseMask(self);

						var box = self.selectedItem;

						box.removeClass('vp-select');

						if (self.selection && equalsMask(self.selection.target, self.selectedItem))
							self.selection = null;

						self.selectedItem = null;

						removeMask(self, box);

						updatePreview(self);
					}
				}
			});

			//-----------------------------------------------------------

			controller.buttons.on('click', controller, function(event){
				var self = event.data;
				var e = $(this);

				var cmd = e.attr('vp-btn');
				var value = e.attr('vp-value');
	
				controller.command(cmd, value);
			});
		}

		function equalsMask(a, b){
			return a && b && a.attr('vp-cid') === b.attr('vp-cid');
		}

		function removeMask(controller, e){
			var cid = e.attr('vp-cid');

			var len = controller.maskList.length;
			for (var i=0; i < len; i++){
				var le = controller.maskList[i];
				var lcid = le.attr('vp-cid');

				if (lcid === cid){
					controller.maskList.splice(i, 1);
					le.remove();
					break;
				}
			}

			if (controller.maskList.length === 0){
				controller.creationIndex = 0;
			}
		}

		function mouseLeftDown(controller, event){
			var self = controller;
			var e = $(event.target);

			var startOffset = getCanvasStartOffset(self);
			var offset = getCanvasOffset(self, event);

			var sel = indexLabelOf(self, e);
			if (sel){
				var textEditor = hitTestTextEditor(offset.x + startOffset.x, offset.y + startOffset.y, sel.label);
				
				//console.log('<<LABEL::FOUND>>> ', textEditor);

				if (!textEditor){
					sel.label.addClass('vp-select');

					var pos = sel.label.position();
					var posLeft = pos.left - startOffset.x;
					var posTop = pos.top - startOffset.y;
					
					var bounds = getBounds(sel.label);

					self.selection = {
						x: offset.x,
						y: offset.y,
						left: posLeft,
						top: posTop,
						right: posLeft + bounds.width,
						bottom: posTop + bounds.height,
						cx: offset.x - posLeft,
						cy: offset.y - posTop,
						index: sel.index,
						editPoint: sel.editPoint,
						target: sel.label,
					};
				}
			}else{
				var box = createMask(self);
				box.css({
					left: offset.x + startOffset.x,
					top: offset.y + startOffset.y,
				});

				box.addClass('vp-select');

				self.e.canvas.append(box);

				self.selection = {
					x: offset.x,
					y: offset.y,
					index: -1,
					target: box,
				};	
			}

			if (self.selection){
				selectMask(self, self.selection.target);
			}else{
				releaseMask(self);
			}
		}

		function mouseLeftMove(controller, event){
			var self = controller;

			if (self.selection){
				var startOffset = getCanvasStartOffset(self);
				var offset = getCanvasOffset(self, event);
				var box = self.selection.target;

				if (self.selection.index >= 0){
					if (self.selection.editPoint){
						var ep = self.selection.editPoint;
						var cmd = ep.attr('vp-cmd');

						var left = self.selection.left;
						var top = self.selection.top;
						var right = self.selection.right;
						var bottom = self.selection.bottom;

						var x = offset.x, y = offset.y;
						var rect = null;

						switch(cmd){
						case 'LT': rect = getRect(x, y, right, bottom); break;
						case 'CT': rect = getRect(left, y, right, bottom); break;
						case 'RT': rect = getRect(left, y, x, bottom); break;
						case 'LC': rect = getRect(x, top, right, bottom); break;
						case 'RC': rect = getRect(left, top, x, bottom); break;
						case 'LB': rect = getRect(x, top, right, y); break;
						case 'CB': rect = getRect(left, top, right, y); break;
						case 'RB': rect = getRect(left, top, x, y); break;
						}

						if (rect){
							box.css({
								left: rect.left + startOffset.x,
								top: rect.top + startOffset.y,
								width: rect.width,
								height: rect.height,
								lineHeight2: rect.height + 'px'
							});
						}

					}else{
						var cx = self.selection.cx;
						var cy = self.selection.cy;

						box.css({
							left: offset.x + startOffset.x - cx,
							top: offset.y + startOffset.y - cy,
						});	
					}
				}else{
					var x = self.selection.x;
					var y = self.selection.y;
					var cx = offset.x - x;
					var cy = offset.y - y;
					var w = Math.abs(cx);
					var h = Math.abs(cy);

					if (cx < 0) x = x - w;
					if (cy < 0) y = y - h;

					box.css({
						left: x + startOffset.x,
						top: y + startOffset.y,
						width: w,
						height: h,
						lineHeight2: h + 'px',
					});
				}

			}
		}

		function getMask(controller, cid){
			var len = controller.maskList.length;
			for (var i=0; i < len; i++){
				var e = controller.maskList[i];

				if (e.attr('vp-cid') === cid){
					return {
						index: i,
						e: e,
					};
				}
			}
			return null;
		}

		function selectMask(controller, e){
		}

		function releaseMask(controller){
		}

		function createMask(controller){
			var e = $('<div class="vp-mask-box"></div>');

			controller.creationIndex++;
			e.attr('vp-cid', controller.creationIndex);
			
			addEditPoint(e, 'LT');
			addEditPoint(e, 'CT');
			addEditPoint(e, 'RT');
			addEditPoint(e, 'LC');
			addEditPoint(e, 'RC');
			addEditPoint(e, 'LB');
			addEditPoint(e, 'CB');
			addEditPoint(e, 'RB');

			return e;
		}

		function addEditPoint(parent, cmd){
			var e = $('<div class="vp-edit-point vp-ep-'+cmd+'" vp-cmd="'+cmd+'"></div>');
			parent.append(e);

			return e;
		}

		function showEditPoints(e, visible){
			var eps = e.find('.vp-edit-point');
			var epsLen = eps.length;
			for (var j = 0; j < epsLen; j++){
				var ep = $(eps.get(j));

				if (visible){
					ep.show();
				}else{
					ep.hide();
				}
			}
		}

		function getRatio(srcWidth, srcHeight, limitWidth, limitHeight){
			var ratioX = limitWidth / srcWidth;
			var ratioY = limitHeight / srcHeight;

			return {
				ratio: ratioX > ratioY ? ratioY : ratioX,
				ratioX: ratioX,
				ratioY: ratioY
			};
		}

		function preparePreview(controller){
			var epreview = controller.e.preview.body;
			var eimg = controller.e.preview.image;

			var origin = controller.origin;
			if (!origin){
				epreview.addClass('vp-hide');
				eimg.removeAttr('src');
				return;
			}

			var ecanvas = controller.e.preview.canvas;
			var canvas = controller.eo.preview.canvas;
			var source = controller.origin.source;

			//-----------------------------------------------------------

			var bounds = getBounds(epreview.parent());

			//console.log('preview bounds=', bounds);

			var originWidth = origin.width;
			var originHeight = origin.height;

			var ratioX = bounds.width / originWidth;
			var ratioY = bounds.height / originHeight;
		
			var ratio = ratioX > ratioY ? ratioY : ratioX;

			var width = Math.round(originWidth * ratio);
			var height = Math.round(originHeight * ratio);

			//-----------------------------------------------------------

			eimg.css({
				width: width,
				height: height,
			});
			canvas.width = width;
			canvas.height = height;
			
			eimg.attr('vp-width', width);
			eimg.attr('src', source);

			//-----------------------------------------------------------

			ecanvas.attr('ratio', ratio);

			//-----------------------------------------------------------

			epreview.removeClass('vp-hide');
		}

		function updatePreview(controller){
			var origin = controller.origin;
			if (!origin) return;

			var ecanvas = controller.e.preview.canvas;
			var canvas = controller.eo.preview.canvas;
			
			var startOffset = getCanvasStartOffset(controller);

			//-----------------------------------------------------------

			var ratio = parseFloat(ecanvas.attr('ratio'));

			//-----------------------------------------------------------

			var ctx = canvas.getContext('2d');

			//-----------------------------------------------------------

			//console.log('<<CTX>>>', ctx);

			//-----------------------------------------------------------

			ctx.fillStyle = 'rgba(0,0,0,1)';
			ctx.strokeStyle = 'transparent';
			ctx.lineWidth = 0;
			ctx.mozFillRule = 'evenodd';
			ctx.msFillRule = 'evenodd';
			ctx.clearRect(0, 0, canvas.width, canvas.height);

			ctx.beginPath();

			if (controller.invert)
				ctx.rect(0, 0, canvas.width, canvas.height);

			var editRatio = controller.ratio;
			var len = controller.maskList.length;
			for (var i=0; i < len; i++){
				var mask = controller.maskList[i];

				var pos = mask.position();
				var posLeft = pos.left - startOffset.x;
				var posTop = pos.top - startOffset.y;
				
				var b = getBounds(mask);

				var rect = {
					x: posLeft / editRatio,
					y: posTop / editRatio,
					width: b.width / editRatio,
					height: b.height / editRatio,
				};

				rect.x *= ratio;
				rect.y *= ratio;
				rect.width *= ratio;
				rect.height *= ratio;

				ctx.rect(rect.x, rect.y, rect.width, rect.height);
			}

			ctx.fill('evenodd');
			ctx.closePath();
		}

		function drawRect(ctx, x, y, w, h){
			ctx.moveTo(x, y);
			ctx.lineTo(x + w, y);
			ctx.lineTo(x + w, y + h);
			ctx.lineTo(x, y + h);
			ctx.lineTo(x, y);
		}

		function drawRectReverse(ctx, x, y, w, h){
			ctx.moveTo(x, y);
			ctx.lineTo(x, y + h);
			ctx.lineTo(x + w, y + h);
			ctx.lineTo(x + w, y);
			//ctx.lineTo(x, y);
		}

	})();

	//-----------------------------------------------------------
	//-----------------------------------------------------------
	//-----------------------------------------------------------

	return _VideoMask;
})();