var VideoImageRegister = (function(){
	function defined(a, b){ return typeof a != 'undefined' ? a : b; }
	function definedBoolean(a, b){ return typeof a == 'boolean' ? a : b; }
	function definedNumber(a, b){ return typeof a == 'number' ? a : b; }
	function definedFunction(a, b){ return typeof a == 'function' ? a : b; }

	function vp(e, name) { return e.find('[vp-name="' + name + '"]'); }
	function vpc(e, name) { return e.children('[vp-name="' + name + '"]'); }
	function tf(value) { return value < 10 ? '0' + value : value; }

	function eo(e) { return e.length ? e.get(0) : null; }
	function is(e, te) { return eo(e) == eo(te); }
	function has(e, te) {
		if (is(e, te)) return true;
		return e.has(te).length > 0;
	}

	function getTemplates(controller, name){
		var e = controller.e.templates;

		var te = e.children('[vp-template="'+name+'"]');
		if (te.length)
			return te.children(':eq(0)').clone();
		return null;
	}

	function hasAttr(e, name){
		if (e.length){
			return e.get(0).hasAttribute(name);
		}
		return false;
	}

	function getArgs(e, prefix){
		if (e.length){
			var args = {}, numArgs = 0;
			var attrs = e.get(0).attributes;

			var valuePrefix = 'val-' + prefix;

			for (var i=0; i < attrs.length; i++){
				var attr = attrs[i];

				if (attr.name){
					if (attr.name.indexOf(valuePrefix) == 0){
						var ine = $(attr.value);
						if (ine.length){
							args[attr.name.substr(valuePrefix.length)] = ine.val();
							numArgs++;
						}
					}else if (attr.name.indexOf(prefix) == 0){
						args[attr.name.substr(prefix.length)] = attr.value;
						numArgs++;
					}
				}
			}
			if (numArgs)
				return args;
		}
		return null;
	}

	function uuid(){
		// http://www.ietf.org/rfc/rfc4122.txt
		var s = [];
		var hexDigits = '0123456789abcde';
		for (var i = 0; i < 36; i++) {
			s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
		}
		s[14] = '4';  // bits 12-15 of the time_hi_and_version field to 0010
		s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);  // bits 6-7 of the clock_seq_hi_and_reserved to 01
		s[8] = s[13] = s[18] = s[23] = '-';

		var uuid = s.join('');
		return uuid;
	}

	//-----------------------------------------------------------
	//-----------------------------------------------------------
	//-----------------------------------------------------------

	function _VideoImageRegister (options){
		if (!options) options = {};

		var labelDisplayTimeRangeOptions = options.labelDisplayTimeRange || {};

		this.selector = options.selector || '#vie';

		var e = $(this.selector);
		var evolume = vp(e, 'volume');
		var eduration = vp(e, 'duration');
		var eprogress = vp(e, 'progress');
		var ebuttons = vp(e, 'buttons');

		var edraws = vp(e, 'draws');
		var elabelCanvas = $('<canvas></canvas>');
		var emaskCanvas = $('<canvas></canvas>');

		edraws.append(elabelCanvas);
		edraws.append(emaskCanvas);

		this.e = {
			player: e,
			image: e.find('.vp-video').find('img[vp-name="image"]'),
			draws: {
				body: edraws,
				labelCanvas: elabelCanvas,
				maskCanvas: emaskCanvas,
			},
			panel: vp(e, 'panel'),
			panelPadding: vp(e, 'panel-padding'),
			snapshot: vp(e, 'snapshot'),
			buttons: ebuttons,
			external: vp(e, 'external'),
			templates: vp(e, 'templates'),
		};

		this.eo = {
			image: eo(this.e.image),
			draws: {
				body: eo(this.e.draws.body),
				labelCanvas: eo(this.e.draws.labelCanvas),
				maskCanvas: eo(this.e.draws.maskCanvas),
			},
		};

		//-----------------------------------------------------------
		// button

		this.buttons = ebuttons.children('[vp-btn]');
		
		this.buttons.click(this, function (event){
			var e = $(this);
			var self = event.data;

			if (!self.canPlay)
				return;

			var cmd = e.attr('vp-btn');
			var value = e.attr('vp-value');

			self.command(cmd, value);
		});

		//-----------------------------------------------------------
		// Notify button

		this.notifyButtons = ebuttons.find('[vp-notify]');
		
		this.notifyButtons.click(this, function (event){
			var e = $(this);
			var self = event.data;

			if (!self.canPlay)
				return;

			var cmd = e.attr('vp-notify');
			var value = null;
			if (hasAttr(e, 'vp-value')){
				value = e.attr('vp-value');
			}else{
				value = getArgs(e, 'arg-');
			}

			self.notifyObservers('u:' + cmd, value);
		});

		//-----------------------------------------------------------
		// Flags

		this.canPlay = false;
		this.isPlaying = false;
	
		//-----------------------------------------------------------

		this.labelDisplayTimeRange = {
			enabled: definedBoolean(labelDisplayTimeRangeOptions.enabled, true),
			// start: definedNumber(labelDisplayTimeRangeOptions.start, -0.2),
			// end: definedNumber(labelDisplayTimeRangeOptions.end, +0.2),
		};
	
		//-----------------------------------------------------------

		this.viewport = {
			ratio: 1,

			left: 0,
			top: 0,
			width: 0,
			height: 0,

			imageLeft: 0,
			imageTop: 0,
			imageWidth: 0,
			imageHeight: 0,

			resizing: false,
		};
	
		//-----------------------------------------------------------

		this.timeRes = {
			playChanged: null,
			beginResized: null,
		};

		this.templates = {
			hoverSnapshot: getTemplates(this, 'snapshot'),
			snapshot: getTemplates(this, 'snapshot-button'),
		};

		this.data = {
			screen: {
				userRegisted: false,
				width: 0,
				height: 0,
				source: null,
			},
				
			hoverSnapshot: null,
			mask: null,

			snapshot: null,
		};
		
		//-----------------------------------------------------------

		this.observers = [];

		//-----------------------------------------------------------

		this.initialize();
	}

	_VideoImageRegister.prototype = (function(){
		return {
			constructor: _VideoImageRegister,
		
			//-----------------------------------------------------------

			THUMB_WIDTH: 120,
			THUMB_HEIGHT: 120,
		
			//-----------------------------------------------------------

			initialize: function(){
				attachImageEvents(this);
				attachPlayerEvents(this);
			},

			//-----------------------------------------------------------

			/**
			 * 옵저버 리스너를 등록합니다.
			 */
			observe: function(observer){ this.observers.push(observer); },
			/**
			 * 옵저버 리스너를 제거합니다.
			 */
			stopObserve: function(observer){
				var idx = this.observers.indexOf(observer);
				if (idx >= 0) this.observers.splice(idx, 1);
			},

			/**
			 * 등록된 옵저버에 Notify합니다.
			 * @param {String} topic 토픽
			 * @param {String} [value] 값
			 */
			notifyObservers: function(topic, value){
				var len = this.observers.length;
				for (var i=0; i < len; i++){
					var o = this.observers[i];

					if (typeof o == 'function')
						o(this, topic, value, this.shape);
					else if (typeof o.styleNotify == 'function')
						o.notifyPlayer(this, topic, value, this.shape);
				}
			},
		
			//-----------------------------------------------------------

			imageEvent: function (event){
				//console.log(event.type, event);

				this.notifyObservers('event:fired', event);
			},
			
			//-----------------------------------------------------------

			showExternal: function(visible, name){
//				if (visible){
//					if (name){
//						showExternal(this, name);
//					}
//				}else{
//					hideExternal(this);
//				}
			},
			
			//-----------------------------------------------------------
			
			clear: function(){
				this.data.screen.userRegisted = false;
				this.data.screen.width = 0;
				this.data.screen.height = 0;
				this.data.screen.source = null;
				
				this.data.hoverSnapshot = null;
				this.data.mask = null;
				
				this.data.snapshot = null;
			},
	
			//-----------------------------------------------------------

			open: function (src, mime){
				setSource(this, src, mime);
			},
			
			close: function(){
				removeSource(this);
			},
			
			//-----------------------------------------------------------
			
			show: function (){
				this.e.player.css({
					visibility: 'visible',
					zIndex: '',
					position: 'relative',
					top: '',
				});
			},
			
			hide: function(){
				this.e.player.css({
					visibility: 'hidden',
					zIndex: -1,
					position: 'absolute',
					top: -10000000,
				});
			},
		
			//-----------------------------------------------------------

			command: function (cmd, value){
				var image = this.eo.image;
				var returnValue = null;
				var ts = null;

				if (this.viewport.resizing){
					//console.log('<<<RESIZED>>> COMMAND');

					this.resized();
				}

				switch(cmd){
				case 'capture':
					returnValue = captureWithLabel(this);
					break;

				case 'capture:mask':
					returnValue = captureForMask(this);
					break;
					
				case 'get:label':
					returnValue = getLabel(this);
					break;

				case 'remove:label':
					if (this.data.snapshot){
						var d = this.data.snapshot;
						returnValue = d;

						this.data.snapshot = null;
						drawLabels(this);
					}else{
						returnValue = null;
					}
					
					break;

				case 'remove:mask':
					this.data.mask = null;
					drawMasks(this);
					break;

				case 'screen':
					returnValue = drawScreen(this);
					break;
				}
				return returnValue;
			},
		
			//-----------------------------------------------------------

			mask: function(data){

				if (this.viewport.resizing){
					//console.log('<<<RESIZED>>> COMMAND');

					this.resized();
				}

				this.data.mask = data;

				drawMasks(this);
			},

			label: function(data, display, duration){
				if (typeof display != 'boolean') display = true;

				if (this.viewport.resizing){
					//console.log('<<<RESIZED>>> COMMAND');

					this.resized();
				}

				this.data.snapshot = data;
				drawLabels(this);
			},
			
			//-----------------------------------------------------------
			
			refresh: function(){
				drawLabels(this);
				drawMasks(this);
			},
		
			//-----------------------------------------------------------
			
			prepared: function(){
				if (this.data.screen.userRegisted)
					return;
				
				var d = capture(this);

				if (d){
					this.data.screen.width = d.width;
					this.data.screen.height = d.height;
					this.data.screen.source = d.source;
				}else{
					this.data.screen.width = 0;
					this.data.screen.height = 0;
					this.data.screen.source = null;
				}
			},
	
			//-----------------------------------------------------------

			canPlayChanged: function(){
				if (this.canPlay){
					this.buttons.removeClass('vp-disabled');
				}else{
					this.buttons.addClass('vp-disabled');
				}
			},
	
			//-----------------------------------------------------------

			showPanel: function(visible){
				if (visible)
					this.e.panel.removeClass('vp-hide');
				else
					this.e.panel.addClass('vp-hide');
			},
			
			//-----------------------------------------------------------

			beginResized: function(){
				if (this.timeRes.beginResized)
					clearTimeout(this.timeRes.beginResized);

				this.viewport.resizing = true;

				var bounds = getBounds(this.e.image.parent());

				//console.log('RESIZING, BOUNDS=', bounds);

				var maskCanvas = this.eo.draws.maskCanvas;

				var width = maskCanvas.width;
				var height = maskCanvas.height;

				var ratioX = bounds.width / width;
				var ratioY = bounds.height / height;

				var ratio = ratioX > ratioY ? ratioY : ratioX;

				VpCommon.cssScale(this.e.draws.labelCanvas, ratio, ratio);
				VpCommon.cssScale(this.e.draws.maskCanvas, ratio, ratio);

				this.timeRes.beginResized = setTimeout(function(){
					if (this.viewport.resizing){
						//console.log('<<<RESIZED>>> WINDOW');

						this.resized();
					}
				}.bind(this), 1000);
			},

			resized: function(){
				this.viewport.resizing = false;

				var image = this.eo.image;
				var labelCanvas = this.eo.draws.labelCanvas;
				var maskCanvas = this.eo.draws.maskCanvas;

				var srcWidth = image.naturalWidth;
				var srcHeight = image.naturalHeight;

				try
				{
					var bounds = getBounds(this.e.image.parent());

					console.log('RESIZED, BOUNDS=', bounds);

					labelCanvas.width = bounds.width;
					labelCanvas.height = bounds.height;

					maskCanvas.width = bounds.width;
					maskCanvas.height = bounds.height;

					this.viewport.left = bounds.left;
					this.viewport.top = bounds.top;
					this.viewport.width = bounds.width;
					this.viewport.height = bounds.height;
					
					this.viewport.imageWidth = srcWidth;
					this.viewport.imageHeight = srcHeight;

					this.viewport.ratio = getRatio(
						srcWidth, srcHeight,
						bounds.width, bounds.height).ratio;
					
					var bwidth = bounds.width;
					var bheight = bounds.height;
					
					var vwidth = srcWidth * this.viewport.ratio;
					var vheight = srcHeight * this.viewport.ratio;

					this.viewport.imageLeft = (bwidth - vwidth) * 0.5;
					this.viewport.imageTop = (bheight - vheight) * 0.5;
					
					VpCommon.removeCssScale(this.e.draws.labelCanvas);
					VpCommon.removeCssScale(this.e.draws.maskCanvas);

					console.log('RESIZED, VIEWPORT=', this.viewport, bwidth, vwidth);
	
					updateDraws(this);
				}
				catch(e)
				{
					throw e;
				}
			},
			
			//-----------------------------------------------------------

		};

		//-----------------------------------------------------------
		//-----------------------------------------------------------
		//-----------------------------------------------------------

		function getRootScrollOffset(e){ var a = e.parents(); var len = a ? a.length : 0; if (len){ var p = a.get(len - 1); return { left: typeof p.scrollLeft == 'number' ? p.scrollLeft : 0, top: typeof p.scrollTop == 'number' ? p.scrollTop : 0, }; } return { left: 0, top: 0 }; }
		function getBounds(e, original){ var r = eo(e).getBoundingClientRect(); if (original) return r; var s = getRootScrollOffset(e); return { left: r.left + s.left, top: r.top + s.top, width: r.width, height: r.height, }; }

		function getRatio(srcWidth, srcHeight, limitWidth, limitHeight){
			var ratioX = limitWidth / srcWidth;
			var ratioY = limitHeight / srcHeight;

			return {
				ratio: ratioX > ratioY ? ratioY : ratioX,
				ratioX: ratioX,
				ratioY: ratioY
			};
		}
		
		//-----------------------------------------------------------

		function getElementOffset(controller, name, subname, event){
			var pe = controller.e[name];
			var e = subname ? pe[subname] : e;

			var b = getBounds(e);

			return {
				x: event.pageX - b.left,
				y: event.pageY - b.top,
				width: b.width,
				height: b.height
			};
		}

		//-----------------------------------------------------------

		function updateDraws(controller){
			drawLabels(controller);
			drawMasks(controller);
		}

		function drawLabels(controller){
			var image = controller.e.image;
			var canvas = controller.eo.draws.labelCanvas;
			var viewport = controller.viewport;

			var current = controller.data.snapshot;

			//if (!controller.canPlay) return;

			//-----------------------------------------------------------

			var ratio = viewport.ratio;

			//-----------------------------------------------------------

			var ctx = canvas.getContext('2d');

			//-----------------------------------------------------------

			ctx.clearRect(0, 0, canvas.width, canvas.height);

			//-----------------------------------------------------------

			if (current){
				var data = current;
				
				VideoDrawer.labels(ctx, data.labels, {
					ratio: ratio,
					left: viewport.imageLeft,
					top: viewport.imageTop,
				});
			}
		}

		function drawMasks(controller){
			var image = controller.e.image;
			var canvas = controller.eo.draws.maskCanvas;
			var viewport = controller.viewport;
			var maskData = controller.data.mask;

			//if (!controller.canPlay) return;

			//-----------------------------------------------------------

			var ratio = viewport.ratio;

			//-----------------------------------------------------------

			var ctx = canvas.getContext('2d');

			//-----------------------------------------------------------

			ctx.clearRect(0, 0, canvas.width, canvas.height);

			if (!maskData)
				return;

			ctx.fillStyle = 'rgba(0,0,0,1)';
			ctx.strokeStyle = 'transparent';
			ctx.lineWidth = 0;
			ctx.mozFillRule = 'evenodd';
			ctx.msFillRule = 'evenodd';
	
			ctx.beginPath();

			if (maskData.invert) ctx.rect(0, 0, canvas.width, canvas.height);

			var len = maskData.masks.length;
			for (var i=0; i < len; i++){
				var mask = maskData.masks[i];

				ctx.rect(
					mask.x * ratio + viewport.imageLeft,
					mask.y * ratio + viewport.imageTop,
					mask.width * ratio,
					mask.height * ratio
				);
			}

			ctx.fill('evenodd');
			ctx.closePath();

		}

		//-----------------------------------------------------------

		function drawScreen(controller){
			var viewport = controller.viewport;
			var image = controller.eo.image;
			var maskData = controller.data.mask;
			var snapshotData = controller.data.snapshot;

			//var left = viewport.imageLeft;
			//var top = viewport.imageTop;
			
			var left = 0, top = 0;
			
			var w = image.naturalWidth;
			var h = image.naturalHeight;

			var canvas = $('<canvas width="'+w+'" height="'+h+'"/>');
			var ctx = canvas.get(0).getContext('2d');

			ctx.drawImage(image, 0, 0, w, h);

			//-----------------------------------------------------------

			if (maskData){
				ctx.fillStyle = 'rgba(0,0,0,1)';
				ctx.strokeStyle = 'transparent';
				ctx.lineWidth = 0;
				ctx.mozFillRule = 'evenodd';
				ctx.msFillRule = 'evenodd';
		
				ctx.beginPath();
	
				if (maskData.invert) ctx.rect(0, 0, ctx.canvas.width, ctx.canvas.height);
	
				var len = maskData.masks.length;
				for (var i=0; i < len; i++){
					var mask = maskData.masks[i];
	
					ctx.rect(
						mask.x + left,
						mask.y + top,
						mask.width,
						mask.height
					);
				}
	
				ctx.fill('evenodd');
				ctx.closePath();
			}

			//-----------------------------------------------------------
				
			if (snapshotData){
				VideoDrawer.labels(ctx, snapshotData.labels, {
					ratio: 1,
					left: left,
					top: top,
				});
			}

			var src = ctx.canvas.toDataURL('image/jpeg');

			return src;
		}
	
		function showButton(e, name, visible){
			visible = definedBoolean(visible, true);

			var ebtn = e.children('[vp-btn="'+name+'"]');
			if (visible) ebtn.removeClass('vp-hide');
			else ebtn.addClass('vp-hide');
		}

		function setSource(controller, src, mime){
			controller.canPlay = false;

			var e = controller.e.image;

			e.attr('src', src);
			if (mime)
				e.attr('mime-type', mime);
		}
		
		function removeSource(controller){
			var e = controller.e.image;

			e.attr('src', '');

			controller.canPlay = false;
			controller.canPlayChanged();

			controller.prepared();
		}

		function method(e, cmd){
			try
			{
				var element = e && e.length ? e.get(0) : null;
				if (element){
					var a = [];
					for (var i=2; i < arguments.length; i++) a.push(arguments[i]);

					switch(cmd){
					case 'load': return element.load.apply(element, a);
					case 'play': return element.play.apply(element, a);
					case 'pause': return element.pause.apply(element, a);

					case 'addTextTrack': return element.addTextTrack.apply(element, a);
					case 'captureStream': return element.captureStream.apply(element, a);
					case 'canPlayType': return element.canPlayType.apply(element, a);

					case 'mozCaptureStream': return element.mozCaptureStream.apply(element, a);
					case 'mozCaptureStreamUntilEnded': return element.mozCaptureStreamUntilEnded.apply(element, a);
					case 'mozGetMetadata': return element.mozGetMetadata.apply(element, a);

					case 'fastSeek': return element.fastSeek.apply(element, a);
					case 'seekToNextFrame': return element.seekToNextFrame.apply(element, a);
					case 'setMediaKeys': return element.setMediaKeys.apply(element, a);
					case 'setSinkId': return element.setSinkId.apply(element, a);
					}
				}
				return null;
			}
			catch(exception)
			{	
				return null;
			}
		}

		function attachImageEvents(contoller){
			var eImage = contoller.e.image;

			eImage.on('load', contoller, function (event){
				var self = event.data;
				self.imageEvent(event);

				self.canPlay = true;
				self.canPlayChanged();
				
				self.prepared();
				self.resized();
			});
	
			eImage.on('error', contoller, function (event){
				var self = event.data;
				self.imageEvent(event);

				self.canPlay = false;
				self.canPlayChanged();
			});
		}
		
		//-----------------------------------------------------------

		function attachPlayerEvents(controller){
			var win = $(window);
			win.on('resize', controller, function(event){
				controller.beginResized();
			});

		}

		//-----------------------------------------------------------
		//-----------------------------------------------------------
		//-----------------------------------------------------------

		function captureStream(controller){
			var image = controller.eo.image;

			if (typeof image.mozCaptureStream == 'function'){
				return image.mozCaptureStream();
			}else{
				return image.captureStream();
			}
		}

		function capture(controller){
			var eImage = controller.e.image;
			var image = controller.eo.image;

			var w = image.naturalWidth;
			var h = image.naturalHeight;

			return {
				time: 0,
				width: w,
				height: h,
				source: eImage.attr('src'),
			};
		}

		function captureWithLabel(controller){
			var eImage = controller.e.image;
			var image = controller.eo.image;

			var data = controller.data.snapshot;
			var src = eImage.attr('src');

			var w = image.naturalWidth;
			var h = image.naturalHeight;

			return {
				time: 0,
				width: w,
				height: h,
				data: data,
				source: src,
			};
		}
		
		function getLabel(controller){
			var data = controller.data.snapshot;
			if (!data)
				return null;
			
			return {
				time: 0,
				width: data.image.width,
				height: data.image.height,
				data: data,
				source: data.image.source,
			};
		}

		function captureForMask(controller){
			var d = capture(controller);
			if (controller.data.mask)
				d['mask'] = controller.data.mask;
			return d;
		}

		function showExternal(controller, name){
			var image = controller.e.image;
			var ext = controller.e.external;

			var sub = vp(ext, name);
			if (sub.length){
				image.addClass('vp-hide');

				var showName = ext.attr('vp-show-item');

				if (showName !== name){
					var old = vp(ext, showName);
					old.addClass('vp-hide');
				}

				ext.attr('vp-show-item', name);

				sub.removeClass('vp-hide');

				if (ext.hasClass('vp-hide'))
					ext.removeClass('vp-hide');
			}
		}

		function hideExternal(controller){
			var image = controller.e.image;
			var ext = controller.e.external;

			var showName = ext.attr('vp-show-item');

			if (showName){
				var old = vp(ext, showName);
				old.addClass('vp-hide');
			}

			ext.addClass('vp-hide');

			image.removeClass('vp-hide');
		}

		//-----------------------------------------------------------
	
	})();

	//-----------------------------------------------------------
	//-----------------------------------------------------------
	//-----------------------------------------------------------

	return _VideoImageRegister;
})();