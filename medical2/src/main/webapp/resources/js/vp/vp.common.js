
/**
 * 작업 진행 콜백 함수
 * @callback ArrayProcCallback
 * @param {Number} cnt 현재 위치
 * @param {Number} siz 전체 수량
 */



/**
 * 공용 도구 모음
 * @namespace
 */
var VpCommon = {
	ieVersion: function(){
		var userAgent = navigator.userAgent.toLowerCase();
	
		var word = null;
		
		if (navigator.appName == 'Microsoft Internet Explorer') word = 'msie'; // IE old version (IE10 or Lower)
		else if (userAgent.search('trident') > -1) word = 'trident/.*rv:'; // IE11
		else if (userAgent.search('edge/') > -1 ) word = 'edge/'; // Microsoft Edge
		else return -1;
	
		var reg = new RegExp(word +'([0-9]{1,})(\\.{0,}[0-9]{0,1})');
		if (reg.exec(userAgent) != null) return parseFloat( RegExp.$1 + RegExp.$2 );
		return -1;
	},
	
	createImage: function(){
		var img = new Image();
		
		// IE에서는 로컬 이미지를 로드하면 SecurityError가 발생한다.
		// 하지만, 맥용 사파리에서는 crossOrigin을 세팅하면 CORS 오류가 발생한다.
		// 따라서, IE인 경우만 crossOrigin을 세팅한다.
		if (VpCommon.ieVersion() != -1)
			img.crossOrigin = 'Anonymous';
		
		return img;
	},

	/**
	 * 값이 DATA URL 인지 확인합니다.
	 * @memberof VpCommon
	 * @param {String} s 값
	 * @return {Boolean} DATA URL이면 true
	 */
	isDataUrl: function (s){
		return s && typeof s == 'string' && s.indexOf('data:') == 0;
	},
	
	loadImage: function(url){
		return new Promise(function(resolve, reject){
			var img = VpCommon.createImage();
			/**
			 * @param {Event} e HTML Event
			 */
			img.onload = function(e){
				setTimeout(resolve.bind(null, this), 0);
			};
			/**
			 * @param {Event} e HTML Event
			 */
			img.onerror = function(e){
				setTimeout(reject.bind(null, new Error('It is not an image file')), 0);
			};
			img.src = url;
		});
	},
	
	/**
	 * 이미지를 DATA URL 형태로 불러옵니다.
	 * @memberof VpCommon
	 * @param {String} url 이미지 URL
	 * @return {Promise} Promise 객체
	 */
	loadImageAsDataURL: function (url){
		return new Promise(function(resolve, reject){
			if (VpCommon.isDataUrl(url)){
				resolve(url);
			}else{
				var fileReader = new FileReader();
				fileReader.onload = function(e){
					resolve(e.target.result);
				};
				
				var xhr = VpCommon.xmlHttpRequest({
					path: url,
					responseType: 'blob',
					
					load: function (e, xhr){
						fileReader.readAsDataURL(xhr.response);
					},
					
					error: function (e, xhr){
						reject(e);
					},
				});
				
				xhr.send();
			}
		});
	},
	
	dataUrlToFile: function(url, filename, usrMimeType){
		var dataIdx = url.indexOf('data:');
		if (url.indexOf('data:') == 0){
			var headStartIdx = url.indexOf(':');
			var headIdx = url.indexOf(',');
			var head = url.substr(headStartIdx + 1, (headIdx - headStartIdx) - 1);
			
			var sepIdx = head.indexOf(';');
			
			var mimeType = head.substr(0, sepIdx);
			var encode = head.substr(sepIdx + 1);
			
			var b = atob(url.substr(headIdx + 1));
			var len = b.length;
		
			var ua = new Uint8Array(len);
			while (len--) ua[len] = b.charCodeAt(len);
			
			var fileInstance = null;
			var fileMimeType = typeof usrMimeType == 'string' ? usrMimeType : mimeType;
			
			try
			{
				fileInstance = new File([ua], filename, { type: fileMimeType });
			}
			catch(ex)
			{
				fileInstance = new Blob([ua], { type: fileMimeType });
				fileInstance['lastModifiedDate'] = new Date();
				fileInstance['name'] = filename;
			}
			
			return fileInstance;
		}else{
			return null;
		}
	},

	//-----------------------------------------------------------

	cssScale: function(e, x, y){
		var scaleValue = 'scale(' + x + ', ' + y + ')';

		e.css({
			'transform-origin': 'top left',
			'-moz-transform': scaleValue,
			'-ms-transform': scaleValue,
			'-webkit-transform': scaleValue,
			transform: scaleValue,
		});
	},

	removeCssScale: function(e){
		e.css({
			'transform-origin': '',
			'-moz-transform': '',
			'-ms-transform': '',
			'-webkit-transform': '',
			transform: '',
		});
	},
	
	REGEX_NUMBER: /^[+-]{0,1}(\d+\.\d+|\d+\.|\.\d+|\d+)$/g,
	
	isNumber: function (s){
		if (typeof s == 'number')
			return true;
		
		if (typeof s != 'string')
			s = '' + s;
		
		if (isNaN(s))
			return false;
		
		this.REGEX_NUMBER.lastIndex = 0;
		var m = this.REGEX_NUMBER.exec(s);
		if (!m)
			return false;
		
		return true;
	},
	
	filePath: {
	    join : function () {
	        return Array.prototype.join.call(arguments,'/')
	    },
	    
	    dirname : function (x) {
	        var arr = x.split('/')
	        arr.length = arr.length - 1
	        return arr.join('/')
	    },
	    
	    basename : function (x) {
	        var arr = x.split('/')
	        var y = arr[arr.length-1].split('.')
	        if (y.length>1) {
	            y.length = y.length - 1
	        }
	        return y.join('.')
	    },
	},
	
	times: {
		toTimeSpan: function(duration){
			if (isNaN(duration))
				return null;

			duration = parseFloat(duration).toFixed(0);

			var h = duration / 60 / 60;
			var m = (duration / 60) % 60;
			var s = duration % 60;

			h = Math.floor(h);
			m = Math.floor(m);
			s = Math.ceil(s);
			
			return {
				value: duration,
				hours: h,
				minutes: m,
				seconds: s,

				formatString: function(useHour){
					function tf(value) { return value < 10 ? '0' + value : value; }
					
					var fh = tf(this.hours);
					var fm = tf(this.minutes);
					var fs = tf(this.seconds);
			
					if (useHour === true || this.hours > 0)
						return fh + ':' + fm + ':' + fs;
					return fm + ':' + fs;		
				},
			};
		},

		toTimeSpanFormat: function(duration, useHour){
			if (isNaN(duration))
				return useHour === true ? '00:00:00' : '00:00';
			
			function tf(value) { return value < 10 ? '0' + value : value; }

			var t = this.toTimeSpan(duration);

			var fh = tf(t.hours);
			var fm = tf(t.minutes);
			var fs = tf(t.seconds);

			if (useHour === true || t.hours > 0)
				return fh + ':' + fm + ':' + fs;
			return fm + ':' + fs;
		},

		REG_TIMESPAN: /([-+]{0,1}[0-9]+)([sm]{0,1})/g,

		getTimeSpan: function(value){
			var unit = 's';
			var timeValue = 0;
			var time = 0;

			if (typeof value == 'number'){
				timeValue = value;
			}else if (typeof value == 'string'){
				this.REG_TIMESPAN.lastIndex = 0;
				var m = this.REG_TIMESPAN.exec(value);
				if (m){
					timeValue = parseInt(m[1]);
		
					if (m.length > 2)
						unit = m[2];
				}
			}else{

			}
			
			time = unit === 'm' ? timeValue * 60 : timeValue;

			return {
				value: timeValue,
				unit: unit,
				time: time,
				absTime: Math.abs(time),
			};
		}

	},
	
	promise: {
		/**
		 * Promise 객체 배열의 전체 원소들을 태스크 큐을 통해 수행합니다.
		 * @memberof VpCommon
		 * @param {Array} a Promise 객체 배열
		 * @param {ArrayProcCallback} [progress] 진행 중 호출되는 함수
		 * @param {(Number|Object)} [options] 옵션, 숫자로 값을 주면 resolve 수행 대기 시간을 설정하게 됩니다.
		 * @param {Object} [options.term] 대기시간 옵션
		 * @param {Object} [options.term.resolve=0] resolve 수행 대기 시간을 설정합니다.
		 * @param {Object} [options.term.progree=0] progress 함수 호출 대기 시간을 설정합니다.
		 * @return {Promise} Promise 객체
		 */
		all: function (a, progress, options){
			var resolveDelay = 0, progreeDelay = 0, promiseDelay = 0;
			if (typeof options == 'number'){
				resolveDelay = options;
			}else{
				if (!options) options = {};
				var termOptions = options.term || {};

				resolveDelay = termOptions.resolve || 0;
				progreeDelay = termOptions.progree || 0;
				promiseDelay = termOptions.promise || 0;
			}
			if (!$.isArray(a)) a = [a];

			return new Promise(function(resolve, reject){
				var siz = a.length;
				var cnt = 0;

				if (siz == 0){
					setTimeout(resolve.bind(null), resolveDelay);
					return;
				}

				for (var i=0; i < siz; i++){
					var func = function(){
						var promise = arguments.callee.promise;

						promise.then(function (){
							cnt++;
							if (cnt >= siz){
								if (progress) setTimeout(function (){ progress(cnt, siz) }, progreeDelay);
								setTimeout(resolve.bind(null), resolveDelay);
							}else{
								if (progress) setTimeout(function (){ progress(cnt, siz) }, progreeDelay);
							}
						}).catch(function (e){
							reject(e);
						})
					};
					func.promise = a[i];

					setTimeout(func, 0);
				}
			});
		},

		/**
		 * Promise 객체 배열의 전체 원소들을 순차적으로 수행합니다.
		 * @memberof VpCommon
		 * @param {Array} a Promise 객체 배열
		 * @param {ArrayProcCallback} [progress] 진행 중 호출되는 함수
		 * @param {(Number|Object)} [options] 옵션, 숫자로 값을 주면 resolve 수행 대기 시간을 설정하게 됩니다.
		 * @param {Object} [options.term] 대기시간 옵션
		 * @param {Object} [options.term.resolve=0] resolve 수행 대기 시간을 설정합니다.
		 * @param {Object} [options.term.progree=0] progress 함수 호출 대기 시간을 설정합니다.
		 * @return {Promise} Promise 객체
		 */
		sequenceAll: function (a, progress, options){
			var resolveDelay = 0, progreeDelay = 0, promiseDelay = 0, execDelay = 0;
			if (typeof options == 'number'){
				resolveDelay = options;
			}else{
				if (!options) options = {};
				var termOptions = options.term || {};

				resolveDelay = termOptions.resolve || 0;
				progreeDelay = termOptions.progree || 0;
				promiseDelay = termOptions.promise || 0;
				execDelay = termOptions.exec || 0;
			}
			if (!$.isArray(a)) a = [a];

			return new Promise(function(resolve, reject){
				var siz = a.length;
				var cnt = 0;

				if (siz == 0){
					setTimeout(resolve.bind(null), resolveDelay);
					return;
				}

				var exec = function(){
					var promise = a[cnt];

					promise.then(function (){
						cnt++;
						if (cnt >= siz){
							if (progress) setTimeout(function (){ progress(cnt, siz) }, progreeDelay);
							setTimeout(resolve.bind(null), resolveDelay);
						}else{
							if (progress) setTimeout(function (){ progress(cnt, siz) }, progreeDelay);

							setTimeout(exec, execDelay);
						}
					}).catch(function (e){
						reject(e);
					})
				};

				setTimeout(exec, execDelay);
			});
		},
	},
	
	/**
	 * XML HTTP Request(XHR) 객체를 생성합니다.
	 * @memberof VpCommon
	 * @param {Object} [options] 수행 옵션
	 * @param {Boolean} [options.async=true] 비동기 요청 여부
	 * @param {String} [options.type=GET] 전송 방식 (GET|HEAD|POST|PUT|DELETE|CONNECT|OPTIONS|TRACE|PATCH)
	 * @param {String} [options.path] 요청 URL
	 * @param {String} [options.responseType] 회신 받을 타입 (arraybuffer|blob|document|json|text)
	 * @param {XHRProgressCallback} [options.progress] 진행 중 호출되는 콜백 함수
	 * @param {XHRLoadCallback} [options.load] 로드 후 호출되는 콜백 함수
	 * @param {XHRErrorCallback} [options.error] 오류 시 호출되는 콜백 함수
	 * @return {XMLHttpRequest} XML HTTP Request(XHR) 객체
	 */
	xmlHttpRequest: function(options){
		if (!options) options = {};
		var xhr = new XMLHttpRequest();
		var asyncExec = typeof options.async == 'boolean' ? options.async : true;

		if (options.path)
			xhr.open(options.type || 'GET', options.path, asyncExec);

		// arraybuffer
		// blob
		// document
		// json
		// text
		if (typeof options.responseType == 'string')
			xhr.mozResponseType = xhr.responseType = options.responseType;

		if (options.progress && typeof options.progress == 'function')
			xhr.upload.onprogress = options.progress;

		if (options.load)
			xhr.onload = function(e){
				if (this.status == 200 || this.status === 0) {
					if (typeof options.load == 'function')
						options.load.call(options, e, xhr);
				}else{
					if (typeof options.error == 'function')
						options.error.call(options, new Error('Could not load (' + this.status + ') ' + e), xhr);
				}
			};

		if (options.error)
			xhr.onerror = function(e){
				if (typeof options.error == 'function')
					options.error.call(options, e, xhr);
			};

		return xhr;
	},

	/**
	 * XML HTTP Request(XHR) 객체에서 arrayBuffer 데이터를 가져옵니다.
	 * @memberof VpCommon
	 * @param {XMLHttpRequest} xhr XML HTTP Request(XHR) 객체
	 * @return {arrayBuffer} arrayBuffer 데이터
	 */
	xhrArrayBufferResponse: function (xhr){
		return xhr.mozResponseArrayBuffer || xhr.mozResponse || xhr.responseArrayBuffer || xhr.response;
	},
	
	//-------------------------------------------------------------------------------------------------------
	// Ajax
	//-------------------------------------------------------------------------------------------------------

	/**
	 * Ajax을 호출합니다.
	 * @memberof VpCommon
	 * @see {@link http://api.jquery.com/jquery.ajax/} jQuery.ajax() | jQuery API Documentation
	 * @param {Object} options 옵션
	 * @param {Object} options.url 요청 URL
	 * @param {Object} [options.caller] 콜백 함수의 this 지정자
	 * @param {(String|Number|Boolean|Object|Array)} [options.data] 요청 URL로 전달할 값
	 * @param {String} [options.type=POST] 전송 방식 (GET|HEAD|POST|PUT|DELETE|CONNECT|OPTIONS|TRACE|PATCH)
	 * @param {String} [options.dataType=json] 회신 데이터 유형 (xml|html|script|json|jsonp|text)
	 * @param {Number} [options.timeout=1000000] 요청 시간제한
	 * @param {Boolean} [options.logAll=false] 모든 로그를 콘솔에 남길 지 여부
	 * @param {Boolean} [options.logSuccess=false] 회신 성공한 로그를 콘솔에 남길 지 여부
	 * @param {Boolean} [options.logFail=true] 오류 또는 회신 실패한 로그를 콘솔에 남길 지 여부
	 * @param {Boolean} [options.async=true] 비동기 요청 여부
	 * @param {Boolean} [options.useResponseType=false] 요청 URL에 responseType 필드를 추가할 지 여부
	 * @param {Number} [options.delayExec=0] 성공 및 오류 대한 처리 대기 시간
	 * @param {LoadingCallback} [options.loadFunc] 로더 표시 콜백 함수
	 * @param {String} [options.title] 로그 작성 시의 타이틀
	 * @param {AjaxSuccessCallback} [options.success] 성공 시 호출되는 콜백 함수
	 * @param {AjaxErrorCallback} [options.error] 오류 시 호출되는 콜백 함수
	 */
	ajax: function (options) {
		if (!options || !options.url || options.url.replace(/\s/g, '') == '')
			return;
	
		if (!options.type)
			options.type = 'POST';
		else
			options.type = options.type.toUpperCase();
	
		if (!options.dataType)
			options.dataType = 'json';
		else
			options.dataType = options.dataType.toLowerCase();
	
		if (!options.timeout || options.timeout <= 0)
			options.timeout = 1000000;
	
		if (typeof options.loading == 'undefined') options.loading = true;
	
		var logAll = typeof options.logAll == 'boolean' ? options.logAll : false;
		var logSuccess = typeof options.logSuccess == 'boolean' ? options.logSuccess : false;
		var logFail = typeof options.logFail == 'boolean' ? options.logFail : true;
		var asyncExec = typeof options.async == 'boolean' ? options.async : true;
	
		//var url = $url(options.url);
		var url = options.url;
	
		if (options.useResponseType === true) {
			if (url.indexOf('?') >= 0)
				url += '&';
			else
				url += '?';
	
			url += 'responseType=' + options.dataType;
		}
	
		//--------------------------------------------------------
	
		var delayExec = options.delayExec || 0;
	
		//--------------------------------------------------------
	
		if (typeof options.loadFunc == 'function') options.loadFunc.call(options.caller || options.loadFunc, true);
	
		//--------------------------------------------------------
	
		$.ajax({
			type: options.type,
			url: url,
			data: options.data,
			async: asyncExec,
			dataType: options.dataType,
			timeout: options.timeout,
			success: function (result) {
				if (typeof options.loadFunc == 'function') options.loadFunc.call(options.caller || options.loadFunc, false);
	
				var successFunc = function () {
					var result = arguments.callee.result;
					var options = arguments.callee.options;
					var logAll = arguments.callee.logAll;
					var logSuccess = arguments.callee.logSuccess;
					var logFail = arguments.callee.logFail;
	
					if (logAll || logSuccess) {
						var title = options.title ? options.title + '에 실패했습니다\n' : ''
						var msg = title + ' 전송 성공';
	
						// try {
						// 	msg += ' responseText:' + toString(responseText) + ', statusText:' + statusText + ', xhr:' + xhr + ', $form:' + $form;
						// } catch (e) { }
	
						console.log(msg);
					}
	
					if (options.success && typeof options.success == 'function')
						options.success.call(options.caller ? options.caller : options.success, result);
				};
				successFunc.result = result;
				successFunc.options = options;
				successFunc.logAll = logAll;
				successFunc.logSuccess = logSuccess;
				successFunc.logFail = logFail;
				
				if (asyncExec && delayExec >= 0)
					setTimeout(successFunc, delayExec);
				else
					successFunc();
	
			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				if (typeof options.loadFunc == 'function') options.loadFunc.call(options.caller || options.loadFunc, false);
	
				var errorFunc = function () {
					var xhr = arguments.callee.xhr;
					var textStatus = arguments.callee.ts;
					var errorThrown = arguments.callee.et;
					var options = arguments.callee.options;
					var logAll = arguments.callee.logAll;
					var logSuccess = arguments.callee.logSuccess;
					var logFail = arguments.callee.logFail;
	
					var r = VpCommon.remoteErrorProcess(options, xhr, textStatus, errorThrown, logAll || logFail);
	
					if (options.error && typeof options.error == 'function')
						options.error.call(options.caller ? options.caller : options.error, xhr, textStatus, errorThrown);
	
					if (typeof r.retfunc == 'function') {
						var vRetFunc = function () {
							var callback = arguments.callee.callback;
							callback.call(arguments.callee.owner || callback);
						};
						vRetFunc.owner = options.caller;
						vRetFunc.callback = r.retfunc;
	
						setTimeout(vRetFunc, delayExec);
					}
				};
				errorFunc.xhr = XMLHttpRequest;
				errorFunc.ts = textStatus;
				errorFunc.et = errorThrown;
				errorFunc.options = options;
				errorFunc.logAll = logAll;
				errorFunc.logSuccess = logSuccess;
				errorFunc.logFail = logFail;
	
				if (asyncExec && delayExec >= 0)
					setTimeout(errorFunc, delayExec);
				else
					errorFunc();
			}
		});
	},

	
	//-------------------------------------------------------------------------------------------------------
	// Ajax 오류 메시지 관련 함수
	//-------------------------------------------------------------------------------------------------------
	
	/**
	 * Ajax 오류 메시지를 작성합니다.
	 * @memberof VpCommon
	 * @param {XMLHttpRequest} XMLHttpRequest XMLHttpRequest 객체
	 * @param {String} textStatus 상태 정보
	 * @param {String} errorThrown 발생한 오류 정보
	 * @return {String} 오류 메시지
	 */
	toStringAjaxFail: function (XMLHttpRequest, textStatus, errorThrown) {
		if (XMLHttpRequest) {
			return VpCommon.toStringXmlHttpRequest(XMLHttpRequest);
		} else {
			return 'Status: ' + textStatus;
		}
	},
	
	/**
	 * XML Http Request 객체에서 오류메시지를 가져옵니다.
	 * @memberof VpCommon
	 * @param {XMLHttpRequest} XMLHttpRequest XMLHttpRequest 객체
	 * @return {String} 오류메시지
	 */
	toStringXmlHttpRequest: function(XMLHttpRequest) {
		if (XMLHttpRequest) {
			var s = '';
	
			if (XMLHttpRequest.responseJSON) {
				var msg = XMLHttpRequest.responseJSON.Message || XMLHttpRequest.responseJSON.message;
				var etype = XMLHttpRequest.responseJSON.ExceptionType || XMLHttpRequest.responseJSON.exceptionType;
				var emsg = XMLHttpRequest.responseJSON.ExceptionMessage || XMLHttpRequest.responseJSON.exceptionMessage;
	
				if (etype && emsg)
					//s += emsg;
					s += msg + ' [' + etype + ']: ' + emsg + '';
				else if (etype)
					s += msg + ' [' + etype + ']';
				else if (emsg)
					//s += emsg;
					s += msg + ' [Exception]: ' + emsg + '';
				else
					s += msg;
			} else if (XMLHttpRequest.responseText) {
				s += XMLHttpRequest.responseText;
			}
	
			if (s.length) s += '\n\n';
			s += 'Status: ' + XMLHttpRequest.status;
	
			if (XMLHttpRequest.statusText)
				s += ' (' + XMLHttpRequest.statusText + ')';
	
			return s;
		}
		return '' + XMLHttpRequest;
	},	

	/**
	 * XML Http Request 객체의 responseJSON 필드에서 회신 정보를 추출합니다.
	 * @memberof VpCommon
	 * @param {XMLHttpRequest} xhr XMLHttpRequest 객체
	 * @return {XmlHttpRequestResponseInfo} 회신 정보
	 */
	readxhr: function(xhr) {
		var json = typeof xhr.responseJSON != 'undefined' ? xhr.responseJSON : null;
	
		if (json) {
			var xhrd = {
				status: null,
				exception: null,
				errorCode: null,
				messageType: null,
				message: null,
				stackTrace: null,
				token: null,
			};
	
			if (typeof xhr.status != 'undefined') xhrd.status = xhr.status;
			
			if (typeof json.Name != 'undefined') xhrd.exception = json.Name;
			if (typeof json.ErrorCode != 'undefined') xhrd.errorCode = json.ErrorCode;
			if (typeof json.MessageType != 'undefined') xhrd.messageType = json.MessageType;
			if (typeof json.Message != 'undefined') xhrd.message = json.Message;
			if (typeof json.StackTrace != 'undefined') xhrd.stackTrace = json.StackTrace;
			if (typeof json.Token != 'undefined') xhrd.token = json.Token;
	
			return xhrd;
		}
		return null;
	},
	
	/**
	 * 원격 오류 정보를 추출합니다.
	 * @memberof VpCommon
	 * @param {XMLHttpRequest} xhr XMLHttpRequest 객체
	 * @param {String} textStatus 상태 정보
	 * @param {String} errorThrown 발생한 오류 정보
	 * @return {XmlHttpRequestResponseInfo} 회신 정보
	 */
	readRemoteError: function(xhr, textStatus, errorThrown) {
		var xhrd = VpCommon.readxhr(xhr);
		if (!xhrd) xhrd = {};
		xhrd.textStatus = textStatus;
		xhrd.errorThrown = errorThrown;
		return xhrd;
	},
	
	/**
	 * 오류 메시지를 추출하는 작업을 수행합니다.
	 * @memberof VpCommon
	 * @param {Object} options Ajax 옵션 ({@link VpCommon.ajax}를 참고하세요)
	 * @param {XMLHttpRequest} xhr XMLHttpRequest 객체
	 * @param {String} textStatus 상태 정보
	 * @param {String} errorThrown 발생한 오류 정보
	 * @param {Boolean} [logging] 콘솔 기록 여부
	 * @return {XHRRemoteErrorProcessResult} 오류 메시지 추출 수행 결과 정보
	 */
	remoteErrorProcess: function(options, xhr, textStatus, errorThrown, logging) {
		var rerror = VpCommon.readRemoteError(xhr);
		var skip = false, r = null, func = null, arg = null;
		var skipRetFunc = null;
		var msg = null;
	
		if (typeof options.confirm == 'function') {
			r = options.confirm.call(options.caller || options.confirm, rerror);
		} else if (options.skips) {
			if (typeof options.skips.errorcode == 'function') {
				func = options.skips.errorcode;
				arg = rerror.errorCode;
			} else if (typeof options.skips.status == 'function') {
				func = options.skips.status;
				arg = rerror.status;
			} else if (options.skips.type && typeof options.skips.type[rerror.messageType] == 'function') {
				r = options.skips.type[rerror.messageType];
			} else if (options.skips.token && typeof options.skips.token[rerror.token] == 'function') {
				r = options.skips.token[rerror.token];
			} else if (options.skips.exception && typeof options.skips.exception[rerror.exception] == 'function') {
				r = options.skips.exception[rerror.exception];
			}
	
			if (func) r = func.call(options.caller || func, arg);
		}
	
		func = null;
		arg = null;
	
		if (r === false) skip = true;
		else if (typeof r == 'function') {
			skip = true;
			skipRetFunc = r;
		}
	
		if (!skip) {
			if (options.msgs) {
				if (typeof options.msgs == 'function') {
					func = options.msgs;
					arg = rerror;
				} else {
					if (typeof options.msgs.errorcode == 'function') {
						func = options.msgs.errorcode;
						arg = rerror.errorCode;
					} else if (typeof options.msgs.status == 'function') {
						func = options.msgs.status;
						arg = rerror.status;
					} else if (options.msgs.type && options.msgs.type[rerror.messageType]) {
						msg = options.msgs.type[rerror.messageType];
					} else if (options.msgs.token && options.msgs.token[rerror.token]) {
						msg = options.msgs.token[rerror.token];
					} else if (options.msgs.exception && options.msgs.exception[rerror.exception]) {
						msg = options.msgs.exception[rerror.exception];
					}
				}
	
				if (func) msg = func.call(options.caller || func, arg);
			}
	
			/*
			if (options.msgs) {
				if (isFunction(options.msgs)) {
					msg = options.msgs.call(options.caller || options.msgs, rerror.exception);
				} else {
					if (rerror.exception) msg = options.msgs[rerror.exception];
					else msg = options.msgs[rerror.status];
				}
			}
			*/
	
			if (msg == null) msg = VpCommon.toStringAjaxFail(xhr, textStatus, errorThrown);
	
			var title = options.title ? options.title + '에 실패했습니다\n' : ''
			if (!msg) msg = '';
			msg = title + msg;
	
			if (msg && !options.nomsg) {
				if (logging) console.log(msg);
	
				if (typeof options.msgFunc == 'function')
					options.msgFunc.call(options.caller || options.msgFunc, msg);
				else{
					//alert(msg);
					console.log(msg);
				}
			}
		}
	
		return {
			message: msg,
			retfunc: skipRetFunc
		};
	},

};