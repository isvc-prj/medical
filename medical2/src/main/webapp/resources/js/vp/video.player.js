var VideoPlayer = (function(){
	function defined(a, b){ return typeof a != 'undefined' ? a : b; }
	function definedBoolean(a, b){ return typeof a == 'boolean' ? a : b; }
	function definedNumber(a, b){ return typeof a == 'number' ? a : b; }
	function definedFunction(a, b){ return typeof a == 'function' ? a : b; }

	function vp(e, name) { return e.find('[vp-name="' + name + '"]'); }
	function vpc(e, name) { return e.children('[vp-name="' + name + '"]'); }
	function tf(value) { return value < 10 ? '0' + value : value; }

	function eo(e) { return e.length ? e.get(0) : null; }
	function is(e, te) { return eo(e) == eo(te); }
	function has(e, te) {
		if (is(e, te)) return true;
		return e.has(te).length > 0;
	}

	function durationToTimeSpan(duration){
		if (isNaN(duration))
			return null;

		var originDuration = duration;
		duration = parseFloat(parseFloat(duration).toFixed(0));

		var h = duration / 60 / 60;
		var m = (duration / 60) % 60;
		var s = duration % 60;

		h = Math.floor(h);
		m = Math.floor(m);
		s = Math.ceil(s);
		
		return {
			origin: originDuration,
			value: duration,
			hours: h,
			minutes: m,
			seconds: s,

			formatString: function(useHour){
				var fh = tf(this.hours);
				var fm = tf(this.minutes);
				var fs = tf(this.seconds);
		
				if (useHour === true || this.hours > 0)
					return fh + ':' + fm + ':' + fs;
				return fm + ':' + fs;		
			},
		};
	}

	function durationTimeSpanFormat(duration, useHour){
		if (isNaN(duration))
			return useHour === true ? '00:00:00' : '00:00';

		var t = durationToTimeSpan(duration);

		var fh = tf(t.hours);
		var fm = tf(t.minutes);
		var fs = tf(t.seconds);

		if (useHour === true || t.hours > 0)
			return fh + ':' + fm + ':' + fs;
		return fm + ':' + fs;
	}

	var REG_TIMESPAN = /([-+]{0,1}[0-9]+)([sm]{0,1})/g;

	function getTimeSpan(value){
		var unit = 's';
		var timeValue = 0;
		var time = 0;

		if (typeof value == 'number'){
			timeValue = value;
		}else if (typeof value == 'string'){
			REG_TIMESPAN.lastIndex = 0;
			var m = REG_TIMESPAN.exec(value);
			if (m){
				timeValue = parseInt(m[1]);
	
				if (m.length > 2)
					unit = m[2];
			}
		}else{

		}
		
		time = unit === 'm' ? timeValue * 60 : timeValue;

		return {
			value: timeValue,
			unit: unit,
			time: time,
			absTime: Math.abs(time),
		};
	}

	function getTemplates(controller, name){
		var e = controller.e.templates;

		var te = e.children('[vp-template="'+name+'"]');
		if (te.length)
			return te.children(':eq(0)').clone();
		return null;
	}

	function hasAttr(e, name){
		if (e.length){
			return e.get(0).hasAttribute(name);
		}
		return false;
	}

	function getArgs(e, prefix){
		if (e.length){
			var args = {}, numArgs = 0;
			var attrs = e.get(0).attributes;

			var valuePrefix = 'val-' + prefix;

			for (var i=0; i < attrs.length; i++){
				var attr = attrs[i];

				if (attr.name){
					if (attr.name.indexOf(valuePrefix) == 0){
						var ine = $(attr.value);
						if (ine.length){
							args[attr.name.substr(valuePrefix.length)] = ine.val();
							numArgs++;
						}
					}else if (attr.name.indexOf(prefix) == 0){
						args[attr.name.substr(prefix.length)] = attr.value;
						numArgs++;
					}
				}
			}
			if (numArgs)
				return args;
		}
		return null;
	}

	function uuid(){
		// http://www.ietf.org/rfc/rfc4122.txt
		var s = [];
		var hexDigits = '0123456789abcde';
		for (var i = 0; i < 36; i++) {
			s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
		}
		s[14] = '4';  // bits 12-15 of the time_hi_and_version field to 0010
		s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);  // bits 6-7 of the clock_seq_hi_and_reserved to 01
		s[8] = s[13] = s[18] = s[23] = '-';

		var uuid = s.join('');
		return uuid;
	}

	function getTimeFrame(time){
		return time - (time % 0.5);
	}

	//-----------------------------------------------------------
	//-----------------------------------------------------------
	//-----------------------------------------------------------

	function _VideoPlayer (options){
		if (!options) options = {};

		var labelDisplayTimeRangeOptions = options.labelDisplayTimeRange || {};

		this.selector = options.selector || '#vp';

		var e = $(this.selector);
		var evolume = vp(e, 'volume');
		var eduration = vp(e, 'duration');
		var eprogress = vp(e, 'progress');
		var ebuttons = vp(e, 'buttons');

		var edraws = vp(e, 'draws');
		var elabelCanvas = $('<canvas></canvas>');
		var emaskCanvas = $('<canvas></canvas>');

		edraws.append(elabelCanvas);
		edraws.append(emaskCanvas);

		this.e = {
			player: e,
			video: e.find('video'),
			draws: {
				body: edraws,
				labelCanvas: elabelCanvas,
				maskCanvas: emaskCanvas,
			},
			panel: vp(e, 'panel'),
			panelPadding: vp(e, 'panel-padding'),
			snapshot: vp(e, 'snapshot'),
			buttons: ebuttons,
			volume: {
				body: evolume,
				track: vp(evolume, 'track'),
				handle: vp(evolume, 'handle'),
			},
			duration: {
				body: eduration,
				current: vp(eduration, 'current'),
				duration: vp(eduration, 'duration'),
			},
			rate: {
				button: vp(ebuttons, 'rate'),
				list: vp(ebuttons, 'rate-list'),
			},
			progress: {
				body: eprogress,
				loading: vp(eprogress, 'loading'),
				current: vp(eprogress, 'current'),
				hover: vp(eprogress, 'hover'),
				snapshot: vp(eprogress, 'snapshot-list'),
			},
			external: vp(e, 'external'),
			templates: vp(e, 'templates'),
		};

		this.eo = {
			video: eo(this.e.video),
			draws: {
				body: eo(this.e.draws.body),
				labelCanvas: eo(this.e.draws.labelCanvas),
				maskCanvas: eo(this.e.draws.maskCanvas),
			},
			volume: {
				body: eo(this.e.volume.body),
				track: eo(this.e.volume.track),
				handle: eo(this.e.volume.handle),
			},
			progress: {
				body: eo(this.e.progress.body),
				loading: eo(this.e.progress.loading),
				current: eo(this.e.progress.current),
				hover: eo(this.e.progress.hover),
				snapshot: eo(this.e.progress.snapshot),
			},
		};

		//-----------------------------------------------------------
		// button

		this.buttons = ebuttons.children('[vp-btn]');
		
		this.buttons.click(this, function (event){
			var e = $(this);
			var self = event.data;

			if (!self.canPlay)
				return;

			var cmd = e.attr('vp-btn');
			var value = e.attr('vp-value');

			self.command(cmd, value);
		});

		//-----------------------------------------------------------
		// Notify button

		this.notifyButtons = ebuttons.find('[vp-notify]');
		
		this.notifyButtons.click(this, function (event){
			var e = $(this);
			var self = event.data;

			if (!self.canPlay)
				return;

			var cmd = e.attr('vp-notify');
			var value = null;
			if (hasAttr(e, 'vp-value')){
				value = e.attr('vp-value');
			}else{
				value = getArgs(e, 'arg-');
			}

			self.notifyObservers('u:' + cmd, value);
		});

		//-----------------------------------------------------------
		// Flags

		this.canPlay = false;
		this.isPlaying = false;
	
		//-----------------------------------------------------------

		this.labelDisplayTimeRange = {
			enabled: definedBoolean(labelDisplayTimeRangeOptions.enabled, true),
			// start: definedNumber(labelDisplayTimeRangeOptions.start, -0.2),
			// end: definedNumber(labelDisplayTimeRangeOptions.end, +0.2),
		};
	
		//-----------------------------------------------------------

		this.viewport = {
			ratio: 1,

			left: 0,
			top: 0,
			width: 0,
			height: 0,

			videoLeft: 0,
			videoTop: 0,
			videoWidth: 0,
			videoHeight: 0,

			resizing: false,
		};
	
		//-----------------------------------------------------------

		this.duration = null;

		this.timeRes = {
			playChanged: null,
			beginResized: null,
		};

		this.templates = {
			hoverSnapshot: getTemplates(this, 'snapshot'),
			snapshot: getTemplates(this, 'snapshot-button'),
		};
		
		this.one = {
			visibleLabel: null,	
		};

		this.data = {
			screen: {
				userRegisted: false,
				width: 0,
				height: 0,
				source: null,
			},
				
			hoverSnapshot: null,
			mask: null,

			snapshot: {
				current: null,

				list: [],
				map: {},
				timeFrames: {},

				clear: function(){
					this.current = null;

					this.map = {};
					this.timeFrames = {};
					this.list.splice(0, this.list.length);
				},
	
				add: function (data){
					//data.uid = uuid();
	
					this.list.push(data);
					this.map[data.time] = data;

					var key = getTimeFrame(data.time);

					this.timeFrames[key] = data;
				},

				update: function (data){
					var i = this.indexOf(data.time);
					if (i >= 0){
						this.list[i] = data;
						this.map[data.time] = data;

						var key = getTimeFrame(data.time);

						this.timeFrames[key] = data;
					}
				},

				remove: function (time){
					var i = this.indexOf(time);
					if (i >= 0){
						this.list.splice(i, 1);
						delete this.map[time];

						var key = getTimeFrame(time);

						delete this.timeFrames[key];

						if (this.current === time){
							this.current = null;
							return true;
						}
					}
					return false;
				},

				indexOf: function(time){
					var len = this.list.length;
					for (var i=0; i < len; i++){
						var l = this.list[i];

						if (l.time === time){
							return i;
						}
					}
					return -1;
				},
			},
		};
		
		//-----------------------------------------------------------

		this.observers = [];

		//-----------------------------------------------------------

		this.initialize();
	}

	_VideoPlayer.prototype = (function(){
		return {
			constructor: _VideoPlayer,
		
			//-----------------------------------------------------------

			THUMB_WIDTH: 120,
			THUMB_HEIGHT: 120,
		
			//-----------------------------------------------------------

			initialize: function(){
				attachVideoEvents(this);
				attachPlayerEvents(this);

				this.updateVolume();
			},

			//-----------------------------------------------------------

			/**
			 * 옵저버 리스너를 등록합니다.
			 */
			observe: function(observer){ this.observers.push(observer); },
			/**
			 * 옵저버 리스너를 제거합니다.
			 */
			stopObserve: function(observer){
				var idx = this.observers.indexOf(observer);
				if (idx >= 0) this.observers.splice(idx, 1);
			},

			/**
			 * 등록된 옵저버에 Notify합니다.
			 * @param {String} topic 토픽
			 * @param {String} [value] 값
			 */
			notifyObservers: function(topic, value){
				var len = this.observers.length;
				for (var i=0; i < len; i++){
					var o = this.observers[i];

					if (typeof o == 'function')
						o(this, topic, value, this.shape);
					else if (typeof o.styleNotify == 'function')
						o.notifyPlayer(this, topic, value, this.shape);
				}
			},
		
			//-----------------------------------------------------------

			videoEvent: function (event){
				//console.log(event.type, event);

				this.notifyObservers('event:fired', event);
			},
			
			//-----------------------------------------------------------

			showExternal: function(visible, name){
//				if (visible){
//					if (name){
//						showExternal(this, name);
//					}
//				}else{
//					hideExternal(this);
//				}
			},
			
			//-----------------------------------------------------------
			
			clear: function(){
				this.data.screen.userRegisted = false;
				this.data.screen.width = 0;
				this.data.screen.height = 0;
				this.data.screen.source = null;
				
				this.data.hoverSnapshot = null;
				this.data.mask = null;
				
				this.data.snapshot.clear();
				
				this.e.progress.snapshot.children().remove();
			},
	
			//-----------------------------------------------------------

			open: function (src, mime){
				setSource(this.e.video, src, mime);
				
				this.command('load');
			},
			
			close: function(){
				if (this.canPlay){
					var video = this.eo.video;

					video.pause();
					
					removeSource(this.e.video);

					video.src = '';
					video.load();
				}
			},
			
			//-----------------------------------------------------------
			
			show: function (){
				this.e.player.css({
					visibility: 'visible',
					zIndex: '',
					position: 'relative',
					top: '',
				});
			},
			
			hide: function(){
				this.e.player.css({
					visibility: 'hidden',
					zIndex: -1,
					position: 'absolute',
					top: -10000000,
				});
			},
		
			//-----------------------------------------------------------

			command: function (cmd, value){
				var video = this.eo.video;
				var returnValue = null;
				var ts = null;

				if (this.viewport.resizing){
					//console.log('<<<RESIZED>>> COMMAND');

					this.resized();
				}

				switch(cmd){
				case 'visible-label':
					this.one.visibleLabel = value;
					returnValue = this.one.visibleLabel;
					break;
					
				case 'load':
					returnValue = video.load();
					break;
				case 'play':
					returnValue = video.play();
					break;

				case 'pause':
					returnValue = video.pause();
					break;

				case 'sound':
					video.muted = true;
					returnValue = video.muted;
					break;

				case 'mute':
					video.muted = false;
					returnValue = video.muted;
					break;

				case 'current':
					video.currentTime = value;
					returnValue = video.currentTime;
					break;

				case 'current:per':
					console.log('<<CURRENT>>, canPlay=', this.canPlay);
					
					if (!isNaN(video.duration)){
						var duration = video.duration;
						var time = (value * duration).toFixed(3);

						video.currentTime = time;
						
						console.log('<<CURRENT-TIME>>, try time=', time, ' currentTime=', video.currentTime);
					}
					returnValue = video.currentTime;
					break;

				case 'seek':
					ts = getTimeSpan(value);
					video.currentTime += ts.time;
					returnValue = video.currentTime;
					break;

				case 'forward':
					ts = getTimeSpan(value);

					//console.log('<<<forward>>>', video.currentTime, value, ts);

					//seek(video, ts.absTime);

					video.currentTime += ts.absTime;
					returnValue = video.currentTime;

					console.log('\t', video.currentTime);

					break;

				case 'rewind':
					ts = getTimeSpan(value);

					//console.log('<<<rewind>>>', video.currentTime, value, ts);

					//seek(video, -ts.absTime);

					video.currentTime -= ts.absTime;
					returnValue = video.currentTime;

					//console.log('\t', video.currentTime);
					break;

				case 'volume':
					if (value < 0) value = 0;
					else if (value > 1) value = 1;

					//console.log ('<<<VOLUMNE>>>', value);

					video.volume = value;
					returnValue = video.volume;
					break;

				case 'rate':
					if (value < 0.25) value = 0.25;
					else if (value > 5) value = 5;

					//console.log ('<<<VOLUMNE>>>', value);

					video.playbackRate = value;
					returnValue = video.playbackRate;
					break;

				case 'stream':
					returnValue = captureStream(this);
					break;

				case 'capture':
					returnValue = captureWithLabel(this);
					break;

				case 'capture:mask':
					returnValue = captureForMask(this);
					break;
					
				case 'get:label':
					returnValue = getLabel(this, value);
					break;

				case 'remove:label':
					var index = this.data.snapshot.indexOf(value);
					if (index >= 0){
						var d = this.data.snapshot.list[index];
						returnValue = d;
						
						if (this.data.snapshot.remove(value)){
							drawLabels(this);
						}

						var e = this.e.progress.snapshot.find('[vp-time="'+value+'"]');
						if (e.length)
							e.remove();
					}else{
						returnValue = null;
					}
					
					break;

				case 'remove:mask':
					this.data.mask = null;
					drawMasks(this);
					break;
				}
				return returnValue;
			},
		
			//-----------------------------------------------------------

			mask: function(data){
				if (this.viewport.resizing){
					//console.log('<<<RESIZED>>> COMMAND');

					this.resized();
				}

				this.data.mask = data;

				drawMasks(this);
			},

			label: function(data, display, duration){
				if (typeof display != 'boolean') display = true;

				if (this.viewport.resizing){
					//console.log('<<<RESIZED>>> COMMAND');

					this.resized();
				}

				var video = this.eo.video;
				
				if (typeof duration != 'number')
					duration = video.duration;

				var mapData = this.data.snapshot.map[data.time];
				if (!mapData){
					this.data.snapshot.add(data);

					var pos = data.time / duration;
	
					var snapshotButton = this.templates.snapshot.clone();
					snapshotButton.attr('vp-time', data.time);
					snapshotButton.css('left', (pos * 100) + '%');
	
					this.e.progress.snapshot.append(snapshotButton);
				}else{
					this.data.snapshot.update(data);
				}

				if (display && this.labelDisplayTimeRange.enabled){
					video.currentTime = data.time;
					this.data.snapshot.current = data.time;
					drawLabels(this);
				}
			},
			
			//-----------------------------------------------------------
			
			refresh: function(){
				drawLabels(this);
				drawMasks(this);
			},
		
			//-----------------------------------------------------------

			get: function (cmd){
				var video = this.eo.video;

				switch (cmd){
				case 'duration': return video.duration;
				case 'current': return video.currentTime;
				case 'snapshots': return this.data.snapshot ? this.data.snapshot.list : [];
				}
			},
			
			//-----------------------------------------------------------
			
			prepared: function(){
				if (this.data.screen.userRegisted)
					return;
				
				var d = capture(this);

				if (d){
					this.data.screen.width = d.width;
					this.data.screen.height = d.height;
					this.data.screen.source = d.source;
				}else{
					this.data.screen.width = 0;
					this.data.screen.height = 0;
					this.data.screen.source = null;
				}
			},
	
			//-----------------------------------------------------------

			canPlayChanged: function(){
				if (this.canPlay){
					this.buttons.removeClass('vp-disabled');
				}else{
					this.buttons.addClass('vp-disabled');
				}
			},

			playChanged: function(){
				if (this.timeRes.playChanged)
					clearTimeout(this.timeRes.playChanged);

				this.timeRes.playChanged = null;

				if (this.isPlaying){
					this.timeRes.playChanged = setTimeout(function(){
						this.timeRes.playChanged = null;
						this.showPanel(false);
					}.bind(this), 1000);
				}else{
					this.showPanel(true);
				}
			},
	
			//-----------------------------------------------------------

			showPanel: function(visible){
				if (visible)
					this.e.panel.removeClass('vp-hide');
				else
					this.e.panel.addClass('vp-hide');
			},
	
			//-----------------------------------------------------------

			updateVolume: function (){
				var video = this.eo.video;

				if (video.muted){
					this.e.volume.body.removeClass('open');

					showButton(this.e.buttons, 'sound', false);
					showButton(this.e.buttons, 'mute', true);
				}else{
					showButton(this.e.buttons, 'sound', true);
					showButton(this.e.buttons, 'mute', false);

					var value = video.volume;

					// min/max 처리
					var container = this.e.volume.body;
					var w = container.width();

					var ce = this.e.volume.handle;

					var wp = w == 0 ? 0 : ce.width() / w;
					if (value < wp) value = wp;

					//console.log('<<<VOLUME VIEW>>> w=', w, ', ce w=', ce.width(), ', wp=', wp, ' value=', value);
	
					this.e.volume.body.addClass('open');
					this.e.volume.track.css('width', (value * 100) + '%');
				}
			},

			updateTime: function(){
				var time = this.eo.video.currentTime;

				// var key = getTimeFrame(time);
				// console.log('<<<KEY>>> ', key, 'src=', time);

				if (this.labelDisplayTimeRange.enabled){
					// var current = this.data.snapshot.current;
					// this.data.snapshot.current = null;

					// var start = this.labelDisplayTimeRange.start;
					// var end = this.labelDisplayTimeRange.end;

					// var snapshotList = this.data.snapshot.list;
					// var len = snapshotList.length;
					// for (var i=0; i < len; i++){
					// 	var l = snapshotList[i];

					// 	if (l.time + start <= time && time <= l.time + end){
					// 		this.data.snapshot.current = l.time;
					// 		break;
					// 	}
					// }

					// if (current !== this.data.snapshot.current){
					// 	drawLabels(this);
					// }

					var current = this.data.snapshot.current;
					this.data.snapshot.current = null;

					var key = getTimeFrame(time);

					var keyFrame = this.data.snapshot.timeFrames[key];
					if (keyFrame){
						this.data.snapshot.current = keyFrame.time;
					}

					if (current !== this.data.snapshot.current){
						drawLabels(this);
					}					
				}
			},

			updateDuration: function(){
				var video = this.eo.video;
	
				var timeSpan = this.duration;
				var existHours = timeSpan && timeSpan.hours > 0;

				var empty = '00:00';
				var nDuration = 0, nCurrent = 0;
				var sDuration = empty, sCurrent = empty;

				if (timeSpan){
					var current = video.currentTime;

					if (isNaN(current)){
						sCurrent = empty;
						sDuration = timeSpan.formatString();
	
						nCurrent = 0;
						nDuration = timeSpan.value;
					}else{
						sDuration = timeSpan.formatString();
						sCurrent = durationTimeSpanFormat(current, existHours);
	
						nDuration = timeSpan.value;
						nCurrent = current;
					}
				}else{

				}
	
				this.e.duration.current.text(sCurrent);
				this.e.duration.duration.text(sDuration);

				this.e.progress.current.css('width', nCurrent == 0 || nDuration == 0 ? '0' : (nCurrent / nDuration * 100) + '%');
			},

			updateProgress: function(){
				var video = this.eo.video;

				var range = 0;
				var bf = video.buffered;
				var time = video.currentTime;

				if (!bf.length){
					this.e.progress.loading.css({
						left: 0,
						width: 0,
					});
					return;
				}

				while(range < bf.length && !(bf.start(range) <= time && time <= bf.end(range))) {
					range += 1;
				}
				if (range >= bf.length) range = bf.length - 1;

				//range = 0;

				var start = bf.start(range);
				var end = bf.end(range);

				var loadStartPercentage = start / video.duration;
				var loadEndPercentage = (end - start) / video.duration;
				//var loadPercentage = loadEndPercentage - loadStartPercentage;

				this.e.progress.loading.css('left', (loadStartPercentage * 100) + '%');
				this.e.progress.loading.css('width', (loadEndPercentage * 100) + '%');
			},

			updateRate: function(){
				var video = this.eo.video;

				var value = video.playbackRate;

				this.e.rate.button.text('x' + value);
			},

			updateHover: function (x, preessd, snapshotKey){
				if (x == null){
					this.e.progress.hover.css('width', 0);
					this.showHoverSnapshot(false);
					return;
				}

				if (x < 0){
					return;
				}

				var video = this.eo.video;
				var container = this.e.progress.body;
				var w = container.width();

				if (x > w){
					return;
				}

				//console.log('<<<HOVER>>> x=' + x + ', w=' + w);

				var p = x / w;
				var pos = (p * 100) + '%';

				this.e.progress.hover.css('width', pos);

				var imgValue = null;
				if (snapshotKey){
					var data = this.data.snapshot.map[snapshotKey];

					p = data.time / video.duration;

					this.showHoverSnapshot(true, p, data.thumbnail.source);
				}else{
					this.showHoverSnapshot(true, p);
				}
			},

			endHover: function (x, snapshotKey){
				var video = this.eo.video;

				//console.log('<<<HOVER>>> x=' + x + ', w=' + w);

				this.showHoverSnapshot(false);

				if (snapshotKey){
					var data = this.data.snapshot.map[snapshotKey];

					this.command('current', data.time);
				}else{
					var container = this.e.progress.body;
					var w = container.width();
	
					var p = x / w;

					this.command('current:per', p);
				}
			},
			
			//-----------------------------------------------------------

			showHoverSnapshot: function(visible, current, imgValue){
				if (visible){
					var e = null;
					if (!this.data.hoverSnapshot){
						e = this.templates.hoverSnapshot;

						this.e.snapshot.append(e);
						this.data.hoverSnapshot = e;
					}else{
						e = this.data.hoverSnapshot;
					}

					this.e.panelPadding.removeClass('vp-hide');

					if (imgValue){
						this.displaySnapshot(e, current, imgValue);
					}else{
						this.displaySnapshot(e, current);
					}
				}else{
					this.e.panelPadding.addClass('vp-hide');

					if (this.data.hoverSnapshot){
						this.data.hoverSnapshot.remove();
						this.data.hoverSnapshot = null;
					}
				}
			},
		
			//-----------------------------------------------------------

			displaySnapshot: function(e, value, imgValue){
				var video = this.eo.video;

				var timeSpan = this.duration;
				var existHours = timeSpan && timeSpan.hours > 0;

				var duration = isNaN(video.duration) ? 0 : video.duration;
				var time = value * duration;
				var timeSpan = durationTimeSpanFormat(time, existHours);

				var eimg = vp(e, 'img');
				var eimgSrc = eimg.find('img');
				var etime = vp(e, 'time');

				if (arguments.length >= 3){
					if (imgValue){
						eimgSrc.attr('src', imgValue);
						eimg.removeClass('vp-hide');
					}else{
						eimgSrc.attr('src', '');
						eimg.addClass('vp-hide');
					}
				}else{
					eimgSrc.attr('src', '');
					eimg.addClass('vp-hide');
				}

				etime.text(timeSpan);

				// min/max 처리
				var container = this.e.progress.body;
				var w = container.width();
				
				var ce = e;
				if (eimg.hasClass('vp-hide')) ce = etime;

				var wp = ce.width() / w / 2;
				if (value < wp) value = wp;
				if (value > 1 - wp) value = 1 - wp;
				
				e.css('left', (value * 100) + '%');
			},
			
			//-----------------------------------------------------------

			beginResized: function(){
				if (this.timeRes.beginResized)
					clearTimeout(this.timeRes.beginResized);

				this.viewport.resizing = true;

				var bounds = getBounds(this.e.video);

				//console.log('RESIZING, BOUNDS=', bounds);

				var maskCanvas = this.eo.draws.maskCanvas;

				var width = maskCanvas.width;
				var height = maskCanvas.height;

				var ratioX = bounds.width / width;
				var ratioY = bounds.height / height;

				var ratio = ratioX > ratioY ? ratioY : ratioX;

				VpCommon.cssScale(this.e.draws.labelCanvas, ratio, ratio);
				VpCommon.cssScale(this.e.draws.maskCanvas, ratio, ratio);

				this.timeRes.beginResized = setTimeout(function(){
					if (this.viewport.resizing){
						//console.log('<<<RESIZED>>> WINDOW');

						this.resized();
					}
				}.bind(this), 1000);
			},

			resized: function(){
				this.viewport.resizing = false;

				var video = this.eo.video;
				var labelCanvas = this.eo.draws.labelCanvas;
				var maskCanvas = this.eo.draws.maskCanvas;

				try
				{
					var bounds = getBounds(this.e.video);

					//console.log('RESIZED, BOUNDS=', bounds);

					labelCanvas.width = bounds.width;
					labelCanvas.height = bounds.height;

					maskCanvas.width = bounds.width;
					maskCanvas.height = bounds.height;

					this.viewport.left = bounds.left;
					this.viewport.top = bounds.top;
					this.viewport.width = bounds.width;
					this.viewport.height = bounds.height;
					
					this.viewport.videoWidth = video.videoWidth;
					this.viewport.videoHeight = video.videoHeight;

					this.viewport.ratio = getRatio(
						video.videoWidth, video.videoHeight,
						bounds.width, bounds.height).ratio;
					
					var bwidth = bounds.width;
					var bheight = bounds.height;
					
					var vwidth = video.videoWidth * this.viewport.ratio;
					var vheight = video.videoHeight * this.viewport.ratio;

					this.viewport.videoLeft = (bwidth - vwidth) >> 1;
					this.viewport.videoTop = (bheight - vheight) >> 1;
					
					VpCommon.removeCssScale(this.e.draws.labelCanvas);
					VpCommon.removeCssScale(this.e.draws.maskCanvas);
	
					updateDraws(this);
				}
				catch(e)
				{
					throw e;
				}
			},
			
			//-----------------------------------------------------------

			isProgressElement: function(e){
				return has(this.e.progress.body, e);
			},
		};

		//-----------------------------------------------------------
		//-----------------------------------------------------------
		//-----------------------------------------------------------

		function getRootScrollOffset(e){ var a = e.parents(); var len = a ? a.length : 0; if (len){ var p = a.get(len - 1); return { left: typeof p.scrollLeft == 'number' ? p.scrollLeft : 0, top: typeof p.scrollTop == 'number' ? p.scrollTop : 0, }; } return { left: 0, top: 0 }; }
		function getBounds(e, original){ var r = eo(e).getBoundingClientRect(); if (original) return r; var s = getRootScrollOffset(e); return { left: r.left + s.left, top: r.top + s.top, width: r.width, height: r.height, }; }

		function getRatio(srcWidth, srcHeight, limitWidth, limitHeight){
			var ratioX = limitWidth / srcWidth;
			var ratioY = limitHeight / srcHeight;

			return {
				ratio: ratioX > ratioY ? ratioY : ratioX,
				ratioX: ratioX,
				ratioY: ratioY
			};
		}
		
		//-----------------------------------------------------------

		function getElementOffset(controller, name, subname, event){
			var pe = controller.e[name];
			var e = subname ? pe[subname] : e;

			var b = getBounds(e);

			return {
				x: event.pageX - b.left,
				y: event.pageY - b.top,
				width: b.width,
				height: b.height
			};
		}

		function getSnapshotFromProgressOffset(controller, x){
			var bounds = getBounds(controller.e.progress.body, true);

			var y = bounds.top + (bounds.height >> 1);

			var snapshotKey = null;
			var target = document.elementFromPoint(bounds.left + x, y);
			if (target){
				var etarget = $(target);
				if (etarget.attr('vp-type') === 'snapshot:button'){
					return etarget.attr('vp-time');
				}
			}

			return null;
		}

		//-----------------------------------------------------------

		function updateDraws(controller){
			drawLabels(controller);
			drawMasks(controller);
		}

		function drawLabels(controller){
			var video = controller.e.video;
			var canvas = controller.eo.draws.labelCanvas;
			var viewport = controller.viewport;

			var current = controller.data.snapshot.current;

			//if (!controller.canPlay) return;

			//-----------------------------------------------------------

			var ratio = viewport.ratio;

			//-----------------------------------------------------------

			var ctx = canvas.getContext('2d');

			//-----------------------------------------------------------

			ctx.clearRect(0, 0, canvas.width, canvas.height);

			//-----------------------------------------------------------

			if (current != null && current >= 0){
				var visibleLabel = controller.one.visibleLabel;
				var data = controller.data.snapshot.map[current];
				var labels = data.labels;
				
				if (typeof visibleLabel == 'number' && visibleLabel >= 0 && visibleLabel < labels.length){
					labels = [ labels[visibleLabel] ];
				}
				
				VideoDrawer.labels(ctx, labels, {
					ratio: ratio,
					left: viewport.videoLeft,
					top: viewport.videoTop,
				});
				
				controller.one.visibleLabel = null;
			}
		}

		function drawMasks(controller){
			var video = controller.e.video;
			var canvas = controller.eo.draws.maskCanvas;
			var viewport = controller.viewport;
			var maskData = controller.data.mask;

			//if (!controller.canPlay) return;

			//-----------------------------------------------------------

			var ratio = viewport.ratio;

			//-----------------------------------------------------------

			var ctx = canvas.getContext('2d');

			//-----------------------------------------------------------

			ctx.clearRect(0, 0, canvas.width, canvas.height);

			if (!maskData)
				return;

			ctx.fillStyle = 'rgba(0,0,0,1)';
			ctx.strokeStyle = 'transparent';
			ctx.lineWidth = 0;
			ctx.mozFillRule = 'evenodd';
			ctx.msFillRule = 'evenodd';
	
			ctx.beginPath();

			if (maskData.invert) ctx.rect(0, 0, canvas.width, canvas.height);

			var len = maskData.masks.length;
			for (var i=0; i < len; i++){
				var mask = maskData.masks[i];

				ctx.rect(
					mask.x * ratio + viewport.videoLeft,
					mask.y * ratio + viewport.videoTop,
					mask.width * ratio,
					mask.height * ratio
				);
			}

			ctx.fill('evenodd');
			ctx.closePath();

		}
	
		function showButton(e, name, visible){
			visible = definedBoolean(visible, true);

			var ebtn = e.children('[vp-btn="'+name+'"]');
			if (visible) ebtn.removeClass('vp-hide');
			else ebtn.addClass('vp-hide');
		}

		function setSource(e, src, mime){
			var esrc = e.find('source');
			if (!esrc.length){
				esrc = $('<source></source>');
				e.append(esrc);
			}
			if (mime)
				esrc.attr('type', mime);
			esrc.attr('src', src);
		}

		function addSource(e, src, mime){
			var esrc = $('<source></source>');
			if (mime)
				esrc.attr('type', mime);
			esrc.attr('src', src);
			e.append(esrc);
		}
		
		function removeSource(e){
			e.find('source').remove();
		}

		function method(e, cmd){
			try
			{
				var element = e && e.length ? e.get(0) : null;
				if (element){
					var a = [];
					for (var i=2; i < arguments.length; i++) a.push(arguments[i]);

					switch(cmd){
					case 'load': return element.load.apply(element, a);
					case 'play': return element.play.apply(element, a);
					case 'pause': return element.pause.apply(element, a);

					case 'addTextTrack': return element.addTextTrack.apply(element, a);
					case 'captureStream': return element.captureStream.apply(element, a);
					case 'canPlayType': return element.canPlayType.apply(element, a);

					case 'mozCaptureStream': return element.mozCaptureStream.apply(element, a);
					case 'mozCaptureStreamUntilEnded': return element.mozCaptureStreamUntilEnded.apply(element, a);
					case 'mozGetMetadata': return element.mozGetMetadata.apply(element, a);

					case 'fastSeek': return element.fastSeek.apply(element, a);
					case 'seekToNextFrame': return element.seekToNextFrame.apply(element, a);
					case 'setMediaKeys': return element.setMediaKeys.apply(element, a);
					case 'setSinkId': return element.setSinkId.apply(element, a);
					}
				}
				return null;
			}
			catch(exception)
			{	
				return null;
			}
		}

		function attachVideoEvents(contoller){
			var eVideo = contoller.e.video;

			eVideo.on('canplay', contoller, function (event){
				var self = event.data;
				self.videoEvent(event);

				self.canPlay = true;
				self.canPlayChanged();
				
				setTimeout(function(){
					this.prepared();
				}.bind(self), 0);
			});
	
			eVideo.on('canplaythrough', contoller, function (event){
				var self = event.data;
				self.videoEvent(event);
			});
	
			eVideo.on('complete', contoller, function (event){
				var self = event.data;
				self.videoEvent(event);
			});
	
			eVideo.on('durationchange', contoller, function (event){
				var self = event.data;
				self.videoEvent(event);
				
				self.duration = durationToTimeSpan(self.eo.video.duration);
				self.updateDuration();
			});
	
			eVideo.on('emptied', contoller, function (event){
				var self = event.data;
				self.videoEvent(event);

				self.canPlay = false;
				self.isPlaying = false;
				self.duration = null;

				self.canPlayChanged();
				self.updateDuration();
			});
	
			eVideo.on('ended', contoller, function (event){
				var self = event.data;
				self.videoEvent(event);

				self.isPlaying = false;
			});
	
			eVideo.on('loadeddata', contoller, function (event){
				var self = event.data;
				self.videoEvent(event);

				self.resized();
			});
	
			eVideo.on('loadedmetadata', contoller, function (event){
				var self = event.data;
				self.videoEvent(event);

				self.canPlay = false;

				//var video = self.eo.video;
				//console.log('<<<META>> video width=' + video.videoWidth + ', height=' + video.videoHeight);
			});
	
			eVideo.on('pause', contoller, function (event){
				var self = event.data;
				self.videoEvent(event);
	
				showButton(self.e.buttons, 'pause', false);
				showButton(self.e.buttons, 'play', true);
			
				self.isPlaying = false;
				self.playChanged();
			});
	
			eVideo.on('play', contoller, function (event){
				var self = event.data;
				self.videoEvent(event);
			});
	
			eVideo.on('playing', contoller, function (event){
				var self = event.data;
				self.videoEvent(event);
	
				showButton(self.e.buttons, 'play', false);
				showButton(self.e.buttons, 'pause', true);

				self.isPlaying = true;

				self.playChanged();
			});
	
			eVideo.on('ratechange', contoller, function (event){
				var self = event.data;
				self.videoEvent(event);

				self.updateRate();
			});
	
			eVideo.on('seeked', contoller, function (event){
				var self = event.data;
				self.videoEvent(event);
			});
	
			eVideo.on('seeking', contoller, function (event){
				var self = event.data;
				self.videoEvent(event);
			});
	
			eVideo.on('stalled', contoller, function (event){
				var self = event.data;
				self.videoEvent(event);
			});
	
			eVideo.on('suspend', contoller, function (event){
				var self = event.data;
				self.videoEvent(event);
			});
	
			eVideo.on('timeupdate', contoller, function (event){
				var self = event.data;
				self.videoEvent(event);

				var video = self.eo.video;
				//console.log('<<TIMEUPDATE>>', video.currentTime, video.duration);

				self.duration = durationToTimeSpan(video.duration);

				self.updateDuration();
				self.updateTime();
			});
	
			eVideo.on('volumechange', contoller, function (event){
				var self = event.data;
				self.videoEvent(event);

				self.updateVolume();
			});
	
			eVideo.on('waiting', contoller, function (event){
				var self = event.data;
				self.videoEvent(event);
			});	
	
			eVideo.on('progress', contoller, function (event){
				var self = event.data;
				self.videoEvent(event);

				self.updateProgress();
			});	
		}
		
		//-----------------------------------------------------------

		function attachPlayerEvents(controller){
			var win = $(window);
			win.on('resize', controller, function(event){
				controller.beginResized();
			});

			//-----------------------------------------------------------

			var doc = $(document);
			doc.on('mousemove', controller, function(event){
				var self = event.data;

				var e = $(event.target);
				

				var isActive = self.e.progress.body.hasClass('active');
				if (isActive){
					var offset = getElementOffset(controller, 'progress', 'body', event);
					var offsetX = offset.x;

					var snapshotKey = getSnapshotFromProgressOffset(self, offsetX);

					self.updateHover(offsetX, isActive, snapshotKey);
				}else{
					if (self.isProgressElement(e)){
						var offset = getElementOffset(controller, 'progress', 'body', event);
						var offsetX = offset.x;

						var snapshotKey = getSnapshotFromProgressOffset(self, offsetX);

						self.updateHover(offsetX, null, snapshotKey);
					}else{
						self.updateHover(null);
					}
				}
				
				isActive = self.e.volume.body.hasClass('active');
				if (isActive){
					var pageX = event.pageX;
					var bounds = getBounds(self.e.volume.body);

					var offsetX = pageX - bounds.left;

					var value = offsetX / bounds.width;

					self.command('volume', value);
				}
			});
			doc.on('mousedown', controller, function(event){
				var self = event.data;

				var e = $(event.target);

				var rateButton = self.e.rate.button;
				var rateList = self.e.rate.list;

				if (!rateList.hasClass('vp-hide')){
					if (!has(rateButton, e) && !has(rateList, e)){
						rateList.addClass('vp-hide');

						//console.log('<<<MOUSEDOWN>> OFF RATE-LIST', e);
					}
				}
			});
			doc.on('mouseup', controller, function(event){
				var self = event.data;

				var e = $(event.target);

				var isActive = self.e.progress.body.hasClass('active');
				if (isActive){
					self.e.progress.body.removeClass('active');

					var offset = getElementOffset(controller, 'progress', 'body', event);
					var offsetX = offset.x;

					var snapshotKey = getSnapshotFromProgressOffset(self, offsetX);

					self.endHover(offsetX, snapshotKey);

					if (!self.isProgressElement(e)){
						self.updateHover(null);
					}
				}

				isActive = self.e.volume.body.hasClass('active');
				if (isActive){
					self.e.volume.body.removeClass('active');
				}
			});

			//-----------------------------------------------------------
	
			var progress = controller.e.progress.body;

			progress.on('mousedown', controller, function (event){
				var self = event.data;
				self.e.progress.body.addClass('active');
			});

			//-----------------------------------------------------------

			var volume = controller.e.volume.body;

			volume.on('mousedown', controller, function (event){
				var self = event.data;
				self.e.volume.body.addClass('active');

				var offset = getElementOffset(controller, 'volume', 'body', event);
				var offsetX = offset.x;
				var value = offsetX / offset.width;

				self.command('volume', value);
			});

			//-----------------------------------------------------------

			var draws = controller.e.draws.body;

			draws.on('click', controller, function (event){
				var self = event.data;
				if (!self.canPlay)
					return;

				var video = self.eo.video;

				if (self.isPlaying){
					self.command('pause');
				}else{
					self.command('play');
				}
			});

			//-----------------------------------------------------------

			var rate = controller.e.rate.button;

			rate.on('click', controller, function (event){
				var self = event.data;
				if (!self.canPlay)
					return;
				
				var rateList = self.e.rate.list;

				if (rateList.hasClass('vp-hide'))
					rateList.removeClass('vp-hide');
				else
					rateList.addClass('vp-hide');
			});

			//-----------------------------------------------------------

			var rateList = controller.e.rate.list.find('[vp-value]');

			rateList.on('click', controller, function (event){
				var self = event.data;
				if (!self.canPlay)
					return;

				var e = $(this);
				var value = parseFloat(e.attr('vp-value'));

				self.command('rate', value);

				self.e.rate.list.addClass('vp-hide');
			});

		}

		//-----------------------------------------------------------
		//-----------------------------------------------------------
		//-----------------------------------------------------------

		function captureStream(controller){
			var video = controller.eo.video;

			if (typeof video.mozCaptureStream == 'function'){
				return video.mozCaptureStream();
			}else{
				return video.captureStream();
			}
		}

		function capture(controller){
			var video = controller.eo.video;
			var time = video.currentTime;

			var w = video.videoWidth;
			var h = video.videoHeight;

			var canvas = $('<canvas width="'+w+'" height="'+h+'"/>');
			var ctx = canvas.get(0).getContext('2d');

			ctx.drawImage(video, 0, 0, w, h);

			var src = ctx.canvas.toDataURL('image/jpeg');

			return {
				time: time,
				width: w,
				height: h,
				source: src,
			};
		}

		function captureWithLabel(controller){
			var video = controller.eo.video;

			var time = video.currentTime;
			var data = controller.data.snapshot.map[time];
			var src = null;

			var w = video.videoWidth;
			var h = video.videoHeight;

			if (data){
				src = data.image.source;
			}else{
				var canvas = $('<canvas width="'+w+'" height="'+h+'"/>');
				var ctx = canvas.get(0).getContext('2d');
	
				ctx.drawImage(video, 0, 0, w, h);
	
				src = ctx.canvas.toDataURL('image/jpeg');
			}

			return {
				time: time,
				width: w,
				height: h,
				data: data,
				source: src,
			};
		}
		
		function getLabel(controller, time){
			var data = controller.data.snapshot.map[time];
			if (!data)
				return null;
			
			return {
				time: time,
				width: data.image.width,
				height: data.image.height,
				data: data,
				source: data.image.source,
			};
		}

		function captureForMask(controller){
			var d = capture(controller);
			if (controller.data.mask)
				d['mask'] = controller.data.mask;
			return d;
		}

		function showExternal(controller, name){
			var video = controller.e.video;
			var ext = controller.e.external;

			var sub = vp(ext, name);
			if (sub.length){
				video.addClass('vp-hide');

				var showName = ext.attr('vp-show-item');

				if (showName !== name){
					var old = vp(ext, showName);
					old.addClass('vp-hide');
				}

				ext.attr('vp-show-item', name);

				sub.removeClass('vp-hide');

				if (ext.hasClass('vp-hide'))
					ext.removeClass('vp-hide');
			}
		}

		function hideExternal(controller){
			var video = controller.e.video;
			var ext = controller.e.external;

			var showName = ext.attr('vp-show-item');

			if (showName){
				var old = vp(ext, showName);
				old.addClass('vp-hide');
			}

			ext.addClass('vp-hide');

			video.removeClass('vp-hide');
		}

		//-----------------------------------------------------------
	
	})();

	//-----------------------------------------------------------
	//-----------------------------------------------------------
	//-----------------------------------------------------------

	return _VideoPlayer;
})();