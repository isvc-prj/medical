
var VisualProgress = (function(){

	//-----------------------------------------------------------
	//-----------------------------------------------------------
	//-----------------------------------------------------------

	function _VisualProgress (options){
		if (!options) options = {};
		
		this.selector = options.selector || '#vprog-upload';
		this.e = $(this.selector);
		
		this.data = {
			all: {
				cur: 0,
				total: 0,
			},
		};
		
		this.prog = {
			cur: { 
				view: null,
				bar: null,
			},
			
			all: {
				view: null,
				bar: null,
			},
		};
	}

	_VisualProgress.prototype = (function(){
		function n(e, name) { return e.find('[name="' + name + '"]'); }
		function pe(e, name) { return e.find('.vprog-' + name); }
		
		//-----------------------------------------------------------
		
		return {
			constructor: _VisualProgress,
			
			show: function(){
				var topic = this.e.attr('vprog-topic');
				setProgress(this, topic);
				
				this.e.show();
			},
			
			hide: function(){
				this.e.hide();
			},
			
			topic: function (name){
				this.e.attr('vprog-topic', name);
				setProgress(this, name);
			},
			
			current: function (cur, total){
				var percent = cur / total * 100;
				setProgressData(this.prog.cur.bar, percent);
			},
			
			all: function (cur, total){
				var percent = cur / total * 100;
				setProgressData(this.prog.all.bar, percent);
			},
			
			//-----------------------------------------------------------
			
			setAll: function(total, cur){
				if (!cur) cur = 0;
				
				this.data.all.cur = cur;
				this.data.all.total = total;
			},
			
			nextAll: function(){
				this.data.all.cur++;
				this.all(this.data.all.cur, this.data.all.total);
			},
		};
		
		//-----------------------------------------------------------
		
		function setProgress(controller, name, window){
			var eview = pe(controller.e, name);
			
			var ecur = eview.find('.vprog-current');
			controller.prog.cur.view = ecur;
			controller.prog.cur.bar = getProgressBar(ecur);
			
			var eall = eview.find('.vprog-all');
			controller.prog.all.view = eall;
			controller.prog.all.bar = getProgressBar(eall);
		}
		
		function getProgressBar(e){
			var ebar = e.find('.bar');
			
			if (ebar.length)
				ebar.css('width', '0%');
			
			return ebar;
		}
		
		function setProgressData(e, value){
			e.css('width', parseFloat(value).toFixed(1) + '%');
		}
	})();

	//-----------------------------------------------------------
	//-----------------------------------------------------------
	//-----------------------------------------------------------

	return _VisualProgress;
})();
