
var VisualLoader = (function(){

	//-----------------------------------------------------------
	//-----------------------------------------------------------
	//-----------------------------------------------------------

	function _VisualLoader (options){
		if (!options) options = {};
		
		this.selector = options.selector || '#v-loader';
		this.e = $(this.selector);
	}

	_VisualLoader.prototype = (function(){
		function n(e, name) { return e.find('[name="' + name + '"]'); }
		function pe(e, name) { return e.find('.vprog-' + name); }
		
		//-----------------------------------------------------------
		
		return {
			constructor: _VisualLoader,
			
			show: function(){
				var topic = this.e.attr('vprog-topic');
				
				this.e.show();
			},
			
			hide: function(){
				this.e.hide();
			},
			
			topic: function (name){
				this.e.attr('vprog-topic', name);
			},
		};
		
		//-----------------------------------------------------------
		
	})();

	//-----------------------------------------------------------
	//-----------------------------------------------------------
	//-----------------------------------------------------------

	return _VisualLoader;
})();
