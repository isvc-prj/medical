var MMTerm = {
	SLOW: 100,	
};

var MMSearcher = (function(){
	function n(e, name) { return e.find('[name="' + name + '"]'); }
	function vs(e, name) { return e.find('[vs-name="' + name + '"]'); }
	
	return {
		POST_SEARCH_TERM: 1500, // 1.5초, 0 보다 작으면 필드를 수정했을 떄 search를 진행하지 않음
		
		selector: '#visual-searcher',
		filterSelector: '#filter',
		
		debug: {
			useProgressConsole: false,
		},
		
		e: {
			searchBox: {
				body: null,
				
				filters: null,
			},
			
			filterBox: {
				body: null,
			},
		},
		
		templates: null,

		q: null, // 파라메터로 전달된 값
		data: {},
		
		observers: [],
		
		timeRes: {
			search: null,
		},

		//-----------------------------------------------------------
		
		initialize: function(options){
			if (!options) options = {};

			//-----------------------------------------------------------
			
			this.e.searchBox.body = $(this.selector);
			this.e.searchBox.filters = vs(this.e.searchBox.body, 'filters');
				
			this.e.filterBox.body = $(this.filterSelector);

			//-----------------------------------------------------------
			
			this.templates = new MMTemplates({
				selector: '#visual-searcher-templates',
			});

			//-----------------------------------------------------------
			
			attachFilters(this);
		},

		//-----------------------------------------------------------

		/**
		 * 옵저버 리스너를 등록합니다.
		 */
		observe: function(observer){ this.observers.push(observer); },
		/**
		 * 옵저버 리스너를 제거합니다.
		 */
		stopObserve: function(observer){
			var idx = this.observers.indexOf(observer);
			if (idx >= 0) this.observers.splice(idx, 1);
		},

		/**
		 * 등록된 옵저버에 Notify합니다.
		 * @param {String} topic 토픽
		 * @param {String} [value] 값
		 */
		notifyObservers: function(topic, value){
			var len = this.observers.length;
			for (var i=0; i < len; i++){
				var o = this.observers[i];

				if (typeof o == 'function')
					o(this, topic, value);
				else if (typeof o.styleNotify == 'function')
					o.notifySearcher(this, topic, value);
			}
		},
		
		//-----------------------------------------------------------
		
		enabled: function(){
			return this.e.searchBox.body && this.e.searchBox.body.length > 0;
		},
		
		//-----------------------------------------------------------

		read: function (q){
			if (!this.enabled())
				return;
			
			this.q = q;
			
			console.log('q=', q);
			
			var filter = JSON.parse(q);
			
			console.log('filter=', filter);
			
			this.data = {};
			
			for (var name in filter){
				var val = filter[name];
				
				if (val && $.isArray(val)){
					var e = n(this.e.filterBox.body, name);
					e.prop('checked', false);
					
					var elen = e.length;
					var len = val.length;
					for (var i=0; i < len; i++){
						var itm = val[i];
						
						for (var j=0; j < elen; j++){
							var ce = $(e.get(j));
							if (ce.val() == itm){
								ce.prop('checked', true);
								
								var text = $.trim(ce.parent().text());
								this.push('check', name, {
									text: text,
									value: itm,
								});
								break;
							}
						}
					}
				}else if (val && (typeof val.min != 'undefined' || typeof val.max != 'undefined')){
					var e = n(this.e.filterBox.body, name);
					
					this.push('range', name, {
						checked: true,
						min: val.min,
						max: val.max
					});
					

					e.slider('option','suspended', true);
					e.slider('option', 'values', [val.min, val.max]);
					var valueFormat = e.slider('option', 'valueFormat');
					
					setSlideHandle(e, $('.ui-slider-handle', e), e, valueFormat);
					
					e.slider('option','suspended', false);
					
				}else{
					var e = null;
					if (name == 'title'){
						e = vs(this.e.searchBox.body, 'text-title');
					}else{
						e = n(this.e.filterBox.body, name);
					}
					
					this.push('text', name, val);
					
					e.val(val);
				}
			}
		},
		
		//-----------------------------------------------------------
		
		updateField: function (name){
			console.log('<<UPDATE::FIELD>> ', name);
			
			if (this.POST_SEARCH_TERM >= 0){
				if (this.timeRes.search)
					clearTimeout(this.timeRes.search);
				
				this.timeRes.search = setTimeout(function(){
					clearTimeout(this.timeRes.search);
					this.timeRes.search = null;
					
					this.search();
				}.bind(this), this.POST_SEARCH_TERM);
			}
		},
		
		//-----------------------------------------------------------
		
		search: function(){
			task(this, function(){
				var filter = {};
				var numFilters = 0;
				
				for (var p in this.data){
					var val = this.data[p];
					
					if ($.isArray(val)){
						filter[p] = toValueArray(val);
						numFilters++;
					}else if (val){
						filter[p] = val;
						numFilters++;
					}
				}
				
				console.log('<<SEARCH>> ', filter);
				
				var q = encodeURIComponent(JSON.stringify(filter));
				
				console.log('\t', q);
				
				if (numFilters)
					location.href = '/?q=' + q;
				else
					location.href = '/';
				
			});
		},
		
		//-----------------------------------------------------------
	
		push: function(inputType, name, value){
			var action = '';
			var actionData = null;
			
			switch(inputType){
			case 'check':
				var a = this.data[name];
				if ($.isArray(a)){
					if (indexCheckOf(a, value.value) < 0){
						action = 'add';
						actionData = {
							text: value.text,
							value: value.value,
						};
						
						a.push({
							text: value.text,
							value: value.value,
						});
					}
				}else{
					action = 'add';
					actionData = {
						text: value.text,
						value: value.value,
					};
					
					this.data[name] = [{
						text: value.text,
						value: value.value,
					}];
				}
				break;
			case 'text':
				if (name !== 'title'){
					if (this.data[name]) action = 'modify';
					else action = 'add';
					
					actionData = {
						text: value,
						value: value,
					};
				}
				
				this.data[name] = value;
				break;
			case 'range':
				if (this.data[name]) action = 'modify';
				else action = 'add';
				
				var e = n(this.e.filterBox.body, name);
				var valueFormat = e.slider('option', 'valueFormat');
				var text = '';
				
				if (typeof valueFormat == 'function'){
					text = valueFormat(value.min) + '~' + valueFormat(value.max);
				}else{
					text = value.min + '~' + value.max;
				}
				
				actionData = {
					text: text,
					min: value.min,
					max: value.max,
				};
				
				this.data[name] = { min: value.min, max: value.max };		
				break;
			}
			
			console.log('<<PUSH>>>', this.data);
			
			switch(action){
			case 'add':
				var e = this.templates.get('box').clone();
				e.attr('vs-filter-type', inputType);
				e.attr('vs-filter-name', name);
				
				if (inputType == 'range'){
					e.attr('vs-filter-min', actionData.min);
					e.attr('vs-filter-max', actionData.max);
				}else{
					e.attr('vs-filter-value', actionData.value);
				}
			
				this.templates.gete(e, 'text').text(actionData.text);
				
				var eclose = this.templates.gete(e, 'close');
				
				eclose.click(this, function(event){
					var e = $(this);
					var pe = e.parents('[vs-filter-type]');
					
					var inputType = pe.attr('vs-filter-type');
					var name = pe.attr('vs-filter-name');
					
					if (inputType == 'check'){
						var value = pe.attr('vs-filter-value');
						
						event.data.cancel(inputType, name, value);
					}else{
						event.data.cancel(inputType, name);
					}
				});
				
				e.attr('title', name);
				
				this.e.searchBox.filters.append(e);				
				break;
			case 'modify':
				var e = this.e.searchBox.filters.find('[vs-filter-type="'+inputType+'"][vs-filter-name="'+name+'"]');
				
				if (inputType == 'range'){
					e.attr('vs-filter-min', actionData.min);
					e.attr('vs-filter-max', actionData.max);
				}else{
					e.attr('vs-filter-value', actionData.value);
				}
				
				this.templates.gete(e, 'text').text(actionData.text);
				break;
			}
			
			return {
				action: action,
				actionData: actionData,
			}
		},
		
		pop: function(inputType, name){
			if (inputType == 'check'){
				var value = arguments[2];
				
				var a = this.data[name];
				if ($.isArray(a)){
					var seeked = -1;
					if ((seeked = indexCheckOf(a, value)) >= 0){
						a.splice(seeked, 1);
					}
				}
				
				if (a.length == 0)
					delete this.data[name];
				
				this.e.searchBox.filters.find('[vs-filter-type="'+inputType+'"][vs-filter-name="'+name+'"][vs-filter-value="'+value+'"]').remove();
			}else{
				delete this.data[name];
				
				if (name !== 'title'){
					this.e.searchBox.filters.find('[vs-filter-type="'+inputType+'"][vs-filter-name="'+name+'"]').remove();
				}
			}
			
			console.log('<<POP>>>', this.data);
		},
		
		pushField: function(inputType, name, value){
			var r = this.push(inputType, name, value);
			if (r && r.action === 'add'){
				if (this.e.searchBox.filters.length)
					this.e.searchBox.filters.animate({
						scrollTop: this.e.searchBox.filters.get(0).scrollHeight,
					}, 500);
			}
			
			this.updateField(name);
		},
		
		popField: function(inputType, name){
			this.pop.apply(this, arguments);
			this.updateField(name);
		},
	
		cancel: function (inputType, name, value){
			this.popField(inputType, name, value);
			
			if (inputType == 'check'){
				var e = n(this.e.filterBox.body, name);
				e.each(function(i, item){
					var eitem = $(item);
					if (eitem.val() == value){
						eitem.prop('checked', false);
					}
				});
				
			}else if (inputType == 'range'){
				var e = n(this.e.filterBox.body, name);
				var min = e.slider('option', 'min');
				var max = e.slider('option', 'max');
				
				e.slider('option','suspended', true);
				e.slider('option', 'values', [min, max]);
				//e.slider('option', 'slide');
				var valueFormat = e.slider('option', 'valueFormat');
				
				resetSlideHandle(e, $('.ui-slider-handle', e), e, valueFormat);
				e.slider('option','suspended', false);
				
			}else{
				n(this.e.filterBox.body, name).val('');
			}
			
			console.log('<<CANCEL>>>', this.data);
		},
	};

	//-----------------------------------------------------------
	//-----------------------------------------------------------
	//-----------------------------------------------------------
	
	
	//-----------------------------------------------------------
	
	function indexCheckOf(a, value){
		var len = a ? a.length : 0;
		for (var i=0; i < len; i++){
			var d = a[i];
			if (d.value == value)
				return i;
		}
		return -1;
	}
	
	function toValueArray(a){
		var r = [];
		var len = a ? a.length : 0;
		for (var i=0; i < len; i++){
			r.push(a[i].value);
		}
		return r;
	}
	
	function arrayToString(a){
		var r = [];
		var len = a ? a.length : 0;
		for (var i=0; i < len; i++){
			r.push(a[i].value);
		}
		return r.join(',');
	}
	
	//-----------------------------------------------------------
	
	function task(constroller, func, timeOut){
		if (!timeOut) timeOut = 0;
		setTimeout(func.bind(constroller), timeOut);
	}
	
	//-----------------------------------------------------------
	
	function attachFilters(controller){
		
		//-----------------------------------------------------------
		// 버튼 연결
		
		vs(controller.e.searchBox.body, 'btn-search').click(controller, function(event){
			event.data.search();
		});
		
		//-----------------------------------------------------------
		// 제목

		var titleInput = vs(controller.e.searchBox.body, 'text-title');
		titleInput.keyup(controller, function(event){
			if (event.keyCode == 13){
				var self = event.data;
				var e = $(this);
				
				if (e.attr('vs-changed') !== 'true')
					self.pushField('text', 'title', e.val());
				
				//console.log('<<<TITLE>>> ENTER');
				
				self.search();
			}
		});
		titleInput.change(controller, function(event){
			var self = event.data;
			var e = $(this);
			
			e.attr('vs-changed', 'true');
			
			self.pushField('text', 'title', e.val());
		});
		titleInput.blur(controller, function(event){
			var self = event.data;
			var e = $(this);
			
			e.attr('vs-changed', 'false');
		});
	
		//-----------------------------------------------------------
		// 환자 나이
		
		slider(controller, 'pat_age', 0, 200);
		
		//-----------------------------------------------------------
		// 환자 체중

		slider(controller, 'pat_weight', 0, 500);
		
		//-----------------------------------------------------------
		// 환자 신장
		
		slider(controller, 'pat_height', 0, 300);
		
		//-----------------------------------------------------------
		// 맹장 도달 시간
	
		slider(controller, 'appx_reach_time', 0, 900, 1, durationFormat);
		
		//-----------------------------------------------------------
		// 맹장 회수 시간
	
		slider(controller, 'appx_remove_time', 0, 900, 1, durationFormat);
		
		//-----------------------------------------------------------
		// 전체 검사 시간
	
		slider(controller, 'appx_total_time', 0, 900, 1, durationFormat);
		
		//-----------------------------------------------------------
		// 용종발견 개수
	
		slider(controller, 'polyp_cnt', 0, 100);
		
		//-----------------------------------------------------------
		// 용종크기
	
		slider(controller, 'polyp_size', 0, 100);
		
		//-----------------------------------------------------------
		// 체크 박스

		var chks = controller.e.filterBox.body.find('input[name][type="checkbox"]');
		chks.change(controller, function(event){
			var self = event.data;
			var e = $(this);
			var name = e.attr('name');
			var checked = e.is(':checked');
			var text = $.trim(e.parent().text());
			
			console.log('<<<CHECKBOX>>> NAME=', name,' VALUE=', e.val(), ', CHECKED=', checked, ', TEXT=', text);
			
			if (checked){
				controller.pushField('check', name, {
					value: $.trim(e.val()),
					text: text,
				});
			}else{
				controller.popField('check', name, $.trim(e.val()));
			}
		});
		
		//-----------------------------------------------------------
		// 입력 상자

		var textInputs = controller.e.filterBox.body.find('input[name][type="text"]');
		textInputs.change(controller, function(event){
			var self = event.data;
			var e = $(this);
			var name = e.attr('name');
			
			console.log('<<<TEXTBOX>>> NAME=', name,' VALUE=', e.val());
			
			var value = $.trim(e.val());
			if (value){
				controller.pushField('text', name, value);
			}else{
				controller.popField('text', name);
			}				
		});
	}
	
	//-----------------------------------------------------------
	
	function slider(controller, type, min, max, step, valueFormat, changeValueFormat){
		if (typeof step != 'number') step = 1;
		
		n(controller.e.filterBox.body,type).slider({
			range: true,
			min: min,
			max: max,
			step: step,
			valueFormat: valueFormat,
			changeValueFormat: changeValueFormat,
			suspended: false,
			
			values: [ min, max ],
			
			slide: function( event, ui ) {
				slide(controller, type, event, ui, valueFormat);
			},
			
			create: function(event, ui){
				createSlide(controller, type, event, ui, valueFormat);
			},
			
			change: function (event, ui){
				changeSlide(controller, type, event, ui, changeValueFormat);
			},
		});
	}
	
	//-----------------------------------------------------------
	
	function createSlide(controller, type, event, ui, valueFormat){
		var owner = n(controller.e.filterBox.body,type);
		var target = $(event.target);
		var handle = target.find('.ui-slider-handle');
		
		resetSlideHandle(owner, handle, target, valueFormat);
	}
	
	function slide(controller, type, event, ui, valueFormat){
		var owner = n(controller.e.filterBox.body,type);
		var target = $(event.target);
		var handle = target.find('.ui-slider-handle');
		
		handle.each(function(i,e){
			var tooltip = target.find('.tooltip').eq(i);
			var left = $(e).position().left - 28;
			var val = owner.slider('values',i);
			
			if (typeof valueFormat == 'function') val = valueFormat(val);
			
			tooltip.css('left', left);
			tooltip.text(val);
		});
	}
	
	function changeSlide(controller, type, event, ui, valueFormat){
		var owner = n(controller.e.filterBox.body,type);
		var target = $(event.target);
		var handle = target.find('.ui-slider-handle');

		if (owner.slider('option','suspended') === true)
			return;
		
		var min = 0, max = 0;
		
		handle.each(function(i,e){
			var val = owner.slider('values',i);
			
			if (typeof valueFormat == 'function') val = valueFormat(val);
			
			if (i == 0) min = val;
			else if (i > 0) max = val;
		});
		
		console.log('<<<CHANGE::SLIDER>>> (',type,') min=', min,', max=', max);
		
		var limitMin = owner.slider('option', 'min');
		var limitMax = owner.slider('option', 'max');
		
		if (min === limitMin && max === limitMax)
			controller.popField('range', type);
		else
			controller.pushField('range', type, { checked: true, min: min, max: max });
	}
	
	//-----------------------------------------------------------
	
	function resetSlideHandle(owner, handle, target, valueFormat){
		handle.each(function(i,e){
			var tooltip = target.find('.tooltip').eq(i);
			var val = owner.slider('values',i); 
			
			if (typeof valueFormat == 'function') val = valueFormat(val);
			
			if (i == 0){
				left = 'calc(0% - 28px)';
			}else{
				left = 'calc(100% - 28px)';
			}
			
			tooltip.css('left', left);
			tooltip.text(val);
		});
	}
	
	function setSlideHandle(owner, handle, target, valueFormat){
		var min = owner.slider('option', 'min');
		var max = owner.slider('option', 'max');
		
		handle.each(function(i,e){
			var tooltip = target.find('.tooltip').eq(i);
			var val = owner.slider('values',i); 
			
			if (typeof valueFormat == 'function') val = valueFormat(val);
			
			var p = ((val - min) / max * 100).toFixed(2);
			
			left = 'calc('+p+'% - 28px)';
			
			tooltip.css('left', left);
			tooltip.text(val);
		});
	}

})();
