var MMTemplates = (function(){
	function defined(a, b){ return typeof a != 'undefined' ? a : b; }
	function definedBoolean(a, b){ return typeof a == 'boolean' ? a : b; }
	function definedFunction(a, b){ return typeof a == 'function' ? a : b; }

	function template(e, name) { return e.children('[t-template="' + name + '"]'); }
	function t(e, name) { return e.find('[t-name="' + name + '"]'); }
	
	function eo(e) { return e.length ? e.get(0) : null; }
	function is(e, te) { return eo(e) == eo(te); }
	function has(e, te) {
		if (is(e, te)) return true;
		return e.has(te).length > 0;
	}

	function hasAttr(e, name){
		if (e.length){
			return e.get(0).hasAttribute(name);
		}
		return false;
	}

	//-----------------------------------------------------------
	//-----------------------------------------------------------
	//-----------------------------------------------------------
	
	function _MMTemplates(options){
		if (!options) options = {};

		//-----------------------------------------------------------
		
		this.selector = options.selector;

		this.e = $(this.selector);

		//-----------------------------------------------------------
		
		this.templates = {};
	};
	
	_MMTemplates.prototype = (function(){
		return {
			constructor: _MMTemplates,
		
			//-----------------------------------------------------------

			initialize: function(){
			},
			
			//-----------------------------------------------------------
			
			get: function (name){
				if (!this.templates[name]){
					var e = template(this.e, name);
					if (e.length){
						this.templates[name] = e.children(':eq(0)').clone();
					}
				}
				return this.templates[name];
			},
			
			//-----------------------------------------------------------

			gete: function (e, name){
				return t(e, name);
			},
		};
	})();
	
	//-----------------------------------------------------------
	//-----------------------------------------------------------
	//-----------------------------------------------------------
	
	return _MMTemplates;
})();