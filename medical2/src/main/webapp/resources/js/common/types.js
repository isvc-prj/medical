var MVLabels = {
	// 용종 위치
	POLYP_POS: ['', 'Cecum', 'A colon', 'HF', 'T colon', 'SF', 'D colon', 'S colon', 'Rectum'],
	// 용종 모양
	POLYP_SHAPE: ['', 'Yamada type 1', 'Yamada type 2', 'Yamada type 3', 'Yamada type 4'],
	// 용종 조직검사
	POLYP_BIO: ['Nonspecifc', 'Hyperplastic', 'TA LG', 'TA MG', 'TA HG', 'Cancer', 'Serrated polyp', 'other'],
	
	//-----------------------------------------------------------

	PAT_GEN: { M: '남자', W: '여자'},
	CFS_IX: ['', 'screening', 'CRN f/u', 'bowel habit change', 'abdominal pain', 'blood in stool (M/H/FOB)', 'anemia', 'tenesmus', 'wt. loss', 'CRC FGx', 'CPP/EMR', 'others'],
	EXCLUSION: ['없음', '대장 절제술', '이전에 염증성 장질환을 진단', '용종증(polyposis syndrome)이나 유전적 비용종증 직장결장암의 과거력', '동반된 심한 내과적 또는 외과적 질환', '임신, 수유 중이거나 부인과 질환', '기타 본 연구에 참여가 부적절하다고 연구자가 판단한 자'],
	HISTORY: ['없음', '저위험군', '고위험군'],
	ENDOSCOPY_STAT: ['없음', '있음'],
	
	//-----------------------------------------------------------

	APPENDIX_REACH: ['실패', '성공'],
	RIGHT_COLON: { '-1': 'N/A', 0: 0, 1: 1, 2: 2, 3: 3 },
	TRANS_COLON: { '-1': 'N/A', 0: 0, 1: 1, 2: 2, 3: 3 },
	LEFT_COLON: { '-1': 'N/A', 0: 0, 1: 1, 2: 2, 3: 3 },

	CFS_OPI: ['', '정상', '과형성용종', '선종', '대장암', '게실', 'IBD', '기타'],
	
};