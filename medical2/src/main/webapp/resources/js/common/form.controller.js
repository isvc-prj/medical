
//-----------------------------------------------------------
//-----------------------------------------------------------
//-----------------------------------------------------------
// Form Validator
//-----------------------------------------------------------
//-----------------------------------------------------------
//-----------------------------------------------------------

var FormValidator = (function(){

	//-----------------------------------------------------------
	//-----------------------------------------------------------
	//-----------------------------------------------------------

	function _FormValidator (options){
		if (!options) options = {};
		
		this.callbacks = {
			msgBox: options.msgBox,	
		};
		
		this.e = options.form;
	}

	_FormValidator.prototype = (function(){
		function n(e, name) { return e.find('[name="' + name + '"]'); }
		
		//-----------------------------------------------------------
		
		return {
			constructor: _FormValidator,
			
			date: function(name, title, type){
				var e = n(this.e, name);
				var value = $.trim(e.val());
				if (!value){
					msgBox(this, title + '을(를) 입력하세요');
					return { type: type, e: e };
				}
				return true;
			},
			
			text: function(name, title, type){
				var e = n(this.e, name);
				var value = $.trim(e.val());
				if (!value){
					msgBox(this, title + '을(를) 입력하세요');
					return { type: type, e: e };
				}
				return true;
			},
			
			number: function(name, title, type){
				var r = checkNumber(this,name);
				if (r !== true){
					if (r.result === 'nan'){
						msgBox(this, title + '은(는) 숫자로 입력하세요');
						return { type: type, e: r.e };
					}
					if (r.result === 'max'){
						msgBox(this, title + '은(는) '+r.limit+'보다 작거나 같은 숫자여야 합니다');
						return { type: type, e: r.e };
					}
					if (r.result === 'min'){
						msgBox(this, title + '은(는) '+r.limit+'보다 크거나 같은 숫자여야 합니다');
						return { type: type, e: r.e };
					}
					
					msgBox(this, title + '을(를) 입력하세요');
					return { type: type, e: r.e };
				}
				
				return true;
			},
			
			time: function(minName, secName, title, type){
				var rmin = checkNumber(this,minName);
				var rsec = checkNumber(this,secName);
				
				if (rmin !== true || rsec !== true){
					if (rmin.result == 'empty' && rsec.result == 'empty'){
						msgBox(this, title + '을(를) 입력하세요');
						return { type: type, e: rmin.e };
					}
					
					if (rmin !== true){					
						if (rmin.result === 'nan'){
							msgBox(this, title + '은(는) 숫자로 입력하세요');
							return { type: type, e: rmin.e };
						}
						if (rmin.result === 'max'){
							msgBox(this, title + '은(는) '+rmin.limit+'보다 작거나 같은 숫자여야 합니다');
							return { type: type, e: rmin.e };
						}
						if (rmin.result === 'min'){
							msgBox(this, title + '은(는) '+rmin.limit+'보다 크거나 같은 숫자여야 합니다');
							return { type: type, e: rmin.e };
						}
					}
					
					if (rsec !== true){					
						if (rsec.result === 'nan'){
							msgBox(this, title + '은(는) 숫자로 입력하세요');
							return { type: type, e: rsec.e };
						}
						if (rsec.result === 'max'){
							msgBox(this, title + '은(는) '+rsec.limit+'보다 작거나 같은 숫자여야 합니다');
							return { type: type, e: rsec.e };
						}
						if (rsec.result === 'min'){
							msgBox(this, title + '은(는) '+r.limit+'보다 크거나 같은 숫자여야 합니다');
							return { type: type, e: rsec.e };
						}
					}
					
					if (rmin !== true && rsec !== true){
						msgBox(this, title + '을(를) 입력하세요');
						return { type: type, e: rmin.e };
					}
				}
				
				return true;
			},
			
			radio: function(name, title, type){
				var e = n(this.e, name);
				
				var value = getCheckInput(e);
				if (!value){
					msgBox(this, title + '을(를) 선택하세요');
					return { type: type, e: e };
				}
				return true;
			},
		};
		
		//-----------------------------------------------------------

		function getCheckInput(e){
			var len = e ? e.length : 0;
			for (var i=0; i < len; i++){
				var ce = $(e.get(i));
				
				if (ce.is(':checked'))
					return ce.val();
			}
			return null;
		}
		
		function msgBox(controller, s){
			if (typeof controller.callbacks.msgBox == 'function')
				controller.callbacks.msgBox(s);
		}
		
		function checkNumber(controller, name){
			var e = n(controller.e, name);
			var value = $.trim(e.val());
			if (!value) return { result: 'empty', e: e };
			
			if (!VpCommon.isNumber(value)) return { result: 'nan', e: e };
			
			value = parseFloat(value);
			
			var max = $.trim(e.attr('max'));
			var min = $.trim(e.attr('min'));
			
			if (max){
				max = parseFloat(max);
				if (value > max) return { result: 'max', e: e, limit: max };
			}
			
			if (min){
				min = parseFloat(min);
				if (value < min) return { result: 'min', e: e, limit: min };
			}
			
			return true;
		}
		
	})();

	//-----------------------------------------------------------
	//-----------------------------------------------------------
	//-----------------------------------------------------------

	return _FormValidator;
})();


//-----------------------------------------------------------
//-----------------------------------------------------------
//-----------------------------------------------------------
// Form Reader
//-----------------------------------------------------------
//-----------------------------------------------------------
//-----------------------------------------------------------

var FormReader = (function(){

	//-----------------------------------------------------------
	//-----------------------------------------------------------
	//-----------------------------------------------------------

	function _FormReader (options){
		if (!options) options = {};
		
		this.e = options.form;
	}

	_FormReader.prototype = (function(){
		function n(e, name) { return e.find('[name="' + name + '"]'); }
		
		//-----------------------------------------------------------
		
		return {
			constructor: _FormReader,
			
			date: function(name){
				var e = n(this.e, name);
				return $.trim(e.val()).replace(/[-\/\.]/g,'');
			},
			
			text: function(name){
				var e = n(this.e, name);
				return $.trim(e.val());
			},
			
			number: function (name, defaultValue){
				if (typeof defaultValue == 'undefined') defaultValue = 0;
				var e = n(this.e, name);
				var value = $.trim(e.val());
				if (!value) return defaultValue;
				value = parseFloat(value);
				return value;
			},
			
			time: function(minName, secName){
				var rmin = this.number(minName);
				var rsec = this.number(secName);
				
				if (isNaN(rmin)) rmin = 0;
				if (isNaN(rsec)) rsec = 0;
				
				return parseInt(rmin) * 60 + parseInt(rsec);
			},
			
			radio: function(name){
				var e = n(this.e, name);
				return getCheckInput(e);
			},
			
			radioNumber: function(name, defaultValue){
				if (typeof defaultValue == 'undefined') defaultValue = 0;
				var e = n(this.e, name);
				var value = getCheckInput(e);
				if (!value) return defaultValue;
				return parseFloat(value);
			},
		};
		
		//-----------------------------------------------------------

		function getCheckInput(e){
			var len = e ? e.length : 0;
			for (var i=0; i < len; i++){
				var ce = $(e.get(i));
				
				if (ce.is(':checked'))
					return ce.val();
			}
			return null;
		}
	})();

	//-----------------------------------------------------------
	//-----------------------------------------------------------
	//-----------------------------------------------------------

	return _FormReader;
})();


//-----------------------------------------------------------
//-----------------------------------------------------------
//-----------------------------------------------------------
// Form Writer
//-----------------------------------------------------------
//-----------------------------------------------------------
//-----------------------------------------------------------

var FormWriter = (function(){
	function n(e, name) { return e.find('[name="' + name + '"]'); }

	//-----------------------------------------------------------
	//-----------------------------------------------------------
	//-----------------------------------------------------------

	function _FormWriter (options){
		if (!options) options = {};
		
		this.e = options.form;
	}

	_FormWriter.prototype = (function(){
		function n(e, name) { return e.find('[name="' + name + '"]'); }
		function tf(value) { return value < 10 ? '0' + value : value; }
	
		//-----------------------------------------------------------
		
		return {
			constructor: _FormWriter,
			
			date: function(name, value){
				var e = n(this.e, name);
				
				if (value && value.length >= 8){
					var y = value.substr(0, 4);
					var m = value.substr(4, 2);
					var d = value.substr(6, 2);
					
					value = y + '-' + m + '-' + d;
				}
				
				e.val(value);
			},
			
			text: function(name, value){
				var e = n(this.e, name);
				e.val(value);
			},
			
			number: function (name, value){
				var e = n(this.e, name);
				e.val(value);
			},
			
			time: function(minName, secName, value){
				var emin = n(this.e, minName);
				var esec = n(this.e, secName);
				
				value = parseFloat(parseFloat(value).toFixed(0));

				var m = (value / 60) % 60;
				var s = value % 60;

				m = Math.floor(m);
				s = Math.ceil(s);
				
				emin.val(tf(m));
				esec.val(tf(s));
			},
			
			radio: function(name, value){
				var e = n(this.e, name);
				return setCheckInput(e, value);
			},
			
			radioNumber: function(name, value){
				var e = n(this.e, name);
				return setCheckInputNumber(e, value);
			},
		};

		//-----------------------------------------------------------
		
		function setCheckInput(e, val){
			var len = e ? e.length : 0;
			for (var i=0; i < len; i++){
				var ce = $(e.get(i));
				
				//console.log('<<<CHECK>>>', ce.val(), ' ::> ', ce.val() === val);
				
				ce.prop('checked', ce.val() === val);
			}
		}
		
		function setCheckInputNumber(e, val){
			if (!val) val = 0;
			else if (isNaN(val)) val = 0;
			else val = parseFloat(val);
			
			var len = e ? e.length : 0;
			for (var i=0; i < len; i++){
				var ce = $(e.get(i));
				var itemVal = parseFloat(ce.val());
				
				//console.log('<<<CHECK>>>', itemVal, ' ::> ', itemVal === val);
				
				ce.prop('checked', itemVal === val);
			}
		}
		
	})();

	//-----------------------------------------------------------
	//-----------------------------------------------------------
	//-----------------------------------------------------------

	return _FormWriter;
})();


//-----------------------------------------------------------
//-----------------------------------------------------------
//-----------------------------------------------------------
// Form Text Writer
//-----------------------------------------------------------
//-----------------------------------------------------------
//-----------------------------------------------------------

var FormTextWriter = (function(){
	function n(e, name) { return e.find('[name="' + name + '"]'); }

	//-----------------------------------------------------------
	//-----------------------------------------------------------
	//-----------------------------------------------------------

	function _FormTextWriter (options){
		if (!options) options = {};
		
		this.e = options.form;
	}

	_FormTextWriter.prototype = (function(){
		function n(e, name) { return e.find('[name="' + name + '"]'); }
		function tf(value) { return value < 10 ? '0' + value : value; }
	
		//-----------------------------------------------------------
		
		return {
			constructor: _FormTextWriter,
			
			date: function(name, value){
				var e = n(this.e, name);
				
				if (value && value.length >= 8){
					var y = value.substr(0, 4);
					var m = value.substr(4, 2);
					var d = value.substr(6, 2);
					
					value = y + '-' + m + '-' + d;
				}
				
				e.text(value);
			},
			
			text: function(name, value, suffix){
				var e = n(this.e, name);
				e.text(value + (suffix ? suffix : ''));
			},
			
			number: function (name, value, suffix){
				var e = n(this.e, name);
				e.text(value + (suffix ? suffix : ''));
			},
			
			time: function(name, value){
				var e = n(this.e, name);
				
				value = parseFloat(parseFloat(value).toFixed(0));

				var m = (value / 60) % 60;
				var s = value % 60;

				m = Math.floor(m);
				s = Math.ceil(s);
				
				e.text(tf(m) + ':' + tf(s));
			},
			
			radio: function(name, value){
				var e = n(this.e, name);
				
				var a = MVLabels[name.toUpperCase()];
				if (a){
					e.text(a[value]);
				}else{
					e.text(value);
				}
			},
			
			radioNumber: function(name, value){
				var e = n(this.e, name);
				
				value = parseFloat(value);
				
				var a = MVLabels[name.toUpperCase()];
				if (a){
					e.text(a[value]);
				}else{
					e.text(value);
				}
			},
		};
		
	})();

	//-----------------------------------------------------------
	//-----------------------------------------------------------
	//-----------------------------------------------------------

	return _FormTextWriter;
})();
