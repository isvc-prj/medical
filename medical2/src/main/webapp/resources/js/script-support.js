function durationFormat(duration){
    duration = Math.floor(duration);
    var min = Math.floor(duration / 60);
    var sec = duration % 60;
    if(min<10) min = '0' + min;
    if(sec<10) sec = '0' + sec;
    return min + ':' + sec;
}