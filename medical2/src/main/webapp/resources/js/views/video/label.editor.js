var MMVideoLabelEditor = (function(){
	function defined(a, b){ return typeof a != 'undefined' ? a : b; }
	function definedBoolean(a, b){ return typeof a == 'boolean' ? a : b; }
	function definedNumber(a, b){ return typeof a == 'number' ? a : b; }
	function definedFunction(a, b){ return typeof a == 'function' ? a : b; }

	function f(e, name) { return e.find('[vl-form="' + name + '"]'); }
	function n(e, name) { return e.find('[name="' + name + '"]'); }
	
	function eo(e) { return e.length ? e.get(0) : null; }
	function is(e, te) { return eo(e) == eo(te); }
	function has(e, te) {
		if (is(e, te)) return true;
		return e.has(te).length > 0;
	}

	//-----------------------------------------------------------
	//-----------------------------------------------------------
	//-----------------------------------------------------------

	function _MMVideoLabelEditor (options){
		if (!options) options = {};

		//-----------------------------------------------------------

		this.selector = options.selector || '#video-label-form';

		var e = $(this.selector);
		
		this.e = {
			body: e,
			
			pos: f(e, 'polyp_pos').find('input'),
			size: f(e, 'polyp_size').find('input'),
			shape: f(e, 'polyp_shape').find('input'),
			bio: f(e, 'polyp_bio').find('input'),
		};

		this.regex_number = /^[+-]{0,1}(\d+\.\d+|\d+\.|\d+)$/g;

		//-----------------------------------------------------------

		this.observers = [];

		//-----------------------------------------------------------
		
		this.initialize();
	}
	
	_MMVideoLabelEditor.prototype = (function(){
		return {
			constructor: _MMVideoLabelEditor,
			
			//-----------------------------------------------------------
			
			initialize: function(){
				this.enable(false);
				
				this.e.pos.click(this, function(event){
					var self = event.data;
					
					var d = {
						name: 'polyp_pos',
						value: getCheckInput(self.e.pos),
					};
					
					self.notifyObservers('input', d);
				});
				this.e.size.change(this, function(event){
					var self = event.data;
					
					var r = $.trim(self.e.size.val());
					if (r !== null && r !== ''){
						if (!VpCommon.isNumber(r))
							r = Number.NaN;
					}else{
						r = null; 
					}
					
					var d = {
						name: 'polyp_size',
						value: r,
					};
					
					self.notifyObservers('input', d);
				});
				this.e.shape.click(this, function(event){
					var self = event.data;
					
					var d = {
						name: 'polyp_shape',
						value: getCheckInput(self.e.shape),
					};
					
					self.notifyObservers('input', d);
				});
				this.e.bio.click(this, function(event){
					var self = event.data;
					
					var d = {
						name: 'polyp_bio',
						value: getCheckInput(self.e.bio),
					};
					
					self.notifyObservers('input', d);
				});
			},

			//-----------------------------------------------------------

			/**
			 * 옵저버 리스너를 등록합니다.
			 */
			observe: function(observer){ this.observers.push(observer); },
			/**
			 * 옵저버 리스너를 제거합니다.
			 */
			stopObserve: function(observer){
				var idx = this.observers.indexOf(observer);
				if (idx >= 0) this.observers.splice(idx, 1);
			},

			/**
			 * 등록된 옵저버에 Notify합니다.
			 * @param {String} topic 토픽
			 * @param {String} [value] 값
			 */
			notifyObservers: function(topic, value){
				var len = this.observers.length;
				for (var i=0; i < len; i++){
					var o = this.observers[i];

					if (typeof o == 'function')
						o(this, topic, value, this.shape);
					else if (typeof o.notifyLabelEditor == 'function')
						o.notifyLabelEditor(this, topic, value, this.shape);
				}
			},
			
			//-----------------------------------------------------------
			
			enable: function(enabled){
				this.e.pos.prop('disabled', !enabled);
				this.e.size.prop('disabled', !enabled);
				this.e.shape.prop('disabled', !enabled);
				this.e.bio.prop('disabled', !enabled);
			},
			
			//-----------------------------------------------------------
			
			validate: function(attrs){
				var r = null;
				
				r = defined(attrs['polyp_pos'],null);
				if (r === null)
					throw new Error('용종 위치를 선택하세요');

				r = defined(attrs['polyp_size'],null);
				if (isNaN(r))
					throw new Error('용종 크기를 숫자로 입력하세요');

				if (r === null)
					throw new Error('용종 크기를 입력하세요');
				
				r = defined(attrs['polyp_shape'],null);
				if (r === null)
					throw new Error('용종 모양을 선택하세요');
				
				r = defined(attrs['polyp_bio'],null);
				if (r === null)
					throw new Error('용종 조직검사를 선택하세요');
				
				return true;
			},
			
			//-----------------------------------------------------------
			
			get: function(){
				var attr = null, r = null;
	
				var grpPolypPos = this.e.pos;
				var grpPolypSize = this.e.size;
				var grpPolypShape = this.e.shape;
				var grpPolypBio = this.e.bio;
				
				r = getCheckInput(grpPolypPos);
				if (r !== null){
					if (!attr) attr = {};
					attr['polyp_pos'] = r;
				}else{
					throw new Error('용종 위치를 선택하세요');
				}
	
				r = $.trim(grpPolypSize.val());
				if (r !== null && r !== ''){
					if (!VpCommon.isNumber(r))
						throw new Error('용종 크기를 숫자로 입력하세요');
	
					if (!attr) attr = {};
					attr['polyp_size'] = r;
				}else{
					throw new Error('용종 크기를 입력하세요');
				}
	
				r = getCheckInput(grpPolypShape);
				if (r !== null){
					if (!attr) attr = {};
					attr['polyp_shape'] = r;
				}else{
					throw new Error('용종 모양을 선택하세요');
				}
	
				r = getCheckInput(grpPolypBio);
				if (r !== null){
					if (!attr) attr = {};
					attr['polyp_bio'] = r;
				}else{
					throw new Error('용종 조직검사를 선택하세요');
				}
				
				return attr;
			},
			
			set: function(attr){
				this.e.body.parent().scrollTop(0);
				
				var grpPolypPos = this.e.pos;
				var grpPolypSize = this.e.size;
				var grpPolypShape = this.e.shape;
				var grpPolypBio = this.e.bio;
				
				if (attr){
					if (attr.hasOwnProperty('polyp_pos')){
						setCheckInput(grpPolypPos, '' + attr['polyp_pos']);
					}else{
						resetCheckInput(grpPolypPos);
					}
					
					if (attr.hasOwnProperty('polyp_size')){
						grpPolypSize.val(attr['polyp_size']);
					}else{
						grpPolypSize.val('');
					}
	
					if (attr.hasOwnProperty('polyp_shape')){
						setCheckInput(grpPolypShape, '' + attr['polyp_shape']);
					}else{
						resetCheckInput(grpPolypShape);
					}
	
					if (attr.hasOwnProperty('polyp_bio')){
						setCheckInput(grpPolypBio, '' + attr['polyp_bio']);
					}else{
						resetCheckInput(grpPolypBio);
					}
				}else{
					resetCheckInput(grpPolypPos);
					grpPolypSize.val('');
					resetCheckInput(grpPolypShape);
					resetCheckInput(grpPolypBio);
				}
			},
			
			setText: function (name, value){
				var e = f(this.e.body, name);
				if (e && e.length){
					e.text(value);
				}
			},
		};
		
	
		//-----------------------------------------------------------
		//-----------------------------------------------------------
		//-----------------------------------------------------------
			
		function resetCheckInput(e){
			var len = e ? e.length : 0;
			for (var i=0; i < len; i++){
				var ce = $(e.get(i));
				
				ce.prop('checked', false);
			}
		}
	
		function setCheckInput(e, val){
			var len = e ? e.length : 0;
			for (var i=0; i < len; i++){
				var ce = $(e.get(i));
				
				ce.prop('checked', ce.val() === val);
			}
		}
	
		function getCheckInput(e){
			var len = e ? e.length : 0;
			for (var i=0; i < len; i++){
				var ce = $(e.get(i));
				
				if (ce.is(':checked'))
					return ce.val();
			}
			return null;
		}
	})();

	//-----------------------------------------------------------
	//-----------------------------------------------------------
	//-----------------------------------------------------------

	return _MMVideoLabelEditor;
})();
