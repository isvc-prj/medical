var MMMediaType = {
	VIDEO: 'V',
	IMAGE: 'I',
};

var MMTerm = {
	SLOW: 100,	
};

var MMViewer = (function(){
	return {
		selector: '#view',
		imageViewerSelector: '#vie',
		
		mediaType: MMMediaType.VIDEO,
		
		debug: {
			useProgressConsole: false,
		},
		
		e: {
			body: null,
			snapshots: null,
			
			snapshotSearch: null,
		},
		
		buttons: {
			remove: null,
		},
		
		video: {
			player: null,
		},
		
		image: {
			viewer: null,
			snapshots: [],
		},
		
		data: {
			key: null,
			serverData: null,
		},
		
		templates: null,
		
		initialize: function(){
			this.e.body = $(this.selector);

			this.e.snapshots = mv(this.e.body, 'snapshot-list');
			
			this.e.snapshotSearch = mv(this.e.body, 'snapshot-search');
		
			this.buttons.remove = mv(this.e.body, 'remove');
			
			//-----------------------------------------------------------
			
			this.image.viewer = $(this.imageViewerSelector);
			
			//-----------------------------------------------------------
			
			this.templates = new MMTemplates({
				selector: '#video-view-templates',
			});

			//-----------------------------------------------------------

			this.video.player = new VideoPlayer({
			});

			//-----------------------------------------------------------

			this.video.player.observe(function(sendor, topic, value){
				var vp = this.video.player;
				
				//console.log('<<PLAYER>>> ', topic, value);
				
				if (topic === 'event:fired'){
					if (value.type === 'canplay'){
						
					}
				}
			}.bind(this));

			//-----------------------------------------------------------		
			
			attachEvents(this);
			
		},

		//-----------------------------------------------------------
		
		msgBox: function(s){
			console.log(s);
			alert(s);
		},

		//-----------------------------------------------------------
		
		remove: function(){
			return new Promise(function(resolve, reject){
				if (confirm('삭제하시겠습니까?')){
					if (this.mediaType === MMMediaType.VIDEO)
						this.video.player.close();
					
					task(this, function(){
						VpCommon.ajax({
							title: '미디어 삭제',
							url: '/api/media/remove',
							data: {
								uid: this.data.key,
							},
							success: function(r){
								resolve(true);
							},
							error: function (e){
								reject(e);
							}
						});
					});
				}else{
					resolve(false);
				}
			}.bind(this))
				.then(function(r){
					if (r === true){
						if (typeof MMSearcher != 'undefined' && MMSearcher.q){
							location.replace('/?q=' + encodeURIComponent(MMSearcher.q));
						}else{
							location.replace('/');
						}
					}
				});
		},

		//-----------------------------------------------------------
		
		open: function (key){
			return new Promise(function(resolve, reject){
				task(this, function(){
					VpCommon.ajax({
						title: '미디어 정보 조회',
						url: '/api/media/' + key,
						success: function(r){
							resolve(r);
						},
						error: function (e){
							reject(e);
						}
					});
					
				});
			}.bind(this))
				.then(function(r){
					this.loaded(r);
				}.bind(this));
		},

		//-----------------------------------------------------------
		
		loaded: function (r){
			this.data.key = r.key;
			this.data.serverData = r;
			
			this.mediaType = r.mediaType;
			
			var displayVideo = this.mediaType === MMMediaType.VIDEO;
			
			if (displayVideo){
				this.image.viewer.hide();
				this.video.player.show();
				
				this.video.player.open('/media/' + r.key);
			}else{
				this.video.player.hide();
				this.image.viewer.show();
				
				this.image.viewer.css('background-image', 'url(/media/screen/' + r.key + ')');
			}
			
			setTimeout(function(){

				if (displayVideo){
				
					//-----------------------------------------------------------
					// mask
					
					this.video.player.mask({
						invert: r.invertMask === true,
						
						masks: r.masks,
					});
					
				}
					
				//-----------------------------------------------------------
				// snapshot
				var len = r.snapshots ? r.snapshots.length : 0;
				for (var i=0; i < len; i++){
					var d = r.snapshots[i];
					
					var labels = [];
					
					var numLabels = d.labels ? d.labels.length : 0;
					for (var j=0; j < numLabels; j++){
						var l = d.labels[j];
						
						labels.push({
							x: l.x,
							y: l.y,
							width: l.width,
							height: l.height,
							points: l.points,
							
							attrs: {
								polyp_pos: l.polypPos,
								polyp_size: l.polypSiz,
								polyp_shape: l.polypShape,
								polyp_bio: l.polypBio,
							},
							
							source: '/media/snapshot-label/' + r.key + '/' + encodeURIComponent(d.time) + '/' + l.seq,
						});
					}
					
					var snapshot = {
						time: d.time,
						
						image: {
							width: d.snapWidth,
							height: d.snapHeight,
							source: '/media/snapshot/' + r.key + '/' + encodeURIComponent(d.time),
						},
						
						thumbnail: {
							width: d.thumbnailWidth,
							height: d.thumbnailHeight,
							source: '/media/snapshot/thumbnail/' + r.key + '/' + encodeURIComponent(d.time),
						},
						
						labels: labels,
					};
					
					if (displayVideo){
						this.video.player.label(snapshot, false, r.duration);
					}else{
						this.image.snapshots.push(snapshot);
					}
					
					this.setSnapshot(snapshot);
					
					//console.log(snapshot);
				}
				
				//-----------------------------------------------------------
				// 환자/수술정보
				
				var patient = r.patient;
				var operation = r.operation;
				
				var writer = new FormTextWriter({
					form: this.e.body,
				});
				
				writer.text('title', r.title);
				
				writer.date('pat_reg_date', patient.registDate);
				writer.text('pat_id', patient.id);
				writer.text('pat_nm', patient.name);
				writer.text('pat_no', patient.no);
				writer.radio('pat_gen', patient.gender);
				writer.number('pat_age', patient.age);
				writer.number('pat_weight', patient.weight, 'cm');
				writer.number('pat_height', patient.height, 'kg');
				writer.radioNumber('cfs_ix', patient.cfsIx);			
				writer.radioNumber('exclusion', patient.exclusion);
				writer.radioNumber('history', patient.history);
				writer.radioNumber('endoscopy_stat', patient.endoscopyStat);
				
				writer.radioNumber('appendix_reach', operation.appendixReach);
				writer.time('appx_reach_tm', operation.appendixReachTime);
				writer.time('appx_remove_tm', operation.appendixReachRemoveTime);
				writer.time('appx_total_tm', operation.appendixTotalTime);
				writer.radioNumber('right_colon', operation.rightColon);
				writer.radioNumber('trans_colon', operation.transColon);
				writer.radioNumber('left_colon', operation.leftColon);
				writer.number('bbps', operation.bbps);
				writer.number('bbps_fitness', operation.bbpsFitness);
				writer.radioNumber('cfs_opi', operation.cfxOpinion);
				writer.number('polyp_cnt', operation.polypCnt);
				
			}.bind(this), 0);
		},

		//-----------------------------------------------------------
		
		clearSnapshot: function(){
			this.e.snapshots.empty();
		},
		
		setSnapshot: function (d, labelIndexes){
			if (!d) return;
		
			var shapeTemplates = this.templates.get('snapshot-shape').clone();
			
			var eshapes = null;
			var e = this.e.snapshots.children('[time="'+d.time+'"]');
			if (!e.length){
				var e = this.templates.get('snapshot').clone();
				eshapes = e.find('[template-name="shapes"]');
				
				e.attr('time', d.time);
				
				this.e.snapshots.append(e);
				
				var children = this.e.snapshots.children();
				
				children.sort(function(a, b){
					var time1 = parseFloat($(a).attr('time'));
					var time2 = parseFloat($(b).attr('time'));
					
					return time1 - time2;
				});
				
				children.appendTo(this.e.snapshots);
			}else{
				eshapes = e.find('[template-name="shapes"]');
				eshapes.empty();
			}

			var sTime = VpCommon.times.toTimeSpanFormat(d.time);
			
			var eimg = this.templates.gete(e, 'image');
			eimg.attr('src', d.thumbnail.source);
			
			var etime = this.templates.gete(e, 'time');
			etime.text(sTime);
			
			var hasLabelIndexes = labelIndexes && $.isArray(labelIndexes) && labelIndexes.length > 0;
			
			// 라벨 생성
			var len = d.labels ? d.labels.length : 0;
			for (var i=0; i < len; i++){
				var s = d.labels[i];
				
				if (hasLabelIndexes && $.inArray(i, labelIndexes) < 0)
					continue;
				
				var eshape = shapeTemplates.clone();
				eshape.attr('label-index', i);
				
				var eimg = this.templates.gete(eshape, 'image');
				eimg.attr('src', s.source);
				
				for (var p in s.attrs){
					var val = s.attrs[p];
					
					var eattr = this.templates.gete(eshape, p);
					
					var a = MVLabels[p.toUpperCase()];
					if (a){
						eattr.text(a[val]);
					}else{
						eattr.text(val);
					}
				}
				
				eshapes.append(eshape);
			}
		},
		
		//-----------------------------------------------------------
		
		seek: function(value, labelIndex){
			if(this.mediaType !== MMMediaType.VIDEO)
				return;
			
			value = parseFloat(value);
			if (isNaN(value))
				return;
			
			labelIndex = defined(labelIndex, null);
			if (labelIndex !== null)
				this.video.player.command('visible-label', parseFloat(labelIndex));
			
			var time = parseFloat(value);
			var currentTime = this.video.player.get('current');
			
			if (time == currentTime){
				this.video.player.refresh();
			}else{
				this.video.player.command('current', time);
			}
		},
		
		seekSnapshot: function (element){
			if(this.mediaType !== MMMediaType.VIDEO)
				return;
			
			var e = $(element);
			var time = e.attr('time');
			if (typeof time == 'undefined'){
				e = e.parents('[time]');
				time = e.attr('time');
			}
			
			this.seek(time);
		},

		seekSnapshotLabel: function (element){
			if(this.mediaType !== MMMediaType.VIDEO)
				return;
			
			var e = $(element);
			var eSnapshot = e.parents('[time]');
			
			var time = eSnapshot.attr('time');
			var labelIndex = e.attr('label-index');
			
			this.seek(time, labelIndex);
		},
	};

	//-----------------------------------------------------------
	//-----------------------------------------------------------
	//-----------------------------------------------------------
	
	function defined(a, b){ return typeof a != 'undefined' ? a : b; }
	function definedBoolean(a, b){ return typeof a == 'boolean' ? a : b; }
	function definedNumber(a, b){ return typeof a == 'number' ? a : b; }
	function definedFunction(a, b){ return typeof a == 'function' ? a : b; }

	function mv(e, name) { return e.find('[mv-name="' + name + '"]'); }
	function n(e, name) { return e.find('[name="' + name + '"]'); }
	
	function eo(e) { return e.length ? e.get(0) : null; }
	function is(e, te) { return eo(e) == eo(te); }
	function has(e, te) {
		if (is(e, te)) return true;
		return e.has(te).length > 0;
	}

	function getBounds(e){
		return eo(e).getBoundingClientRect();
	}
	
	//-----------------------------------------------------------
	
	function task(constroller, func, timeOut){
		if (!timeOut) timeOut = 0;
		setTimeout(func.bind(constroller), timeOut);
	}
	
	//-----------------------------------------------------------

	function attachEvents(controller){
		var e = controller.e.body;
		var eSnapshotSearch = controller.e.snapshotSearch;
		
		//-----------------------------------------------------------
		
		var searchCallback = function (event){
			var e = $(this);
			var self = controller;
			
			var value = $.trim(e.val());
			
			var displayVideo = self.mediaType === MMMediaType.VIDEO;
			
			var snapshots = displayVideo ? self.video.player.get('snapshots') : self.image.snapshots;
			var len = snapshots.length;
			
			self.clearSnapshot();
			
			if (!value){
				for (var i=0; i < len; i++){
					var d = snapshots[i];
					self.setSnapshot(d);
				}
			}else{
				value = value.toLowerCase();
				
				for (var i=0; i < len; i++){
					var d = snapshots[i];
					
					var founds = [];
					
					//if (('' + d.time).indexOf(value) >= 0) ok = true;
					
					var numLabels = d.labels ? d.labels.length : 0;
					for (var ilabel=0; ilabel < numLabels; ilabel++){
						var l = d.labels[ilabel];
						
						var txt = '';
						for (var p in l.attrs){
							var val = l.attrs[p];
							
							var a = MVLabels[p.toUpperCase()];
							if (a){
								txt = '' + a[val];
							}else{
								txt = '' + val;
							}
							
							if (txt.toLowerCase().indexOf(value) >= 0){
								founds.push(ilabel);
								break;
							}
						}
					}
					
					if (founds.length)
						self.setSnapshot(d, founds);
				}
			}
			
		};
		
		//-----------------------------------------------------------

		//eSnapshotSearch.change(searchCallback);
		eSnapshotSearch.keyup(searchCallback);
		
		//-----------------------------------------------------------
		
		controller.buttons.remove.click(controller, function(event){
			event.data.remove();
		});
	}
	
	//-----------------------------------------------------------

})();
