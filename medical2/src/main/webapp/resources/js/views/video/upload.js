var MMMediaType = {
	VIDEO: 'V',
	IMAGE: 'I',
};

var MMTerm = {
	SLOW: 100,	
};

var MMUploader = (function(){
	return {
		PREFIX_IMAGE_TITLE: '[사진] ',
		
		selector: '#upload',
		
		formSelectors: {
			media: '#form-media',
			screen: '#form-screen',
			snapshot: '#form-snapshot',
			meta: '#form-meta',
			
			image: '#form-image',
		},
		
		mediaType: MMMediaType.VIDEO,
		
		debug: {
			useProgressConsole: false,
		},
		
		e: {
			body: null,
			forms: {
				media: null,
				screen: null,
				snapshot: null,
				meta: null,
				
				image: null,
			},
			fileArea: null,
			fileBtn: null,
			videoArea: null,
			snapshots: null,
		},
		
		form: {
			video: null,
		},
		
		buttons: {
			upload: null,
		},
		
		popups: {
			mask: null,
			label: null,
			
			labelEditor: null,			
		},
		
		video: {
			player: null,
		},
		
		image: {
			register: null,
		},
		
		data: {
			video: null,
			image: null,
		},
		
		templates: null,
		
		initialize: function(){
			this.e.body = $(this.selector);
			
			this.e.forms.media = $(this.formSelectors.media);
			this.e.forms.screen = $(this.formSelectors.screen);
			this.e.forms.snapshot = $(this.formSelectors.snapshot);
			this.e.forms.meta = $(this.formSelectors.meta);
			
			this.e.forms.image = $(this.formSelectors.image);
			
			this.e.fileArea = mv(this.e.body, 'file-area');
			this.e.fileBtn = mv(this.e.body, 'file-btn');
			this.e.videoArea = mv(this.e.body, 'video-area');
			this.e.snapshots = mv(this.e.body, 'snapshot-list');
			
			this.form.video = n(this.e.body, 'video');
			
			this.buttons.upload = mv(this.e.body, 'upload');

			//-----------------------------------------------------------
			
			try
			{
				var e = n(this.e.body, 'pat_reg_date');
				e.datepicker({ dateFormat: 'yy-mm-dd' });
			}
			catch (ex)
			{
				console.log(ex);
			}

			//-----------------------------------------------------------
			
			this.templates = new MMTemplates({
				selector: '#video-edit-templates',
			});

			//-----------------------------------------------------------
			
			this.popups.mask = new VideoMask();
			this.popups.label = new VideoLabel();
			this.popups.labelEditor = new MMVideoLabelEditor({
				selector: '#label-editor',
			});

			//-----------------------------------------------------------
			// 비디오/이미지 마스크

			this.popups.mask.observe(function(sendor, topic, value){
				var player = this.mediaType === MMMediaType.VIDEO ? this.video.player : this.image.register;
				
				switch(topic){
				case 'end':
					this.popups.mask.hide();
					break;
				
				case 'save':
					console.log('<<<MASK DATA>>>', value);
					
					player.mask(value);
					break;

				case 'remove':
					if (confirm('삭제하시겠습니까?')){
						player.command('remove:mask');
						this.popups.mask.hide();
					}
				}
			}.bind(this));

			//-----------------------------------------------------------
			// 비디오/이미지 라벨
			
			this.popups.labelEditor.observe(function(sendor, topic, value){
				switch(topic){
				case 'input':
					if (isNaN(value.value)){
						alert('용종 크기를 숫자로 입력하세요');
						return;
					}
					if (defined(value.value,null) === null){
						switch(value.name){
						case 'polyp_pos': alert('용종 위치를 선택하세요'); break;
						case 'polyp_size': alert('용종 크기를 입력하세요'); break;
						case 'polyp_shape': alert('용종 모양을 선택하세요'); break;
						case 'polyp_bio': alert('용종 조직검사를 선택하세요'); break;
						}
						return;
					}
					this.popups.label.setAttr(value.name, value.value);
					break;
				}
			}.bind(this));

			this.popups.label.delegates('attrs', function(label, cmd, value){
				var player = this.mediaType === MMMediaType.VIDEO ? this.video.player : this.image.register;
				
				switch(cmd){
				case 'prepare':
					var time = 0;
					var origin = label.origin;
					if (origin)
						time = origin.time;
					
					var stime = VpCommon.times.toTimeSpanFormat(time);
					this.popups.labelEditor.setText('polyp_seektime', stime);
					break;
					
				case 'validate':
					this.popups.labelEditor.validate(value);
					break;
					
				case 'enable':
					this.popups.labelEditor.enable(true);
					break;
					
				case 'disable':
					this.popups.labelEditor.enable(false);
					break;
					
				case 'set':
					this.popups.labelEditor.set(value);
					break;
				}
			}.bind(this));
			
			this.popups.label.observe(function(sendor, topic, value){
				var player = this.mediaType === MMMediaType.VIDEO ? this.video.player : this.image.register;
				
				switch(topic){
				case 'end':
					this.popups.label.hide();
					break;
				
				case 'save':
					console.log('<<<LABEL DATA>>>', value);
					
					player.label(value);
					
					this.setSnapshot(value);
					
					break;

				case 'remove':
					this.removeSnapshot(value);
					break;
				}
			}.bind(this));

			//-----------------------------------------------------------
			// 비디오 플레이어

			this.video.player = new VideoPlayer({
			});

			//-----------------------------------------------------------

			this.video.player.observe(function(sendor, topic, value){
				var vp = this.video.player;
				var vpLabel = this.popups.label;
				var vpMask = this.popups.mask;
				
				switch(topic){
				case 'u:capture':
					if (vp.canPlay){
						vp.command('pause');
						var imgData = vp.command('capture');

						vpLabel.show();
						vpLabel.set(imgData);
					}
					break;

				case 'u:mask':
					if (vp.canPlay){
						vp.command('pause');
						var imgData = vp.command('capture:mask');
						
						vpMask.show();
						vpMask.set(imgData);
					}
					break;
				}
			}.bind(this));

			//-----------------------------------------------------------
			// 이미지 등록기

			this.image.register = new VideoImageRegister({
			});

			//-----------------------------------------------------------

			this.image.register.observe(function(sendor, topic, value){
				var vp = this.image.register;
				var vpLabel = this.popups.label;
				var vpMask = this.popups.mask;
				
				switch(topic){
				case 'u:capture':
					if (vp.canPlay){
						vp.command('pause');
						var imgData = vp.command('capture');

						vpLabel.show();
						
						vpLabel.set(imgData);
					}
					break;

				case 'u:mask':
					if (vp.canPlay){
						vp.command('pause');
						var imgData = vp.command('capture:mask');
						
						vpMask.show();
						vpMask.set(imgData);
					}
					break;
				}
			}.bind(this));

			//-----------------------------------------------------------		
			
			attachEvents(this);
			
		},

		//-----------------------------------------------------------
	
		openVideo: function(uri){
			this.e.fileBtn.addClass('mv-hide');
			this.e.videoArea.removeClass('mv-hide');
			
			this.video.player.open(uri);		
		},
	
		openImage: function(uri){
			this.e.fileBtn.addClass('mv-hide');
			this.e.videoArea.removeClass('mv-hide');
			
			this.image.register.open(uri);		
		},

		//-----------------------------------------------------------
		
		setSnapshot: function (d){
			if (!d) return;
			
			var shapeTemplates = this.templates.get('snapshot-shape').clone();
			
			var eshapes = null;
			var e = this.e.snapshots.children('[time="'+d.time+'"]');
			if (!e.length){
				e = this.templates.get('snapshot').clone();
				eshapes = e.find('[template-name="shapes"]');
				
				e.attr('time', d.time);
				
				this.e.snapshots.append(e);
				
				var children = this.e.snapshots.children();
				
				children.sort(function(a, b){
					var time1 = parseFloat($(a).attr('time'));
					var time2 = parseFloat($(b).attr('time'));
					
					return time1 - time2;
				});
				
				children.appendTo(this.e.snapshots);
			}else{
				eshapes = e.find('[template-name="shapes"]');
				eshapes.empty();
			}

			var sTime = VpCommon.times.toTimeSpanFormat(d.time);
			
			var eimg = this.templates.gete(e, 'image');
			eimg.attr('src', d.thumbnail.source);
			
			var etime = this.templates.gete(e, 'time');
			etime.text(sTime);
			
			// 라벨 생성
			var len = d.labels ? d.labels.length : 0;
			for (var i=0; i < len; i++){
				var s = d.labels[i];
				
				var eshape = shapeTemplates.clone();
				eshape.attr('label-index', i);
				
				var eimg = this.templates.gete(eshape, 'image');
				eimg.attr('src', s.source);
				
				for (var p in s.attrs){
					var val = s.attrs[p];
					
					var eattr = this.templates.gete(eshape, p);
					
					var a = MVLabels[p.toUpperCase()];
					if (a){
						eattr.text(a[val]);
					}else{
						eattr.text(val);
					}
				}
				
				eshapes.append(eshape);
			}
		},

		//-----------------------------------------------------------
		// 스냅샷 삭제

		removeSnapshot: function (time){
			if (confirm('스크린샷을 삭제하시겠습니까?')){
				var vp = this.video.player;
				if (this.mediaType === MMMediaType.IMAGE){
					vp = this.image.register;
				}
			
				vp.command('remove:label', time);

				this.popups.label.hide();
				
				var e = this.e.snapshots.children('[time="'+time+'"]');
				e.remove();
			}
		},

		//-----------------------------------------------------------
		// 스냅샷 수정/삭제 버튼 클릭 이벤트
		
		modifySnapshotByElement: function (element){
			var ebtn = $(element);
			var e = ebtn.parents('li[time]');
			var time = parseFloat(e.attr('time'));
			
			var vp = this.video.player;
			var vpLabel = this.popups.label;
			var vpMask = this.popups.mask;
			
			if (this.mediaType === MMMediaType.IMAGE){
				vp = this.image.register;
			}
			
			var imgData = vp.command('get:label', time);
			
			vp.command('current', time);
			
			vpLabel.show();
			vpLabel.set(imgData);
		},
		
		removeSnapshotByElement: function (element){
			var ebtn = $(element);
			var e = ebtn.parents('li[time]');
			
			var time = parseFloat(e.attr('time'));
			
			this.removeSnapshot(time);
		},

		//-----------------------------------------------------------
		// 스낵샷 라벨 수정 버튼 클릭 이벤트
		
		modifySnapshotLabelByElement: function (element){
			var ebtn = $(element);
			
			var eSnapshot = ebtn.parents('li[time]');
			var time = parseFloat(eSnapshot.attr('time'));
			
			var e = ebtn.parents('li[label-index]');
			var labelIndex = parseInt(e.attr('label-index'));
			
			var vp = this.video.player;
			var vpLabel = this.popups.label;
			var vpMask = this.popups.mask;
			
			if (this.mediaType === MMMediaType.IMAGE){
				vp = this.image.register;
			}
			
			var imgData = vp.command('get:label', time);
			
			vp.command('current', time);
			
			vpLabel.show();
			vpLabel.set(imgData, labelIndex);
		},

		//-----------------------------------------------------------
		
		clear: function(){
			this.video.player.clear();
			this.image.register.clear();
			
			this.e.snapshots.children().remove();
			
//			this.writeForm({
//				title: '',
//			});
		},

		//-----------------------------------------------------------
		
		mediaSelected: function(file){
			if (file && file instanceof File){
				this.clear();
				
				if (file.type.indexOf('video/') == 0){
					this.image.register.hide();
					this.video.player.show();
					
					this.mediaType = MMMediaType.VIDEO;
					this.data.video = file;
					
					var blob = URL.createObjectURL(file);
					this.openVideo(blob);
				}else if (file.type.indexOf('image/') == 0){
					this.video.player.hide();
					this.image.register.show();
					
					this.mediaType = MMMediaType.IMAGE;
					this.data.image = file;
					
					var blob = URL.createObjectURL(file);
					this.openImage(blob);
				}

				//-----------------------------------------------------------
				// 파일명에서 제목 추출
				
				var baseName = $.trim(VpCommon.filePath.basename(file.name).replace(/[_]/g, ' '));
				
				if (baseName && this.mediaType === MMMediaType.IMAGE){
					baseName = this.PREFIX_IMAGE_TITLE + baseName;
				}
				
				var writer = new FormWriter({
					form: this.e.body,
				});
				
				writer.text('title', baseName);
			}
		},

		//-----------------------------------------------------------
		
		msgBox: function(s){
			console.log(s);
			alert(s);
		},

		//-----------------------------------------------------------
		
		validateForm: function(){
			var checker = new FormValidator({
				form: this.e.body,
				msgBox: this.msgBox,
			});
			var r = null;

			//-----------------------------------------------------------
			
			if ((r = checker.text('title', '제목')) !== true)
				return r;

			//-----------------------------------------------------------
			
			if ((r = checker.text('pat_nm', '환자명', 'pat')) !== true)
				return r;
			
			if ((r = checker.text('pat_id', '환자 아이디', 'pat')) !== true)
				return r;
			
			if ((r = checker.text('pat_no', '환자 번호', 'pat')) !== true)
				return r;
			
			if ((r = checker.number('pat_age', '환자 나이', 'pat')) !== true)
				return r;
			
			if ((r = checker.number('pat_weight', '환자 체중', 'pat')) !== true)
				return r;
			
			if ((r = checker.number('pat_height', '환자 신장', 'pat')) !== true)
				return r;
			
			if ((r = checker.radio('pat_gen', '성별', 'pat')) !== true)
				return r;
			
			if ((r = checker.date('pat_reg_date', '연구등록일', 'pat')) !== true)
				return r;
			
			if ((r = checker.radio('cfs_ix', 'CFS Ix', 'pat')) !== true)
				return r;
			
			if ((r = checker.radio('exclusion', 'Exclusion', 'pat')) !== true)
				return r;
			
			if ((r = checker.radio('history', '수술력', 'pat')) !== true)
				return r;
			
			if ((r = checker.radio('endoscopy_stat', '이전 일차 내시경 시행여부', 'pat')) !== true)
				return r;

			//-----------------------------------------------------------
			
			if ((r = checker.radio('appendix_reach', '맹장도달', 'op')) !== true)
				return r;
			
			if ((r = checker.time('appx_reach_min', 'appx_reach_sec', '맹장 도달 시간', 'op')) !== true)
				return r;
			
			if ((r = checker.time('appx_remove_min', 'appx_remove_sec', '맹장 회수 시간', 'op')) !== true)
				return r;
			
			if ((r = checker.time('appx_total_min', 'appx_total_sec', '맹장 검사 시간', 'op')) !== true)
				return r;
			
			if ((r = checker.radio('right_colon', '장정결도 우측', 'op')) !== true)
				return r;
			
			if ((r = checker.radio('trans_colon', '장정결도 횡행', 'op')) !== true)
				return r;
			
			if ((r = checker.radio('left_colon', '장정결도 좌측', 'op')) !== true)
				return r;
			
			if ((r = checker.radio('cfs_opi', 'CFS 소견', 'op')) !== true)
				return r;
			
			if ((r = checker.number('polyp_cnt', 'CFS 소견: 용종 발견 개수', 'op')) !== true)
				return r;
		
			return true;
		},
		
		writeForm: function (meta){
			var patient = meta.patient || {};
			var operation = meta.operation || {};
			
			var writer = new FormWriter({
				form: this.e.body,
			});
			
			if (typeof meta.title != 'undefined'){
				writer.text('title', meta.title);
			}
			
			writer.date('pat_reg_date', patient.regDate);
			writer.text('pat_id', patient.id);
			writer.text('pat_nm', patient.name);
			writer.text('pat_no', patient.no);
			writer.radio('pat_gen', patient.gender);
			writer.number('pat_age', patient.age);
			writer.number('pat_weight', patient.weight);
			writer.number('pat_height', patient.height);
			writer.radioNumber('cfs_ix', patient.cfxIx);			
			writer.radioNumber('exclusion', patient.exclusion);
			writer.radioNumber('history', patient.history);
			writer.radioNumber('endoscopy_stat', patient.endoscopyStat);
			
			writer.radioNumber('appendix_reach', operation.appendixReach);
			writer.time('appx_reach_min', 'appx_reach_sec', operation.appendixReachTime);
			writer.time('appx_remove_min', 'appx_remove_sec', operation.appendixReachRemoveTime);
			writer.time('appx_total_min', 'appx_total_sec', operation.appendixTotalTime);
			writer.radioNumber('right_colon', operation.rightColon);
			writer.radioNumber('trans_colon', operation.transColon);
			writer.radioNumber('left_colon', operation.leftColon);
			writer.number('bbps', operation.bbps);
			writer.number('bbps_fitness', operation.bbpsFitness);
			writer.radioNumber('cfs_opi', operation.cfxOpinion);
			writer.number('polyp_cnt', operation.polypCnt);
		},

		//-----------------------------------------------------------
		
		upload: function (){
			var existVideo = this.data.video && this.data.video instanceof File;
			var existImage = this.data.image && this.data.image instanceof File;
			
			if (!existVideo && !existImage){
				this.msgBox('비디오나 이미지를 선택하세요');
				return;
			}
			
			if (this.validateForm() !== true)
				return;
			
			if (confirm('등록하시겠습니까?')){
				if (this.mediaType === MMMediaType.VIDEO){
					this.uploadVideo();
				}else{
					this.uploadImage();
				}
			}
		},

		//-----------------------------------------------------------

		uploadVideo: function (){
			var useConsole = this.debug.useProgressConsole;
			
			this.beginUpload();
			
			var uid = null;
			var player = this.video.player;
			var completed = false;
			
			var vprog = new VisualProgress();
			vprog.topic('ready');
			vprog.show();
			
			var numProcess = 4; // media, meta, screen, complete 
			var numAll = numProcess + player.data.snapshot.list.length;

			vprog.setAll(numAll);
			
			this.alloc()
				.then(function(r){
					uid = r.key;
				}.bind(this))		
				.then(function(r){
					vprog.topic('proc');
					
					return this.uploadMediaVideo(uid, function(cur, total){
						vprog.current(cur, total);
					});
				}.bind(this))
				.then(function(r){
					vprog.nextAll();
					
					return this.uploadMeta(uid, function(cur, total){
						vprog.current(cur, total);
					});
				}.bind(this))
				.then(function(r){
					vprog.nextAll();
					
					return this.uploadScreen(uid, function(cur, total){
						vprog.current(cur, total);
					});
				}.bind(this))
				.then(function(r){
					vprog.nextAll();
					
					var snapshotList = player.data.snapshot.list;
					
					var len = snapshotList.length;
					var ps = [];
					for (var i=0; i < len; i++){
						var sn = snapshotList[i];
						
						ps.push(this.uploadSnapshot(uid, sn, function (cur, total){
							vprog.current(cur, total);
						}));
					}
					
					return VpCommon.promise.all(ps, function (cur, total){
						vprog.nextAll();
						
						if (useConsole){
							var p = cur / total;
							console.log('<<<SNAPSHOT::PROMISE::ALL>>> [ ', p, '% ] ', cur, '/', total);
						}
					});
				}.bind(this))
				.then(function(r){
					return this.uploadComplete(uid);
				}.bind(this))
				.then(function(r){
					vprog.nextAll();
					
					completed = true;
					
					this.endUpload();
					
					task(this, function(){
						vprog.hide();
						
						location.replace('/');
					}, MMTerm.SLOW);					
				}.bind(this))
				.catch(function(e){
					console.log('<<<UPLOAD::ERROR>>> ', e);
					
					if (!completed){
						this.uploadCancel(uid)
							.then(function(){
								console.log('<<<UPLOAD::CANCELLED>>>');
								
								task(this, function(){
									vprog.hide();
									
									alert('UPLOAD CANCEL');
								}, MMTerm.SLOW);					
							}.bind(this))
							.catch(function(e){
								console.log('<<<UPLOAD::CANCEL::ERROR>>> ', e);
								
								task(this, function(){
									vprog.hide();
									
									alert('UPLOAD CANCEL');
								}, MMTerm.SLOW);					
							}.bind(this));
					}else{
						task(this, function(){
							vprog.hide();
						}, MMTerm.SLOW);					
					}
				}.bind(this));
		},

		//-----------------------------------------------------------
		
		uploadImage: function(){
			this.beginUpload();
			
			var vprog = new VisualProgress();
			vprog.topic('ready');
			vprog.show();

			vprog.setAll(1);
			vprog.topic('proc');
			
			return new Promise(function(resolve, reject){
				task(this, function(){
					//-----------------------------------------------------------
					// 진행률 콜백 함수
					
					var progress = function(event){
						if (event.lengthComputable){
							vprog.current(event.loaded, event.total);
						}
					}.bind(this);
					
					//-----------------------------------------------------------
					// 준비
					
					var useConsole = this.debug.useProgressConsole;

					var player = this.image.register;
					var viewport = player.viewport;
					
					var d = this.data;
					var imageWidth = viewport.imageWidth;
					var imageHeight = viewport.imageHeight;
					
					var fd = new FormData(eo(this.e.forms.image));
					
					//-----------------------------------------------------------
					// 이미지 원본
					
					fd.append('width', imageWidth);
					fd.append('height', imageHeight);
					fd.append('file', d.image);
					
					//-----------------------------------------------------------
					// 스크린 이미지 (이미지 + 마스크 + 라벨)
					
					var screenSrc = player.command('screen');
					var f = VpCommon.dataUrlToFile(screenSrc, 'screen.jpg');
					
					var meta = {
						width: imageWidth,
						height: imageHeight,
						name: f.name,
						size: f.size,
					};
					
					fd.append('screenMeta', JSON.stringify(meta));
					fd.append('screenFile', f);
					
					//-----------------------------------------------------------
					// 라벨
					
					var snapshot = player.data.snapshot;
					if (snapshot){
						f = VpCommon.dataUrlToFile(snapshot.thumbnail.source, 'thumb_shapshot.jpg');
						fd.append('snapshotFile', f);
						
						var len = snapshot.labels ? snapshot.labels.length : 0;
						var labels = [];
						
						for (var i=0; i < len; i++){
							var l = snapshot.labels[i];
							var attrs = l.attrs || {};
							var name = 'labelFile-' + (i + 1);
							var filename = name + '.png';
							
							labels.push({
								time: 0,
								
								x: l.x,
								y: l.y,
								width: l.width,
								height: l.height,
								points: l.points,
								
								polypPos: parseFloat(attrs.polyp_pos || 0),
								polypSize: parseFloat(attrs.polyp_size || 0),
								polypShape: parseFloat(attrs.polyp_shape || 0),
								polypBio: parseFloat(attrs.polyp_bio || 0),
								
								filename: filename,
								fieldname: name,
							});
							
							var lf = VpCommon.dataUrlToFile(l.source, filename);
							fd.append(name, lf);
						}
						
						// 이미지 업로드의 경우 섬네일만 업로드...
						meta = {
							time: 0,
							
	//						width: snapshot.image.width,
	//						height: snapshot.image.height,
	//						name: f.name,
	//						size: f.size,
							
							thumbnail: {
								width: snapshot.thumbnail.width,
								height: snapshot.thumbnail.height,
								name: f.name,
								size: f.size,
							},
							
							labels: labels,
						};
						
						fd.append('snapshotMeta', JSON.stringify(meta));
					}
					
					//-----------------------------------------------------------
					// 마스크 + 환자정보 + 수술정보
					
					var maskData = player.data.mask;
					
					len = maskData ? maskData.masks.length : 0;
					var masks = [];
					
					if (maskData && maskData.invert){
						masks.push({
							seq: 0,
						});
					}
					
					for (var i=0; i < len; i++){
						var m = maskData.masks[i];
						
						masks.push({
							seq: i + 1,
							x: m.x,
							y: m.y,
							width: m.width,
							height: m.height,
						});
					}
					
					var formReader = new FormReader({
						form: this.e.body,
					});
									
					meta = {
						type: this.mediaType,
						title: formReader.text('title'),
							
						patient: {
							regDate: formReader.date('pat_reg_date'),
							id: formReader.text('pat_id'),
							name: formReader.text('pat_nm'),
							no: formReader.text('pat_no'),
							gender: formReader.radio('pat_gen'),
							age: formReader.number('pat_age'),
							weight: formReader.number('pat_weight'),
							height: formReader.number('pat_height'),
							cfxIx: formReader.radioNumber('cfs_ix'),
							exclusion: formReader.radioNumber('exclusion'),
							history: formReader.radioNumber('history'),
							endoscopyStat: formReader.radioNumber('endoscopy_stat'),
						},
						
						operation: {
							appendixReach: formReader.radioNumber('appendix_reach'),
							appendixReachTime: formReader.time('appx_reach_min', 'appx_reach_sec'),
							appendixReachRemoveTime: formReader.time('appx_remove_min', 'appx_remove_sec'),
							appendixTotalTime: formReader.time('appx_total_min', 'appx_total_sec'),
							rightColon: formReader.radioNumber('right_colon'),
							transColon: formReader.radioNumber('trans_colon'),
							leftColon: formReader.radioNumber('left_colon'),
							bbps: formReader.number('bbps'),
							bbpsFitness: formReader.number('bbps_fitness'),
							cfxOpinion: formReader.radioNumber('cfs_opi'),
							polypCnt: formReader.number('polyp_cnt'),
						},
						
						masks: masks,
					};

					fd.append('meta', JSON.stringify(meta));
					
					//-----------------------------------------------------------
					// 전송
					
					var xhr = VpCommon.xmlHttpRequest({
						type: 'POST',
						path: '/api/media-upload/upload-image',
						
						progress: progress,
						
						load: function(){
							resolve();
						}.bind(this),
						
						error: function(e){
							reject(e);
						},
					});
					
					xhr.send(fd);
					
				});
			}.bind(this))
				.then(function(){
					this.endUpload();
					
					task(this, function(){
						vprog.hide();
						
						location.replace('/');
					}, MMTerm.SLOW);										
				}.bind(this))
				.catch(function(e){
					console.log('<<<UPLOAD::CANCELLED>>>');
					
					task(this, function(){
						vprog.hide();
						
						alert('UPLOAD CANCEL');
					}, MMTerm.SLOW);					
				}.bind(this));
		},
		
		//-----------------------------------------------------------
		
		alloc: function(){			
			return new Promise(function(resolve, reject){
				task(this, function(){
					VpCommon.ajax({
						title: '아이디 할당',
						url: '/api/media-upload/alloc',
						success: function(r){
							resolve(r);
						},
						error: function (e){
							reject(e);
						}
					});
					
				});
			}.bind(this));
		},
		
		uploadMediaVideo: function(uid, progressCallback){
			return new Promise(function(resolve, reject){
				task(this, function(){
					var useConsole = this.debug.useProgressConsole;
					
					var player = this.video.player;
					var viewport = player.viewport;
					
					var progress = typeof progressCallback == 'function' ? function(event){
						if (event.lengthComputable){
							progressCallback(event.loaded, event.total);
							
							if (useConsole){
								var p = (event.loaded / event.total * 100).toFixed(2);
								console.log('<<<UPLOAD::MEDIA>>> [', p, '% ] (', event.loaded, '/', event.total, ')');
							}
						}
					}.bind(this) : null;
					
					var d = this.data;
					
					var fd = new FormData(eo(this.e.forms.media));
					fd.append('uid', uid);
					fd.append('width', viewport.videoWidth);
					fd.append('height', viewport.videoHeight);
					fd.append('file', d.video);
					
					var xhr = VpCommon.xmlHttpRequest({
						type: 'POST',
						path: '/api/media-upload/upload',
						
						progress: progress,
						
						load: function(){
							resolve();
						}.bind(this),
						
						error: function(e){
							reject(e);
						},
					});
					
					xhr.send(fd);
				});
			}.bind(this));
		},
		
		uploadScreen: function(uid, progressCallback){
			return new Promise(function(resolve, reject){
				task(this, function(){
					var useConsole = this.debug.useProgressConsole;
					
					var player = this.video.player;
					var viewport = player.viewport;
					var screen = player.data.screen;
					
					var f = VpCommon.dataUrlToFile(screen.source, 'screen.jpg');
					
					var meta = {
						width: screen.width,
						height: screen.height,
						name: f.name,
						size: f.size,
					};
					
					var progress = typeof progressCallback == 'function' ? function(event){
						if (event.lengthComputable){
							progressCallback(event.loaded, event.total);
							
							if (useConsole){
								var p = (event.loaded / event.total * 100).toFixed(2);
								console.log('<<<UPLOAD::THUMBNAIL>>> [', p, '% ] (', event.loaded, '/', event.total, ')');
							}
						}
					}.bind(this) : null;
					
					var d = this.data;
					
					var fd = new FormData(eo(this.e.forms.screen));
					fd.append('uid', uid);
					fd.append('meta', JSON.stringify(meta));
					fd.append('file', f);
					
					var xhr = VpCommon.xmlHttpRequest({
						type: 'POST',
						path: '/api/media-upload/upload',
						
						progress: progress,
						
						load: function(){
							resolve();
						}.bind(this),
						
						error: function(e){
							reject(e);
						},
					});
					
					xhr.send(fd);
				});
			}.bind(this));
		},
		
		uploadSnapshot: function(uid, snapshot, progressCallback){
			return new Promise(function(resolve, reject){
				task(this, function(){
					var useConsole = this.debug.useProgressConsole;
					
					var player = this.video.player;
					var viewport = player.viewport;
					
					var f = VpCommon.dataUrlToFile(snapshot.image.source, snapshot.time + '.jpg');
					var ft = VpCommon.dataUrlToFile(snapshot.thumbnail.source, 'thumb_' + snapshot.time + '.jpg');
					
					var fd = new FormData(eo(this.e.forms.snapshot));
					fd.append('uid', uid);
					fd.append('file', f);
					fd.append('fileSecond', ft);
					
					var len = snapshot.labels ? snapshot.labels.length : 0;
					var labels = [];
					
					for (var i=0; i < len; i++){
						var l = snapshot.labels[i];
						var attrs = l.attrs || {};
						var name = 'labelFile_'+snapshot.time+'-' + (i + 1);
						var filename = name + '.png';
						
						labels.push({
							time: snapshot.time,
							
							x: l.x,
							y: l.y,
							width: l.width,
							height: l.height,
							points: l.points,
							
							polypPos: parseFloat(attrs.polyp_pos || 0),
							polypSize: parseFloat(attrs.polyp_size || 0),
							polypShape: parseFloat(attrs.polyp_shape || 0),
							polypBio: parseFloat(attrs.polyp_bio || 0),
							
							filename: filename,
							fieldname: name,
						});
						
						var lf = VpCommon.dataUrlToFile(l.source, filename);
						fd.append(name, lf);
					}
					
					var meta = {
						time: snapshot.time,
						
						width: snapshot.image.width,
						height: snapshot.image.height,
						name: f.name,
						size: f.size,
						
						thumbnail: {
							width: snapshot.thumbnail.width,
							height: snapshot.thumbnail.height,
							name: ft.name,
							size: ft.size,
						},
						
						labels: labels,
					};
					
					var progress = typeof progressCallback == 'function' ? function(event){
						if (event.lengthComputable){
							progressCallback(event.loaded, event.total);
							
							if (useConsole){
								var p = (event.loaded / event.total * 100).toFixed(2);
								console.log('<<<UPLOAD::THUMBNAIL>>> [', p, '% ] (', event.loaded, '/', event.total, ')');
							}
						}
					}.bind(this) : null;
					
					var d = this.data;
					fd.append('meta', JSON.stringify(meta));
										
					var xhr = VpCommon.xmlHttpRequest({
						type: 'POST',
						path: '/api/media-upload/upload',
						
						progress: progress,
						
						load: function(){
							resolve();
						}.bind(this),
						
						error: function(e){
							reject(e);
						},
					});
					
					xhr.send(fd);
				});
			}.bind(this));
		},
		
		uploadMeta: function(uid, progressCallback){
			return new Promise(function(resolve, reject){
				task(this, function(){
					var useConsole = this.debug.useProgressConsole;
					
					var player = this.video.player;
					var viewport = player.viewport;
					var screen = player.data.screen;
					var maskData = player.data.mask;
					var duration = player.duration ? player.duration.origin : 0;
					
					var len = maskData ? maskData.masks.length : 0;
					var masks = [];
					
					if (maskData && maskData.invert){
						masks.push({
							seq: 0,
						});
					}
					
					for (var i=0; i < len; i++){
						var m = maskData.masks[i];
						
						masks.push({
							seq: i + 1,
							x: m.x,
							y: m.y,
							width: m.width,
							height: m.height,
						});
					}
					
					var formReader = new FormReader({
						form: this.e.body,
					});
									
					var meta = {
						type: this.mediaType,
						title: formReader.text('title'),
						duration: duration,
							
						patient: {
							regDate: formReader.date('pat_reg_date'),
							id: formReader.text('pat_id'),
							name: formReader.text('pat_nm'),
							no: formReader.text('pat_no'),
							gender: formReader.radio('pat_gen'),
							age: formReader.number('pat_age'),
							weight: formReader.number('pat_weight'),
							height: formReader.number('pat_height'),
							cfxIx: formReader.radioNumber('cfs_ix'),
							exclusion: formReader.radioNumber('exclusion'),
							history: formReader.radioNumber('history'),
							endoscopyStat: formReader.radioNumber('endoscopy_stat'),
						},
						
						operation: {
							appendixReach: formReader.radioNumber('appendix_reach'),
							appendixReachTime: formReader.time('appx_reach_min', 'appx_reach_sec'),
							appendixReachRemoveTime: formReader.time('appx_remove_min', 'appx_remove_sec'),
							appendixTotalTime: formReader.time('appx_total_min', 'appx_total_sec'),
							rightColon: formReader.radioNumber('right_colon'),
							transColon: formReader.radioNumber('trans_colon'),
							leftColon: formReader.radioNumber('left_colon'),
							bbps: formReader.number('bbps'),
							bbpsFitness: formReader.number('bbps_fitness'),
							cfxOpinion: formReader.radioNumber('cfs_opi'),
							polypCnt: formReader.number('polyp_cnt'),
						},
						
						masks: masks,
					};
					
					var progress = typeof progressCallback == 'function' ? function(event){
						if (event.lengthComputable){
							progressCallback(event.loaded, event.total);
							
							if (useConsole){
								var p = (event.loaded / event.total * 100).toFixed(2);
								console.log('<<<UPLOAD::THUMBNAIL>>> [', p, '% ] (', event.loaded, '/', event.total, ')');
							}
						}
					}.bind(this) : null;
					
					var d = this.data;
					
					var fd = new FormData(eo(this.e.forms.meta));
					fd.append('uid', uid);
					fd.append('meta', JSON.stringify(meta));
					
					var xhr = VpCommon.xmlHttpRequest({
						type: 'POST',
						path: '/api/media-upload/upload',
						
						progress: progress,
						
						load: function(){
							resolve();
						}.bind(this),
						
						error: function(e){
							reject(e);
						},
					});
					
					xhr.send(fd);
				});
			}.bind(this));
		},
		
		uploadComplete: function(uid){
			return new Promise(function(resolve, reject){
				task(this, function(){
					VpCommon.ajax({
						title: '업로드 완료',
						url: '/api/media-upload/complete',
						data: {
							uid: uid,
						},
						success: function(r){
							resolve(r);
						},
						error: function (e){
							reject(e);
						}
					});
				});
			}.bind(this));
		},
		
		uploadCancel: function(uid){
			return new Promise(function(resolve, reject){
				task(this, function(){
					VpCommon.ajax({
						title: '업로드 취소',
						url: '/api/media-upload/cancel',
						data: {
							uid: uid,
						},
						success: function(r){
							resolve(r);
						},
						error: function (e){
							reject(e);
						}
					});
				});
			}.bind(this));
		},
		
		//-----------------------------------------------------------
		
		beginUpload: function(){
		},
		
		inProgressUpload: function(c, t, tc, tt){
		},
		
		endUpload: function(){
		},
	};

	//-----------------------------------------------------------
	//-----------------------------------------------------------
	//-----------------------------------------------------------
	
	function defined(a, b){ return typeof a != 'undefined' ? a : b; }
	function definedBoolean(a, b){ return typeof a == 'boolean' ? a : b; }
	function definedNumber(a, b){ return typeof a == 'number' ? a : b; }
	function definedFunction(a, b){ return typeof a == 'function' ? a : b; }

	function mv(e, name) { return e.find('[mv-name="' + name + '"]'); }
	function n(e, name) { return e.find('[name="' + name + '"]'); }
	
	function eo(e) { return e.length ? e.get(0) : null; }
	function is(e, te) { return eo(e) == eo(te); }
	function has(e, te) {
		if (is(e, te)) return true;
		return e.has(te).length > 0;
	}

	function getBounds(e){
		return eo(e).getBoundingClientRect();
	}
	
	//-----------------------------------------------------------
	
	function task(constroller, func, timeOut){
		if (!timeOut) timeOut = 0;
		setTimeout(func.bind(constroller), timeOut);
	}
	
	//-----------------------------------------------------------

	function attachEvents(controller){
		controller.e.fileBtn.click(controller, function(event){
			var self = event.data;
			
			self.form.video.click();
		});
		
		controller.form.video.change(controller, function(event){
			var self = event.data;
			var files = event.currentTarget.files;
			if (!files || !files.length){
				return;
			}
			
			var file = files[0];
			
			self.mediaSelected(file);
		});
		
		
		controller.buttons.upload.click(controller, function(event){
			var self = event.data;
			
			self.upload();
		});
		
		//-----------------------------------------------------------
	
		var fileArea = controller.e.fileArea;
		
		var fileDragHandler = function(event){
			event.stopPropagation();
			event.preventDefault();
		};
		
		fileArea.on('dragover', controller, fileDragHandler);
		fileArea.on('drop', controller, function (event){
			var self = event.data;
			var fileArea = self.e.fileArea;
			
			fileDragHandler(event);
			
			var files = event.originalEvent.dataTransfer.files;
			if (!files || !files.length){
				return;
			}
			
			var file = files[0];
			
//			fileArea.off('dragover');
//			fileArea.off('drop');
//			fileArea.off('dragenter');
//			fileArea.off('dragleave');
//			fileArea.off('dragend');
			
			self.mediaSelected(file);
		});
		fileArea.on('dragover dragenter', controller, function (event){
			var self = event.data;
			var fileArea = self.e.fileArea;
			
			fileArea.addClass('dragover');
		});
		fileArea.on('dragleave dragend drop', controller, function (event){
			var self = event.data;
			var fileArea = self.e.fileArea;
			
			fileArea.removeClass('dragover');
		});
		
		//-----------------------------------------------------------
	
		var e = controller.e.body;

		
		var updateBbpsHandler = function(){
			var eowner = arguments.callee.eowner;
			
			var right_colon = eowner.find('input[name=right_colon]:checked').val();
			var left_colon = eowner.find('input[name=left_colon]:checked').val();
			var trans_colon = eowner.find('input[name=trans_colon]:checked').val();

			if(right_colon == undefined || left_colon == undefined || trans_colon == undefined) return;

			right_colon = right_colon == -1 ? 0 : parseInt(right_colon);
			left_colon = left_colon == -1 ? 0 : parseInt(left_colon);
			trans_colon = trans_colon == -1 ? 0 : parseInt(trans_colon);

			var bbps = right_colon+left_colon+trans_colon;
			n(eowner, 'bbps').val(bbps);

			var efitness = n(eowner, 'bbps_fitness');
			if(right_colon>=2 && left_colon>=2 && trans_colon>=2){
				efitness.val(1);
			} else{
				efitness.val(0);
			}
		};
		updateBbpsHandler.eowner = e;
		
		n(e,'right_colon').change(updateBbpsHandler);
		n(e,'trans_colon').change(updateBbpsHandler);
		n(e,'left_colon').change(updateBbpsHandler);
	}
	
	//-----------------------------------------------------------

	function resetCheckInput(e){
		var len = e ? e.length : 0;
		for (var i=0; i < len; i++){
			var ce = $(e.get(i));
			
			ce.prop('checked', false);
		}
	}

	function setCheckInput(e, val){
		var len = e ? e.length : 0;
		for (var i=0; i < len; i++){
			var ce = $(e.get(i));
			
			ce.prop('checked', ce.val() === val);
		}
	}

	function getCheckInput(e){
		var len = e ? e.length : 0;
		for (var i=0; i < len; i++){
			var ce = $(e.get(i));
			
			if (ce.is(':checked'))
				return ce.val();
		}
		return null;
	}
	
	//-----------------------------------------------------------

})();

//-----------------------------------------------------------
//-----------------------------------------------------------
//-----------------------------------------------------------

