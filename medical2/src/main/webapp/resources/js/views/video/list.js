var MMMediaType = {
	VIDEO: 'V',
	IMAGE: 'I',
};

var MMTerm = {
	SLOW: 100,	
};

var MMListUp = (function(){
	return {
		selector: '#list',
		
		debug: {
			useProgressConsole: false,
		},
		
		e: {
			body: null,
		},
		
		initialize: function(){
			this.e.body = $(this.selector);

			//-----------------------------------------------------------

//			this.searcher = new VisualSearcher();
//			this.searcher.initialize();
//			
//			this.searcher.observe(function(sendor, topic, value){
//				switch(topic){
//				case 'search':
//					break;
//				}
//			}.bind(this));
		},

		//-----------------------------------------------------------
	};

	//-----------------------------------------------------------
	//-----------------------------------------------------------
	//-----------------------------------------------------------
	
	function defined(a, b){ return typeof a != 'undefined' ? a : b; }
	function definedBoolean(a, b){ return typeof a == 'boolean' ? a : b; }
	function definedNumber(a, b){ return typeof a == 'number' ? a : b; }
	function definedFunction(a, b){ return typeof a == 'function' ? a : b; }

	function mv(e, name) { return e.find('[mv-name="' + name + '"]'); }
	function n(e, name) { return e.find('[name="' + name + '"]'); }
	
	function eo(e) { return e.length ? e.get(0) : null; }
	function is(e, te) { return eo(e) == eo(te); }
	function has(e, te) {
		if (is(e, te)) return true;
		return e.has(te).length > 0;
	}

	function getBounds(e){
		return eo(e).getBoundingClientRect();
	}
	
	//-----------------------------------------------------------
	
	function task(constroller, func, timeOut){
		if (!timeOut) timeOut = 0;
		setTimeout(func.bind(constroller), timeOut);
	}
	
	//-----------------------------------------------------------

	function attachEvents(controller){
	}
	
	//-----------------------------------------------------------

})();
