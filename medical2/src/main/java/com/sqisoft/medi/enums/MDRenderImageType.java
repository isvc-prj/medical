package com.sqisoft.medi.enums;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

/**
 * 렌더링 이미지 타입
 * @author Administrator
 *
 */
public enum MDRenderImageType {
	MDRENDER_JPG("JPEG 이미지", 0, Arrays.asList("0", "jpg", "jpeg")),
	MDRENDER_PNG("PNG 이미지", 1, Arrays.asList("1", "png"));
	
	/**
	 * 표시내용
	 */
	private String title;
	/**
	 * 코드
	 */
	private int value;
	/**
	 * 코드 목록
	 */
	private List<String> list = new ArrayList<String>();
	
	MDRenderImageType(String title, int value, List<String> list){
		this.title = title;
		this.value = value;
		this.list = list;
	}
	
	/**
	 * 주어진 코드가 해당되는 지 확인합니다.
	 * @param code 코드
	 * @return 해당되면 true
	 */
	public boolean hasCode(String code){
		return list.stream()
				.anyMatch(type -> type.equalsIgnoreCase(code));
	}
	
	/**
	 * 표시내용을 가져옵니다. 
	 * @return 표시내용
	 */
	public String getTitle() { return title; }
	/**
	 * 코드를 가져옵니다. 
	 * @return 코드
	 */
	public int getValue() { return value; }
	
	/**
	 * 코드를 가져옵니다.
	 * <p>* JSON으로 변환 시 이 메서드의 값을 사용합니다.
	 */
	@Override
	@JsonValue
	public String toString() {
		return "" + this.value;
	}
	
	//-----------------------------------------------------------
	
	/**
	 * 코드로 권한 정보 타입을 가져옵니다.
	 * @param code 코드
	 * @return 권한 정보 타입
	 */
	@JsonCreator
	public static MDRenderImageType find(String code){
		return find(code, MDRENDER_JPG);
	}
	
	/**
	 * 코드로 권한 정보 타입을 가져옵니다.
	 * @param code 코드
	 * @param defaultType 기본 권한 정보 타입
	 * @return 권한 정보 타입
	 */
	public static MDRenderImageType find(String code, MDRenderImageType defaultType){
		if (code == null || code.isEmpty())
			return defaultType;
		
		return Arrays.stream(MDRenderImageType.values())
				.filter(type -> type.hasCode(code))
				.findAny()
				.orElse(defaultType);
	}
}
