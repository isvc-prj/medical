package com.sqisoft.medi.enums;

import java.util.Arrays;

import org.codehaus.jackson.annotate.JsonValue;

/**
 * 환자 성별
 * @author Administrator
 *
 */
public enum MDCfxIx {
	MDCFXIX_UNKNOWN("알수없음", 0),
	MDCFXIX_SCREENING("screening", 1),
	MDCFXIX_CRN_FU("CRN f/u", 2),
	MDCFXIX_BOWEL_HABIT_CHANGE("bowel habit change", 3),
	MDCFXIX_ABDOMINAL_PAIN("abdominal pain", 4),
	MDCFXIX_BLOOD_IN_STOOL("blood in stool (M/H/FOB)", 5),
	MDCFXIX_ANEMIA("anemia", 6),
	MDCFXIX_TENESMUS("tenesmus", 7),
	MDCFXIX_WT_LOSS("wt. loss", 8),
	MDCFXIX_CRC_FGX("CRC FGx", 9),
	MDCFXIX_CPP_EMR("CPP/EMR", 10),
	MDCFXIX_OTHERS("others", 11);
	
	/**
	 * 표시내용
	 */
	private String title;
	/**
	 * 코드
	 */
	private int value;
	
	MDCfxIx(String title, int value){
		this.title = title;
		this.value = value;
	}
	
	/**
	 * 표시내용을 가져옵니다. 
	 * @return 표시내용
	 */
	public String getTitle() { return title; }
	/**
	 * 코드를 가져옵니다. 
	 * @return 코드
	 */
	public Integer getValue() { return value; }
	
	/**
	 * 코드를 가져옵니다.
	 * <p>* JSON으로 변환 시 이 메서드의 값을 사용합니다.
	 */
	@Override
	@JsonValue
	public String toString() {
		return "" + this.value;
	}

	//-----------------------------------------------------------
	
	/**
	 * 코드로 권한 정보 타입을 가져옵니다.
	 * @param code 코드
	 * @return 권한 정보 타입
	 */
	public static MDCfxIx find(Integer code){
		return find(code, MDCFXIX_UNKNOWN);
	}
	
	/**
	 * 코드로 권한 정보 타입을 가져옵니다.
	 * @param code 코드
	 * @param defaultType 기본 권한 정보 타입
	 * @return 권한 정보 타입
	 */
	public static MDCfxIx find(Integer code, MDCfxIx defaultType){
		if (code == null)
			return defaultType;
		
		return Arrays.stream(MDCfxIx.values())
				.filter(type -> type.value == code)
				.findAny()
				.orElse(defaultType);
	}

}
