package com.sqisoft.medi;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.sqisoft.medi.data.filters.SearchMediaFilter;
import com.sqisoft.medi.services.MediaService;
import com.sqisoft.medi.utils.HtmlUtil;
import com.sqisoft.medi.utils.JsonUtil;
import com.sqisoft.medi.utils.SearchFilterUtil;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	@Autowired
	MediaService svc;
	
	//-----------------------------------------------------------
	
	//	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
		
	//	@RequestMapping(value = "/", method = RequestMethod.GET)
	//	public String home(Locale locale, Model model) {
	//		logger.info("Welcome home! The client locale is {}.", locale);
	//		
	//		Date date = new Date();
	//		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
	//		
	//		String formattedDate = dateFormat.format(date);
	//		
	//		model.addAttribute("serverTime", formattedDate );
	//		
	//		return "home";
	//	}
	
	//-----------------------------------------------------------
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(
			@RequestParam(required=false, defaultValue="") String q,
			Model model) throws Exception {
		Object list = null;
		SearchMediaFilter filter = SearchFilterUtil.get(q, model);
		
		if (filter != null) {
			list = svc.list(filter);			
		}else {
			list = svc.list();			
		}
		
		model.addAttribute("list", list);
		
		return "home";
	}
	
}
