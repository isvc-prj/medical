package com.sqisoft.medi;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.sqisoft.medi.dao.MediaStorageDao;
import com.sqisoft.medi.services.MediaService;

@Controller
public class ImageController {

	@Autowired
	MediaService svc;
	
	@Autowired
	private MediaStorageDao storageDao;
	
	//-----------------------------------------------------------
	
	@RequestMapping(value="/image/{id}", method = RequestMethod.GET)
	public void imageService(
			@PathVariable(value="id") String id,
			HttpServletRequest request,
			HttpServletResponse response) throws Exception {
	}
	
}
