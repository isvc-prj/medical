package com.sqisoft.medi.listeners;

import java.util.Date;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.FrameworkServlet;

import com.sqisoft.medi.services.MediaDropService;

/**
 * 세션 리스너입니다.
 * <p>* 세션이 종료될 떄 임시 폴더 및 파일들을 삭제합니다.
 * @author Administrator
 *
 */
public class MDHttpSessionListener implements HttpSessionListener, ServletContextListener {
	
	private static ServletContext context;
	
	private Logger logger = Logger.getLogger(MDHttpSessionListener.class);
	
	@Override
	public void contextDestroyed(ServletContextEvent event) {
		context = null;
	}

	@Override
	public void contextInitialized(ServletContextEvent event) {
		context = event.getServletContext();
	}
	
	//-----------------------------------------------------------------------------------

	/**
	 * 세션이 생성될 때 호출됩니다.
	 * <p>* 세션 아이디를 획득합니다.
	 */
	@Override
	public void sessionCreated(HttpSessionEvent event) {
		String sessionId = null;
		HttpSession session = event.getSession();

		if (session != null)
			sessionId = session.getId();
		
		System.out.println("[HTTP Session] Created, ID: " + sessionId);
		
		if (logger.isDebugEnabled()) {
            logger.debug("[HTTP Session] Session ID".concat(sessionId).concat(" created at ").concat(new Date().toString()));
        }
	}

	/**
	 * 세션 종료될 때 호출됩니다.
	 * <p>* 획득한 세션 아이디의 임시 폴더들을 삭제합니다.
	 * <p>* 임시 폴더 목록
	 * <ul>
	 * 	<li>/WEB-INF/docs/
	 * 	<li>/WEB-INF/files/tmp/
	 * </ul>
	 */
	@Override
	public void sessionDestroyed(HttpSessionEvent event) {
		String sessionId = null;
		HttpSession session = event.getSession();

		if (session != null)
			sessionId = session.getId();
		
		if (logger.isDebugEnabled()) {
            logger.debug("[HTTP Session] Session ID".concat(sessionId).concat(" destroyed at ").concat(new Date().toString()));
        }
		
		logDebug(logger, "[HTTP Session] Destoryed, ID: " + sessionId);
		
		if (context != null && session != null){
			logDebug(logger, "[HTTP Session] Try media drop, ID: " + sessionId);
			
			ApplicationContext appCtx = null;
			MediaDropService svc = null;
			
			try
			{
				//appCtx = WebApplicationContextUtils.getWebApplicationContext(session.getServletContext());
				appCtx = WebApplicationContextUtils.getWebApplicationContext(session.getServletContext(),
						FrameworkServlet.SERVLET_CONTEXT_PREFIX + "appServlet");
			}
			catch (Exception ex)
			{
				logError(logger, "[HTTP Session] Fail get application context !!!");
				logError(logger, ex);
			}

			if (appCtx != null){
				try
				{
					svc = (MediaDropService)appCtx.getBean("mediaDropService");
				}
				catch (Exception ex)
				{
					logError(logger, "[HTTP Session] Fail get media drop service !!!");
					logError(logger, ex);
				}
				
				if (svc != null) {
					svc.tryDrop(session, logger);
				}
			}
		}
	}
	
	//-----------------------------------------------------------------------------------
	
	private void logDebug(Logger logger, Object message) {
		if (logger != null && message != null && logger.isDebugEnabled())
			logger.debug(message.toString());
	}
	
	private void logError(Logger logger, Object message) {
		if (logger != null && message != null)
			logger.error(message.toString());
		
		if (message != null)
			System.out.println(message);
	}

}
