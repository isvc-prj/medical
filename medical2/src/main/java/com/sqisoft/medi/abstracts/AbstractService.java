package com.sqisoft.medi.abstracts;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;

import com.sqisoft.medi.transactions.DbTransaction;

@Service
public abstract class AbstractService {

	/**
	 * 트랜잭션 관리자
	 */
	@Autowired
	private DataSourceTransactionManager transactionManager;

	//-----------------------------------------------------------
	
	/**
	 * 트랜잭션을 시작합니다.
	 * @return 트랜잭션 정보
	 */
	public DbTransaction beginTransaction(){
		return DbTransaction.begin(transactionManager);
	}
	
}
