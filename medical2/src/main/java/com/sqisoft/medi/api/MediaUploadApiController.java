package com.sqisoft.medi.api;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.sqisoft.medi.abstracts.AbstractApiController;
import com.sqisoft.medi.dao.MediaDao;
import com.sqisoft.medi.dao.MediaStorageDao;
import com.sqisoft.medi.dao.MediaUploadDao;
import com.sqisoft.medi.data.AllocKey;
import com.sqisoft.medi.data.PlainText;
import com.sqisoft.medi.data.media.MediaLabelData;
import com.sqisoft.medi.data.media.MediaSnapshotData;
import com.sqisoft.medi.data.media.UploadImageFileInfo;
import com.sqisoft.medi.data.media.UploadLabel;
import com.sqisoft.medi.data.media.UploadMedia;
import com.sqisoft.medi.data.media.UploadMeta;
import com.sqisoft.medi.data.media.UploadRemovedSnapshot;
import com.sqisoft.medi.data.media.UploadSnapshot;
import com.sqisoft.medi.enums.MDMediaType;
import com.sqisoft.medi.enums.MDRenderImageType;
import com.sqisoft.medi.enums.MDYesNo;
import com.sqisoft.medi.storage.MediaStorage;
import com.sqisoft.medi.transactions.DbTransaction;
import com.sqisoft.medi.utils.FileUtil;
import com.sqisoft.medi.utils.GraphicsUtil;
import com.sqisoft.medi.utils.JsonUtil;
import com.sqisoft.medi.utils.WebUtil;
import com.sun.xml.internal.ws.message.MimeAttachmentSet;

/**
 * 미디어 업로드 지원 API
 * @author Administrator
 *
 */
@Controller
public class MediaUploadApiController extends AbstractApiController {
	/**
	 * HTTP 요청 정보
	 */
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private MediaDao rdao;
	
	@Autowired
	private MediaUploadDao dao;
	
	@Autowired
	private MediaStorageDao storageDao;
	
	//-----------------------------------------------------------
	
	private static final int SCREEN_THUMBNAIL_WIDTH = 320;
	private static final int SCREEN_THUMBNAIL_HEIGHT = 180;
	
	//-----------------------------------------------------------

	private String getUserId() {
		return "dev";
	}
	
	//-----------------------------------------------------------

	@RequestMapping(value="/api/media-upload/alloc", method=RequestMethod.POST)
	@ResponseBody
	public Object alloc() {
		String id = WebUtil.uid();
		
		//String id = "A345769BC9C44C31A1E37974F01DB1F2";
		//String id = "BBBBBB";
		
		// 17 자리
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		String alloctime = format.format(new Date());
		
		System.out.println("[ALLOC][TIME][ " + alloctime);
		System.out.println("[ALLOC][ID] " + id);
		
		return new AllocKey(id, alloctime);
	}
	
	//-----------------------------------------------------------

	@RequestMapping(value="/api/media-upload/prepare-modify", method=RequestMethod.POST)
	@ResponseBody
	public Object prepareModify(String uid) throws Exception {
		
		dao.prepareModify(uid);
	
//		MediaStorage ms = new MediaStorage(storageDao, uid);
//		ms.clear(MediaStorage.MS_SCREEN);
//		ms.clear(MediaStorage.MS_SNAPSHOT);
		
		return new PlainText("ok");
	}
	
	//-----------------------------------------------------------

	@RequestMapping(value="/api/media-upload/upload", method=RequestMethod.POST)
	@ResponseBody
	public Object upload(
			MultipartHttpServletRequest request,
			@RequestParam(value="file", required=false) MultipartFile mfile,
			@RequestParam(value="fileSecond", required=false) MultipartFile mfileSecond) throws Exception {
		String utype = request.getParameter("u-type");
		if (utype == null) utype = "";
		
		String uid = request.getParameter("uid");
		if (uid == null || uid.isEmpty()) {
			throw new Exception("UID is null or empty");
		}
		
		String meta = request.getParameter("meta");
		String userId = getUserId();
		
		if (utype.equalsIgnoreCase("media")) {
			MediaStorage ms = new MediaStorage(storageDao, uid);
			ms.move(MediaStorage.MS_MEDIA, mfile);
			
			//-----------------------------------------------------------
			
			String ip = WebUtil.getRemoteIP();
		
			dao.prepareNew(uid, ms.storageID(), ip, userId);
			
			//-----------------------------------------------------------
		
			String name = mfile.getOriginalFilename();
			String contentType = mfile.getContentType();
			long size = mfile.getSize();
			
			//-----------------------------------------------------------
		
			UploadMedia d = new UploadMedia();
			d.mimeType = contentType;
			d.width = getRequestInt(request, "width");
			d.height = getRequestInt(request, "height");
			d.size = size;
			d.name = name;
			
			//-----------------------------------------------------------
			
			dao.media(uid, d);
		
		}else if (utype.equalsIgnoreCase("meta")) {
			
			UploadMeta d = JsonUtil.decode(UploadMeta.class, meta);
			
			dao.meta(uid, d);
			
		}else if (utype.equalsIgnoreCase("screen")) {
			
			UploadImageFileInfo d = JsonUtil.decode(UploadImageFileInfo.class, meta);
			String filename = d.name;
			
			//-----------------------------------------------------------
			
			MediaStorage ms = new MediaStorage(storageDao, uid);
			File destFile = ms.move(MediaStorage.MS_SCREEN, mfile, filename);
			
			//-----------------------------------------------------------
			
			GraphicsUtil.ThumbnailImageResult r = null;
			File thumbFile = null;
			int thumbWidth = 0, thumbHeight = 0;
			
			if (destFile != null) {
				r = GraphicsUtil.renderThumbnail(destFile, SCREEN_THUMBNAIL_WIDTH, SCREEN_THUMBNAIL_HEIGHT, MDRenderImageType.MDRENDER_JPG);
				
				String basename = FilenameUtils.getBaseName(filename);
				
				thumbFile = ms.file(MediaStorage.MS_SCREEN, "thumb_" + basename + ".jpg");
				thumbWidth = r.width;
				thumbHeight = r.height;
				
				GraphicsUtil.write(r.image, r.renderType, thumbFile);
				
				r.dispose();
				r = null;
			}
		
			//-----------------------------------------------------------
			
			dao.screen(uid, d);
			
			if (thumbFile != null) {
				d.width = thumbWidth;
				d.height = thumbHeight;
				d.name = thumbFile.getName();
				d.size = thumbFile.length();
				
				dao.thumbnail(uid, d);
			}
			
		}else if (utype.equalsIgnoreCase("snapshot")) {
			
			UploadSnapshot d = JsonUtil.decode(UploadSnapshot.class, meta);
			String filename = d.name;
			String thumbFileName = d.thumbnail.name;

			//-----------------------------------------------------------
			
			MediaStorage ms = new MediaStorage(storageDao, uid);
			ms.move(MediaStorage.MS_SNAPSHOT, mfile, filename);
			
			ms.move(MediaStorage.MS_SNAPSHOT, mfileSecond, thumbFileName);

			//-----------------------------------------------------------
			
			if (d.hasLabels()) {
				int len = d.labels.size();
				for (int i=0; i < len; i++) {
					UploadLabel label = d.labels.get(i);
					
					MultipartFile file = request.getFile(label.fieldname);
					
					ms.move(MediaStorage.MS_SNAPSHOT, file, label.filename);
				}					
			}

			//-----------------------------------------------------------
			
			dao.snapshot(uid, d);
			
		}else {
			throw new Exception("Unknown");
		}
		
		return new PlainText("ok");
	}
	
	//-----------------------------------------------------------

	@RequestMapping(value="/api/media-upload/upload-modify", method=RequestMethod.POST)
	@ResponseBody
	public Object uploadModify(
			MultipartHttpServletRequest request,
			@RequestParam(value="file", required=false) MultipartFile mfile,
			@RequestParam(value="fileSecond", required=false) MultipartFile mfileSecond) throws Exception {
		String utype = request.getParameter("u-type");
		if (utype == null) utype = "";
		
		String uid = request.getParameter("uid");
		if (uid == null || uid.isEmpty()) {
			throw new Exception("UID is null or empty");
		}
		
		String meta = request.getParameter("meta");
		String userId = "dev";
		
		if (utype.equalsIgnoreCase("removed-snapshot")) {
			UploadRemovedSnapshot d = JsonUtil.decode(UploadRemovedSnapshot.class, meta);
			
			if (d != null && d.times != null && d.times.size() > 0) {
				MediaStorage ms = new MediaStorage(storageDao, uid);
				
				int len = d.times.size();
				for (int i=0; i < len; i++) {
					double time = d.times.get(i);
					
					//-----------------------------------------------------------
					// 스냅샷 라벨 이미지 삭제
					
					List<String> labels = dao.listSnapshotLabelFiles(uid, time);
					int numLabels = labels != null ? labels.size() : 0;
					for (int ilabel=0; ilabel < numLabels; ilabel++) {
						String labelFile = labels.get(ilabel);
						ms.remove(MediaStorage.MS_SNAPSHOT, labelFile);
					}
					
					//-----------------------------------------------------------
					// 스냅샷 이미지 삭제
					
					MediaSnapshotData sd = rdao.getSnapshot(uid, time);
					if (sd != null) {
						ms.remove(MediaStorage.MS_SNAPSHOT, sd.getSnapFile());
						ms.remove(MediaStorage.MS_SNAPSHOT, sd.getThumbnailFile());
					}
					
					dao.removeSnapshot(uid, time);
				}
			}
		}else if (utype.equalsIgnoreCase("snapshot")) {
			
			UploadSnapshot d = JsonUtil.decode(UploadSnapshot.class, meta);
			String filename = d.name;
			String thumbFileName = d.thumbnail.name;

			//-----------------------------------------------------------
			
			MediaStorage ms = new MediaStorage(storageDao, uid);
			
			//-----------------------------------------------------------
			// 기존 스냅샷 라벨 이미지 삭제
			
			List<String> labels = dao.listSnapshotLabelFiles(uid, d.time);
			int numLabels = labels != null ? labels.size() : 0;
			for (int ilabel=0; ilabel < numLabels; ilabel++) {
				String labelFile = labels.get(ilabel);
				ms.remove(MediaStorage.MS_SNAPSHOT, labelFile);
			}

			//-----------------------------------------------------------
			// 스냅샵 라벨 이미지
			
			if (d.hasLabels()) {
				int len = d.labels.size();
				for (int i=0; i < len; i++) {
					UploadLabel label = d.labels.get(i);
					
					MultipartFile file = request.getFile(label.fieldname);
					
					ms.move(MediaStorage.MS_SNAPSHOT, file, label.filename);
				}					
			}

			//-----------------------------------------------------------
			// 스냅샵 이미지
			
			if (d.server) {
				ms.move(MediaStorage.MS_SNAPSHOT, mfileSecond, thumbFileName);

				//-----------------------------------------------------------
				
				dao.modifySnapshot(uid, d.time, d);
			}else {

				//-----------------------------------------------------------
				
				ms.move(MediaStorage.MS_SNAPSHOT, mfile, filename);
				
				ms.move(MediaStorage.MS_SNAPSHOT, mfileSecond, thumbFileName);

				//-----------------------------------------------------------
				
				dao.snapshot(uid, d);
			}
		}else if (utype.equalsIgnoreCase("meta")) {
			
			UploadMeta d = JsonUtil.decode(UploadMeta.class, meta);
			
			dao.meta(uid, d);
		}else {
			throw new Exception("Unknown");
		}
		
		return new PlainText("ok");
	}
	
	//-----------------------------------------------------------

	@RequestMapping(value="/api/media-upload/upload-image", method=RequestMethod.POST)
	@ResponseBody
	public Object uploadImage(
			MultipartHttpServletRequest request,
			@RequestParam(required=true) int width,
			@RequestParam(required=true) int height,
			@RequestParam(value="file", required=true) MultipartFile mImageFile,
			@RequestParam(required=true) String screenMeta,
			@RequestParam(value="screenFile", required=true) MultipartFile mScreenFile,
			@RequestParam(required=false) String snapshotMeta,
			@RequestParam(value="snapshotFile", required=false) MultipartFile mSnapshotFile,
			@RequestParam(required=true) String meta) throws Exception {
		
		String uid = WebUtil.uid();
		String ip = WebUtil.getRemoteIP();
		String userId = getUserId();
		
		boolean existSnapshotMeta = snapshotMeta != null && !snapshotMeta.isEmpty();
		
		UploadImageFileInfo screenMetaData = JsonUtil.decode(UploadImageFileInfo.class, screenMeta);
		UploadSnapshot snapshotMetaData =  existSnapshotMeta ? JsonUtil.decode(UploadSnapshot.class, snapshotMeta) : null;
		UploadMeta metaData = JsonUtil.decode(UploadMeta.class, meta);

		MediaStorage ms = new MediaStorage(storageDao, uid);
		
		DbTransaction trans = null;
		
		try
		{
			ms.move(MediaStorage.MS_MEDIA, mImageFile);
			File destScreenFile = ms.move(MediaStorage.MS_SCREEN, mScreenFile, screenMetaData.name);
			
			if (mSnapshotFile != null && mSnapshotFile.getSize() > 0)
				ms.move(MediaStorage.MS_SNAPSHOT, mSnapshotFile, snapshotMetaData.thumbnail.name);

			//-----------------------------------------------------------
			// 트랜잭션 시작
			
			trans = dao.beginTransaction();
	
			//-----------------------------------------------------------
			// 기본 등록
			
			dao.prepareNew(uid, ms.storageID(), ip, userId);
			
			//-----------------------------------------------------------
			// 이미지 등록
		
			String name = mImageFile.getOriginalFilename();
			String contentType = mImageFile.getContentType();
			long size = mImageFile.getSize();
			
			//-----------------------------------------------------------
		
			UploadMedia imageData = new UploadMedia();
			imageData.mimeType = contentType;
			imageData.width = width;
			imageData.height = height;
			imageData.size = size;
			imageData.name = name;
			
			//-----------------------------------------------------------
			
			dao.media(uid, imageData);
			
			//-----------------------------------------------------------
			// 스크린 이미지 등록
			
			GraphicsUtil.ThumbnailImageResult r = null;
			File thumbFile = null;
			int thumbWidth = 0, thumbHeight = 0;
			
			if (destScreenFile != null) {
				r = GraphicsUtil.renderThumbnail(destScreenFile, SCREEN_THUMBNAIL_WIDTH, SCREEN_THUMBNAIL_HEIGHT, MDRenderImageType.MDRENDER_JPG);
				
				String basename = FilenameUtils.getBaseName(screenMetaData.name);
				
				thumbFile = ms.file(MediaStorage.MS_SCREEN, "thumb_" + basename + ".jpg");
				thumbWidth = r.width;
				thumbHeight = r.height;
				
				GraphicsUtil.write(r.image, r.renderType, thumbFile);
				
				r.dispose();
				r = null;
			}
			
			//-----------------------------------------------------------
			
			dao.screen(uid, screenMetaData);
			
			if (thumbFile != null) {
				screenMetaData.width = thumbWidth;
				screenMetaData.height = thumbHeight;
				screenMetaData.name = thumbFile.getName();
				screenMetaData.size = thumbFile.length();
				
				dao.thumbnail(uid, screenMetaData);
			}
			
			//-----------------------------------------------------------
			// 라벨 이미지 등록
			
			if (snapshotMetaData != null) {
				if (snapshotMetaData.hasLabels()) {
					int len = snapshotMetaData.labels.size();
					for (int i=0; i < len; i++) {
						UploadLabel label = snapshotMetaData.labels.get(i);
						
						MultipartFile file = request.getFile(label.fieldname);
						
						ms.move(MediaStorage.MS_SNAPSHOT, file, label.filename);
					}					
				}
				
				dao.snapshot(uid, snapshotMetaData);
			}
			
			//-----------------------------------------------------------
			// 마스크 + 환자정보 + 수술정보 등록
			
			dao.meta(uid, metaData);
			
			//-----------------------------------------------------------
			// 완료
			
			dao.complete(uid);
			
			//-----------------------------------------------------------
			// 커밋
			
			trans.commit();
		}
		catch(Exception ex) {
			if (ms != null) {
				ms.clear();
			}
			
			//-----------------------------------------------------------
			// 롤백
			
			if (trans != null)
				trans.rollback();
			
			throw ex;
		}
		
		return new PlainText("ok");
	}
	
	//-----------------------------------------------------------

	@RequestMapping(value="/api/media-upload/upload-modify-image", method=RequestMethod.POST)
	@ResponseBody
	public Object uploadModifyImage(
			MultipartHttpServletRequest request,
			@RequestParam(required=true) String uid,
			@RequestParam(required=true) String screenMeta,
			@RequestParam(value="screenFile", required=true) MultipartFile mScreenFile,
			@RequestParam(required=false) String snapshotMeta,
			@RequestParam(value="snapshotFile", required=false) MultipartFile mSnapshotFile,
			@RequestParam(required=true) String meta) throws Exception {
		
		boolean existSnapshotMeta = snapshotMeta != null && !snapshotMeta.isEmpty();
		
		UploadImageFileInfo screenMetaData = JsonUtil.decode(UploadImageFileInfo.class, screenMeta);
		UploadSnapshot snapshotMetaData =  existSnapshotMeta ? JsonUtil.decode(UploadSnapshot.class, snapshotMeta) : null;
		UploadMeta metaData = JsonUtil.decode(UploadMeta.class, meta);

		MediaStorage ms = new MediaStorage(storageDao, uid);
		
		DbTransaction trans = null;
		
		try
		{
			//-----------------------------------------------------------
			// 새 파일 쓰기
			
			File destScreenFile = ms.move(MediaStorage.MS_SCREEN, mScreenFile, screenMetaData.name, true);
			
			if (mSnapshotFile != null && mSnapshotFile.getSize() > 0)
				ms.move(MediaStorage.MS_SNAPSHOT, mSnapshotFile, snapshotMetaData.thumbnail.name, true);

			//-----------------------------------------------------------
			// 트랜잭션 시작
			
			trans = dao.beginTransaction();
	
			//-----------------------------------------------------------
			// 기본 등록
			
			dao.prepareModify(uid);
		
			//-----------------------------------------------------------
			// 스크린 이미지 등록
			
			GraphicsUtil.ThumbnailImageResult r = null;
			File thumbFile = null;
			int thumbWidth = 0, thumbHeight = 0;
			
			if (destScreenFile != null) {
				r = GraphicsUtil.renderThumbnail(destScreenFile, SCREEN_THUMBNAIL_WIDTH, SCREEN_THUMBNAIL_HEIGHT, MDRenderImageType.MDRENDER_JPG);
				
				String basename = FilenameUtils.getBaseName(screenMetaData.name);
				
				thumbFile = ms.file(MediaStorage.MS_SCREEN, "thumb_" + basename + ".jpg");
				if (thumbFile.exists())
					thumbFile.delete();
				
				thumbWidth = r.width;
				thumbHeight = r.height;
				
				GraphicsUtil.write(r.image, r.renderType, thumbFile);
				
				r.dispose();
				r = null;
			}
			
			//-----------------------------------------------------------
			
			dao.screen(uid, screenMetaData);
			
			if (thumbFile != null) {
				screenMetaData.width = thumbWidth;
				screenMetaData.height = thumbHeight;
				screenMetaData.name = thumbFile.getName();
				screenMetaData.size = thumbFile.length();
				
				dao.thumbnail(uid, screenMetaData);
			}
			
			//-----------------------------------------------------------
			// 기존 라벨 이미지 삭제
			
			List<String> labels = dao.listLabelFiles(uid);
			int numLabels = labels != null ? labels.size() : 0;
			for (int ilabel=0; ilabel < numLabels; ilabel++) {
				String labelFile = labels.get(ilabel);
				ms.remove(MediaStorage.MS_SNAPSHOT, labelFile);
			}
			
			//-----------------------------------------------------------
			// 라벨 이미지 등록
			
			dao.clearSnapshots(uid);
			
			if (snapshotMetaData != null) {
				if (snapshotMetaData.hasLabels()) {
					int len = snapshotMetaData.labels.size();
					for (int i=0; i < len; i++) {
						UploadLabel label = snapshotMetaData.labels.get(i);
						
						MultipartFile file = request.getFile(label.fieldname);
						
						ms.move(MediaStorage.MS_SNAPSHOT, file, label.filename);
					}					
				}
				
				dao.snapshot(uid, snapshotMetaData);
			}
			
			//-----------------------------------------------------------
			// 마스크 + 환자정보 + 수술정보 등록
			
			dao.meta(uid, metaData);
			
			//-----------------------------------------------------------
			// 완료
			
			dao.complete(uid);
			
			//-----------------------------------------------------------
			// 커밋
			
			trans.commit();
		}
		catch(Exception ex) {
			if (ms != null) {
				ms.clear();
			}
			
			//-----------------------------------------------------------
			// 롤백
			
			if (trans != null)
				trans.rollback();
			
			throw ex;
		}
		
		return new PlainText("ok");
	}
	
	//-----------------------------------------------------------

	@RequestMapping(value="/api/media-upload/complete", method=RequestMethod.POST)
	@ResponseBody
	public Object complete(String uid) {
		dao.complete(uid);
		
		return new PlainText("ok");
	}
	
	//-----------------------------------------------------------

	@RequestMapping(value="/api/media-upload/cancel", method=RequestMethod.POST)
	@ResponseBody
	public Object cancel(String uid) throws Exception {
		MediaStorage ms = new MediaStorage(storageDao, uid);
		ms.clear();
		
		dao.clear(uid);
		
		return new PlainText("ok");
	}
	
	//-----------------------------------------------------------

	@RequestMapping(value="/api/media-upload/complete-modify", method=RequestMethod.POST)
	@ResponseBody
	public Object completeModify(String uid) {
		dao.complete(uid);
		
		return new PlainText("ok");
	}
	
	//-----------------------------------------------------------

	@RequestMapping(value="/api/media-upload/cancel-modify", method=RequestMethod.POST)
	@ResponseBody
	public Object cancelModify(String uid) throws Exception {
		return new PlainText("ok");
	}

	//-----------------------------------------------------------
	
	private int getRequestInt(HttpServletRequest request, String name) {
		String value = request.getParameter(name);
		return Integer.parseInt(value);
	}
	
	private long getRequestLong(HttpServletRequest request, String name) {
		String value = request.getParameter(name);
		return Long.parseLong(value);
	}
}
