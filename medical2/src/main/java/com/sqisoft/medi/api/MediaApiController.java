package com.sqisoft.medi.api;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.sqisoft.medi.abstracts.AbstractApiController;
import com.sqisoft.medi.dao.MediaDao;
import com.sqisoft.medi.dao.MediaStorageDao;
import com.sqisoft.medi.dao.MediaUploadDao;
import com.sqisoft.medi.data.AllocKey;
import com.sqisoft.medi.data.PlainText;
import com.sqisoft.medi.data.media.MediaVO;
import com.sqisoft.medi.data.media.UploadImageFileInfo;
import com.sqisoft.medi.data.media.UploadMedia;
import com.sqisoft.medi.data.media.UploadMeta;
import com.sqisoft.medi.data.media.UploadSnapshot;
import com.sqisoft.medi.enums.MDMediaType;
import com.sqisoft.medi.enums.MDRenderImageType;
import com.sqisoft.medi.enums.MDYesNo;
import com.sqisoft.medi.services.MediaService;
import com.sqisoft.medi.storage.MediaStorage;
import com.sqisoft.medi.utils.FileUtil;
import com.sqisoft.medi.utils.GraphicsUtil;
import com.sqisoft.medi.utils.JsonUtil;
import com.sqisoft.medi.utils.WebUtil;
import com.sun.xml.internal.ws.message.MimeAttachmentSet;

/**
 * 미디어 조회 지원 API
 * @author Administrator
 *
 */
@Controller
public class MediaApiController extends AbstractApiController {
	
	@Autowired
	private MediaService svc;
	
	@Autowired
	private MediaStorageDao storageDao;
	
	//-----------------------------------------------------------

	@RequestMapping(value="/api/media/{id}", method=RequestMethod.POST)
	@ResponseBody
	public Object media(@PathVariable(value="id") String uid) {
		MediaVO vo = svc.getMediaVO(uid);
		
		return vo;
	}

	@RequestMapping(value="/api/media/remove", method=RequestMethod.POST)
	@ResponseBody
	public Object removeMedia(String uid) throws Exception {
		svc.requestRemove(uid);
		
		return new PlainText("ok");
	}
	
}
