package com.sqisoft.medi.dao;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sqisoft.medi.abstracts.AbstractDao;
import com.sqisoft.medi.data.media.MediaData;
import com.sqisoft.medi.transactions.DbTransaction;

@SuppressWarnings("unchecked")
@Repository
public class MediaDropDao extends AbstractDao {

	public List<MediaData> listDropped() {
		return sqlSession.selectList("md:list-drop");
	}
	
	public void drop(String uid) {
		HashMap<String, Object> param = new HashMap<String, Object>();
		
		param.put("UID", uid);

		DbTransaction tran = beginTransaction();
		
		try
		{
			sqlSession.delete("md:drop-media", param);
			sqlSession.delete("md:drop-patient_operation", param);
			sqlSession.delete("md:drop-masks", param);
			sqlSession.delete("md:drop-snapshots", param);
			sqlSession.delete("md:drop-labels", param);
			
			tran.commit();
			
			System.out.println("<<<DROP::COMMIT>>>");
		}
		catch (Exception e)
		{
			tran.rollback();
			
			System.out.println("<<<DROP::ROLLBACK>>>");
		}
	}
}
