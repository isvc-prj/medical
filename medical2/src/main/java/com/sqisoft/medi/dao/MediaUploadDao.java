package com.sqisoft.medi.dao;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sqisoft.medi.abstracts.AbstractDao;
import com.sqisoft.medi.data.RectInfo;
import com.sqisoft.medi.data.media.MediaLabelData;
import com.sqisoft.medi.data.media.UploadImageFileInfo;
import com.sqisoft.medi.data.media.UploadLabel;
import com.sqisoft.medi.data.media.UploadMask;
import com.sqisoft.medi.data.media.UploadMedia;
import com.sqisoft.medi.data.media.UploadMeta;
import com.sqisoft.medi.data.media.UploadOperation;
import com.sqisoft.medi.data.media.UploadPatient;
import com.sqisoft.medi.data.media.UploadSnapshot;
import com.sqisoft.medi.data.storage.MediaStorageAttribute;
import com.sqisoft.medi.enums.MDYesNo;
import com.sqisoft.medi.transactions.DbTransaction;
import com.sqisoft.medi.utils.DateUtil;
import com.sqisoft.medi.utils.JsonUtil;

@SuppressWarnings("unchecked")
@Repository
public class MediaUploadDao extends AbstractDao {
	
	public void prepareNew(String uid, String storageID, String ip, String userId) {
		HashMap<String, Object> param = new HashMap<String, Object>();
		
		String dt = DateUtil.YMDHIS();
		
		param.put("UID", uid);
		param.put("STORAGE_ID", storageID);
		param.put("REG_ID", userId);
		param.put("IP", ip);
		param.put("REG_DT", dt);
		
		sqlSession.insert("mu:prepare-new", param);
	}
	
	public void prepareModify(String uid) {
		HashMap<String, Object> param = new HashMap<String, Object>();
		
		param.put("UID", uid);

		String dt = DateUtil.YMDHIS();
		param.put("UPDT_DT", dt);
		
		sqlSession.update("mu:prepare-update", param);
	}

	//-----------------------------------------------------------

	public void complete(String uid) {
		HashMap<String, Object> param = new HashMap<String, Object>();
	
		param.put("UID", uid);
		
		sqlSession.update("mu:complete", param);		
	}

	//-----------------------------------------------------------
	
	public void clear(String uid) {
		HashMap<String, Object> param = new HashMap<String, Object>();
		
		param.put("UID", uid);

		DbTransaction tran = beginTransaction();
		
		try
		{
			sqlSession.delete("mu:delete-media", param);
			sqlSession.delete("mu:delete-patient_operation", param);
			sqlSession.delete("mu:delete-masks", param);
			sqlSession.delete("mu:delete-snapshots", param);
			sqlSession.delete("mu:delete-labels", param);
			
			tran.commit();
			
			System.out.println("<<<DB::CLEAR::COMMIT>>>");
		}
		catch (Exception e)
		{
			tran.rollback();
			
			System.out.println("<<<DB::CLEAR::ROLLBACK>>>");
		}
	}
	
	//-----------------------------------------------------------
	//-----------------------------------------------------------
	//-----------------------------------------------------------
	
	public void media(String uid, UploadMedia data) {
		HashMap<String, Object> param = new HashMap<String, Object>();
		
		param.put("UID", uid);
		
		param.put("MIME", data.mimeType);
		param.put("WIDTH", data.width);
		param.put("HEIGHT", data.height);
		param.put("NAME", data.name);
		param.put("SIZ", data.size);
		
		sqlSession.update("mu:media", param);
	}
	
	public void screen(String uid, UploadImageFileInfo data) {
		HashMap<String, Object> param = new HashMap<String, Object>();
		
		param.put("UID", uid);
		
		param.put("WIDTH", data.width);
		param.put("HEIGHT", data.height);
		param.put("NAME", data.name);
		param.put("SIZ", data.size);
		
		sqlSession.update("mu:screen", param);
	}
	
	public void thumbnail(String uid, UploadImageFileInfo data) {
		HashMap<String, Object> param = new HashMap<String, Object>();
		
		param.put("UID", uid);
		
		param.put("WIDTH", data.width);
		param.put("HEIGHT", data.height);
		param.put("NAME", data.name);
		param.put("SIZ", data.size);
		
		sqlSession.update("mu:thumbnail", param);
	}
	
	public void meta(String uid, UploadMeta data) {
		UploadPatient patient = data.patient;
		UploadOperation operation = data.operation;
		
		HashMap<String, Object> param = new HashMap<String, Object>();
		
		//-----------------------------------------------------------
		
		param.put("UID", uid);
		
		param.put("TITLE", data.title);
		param.put("TYPE", data.type.toString());
		param.put("DURATION", data.duration);
		
		sqlSession.update("mu:meta", param);
		
		//-----------------------------------------------------------

		param.clear();		
		param.put("UID", uid);
		
		sqlSession.delete("mu:delete-patient_operation", param);
	
		//-----------------------------------------------------------
		
		param.clear();

		//-----------------------------------------------------------
		
		param.put("UID", uid);
		
		param.put("PAT_NM", patient.name);
		param.put("PAT_NO", patient.no);
		param.put("PAT_ID", patient.id);
		param.put("PAT_REG_DT", patient.regDate);
		param.put("PAT_GEN", patient.gender);
		param.put("PAT_AGE", patient.age);
		param.put("PAT_WGT", patient.weight);
		param.put("PAT_HGT", patient.height);
		param.put("PAT_CFS_IX", patient.cfxIx);
		param.put("PAT_EXC", patient.exclusion);
		param.put("PAT_HIS", patient.history);
		param.put("PAT_EOC_STAT", patient.endoscopyStat);
		
		param.put("OP_APPX_REACH", operation.appendixReach);
		param.put("OP_APPX_REACH_TM", operation.appendixReachTime);
		param.put("OP_APPX_REACH_REM_TM", operation.appendixReachRemoveTime);
		param.put("OP_APPX_TOTAL_TM", operation.appendixTotalTime);
		param.put("OP_RIGHT_COL", operation.rightColon);
		param.put("OP_TRANS_COL", operation.transColon);
		param.put("OP_LEFT_COL", operation.leftColon);
		param.put("OP_BBPS", operation.bbps);
		param.put("OP_BBPS_FIT", operation.bbpsFitness);
		param.put("OP_CFX_OPI", operation.cfxOpinion);
		param.put("OP_POLYP_CNT", operation.polypCnt);
		
		sqlSession.insert("mu:patient_operation", param);
		
		//-----------------------------------------------------------

		param.clear();
		
		param.put("UID", uid);
		
		sqlSession.delete("mu:delete-masks", param);
	
		//-----------------------------------------------------------
		
		if (data.masks != null && data.masks.size() > 0) {
			int len = data.masks.size();
			for (int i=0; i < len; i++) {
				UploadMask mask = data.masks.get(i);
				
				int seq = i + 1;
				
				mask(uid, seq, mask);
			}
		}
	}
	
	public void snapshot(String uid, UploadSnapshot data) {
		HashMap<String, Object> param = new HashMap<String, Object>();
		
		param.put("UID", uid);
		param.put("SNAP_TM", data.time);
		
		param.put("SNAP_WIDTH", data.width);
		param.put("SNAP_HEIGHT", data.height);
		param.put("SNAP_FILE", data.name);
		param.put("SNAP_SIZ", data.size);
		
		param.put("SNAP_THUMB_WIDTH", data.thumbnail.width);
		param.put("SNAP_THUMB_HEIGHT", data.thumbnail.height);
		param.put("SNAP_THUMB_FILE", data.thumbnail.name);
		param.put("SNAP_THUMB_SIZ", data.thumbnail.size);
	
		sqlSession.insert("mu:snapshot", param);
		
		//-----------------------------------------------------------

		param.clear();
		
		param.put("UID", uid);
		param.put("SNAP_TM", data.time);
		
		sqlSession.delete("mu:delete-snapshot-labels", param);
		
		//-----------------------------------------------------------
	
		if (data.hasLabels()) {
			int len = data.labels.size();
			for (int i=0; i < len; i++) {
				UploadLabel label = data.labels.get(i);
				
				int seq = i + 1;
				
				label(uid, seq, label);
			}
		}
	}
	
	public void modifySnapshot(String uid, double time, UploadSnapshot data) {
		HashMap<String, Object> param = new HashMap<String, Object>();
		
		param.put("UID", uid);
		param.put("SNAP_TM", time);
		
		param.put("SNAP_THUMB_WIDTH", data.thumbnail.width);
		param.put("SNAP_THUMB_HEIGHT", data.thumbnail.height);
		param.put("SNAP_THUMB_FILE", data.thumbnail.name);
		param.put("SNAP_THUMB_SIZ", data.thumbnail.size);
	
		sqlSession.update("mu:modify-snapshot", param);
		
		//-----------------------------------------------------------

		param.clear();
		
		param.put("UID", uid);
		param.put("SNAP_TM", data.time);
		
		sqlSession.delete("mu:delete-snapshot-labels", param);
		
		//-----------------------------------------------------------
	
		if (data.labels != null && data.labels.size() > 0) {
			int len = data.labels.size();
			for (int i=0; i < len; i++) {
				UploadLabel label = data.labels.get(i);
				
				int seq = i + 1;
				
				label(uid, seq, label);
			}
		}
	}
	
	//-----------------------------------------------------------
	//-----------------------------------------------------------
	//-----------------------------------------------------------
	
	public void mask(String uid, int seq, UploadMask data) {
		HashMap<String, Object> param = new HashMap<String, Object>();
		
		param.put("UID", uid);
		param.put("SEQ", data.seq);
		
		param.put("X", data.x);
		param.put("Y", data.y);
		param.put("WIDTH", data.width);
		param.put("HEIGHT", data.height);
		
		sqlSession.insert("mu:mask", param);
	}
	
	public void label(String uid, int seq, UploadLabel data) {
		HashMap<String, Object> param = new HashMap<String, Object>();
		
		param.put("UID", uid);
		param.put("SNAP_TM", data.time);
		
		param.put("SEQ", seq);
		
		param.put("X", data.x);
		param.put("Y", data.y);
		param.put("WIDTH", data.width);
		param.put("HEIGHT", data.height);
		
		if (data.points != null) {
			String s = JsonUtil.encode(data.points);
			param.put("DAT", s);
		}
		
		param.put("POLYP_POS", data.polypPos);
		param.put("POLYP_SIZ", data.polypSize);
		param.put("POLYP_SHAPE", data.polypShape);
		param.put("POLYP_BIO", data.polypBio);
		
		param.put("FILE", data.filename);
		
		sqlSession.insert("mu:label", param);
	}
	
	//-----------------------------------------------------------

	public void removeSnapshot(String uid, double time) {
		HashMap<String, Object> param = new HashMap<String, Object>();
		
		param.put("UID", uid);
		param.put("SNAP_TM", time);
		
		sqlSession.delete("mu:delete-snapshot", param);
		sqlSession.delete("mu:delete-snapshot-labels", param);
	}

	//-----------------------------------------------------------
	
	public void clearSnapshots(String uid) {
		HashMap<String, Object> param = new HashMap<String, Object>();
		
		param.put("UID", uid);
		
		sqlSession.delete("mu:delete-snapshots", param);
		sqlSession.delete("mu:delete-labels", param);
	}

	//-----------------------------------------------------------
	// 조회
	//-----------------------------------------------------------

	public List<String> listLabelFiles(String uid){
		HashMap<String, Object> param = new HashMap<String, Object>();
		
		param.put("UID", uid);
		
		return (List<String>) sqlSession.selectList("mu:label-files:list", param);
	}

	public List<String> listSnapshotLabelFiles(String uid, double time){
		HashMap<String, Object> param = new HashMap<String, Object>();
		
		param.put("UID", uid);
		param.put("SNAP_TM", time);
		
		return (List<String>) sqlSession.selectList("mu:snapshot-label-files:list", param);
	}
	
}
