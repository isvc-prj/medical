package com.sqisoft.medi.dao;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.sqisoft.medi.abstracts.AbstractDao;
import com.sqisoft.medi.data.storage.MediaStorageAttribute;

@SuppressWarnings("unchecked")
@Repository
public class MediaStorageDao extends AbstractDao {
	
	public MediaStorageAttribute get (String uid) {
		String sid = getMediaStorage(uid);
		if (sid == null) {
			return getWritable();
		}else {
			return getStorage(sid);
		}
	}
	
	//-----------------------------------------------------------------------------------
	
	public MediaStorageAttribute getWritable () {
		List<MediaStorageAttribute> list = sqlSession.selectList("ms:get-writable");
		return list.get(0);
	}
	
	public MediaStorageAttribute getStorage (String sid) {
		HashMap<String, Object> param = new HashMap<String, Object>();
		param.put("SID", sid);
		
		List<MediaStorageAttribute> list = sqlSession.selectList("ms:get:storage", param);
		return list.size() < 1 ? null : list.get(0);
	}
	
	public String getMediaStorage (String uid) {
		HashMap<String, Object> param = new HashMap<String, Object>();
		param.put("UID", uid);
		
		String sid = (String)sqlSession.selectOne("ms:get:media-storage", param);
		if (sid == null || sid.isEmpty())
			return null;
		return sid;
	}
	
}
