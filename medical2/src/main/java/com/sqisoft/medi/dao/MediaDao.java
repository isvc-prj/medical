package com.sqisoft.medi.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.sqisoft.medi.abstracts.AbstractDao;
import com.sqisoft.medi.data.filters.SearchMediaFilter;
import com.sqisoft.medi.data.media.MediaData;
import com.sqisoft.medi.data.media.MediaLabelData;
import com.sqisoft.medi.data.media.MediaMaskData;
import com.sqisoft.medi.data.media.MediaPatOpData;
import com.sqisoft.medi.data.media.MediaSnapshotData;

@SuppressWarnings("unchecked")
@Repository
public class MediaDao extends AbstractDao {
	
	public List<MediaData> list(SearchMediaFilter filter) {
		if (filter != null) {
			HashMap<String, Object> param = new HashMap<String, Object>();
			
			if (exists(filter.title)) param.put("F_TITLE", filter.title);
			if (exists(filter.pat_nm)) param.put("F_PAT_NM", filter.pat_nm);
			if (exists(filter.pat_id)) param.put("F_PAT_ID", filter.pat_id);
			
			addRange(param, "PAT_AGE", filter.pat_age);
			addRange(param, "PAT_WGT", filter.pat_weight);
			addRange(param, "PAT_HGT", filter.pat_height);
			
			addArray(param, "PAT_CFS_IX", filter.cfx_ix);
			addArray(param, "PAT_EXC", filter.exclusion);
			addArray(param, "PAT_HIS", filter.history);
			addArray(param, "PAT_EOC_STAT", filter.endoscopy_stat);
			
			addArray(param, "OP_APPX_REACH", filter.appendix_reach);
			addRange(param, "OP_APPX_REACH_TM", filter.appx_reach_time);
			addRange(param, "OP_APPX_REACH_REM_TM", filter.appx_remove_time);
			addRange(param, "OP_APPX_TOTAL_TM", filter.appx_total_time);

			addArray(param, "OP_RIGHT_COL", filter.right_colon);
			addArray(param, "OP_TRANS_COL", filter.trans_colon);
			addArray(param, "OP_LEFT_COL", filter.left_colon);

			addArray(param, "OP_BBPS_FIT", filter.bbps_fitness);
			addArray(param, "OP_CFX_OPI", filter.cfs_opi);
			addRange(param, "OP_POLYP_CNT", filter.polyp_cnt);
			
			addArray(param, "SNAP_POLYP_POS", filter.polyp_pos);
			addRange(param, "SNAP_POLYP_SIZ", filter.polyp_size);
			addArray(param, "SNAP_POLYP_SHAPE", filter.polyp_shape);
			addArray(param, "SNAP_POLYP_BIO", filter.polyp_bio);
			
			return sqlSession.selectList("m:search", param);
		}else {
			return sqlSession.selectList("m:list");
		}
	}
	
	//-----------------------------------------------------------
	
	private boolean exists(String text) { return text != null && !text.isEmpty(); }
	
	private void addRange(Map<String, Object> param, String name, SearchMediaFilter.Range range) {
		if (range != null) {
			if (range.min != null)
				param.put("F_" + name + "_MIN", range.min);
			if (range.max != null)
				param.put("F_" + name + "_MAX", range.max);
		}
	}
	
	private void addArray(Map<String, Object> param, String name, List list) {
		if (list != null && list.size() > 0) {
			int len = list.size();
			List<String> a = new ArrayList<>();
			for (int i=0; i < len; i++) {
				a.add(list.get(i).toString());
			}
			
			param.put("F_" + name, String.join(",", a));
		}
	}

	//-----------------------------------------------------------
	
	public List<MediaSnapshotData> snapshots(String key){
		HashMap<String, Object> param = new HashMap<String, Object>();
		
		param.put("UID", key);
		
		return sqlSession.selectList("m:snapshot", param);
	}

	public List<MediaMaskData> masks(String key){
		HashMap<String, Object> param = new HashMap<String, Object>();
		
		param.put("UID", key);
		
		return sqlSession.selectList("m:mask", param);
	}

	public List<MediaLabelData> labels(String key){
		HashMap<String, Object> param = new HashMap<String, Object>();
		
		param.put("UID", key);
		
		List<MediaLabelData> a = sqlSession.selectList("m:label", param);
		int len = a != null ? a.size() : 0;
		for (int i=0; i < len; i++)
			a.get(i).dataToPoints();
		
		return a;
	}

	//-----------------------------------------------------------

	public MediaData get(String key) {
		HashMap<String, Object> param = new HashMap<String, Object>();
		
		param.put("UID", key);
		
		return (MediaData)sqlSession.selectOne("m:get", param);
	}

	public MediaPatOpData getPatientOperation(String key) {
		HashMap<String, Object> param = new HashMap<String, Object>();
		
		param.put("UID", key);
		
		return (MediaPatOpData)sqlSession.selectOne("m:patop:get", param);
	}
	
	public MediaSnapshotData getSnapshot(String key, double time) {
		HashMap<String, Object> param = new HashMap<String, Object>();
		
		param.put("UID", key);
		param.put("SNAP_TM", time);
		
		return (MediaSnapshotData)sqlSession.selectOne("m:snapshot:get", param);
	}
	
	public MediaLabelData getSnapshotLabel(String key, double time, int seq) {
		HashMap<String, Object> param = new HashMap<String, Object>();
		
		param.put("UID", key);
		param.put("SNAP_TM", time);
		param.put("LABEL_SEQ", seq);
		
		MediaLabelData d = (MediaLabelData)sqlSession.selectOne("m:label:get", param);
		d.dataToPoints();
		
		return d;
	}

	//-----------------------------------------------------------
	
	public void requestRemove(String uid) {
		HashMap<String, Object> param = new HashMap<String, Object>();
		
		param.put("UID", uid);
		
		sqlSession.delete("m:request-delete-media", param);
	}
	
	//-----------------------------------------------------------
	//-----------------------------------------------------------
	//-----------------------------------------------------------
	
	//	public List<MediaData> list2() {
	//		HashMap<String, Object> param = new HashMap<String, Object>();
	//		
	//		return sqlSession.selectList("m:list", param);
	//	}
}
