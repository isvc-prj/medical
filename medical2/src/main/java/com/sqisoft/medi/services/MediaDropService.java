package com.sqisoft.medi.services;

import java.io.File;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileDeleteStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sqisoft.medi.dao.MediaDropDao;
import com.sqisoft.medi.dao.MediaStorageDao;
import com.sqisoft.medi.data.media.MediaData;
import com.sqisoft.medi.storage.MediaStorage;
import org.apache.log4j.Logger;

/**
 * 삭제 플래그가 있는 미디어 파일 삭제 지원 서비스 
 * @author Administrator
 *
 */
@Service
public class MediaDropService {

	@Autowired
	MediaDropDao dao;
	
	@Autowired
	MediaStorageDao storageDao;
	
	//-----------------------------------------------------------------------------------
	
	private void logDebug(Logger logger, Object message) {
		if (logger != null && message != null && logger.isDebugEnabled())
			logger.debug(message.toString());
	}
	
	private void logError(Logger logger, Object message) {
		if (logger != null && message != null)
			logger.error(message.toString());
		
		if (message != null)
			System.out.println(message);
	}
	
	//-----------------------------------------------------------------------------------

	/**
	 * 삭제 플래그가 있는 미디어 파일 삭제 시도
	 * @param session
	 * @param logger
	 */
	public void tryDrop(HttpSession session, Logger logger) {
//		MediaDropDao dao = null;
//		MediaStorageDao storageDao = null;
 		
		try
		{
			logDebug(logger, "<<<MEDIA DROP>>> START");
			
			List<MediaData> datas = dao.listDropped();
			
			int numDatas = datas.size();
			for (int i=0; i < numDatas; i++) {
				MediaData data = datas.get(i);
				
				String uid = data.getKey();
				String sid = data.getSid();
				String mediaFileName = data.getMediaFile();
				
				logDebug(logger, "\t---------------------------------------");
				logDebug(logger, "\t<<<TRY>>> UID=" + uid);
				logDebug(logger, "\t<<<TRY>>> SID=" + sid);
				logDebug(logger, "\t<<<TRY>>> FILE=" + mediaFileName);
				
				MediaStorage ms = new MediaStorage(storageDao, uid);
				
				if (mediaFileName == null || mediaFileName.isEmpty()) {
					if (ms.exists()) {
						try
						{
							ms.clear();
						}
						catch (Exception exCls)
						{
							logError(logger, "[HTTP Session][drop-media]" + 
									"Failed to delete folder !!! UID=" + uid);
							continue;
						}
					}
					
					dao.drop(uid);
					logDebug(logger, "\t DROPPED !!!!");
					continue;
				}
				
				File mediaFile = ms.file(MediaStorage.MS_MEDIA, mediaFileName);
				
				try
				{
					FileDeleteStrategy.FORCE.delete(mediaFile);
				}
				catch(Exception exDel)
				{
					logError(logger, "[HTTP Session][drop-media]" + 
							"Failed to delete media file !!! UID=" + uid +
							", Media File=" + mediaFile.getAbsolutePath());
					continue;
				}
				
				if (ms.exists()) {
					try
					{
						ms.clear();
					}
					catch (Exception exCls)
					{
						logError(logger, "[HTTP Session][drop-media]" + 
								"Failed to delete folder !!! UID=" + uid);
						continue;
					}
				}
				
				dao.drop(uid);
				logDebug(logger, "\t DROPPED !!!!");
			}
			
		}
		catch (Exception ex)
		{
			logError(logger, "[HTTP Session][drop-media] Exception !!!");
			logError(logger, ex);
		}
		
		logDebug(logger, "<<MEDIA DROP>>> END");
	}
}
