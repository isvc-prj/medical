package com.sqisoft.medi.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sqisoft.medi.abstracts.AbstractService;
import com.sqisoft.medi.dao.MediaDao;
import com.sqisoft.medi.data.filters.SearchMediaFilter;
import com.sqisoft.medi.data.media.MediaData;
import com.sqisoft.medi.data.media.MediaLabelData;
import com.sqisoft.medi.data.media.MediaMaskData;
import com.sqisoft.medi.data.media.MediaOperationVO;
import com.sqisoft.medi.data.media.MediaPatOpData;
import com.sqisoft.medi.data.media.MediaPatientVO;
import com.sqisoft.medi.data.media.MediaSnapshotData;
import com.sqisoft.medi.data.media.MediaSnapshotVO;
import com.sqisoft.medi.data.media.MediaVO;

@Service
public class MediaService extends AbstractService {

	@Autowired
	MediaDao dao;
	
	//-----------------------------------------------------------

	public List<MediaData> list(){
		return dao.list(null);
	}
	
	public List<MediaData> list(SearchMediaFilter filter){
		return dao.list(filter);
	}
	
	//-----------------------------------------------------------
	
	public List<MediaSnapshotData> snapshots(String key){
		return dao.snapshots(key);
	}
	
	public List<MediaMaskData> masks(String key){
		return dao.masks(key);
	}
	
	public List<MediaLabelData> labels(String key){
		return dao.labels(key);
	}
	
	//-----------------------------------------------------------

	public MediaData get(String key) {
		return dao.get(key);
	}
	
	public MediaPatOpData getPatientOperation(String key) {
		return dao.getPatientOperation(key);
	}
	
	public MediaSnapshotData getSnapshot(String key, double time) {
		return dao.getSnapshot(key, time);
	}
	
	public MediaLabelData getSnapshotLabel(String key, double time, int seq) {
		return dao.getSnapshotLabel(key, time, seq);
	}

	//-----------------------------------------------------------
	
	public void requestRemove(String uid) {
		dao.requestRemove(uid);
	}
	
	//-----------------------------------------------------------
	//-----------------------------------------------------------
	//-----------------------------------------------------------

	public MediaVO getMediaVO(String key) {
		MediaData md = get(key);
		MediaPatOpData mpod = getPatientOperation(key);
		List<MediaMaskData> lmasks = masks(key);
		List<MediaSnapshotData> lsnapshots = snapshots(key);
		List<MediaLabelData> llabels = labels(key);
		
		//-----------------------------------------------------------
		
		MediaPatientVO patVo = new MediaPatientVO();
		patVo.copyFrom(mpod);
		
		MediaOperationVO opVo = new MediaOperationVO();
		opVo.copyFrom(mpod);
		
		//-----------------------------------------------------------

		boolean invertMask = false;
		List<MediaMaskData> masks = new ArrayList<>();
		
		int len = lmasks != null ? lmasks.size() : 0;
		for (int i=0; i < len; i++) {
			MediaMaskData d = lmasks.get(i);
			
			if (d.getSeq() < 1) {
				invertMask = true;
			}else {
				masks.add(d);
			}
		}
		
		//-----------------------------------------------------------

		List<MediaSnapshotVO> snapshots = new ArrayList<>();
		
		len = lsnapshots != null ? lsnapshots.size() : 0;
		for (int i=0; i < len; i++) {
			MediaSnapshotData d = lsnapshots.get(i);
			
			List<MediaLabelData> founds = findLabels(llabels, d.getTime());
			
			MediaSnapshotVO svo = new MediaSnapshotVO();
			svo.copyFrom(d);
			svo.setLabels(founds);
			
			snapshots.add(svo);
		}
		
		//-----------------------------------------------------------

		MediaVO vo = new MediaVO();
		vo.copyFrom(md);
		vo.setPatient(patVo);
		vo.setOperation(opVo);
		vo.setInvertMask(invertMask);
		vo.setMasks(masks);
		vo.setSnapshots(snapshots);
		
		return vo;
	}
	
	private List<MediaLabelData> findLabels(List<MediaLabelData> labels, double time){
		List<MediaLabelData> founds = new ArrayList<>();
		
		int len = labels != null ? labels.size() : 0;
		for (int i=0; i < len; i++) {
			MediaLabelData m = labels.get(i);
			
			if (m.getTime() == time) {
				founds.add(m);
			}
		}
		
		return founds;
	}
}
