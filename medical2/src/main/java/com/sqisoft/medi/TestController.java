package com.sqisoft.medi;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.support.ServletConfigPropertySource;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.FrameworkServlet;

import com.sqisoft.medi.services.MediaDropService;

@Controller
public class TestController {
	
	@Autowired
	HttpSession session;

	@RequestMapping(value="/test/test-upload", method = RequestMethod.GET)
	public String upload() {
		ApplicationContext appCtx = null;
		//AnnotationConfigApplicationContext appCtx = null;
		MediaDropService svc = null;
		
		try
		{
			ServletContext sctx = session.getServletContext();
			
			appCtx = WebApplicationContextUtils.getWebApplicationContext(sctx,
					FrameworkServlet.SERVLET_CONTEXT_PREFIX + "appServlet");
			
			//appCtx = WebApplicationContextUtils.getWebApplicationContext(sctx);
//			appCtx = WebApplicationContextUtils.getWebApplicationContext(sctx,
//					"org.springframework.web.servlet.FrameworkServlet.CONTEXT.dispatcher");
			//appCtx = new AnnotationConfigApplicationContext("com.sqisoft.medi");
		}
		catch (Exception ex)
		{
			System.out.println("[TEST] Fail get application context !!!");
			System.out.println(ex);
		}

		if (appCtx != null){
			String [] arrays = appCtx.getBeanNamesForType(MediaDropService.class);
			putArray(arrays);

			String [] arrays2 = appCtx.getBeanNamesForType(this.getClass());
			putArray(arrays2);
			
			try
			{
				svc = (MediaDropService)appCtx.getBean("mediaDropService");
			}
			catch (Exception ex)
			{
				System.out.println("[TEST] Fail get media drop service !!!");
				System.out.println(ex);
			}
			
			if (svc != null) {
				System.out.println("OKOK");
			}
		}else {
			System.out.println("[TEST] application context is null !!!");
		}
		
		return "test/test-upload";
	}
	
	private void putArray(String[] arrays) {
		System.out.println(arrays);
		if (arrays != null) {
			System.out.println("[ARRAY] length=" + arrays.length);
			
			int len = 0;
			for (int i=0; i < len; i++) {
				String s = arrays[i];
				
				System.out.println("\t[" + i + "] " + s);
			}
		}
	}
	
	@RequestMapping(value="/test/proc-upload", method = RequestMethod.POST)
	public String procUpload(HttpServletRequest request,
			@RequestParam(value="files[]", required=false) MultipartFile[] files) {
		
		System.out.println("files=" + files != null ? files.length : 0);
		
		if (files != null && files.length > 0) {
			MultipartFile file = files[0];
			
			System.out.println("[0] Name=" + file.getName() + ", size=" + file.getSize());
		}
		
		return "redirect://";
	}
}
