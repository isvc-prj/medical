package com.sqisoft.medi.data.media;

import java.util.List;

public class UploadSnapshot extends UploadImageFileInfo {
	public double time;
	public boolean server;

	public UploadImageFileInfo thumbnail;
		
	public List<UploadLabel> labels;
	
	public boolean hasLabels() { return labels != null && labels.size() > 0; }
}
