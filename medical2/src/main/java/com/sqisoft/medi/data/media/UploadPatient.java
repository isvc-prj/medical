package com.sqisoft.medi.data.media;

public class UploadPatient {
	public String regDate;
	public String id;
	public String name;
	public String no;
	public String gender;
	public int age;
	public int weight;
	public int height;
	public int cfxIx;
	public int exclusion;
	public int history;
	public int endoscopyStat;
}
