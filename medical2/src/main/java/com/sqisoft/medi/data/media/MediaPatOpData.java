package com.sqisoft.medi.data.media;

public class MediaPatOpData {
	private String key;
	private String patNm;
	private String patNo;
	private String patId;
	private String patRegDate;
	private String patGen;
	private int patAge;
	private int patWgt;
	private int patHgt;
	private int patCfsIx;
	private int patExc;
	private int patHis;
	private int patEocStat;
	private int opAppxReach;
	private int opAppxReachTm;
	private int opAppxReachRemTm;
	private int opAppxTotalTm;
	private int opRightCol;
	private int opTransCol;
	private int opLeftCol;
	private int opBbps;
	private int opBbpsFit;
	private int opCfxOpi;
	private int opPolypCnt;

	//-----------------------------------------------------------

	public String getKey() { return key; }
	public void setKey(String value) { key = value; }

	public String getPatNm() { return patNm; }
	public void setPatNm(String value) { patNm = value; }

	public String getPatNo() { return patNo; }
	public void setPatNo(String value) { patNo = value; }

	public String getPatId() { return patId; }
	public void setPatId(String value) { patId = value; }

	public String getPatRegDate() { return patRegDate; }
	public void setPatRegDate(String value) { patRegDate = value; }

	public String getPatGen() { return patGen; }
	public void setPatGen(String value) { patGen = value; }

	public int getPatAge() { return patAge; }
	public void setPatAge(int value) { patAge = value; }

	public int getPatWgt() { return patWgt; }
	public void setPatWgt(int value) { patWgt = value; }

	public int getPatHgt() { return patHgt; }
	public void setPatHgt(int value) { patHgt = value; }

	public int getPatCfsIx() { return patCfsIx; }
	public void setPatCfsIx(int value) { patCfsIx = value; }

	public int getPatExc() { return patExc; }
	public void setPatExc(int value) { patExc = value; }

	public int getPatHis() { return patHis; }
	public void setPatHis(int value) { patHis = value; }

	public int getPatEocStat() { return patEocStat; }
	public void setPatEocStat(int value) { patEocStat = value; }

	public int getOpAppxReach() { return opAppxReach; }
	public void setOpAppxReach(int value) { opAppxReach = value; }

	public int getOpAppxReachTm() { return opAppxReachTm; }
	public void setOpAppxReachTm(int value) { opAppxReachTm = value; }

	public int getOpAppxReachRemTm() { return opAppxReachRemTm; }
	public void setOpAppxReachRemTm(int value) { opAppxReachRemTm = value; }

	public int getOpAppxTotalTm() { return opAppxTotalTm; }
	public void setOpAppxTotalTm(int value) { opAppxTotalTm = value; }

	public int getOpRightCol() { return opRightCol; }
	public void setOpRightCol(int value) { opRightCol = value; }

	public int getOpTransCol() { return opTransCol; }
	public void setOpTransCol(int value) { opTransCol = value; }

	public int getOpLeftCol() { return opLeftCol; }
	public void setOpLeftCol(int value) { opLeftCol = value; }

	public int getOpBbps() { return opBbps; }
	public void setOpBbps(int value) { opBbps = value; }

	public int getOpBbpsFit() { return opBbpsFit; }
	public void setOpBbpsFit(int value) { opBbpsFit = value; }

	public int getOpCfxOpi() { return opCfxOpi; }
	public void setOpCfxOpi(int value) { opCfxOpi = value; }

	public int getOpPolypCnt() { return opPolypCnt; }
	public void setOpPolypCnt(int value) { opPolypCnt = value; }

}
