package com.sqisoft.medi.data.filters;

import java.util.List;
import java.util.StringJoiner;
import java.util.stream.IntStream;

import org.springframework.util.StringUtils;

public class SearchMediaFilter {
	public static class Range {
		public Integer min;
		public Integer max;
	}
	
	public String title;
	public String pat_nm;
	public String pat_id;
	public Range pat_age;
	public Range pat_weight;
	public Range pat_height;
	public List<Integer> cfx_ix;
	public List<Integer> exclusion;
	public List<Integer> history;
	public List<Integer> endoscopy_stat;
	public List<Integer> appendix_reach;
	public Range appx_reach_time;
	public Range appx_remove_time;
	public Range appx_total_time;
	public List<Integer> right_colon;
	public List<Integer> trans_colon;
	public List<Integer> left_colon;
	public List<Integer> bbps_fitness;
	public List<Integer> cfs_opi;
	public Range polyp_cnt;
	public List<Integer> polyp_pos;
	public Range polyp_size;
	public List<Integer> polyp_shape;
	public List<Integer> polyp_bio;
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[FILTER]");
		sb.append("\n");
		
		sb.append(String.format("\t-title = %s\n", title));
		sb.append(String.format("\t-pat_nm = %s\n", pat_nm));
		sb.append(String.format("\t-pat_id = %s\n", pat_id));
		
		appendRange(sb, "pat_age", pat_age);
		appendRange(sb, "pat_weight", pat_weight);
		appendRange(sb, "pat_height", pat_height);
		
		sb.append(String.format("\t-cfx_ix = %s\n", joinIntArray(cfx_ix)));
		sb.append(String.format("\t-exclusion = %s\n", joinIntArray(exclusion)));
		sb.append(String.format("\t-history = %s\n", joinIntArray(history)));
		sb.append(String.format("\t-endoscopy_stat = %s\n", joinIntArray(endoscopy_stat)));
		sb.append(String.format("\t-appendix_reach = %s\n", joinIntArray(appendix_reach)));
		
		appendRange(sb, "appx_reach_time", appx_reach_time);
		appendRange(sb, "appx_remove_time", appx_remove_time);
		appendRange(sb, "appx_total_time", appx_total_time);
		
		sb.append(String.format("\t-right_colon = %s\n", joinIntArray(right_colon)));
		sb.append(String.format("\t-trans_colon = %s\n", joinIntArray(trans_colon)));
		sb.append(String.format("\t-left_colon = %s\n", joinIntArray(left_colon)));
		sb.append(String.format("\t-bbps_fitness = %s\n", joinIntArray(bbps_fitness)));
		sb.append(String.format("\t-cfs_opi = %s\n", joinIntArray(cfs_opi)));

		appendRange(sb, "polyp_cnt", polyp_cnt);
		
		sb.append(String.format("\t-polyp_pos = %s\n", joinIntArray(polyp_pos)));

		appendRange(sb, "polyp_size", polyp_size);
		
		sb.append(String.format("\t-polyp_shape = %s\n", joinIntArray(polyp_shape)));
		sb.append(String.format("\t-polyp_bio = %s\n", joinIntArray(polyp_bio)));
		
		return sb.toString();
	}
	
	private void appendRange(StringBuilder sb, String name, Range r) {
		if (r != null) sb.append(String.format("\t-%s = (min:%d, max:%d)\n", name, r.min, r.max));
		else sb.append(String.format("\t-%s = null\n", name));
	}
	
	private String joinIntArray(List<Integer> a) {
		if (a == null) return null;
		
		StringJoiner joiner = new StringJoiner(",");
		//IntStream.of(a).forEach(x -> joiner.add(String.valueOf(x)));
		a.forEach(x -> joiner.add(String.valueOf(x)));
		
		return joiner.toString();
	}
}
