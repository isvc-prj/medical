package com.sqisoft.medi.data.media;

public class MediaOperationVO {
	private int appendixReach;
	private int appendixReachTime;
	private int appendixReachRemoveTime;
	private int appendixTotalTime;
	private int rightColon;
	private int transColon;
	private int leftColon;
	private int bbps;
	private int bbpsFitness;
	private int cfxOpinion;
	private int polypCnt;

	//-----------------------------------------------------------

	public int getAppendixReach() { return appendixReach; }
	public void setAppendixReach(int value) { appendixReach = value; }

	public int getAppendixReachTime() { return appendixReachTime; }
	public void setAppendixReachTime(int value) { appendixReachTime = value; }

	public int getAppendixReachRemoveTime() { return appendixReachRemoveTime; }
	public void setAppendixReachRemoveTime(int value) { appendixReachRemoveTime = value; }

	public int getAppendixTotalTime() { return appendixTotalTime; }
	public void setAppendixTotalTime(int value) { appendixTotalTime = value; }

	public int getRightColon() { return rightColon; }
	public void setRightColon(int value) { rightColon = value; }

	public int getTransColon() { return transColon; }
	public void setTransColon(int value) { transColon = value; }

	public int getLeftColon() { return leftColon; }
	public void setLeftColon(int value) { leftColon = value; }

	public int getBbps() { return bbps; }
	public void setBbps(int value) { bbps = value; }

	public int getBbpsFitness() { return bbpsFitness; }
	public void setBbpsFitness(int value) { bbpsFitness = value; }

	public int getCfxOpinion() { return cfxOpinion; }
	public void setCfxOpinion(int value) { cfxOpinion = value; }

	public int getPolypCnt() { return polypCnt; }
	public void setPolypCnt(int value) { polypCnt = value; }

	//-----------------------------------------------------------

	public void copyFrom(MediaPatOpData d) {
		appendixReach = d.getOpAppxReach();
		appendixReachTime = d.getOpAppxReachTm();
		appendixReachRemoveTime = d.getOpAppxReachRemTm();
		appendixTotalTime = d.getOpAppxTotalTm();
		rightColon = d.getOpRightCol();
		transColon = d.getOpTransCol();
		leftColon = d.getOpLeftCol();
		bbps = d.getOpBbps();
		bbpsFitness = d.getOpBbpsFit();
		cfxOpinion = d.getOpCfxOpi();
		polypCnt = d.getOpPolypCnt();
	}

}
