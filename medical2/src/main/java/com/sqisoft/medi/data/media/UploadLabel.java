package com.sqisoft.medi.data.media;

import java.util.List;

import com.sqisoft.medi.data.PointInfo;
import com.sqisoft.medi.data.RectInfo;

public class UploadLabel extends RectInfo {
	public double time;

	public List<PointInfo> points;
	
	public int polypPos;
	public int polypSize;
	public int polypShape;
	public int polypBio;

	public String filename;
	public String fieldname;
}
