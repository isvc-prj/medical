package com.sqisoft.medi.data.media;

public class MediaMaskData {
	private String key;
	private int seq;
	private double x;
	private double y;
	private double width;
	private double height;

	//-----------------------------------------------------------

	public String getKey() { return key; }
	public void setKey(String value) { key = value; }
	
	public int getSeq() { return seq; }
	public void setSeq(int value) { seq = value; }

	public double getX() { return x; }
	public void setX(double value) { x = value; }

	public double getY() { return y; }
	public void setY(double value) { y = value; }

	public double getWidth() { return width; }
	public void setWidth(double value) { width = value; }

	public double getHeight() { return height; }
	public void setHeight(double value) { height = value; }

}
