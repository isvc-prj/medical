package com.sqisoft.medi.data.media;

public class MediaPatientVO {
	private String name;
	private String no;
	private String id;
	private String registDate;
	private String gender;
	private int age;
	private int weight;
	private int height;
	private int cfsIx;
	private int exclusion;
	private int history;
	private int endoscopyStat;

	//-----------------------------------------------------------

	public String getName() { return name; }
	public void setName(String value) { name = value; }

	public String getNo() { return no; }
	public void setNo(String value) { no = value; }

	public String getId() { return id; }
	public void setId(String value) { id = value; }

	public String getRegistDate() { return registDate; }
	public void setRegistDate(String value) { registDate = value; }

	public String getGender() { return gender; }
	public void setGender(String value) { gender = value; }

	public int getAge() { return age; }
	public void setAge(int value) { age = value; }

	public int getWeight() { return weight; }
	public void setWeight(int value) { weight = value; }

	public int getHeight() { return height; }
	public void setHeight(int value) { height = value; }

	public int getCfsIx() { return cfsIx; }
	public void setCfsIx(int value) { cfsIx = value; }

	public int getExclusion() { return exclusion; }
	public void setExclusion(int value) { exclusion = value; }

	public int getHistory() { return history; }
	public void setHistory(int value) { history = value; }

	public int getEndoscopyStat() { return endoscopyStat; }
	public void setEndoscopyStat(int value) { endoscopyStat = value; }

	//-----------------------------------------------------------

	public void copyFrom(MediaPatOpData d) {
		name = d.getPatNm();
		no = d.getPatNo();
		id = d.getPatId();
		registDate = d.getPatRegDate();
		gender = d.getPatGen();
		age = d.getPatAge();
		weight = d.getPatWgt();
		height = d.getPatHgt();
		cfsIx = d.getPatCfsIx();
		exclusion = d.getPatExc();
		history = d.getPatHis();
		endoscopyStat = d.getPatEocStat();
	}
}
