package com.sqisoft.medi.data.media;

import java.util.ArrayList;
import java.util.List;

public class MediaSnapshotVO extends MediaSnapshotData {
	
	protected List<MediaLabelData> labels = new ArrayList<>();

	//-----------------------------------------------------------

	public List<MediaLabelData> getLabels() { return labels; }
	public void setLabels(List<MediaLabelData> value) { labels = value; }

}
