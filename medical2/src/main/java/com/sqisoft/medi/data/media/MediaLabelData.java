package com.sqisoft.medi.data.media;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.sqisoft.medi.data.PointInfo;
import com.sqisoft.medi.utils.JsonUtil;

public class MediaLabelData {
	private String key;
	private double time;
	private int seq;
	private double x;
	private double y;
	private int width;
	private int height;
	@JsonIgnore
	private String data;
	private List<PointInfo> points = new ArrayList<>();
	private int polypPos;
	private int polypSiz;
	private int polypShape;
	private int polypBio;
	private String fileNm;

	//-----------------------------------------------------------

	public String getKey() { return key; }
	public void setKey(String value) { key = value; }

	public double getTime() { return time; }
	public void setTime(double value) { time = value; }

	public int getSeq() { return seq; }
	public void setSeq(int value) { seq = value; }

	public double getX() { return x; }
	public void setX(double value) { x = value; }

	public double getY() { return y; }
	public void setY(double value) { y = value; }

	public int getWidth() { return width; }
	public void setWidth(int value) { width = value; }

	public int getHeight() { return height; }
	public void setHeight(int value) { height = value; }

	public String getData() { return data; }
	public void setData(String value) { data = value; }

	public int getPolypPos() { return polypPos; }
	public void setPolypPos(int value) { polypPos = value; }

	public int getPolypSiz() { return polypSiz; }
	public void setPolypSiz(int value) { polypSiz = value; }

	public int getPolypShape() { return polypShape; }
	public void setPolypShape(int value) { polypShape = value; }

	public int getPolypBio() { return polypBio; }
	public void setPolypBio(int value) { polypBio = value; }

	public String getFileNm() { return fileNm; }
	public void setFileNm(String value) { fileNm = value; }

	//-----------------------------------------------------------
	
	public List<PointInfo> getPoints() {
		ArrayList<PointInfo> a = new ArrayList<>();
		a.addAll(points);
		return a;
	}
	public void setPoints(List<PointInfo> value) {
		points.clear();
		if (value != null) points.addAll(value);
	}

	//-----------------------------------------------------------

	public void dataToPoints() {
		if (data != null) {
			setPoints(JsonUtil.decodeList(PointInfo.class, data));
		}
	}
}
