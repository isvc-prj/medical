package com.sqisoft.medi.data.media;

public class UploadImageFileInfo {
	public int width;
	public int height;
	public String name;
	public long size;	
}
