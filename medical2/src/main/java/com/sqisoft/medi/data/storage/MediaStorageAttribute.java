package com.sqisoft.medi.data.storage;

/**
 * 미디어 스토로지 속성
 * @author Administrator
 *
 */
public class MediaStorageAttribute {
	public MediaStorageAttribute() {
		
	}
	
	public MediaStorageAttribute(String id, String path, boolean using) {
		this.id = id;
		this.path = path;
		this.useYn = using ? "Y" : "N";
	}

	//-----------------------------------------------------------
	
	private String id;
	private String path;
	private String useYn;

	//-----------------------------------------------------------

	public String getId() { return id; }
	public void setId(String value) { id = value; }

	public String getPath() { return path; }
	public void setPath(String value) { path = value; }

	public String getUseYn() { return useYn; }
	public void setUseYn(String value) { useYn = value; }

	//-----------------------------------------------------------

	public boolean isUsable() { return useYn != null ? useYn.equalsIgnoreCase("Y") : false; }
}
