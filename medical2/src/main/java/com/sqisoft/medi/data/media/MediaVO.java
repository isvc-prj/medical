package com.sqisoft.medi.data.media;

import java.util.ArrayList;
import java.util.List;

import com.sqisoft.medi.enums.MDMediaType;

/**
 * Media Visual Object
 * @author Administrator
 *
 */
public class MediaVO extends MediaData {
	public void setMediaType(MDMediaType value) { mediaType = value; }

	//-----------------------------------------------------------
	
	protected MediaPatientVO patient;
	protected MediaOperationVO operation;
	
	//-----------------------------------------------------------
	
	protected boolean invertMask;
	
	protected List<MediaMaskData> masks = new ArrayList<>();

	//-----------------------------------------------------------

	protected List<MediaSnapshotVO> snapshots = new ArrayList<>();

	//-----------------------------------------------------------

	public MediaPatientVO getPatient() { return patient; }
	public void setPatient(MediaPatientVO value) { patient = value; }

	public MediaOperationVO getOperation() { return operation; }
	public void setOperation(MediaOperationVO value) { operation = value; }

	//-----------------------------------------------------------

	public boolean getInvertMask() { return invertMask; }
	public void setInvertMask(boolean value) { invertMask = value; }

	public List<MediaMaskData> getMasks() { return masks; }
	public void setMasks(List<MediaMaskData> value) { masks = value; }

	//-----------------------------------------------------------

	public List<MediaSnapshotVO> getSnapshots() { return snapshots; }
	public void setSnapshots(List<MediaSnapshotVO> value) { snapshots = value; }

	//-----------------------------------------------------------
}
