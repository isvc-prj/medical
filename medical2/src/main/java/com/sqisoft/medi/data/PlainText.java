package com.sqisoft.medi.data;

/**
 * 플레인 텍스트 정보
 * <p>* 플레인 텍스트를 브라우저에 제공합니다.
 * @author Administrator
 *
 */
public class PlainText {
	/**
	 * 플레인 텍스트 정보
	 */
	public PlainText(){}
	/**
	 * 플레인 텍스트 정보
	 * @param text 텍스트 내용
	 */
	public PlainText(String text){
		this.text = text;
	}
	
	/**
	 * 텍스트 내용
	 */
	public String text;
}
