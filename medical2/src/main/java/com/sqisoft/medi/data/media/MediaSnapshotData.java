package com.sqisoft.medi.data.media;

public class MediaSnapshotData {
	protected String key;
	protected double time;
	protected int snapWidth;
	protected int snapHeight;
	protected String snapFile;
	protected long snapSize;
	protected int thumbnailWidth;
	protected int thumbnailHeight;
	protected String thumbnailFile;
	protected long thumbnailSize;

	//-----------------------------------------------------------

	public String getKey() { return key; }
	public void setKey(String value) { key = value; }

	public double getTime() { return time; }
	public void setTime(double value) { time = value; }

	public int getSnapWidth() { return snapWidth; }
	public void setSnapWidth(int value) { snapWidth = value; }

	public int getSnapHeight() { return snapHeight; }
	public void setSnapHeight(int value) { snapHeight = value; }

	public String getSnapFile() { return snapFile; }
	public void setSnapFile(String value) { snapFile = value; }

	public long getSnapSize() { return snapSize; }
	public void setSnapSize(long value) { snapSize = value; }

	public int getThumbnailWidth() { return thumbnailWidth; }
	public void setThumbnailWidth(int value) { thumbnailWidth = value; }

	public int getThumbnailHeight() { return thumbnailHeight; }
	public void setThumbnailHeight(int value) { thumbnailHeight = value; }

	public String getThumbnailFile() { return thumbnailFile; }
	public void setThumbnailFile(String value) { thumbnailFile = value; }

	public long getThumbnailSize() { return thumbnailSize; }
	public void setThumbnailSize(long value) { thumbnailSize = value; }

	//-----------------------------------------------------------
	
	public void copyFrom(MediaSnapshotData d) {
		key = d.key;
		time = d.time;
		snapWidth = d.snapWidth;
		snapHeight = d.snapHeight;
		snapFile = d.snapFile;
		snapSize = d.snapSize;
		thumbnailWidth = d.thumbnailWidth;
		thumbnailHeight = d.thumbnailHeight;
		thumbnailFile = d.thumbnailFile;
		thumbnailSize = d.thumbnailSize;
	}
}
