package com.sqisoft.medi.data.media;

import com.sqisoft.medi.enums.MDMediaType;

public class MediaData {
	protected String key;
	protected String sid;
	protected String title;
	protected MDMediaType mediaType;
	protected String mimeType;
	protected double duration;
	protected int mediaWidth;
	protected int mediaHeight;
	protected String mediaFile;
	protected long mediaSize;
	protected int screenWidth;
	protected int screenHeight;
	protected String screenFile;
	protected long screenSize;
	protected int thumbnailWidth;
	protected int thumbnailHeight;
	protected String thumbnailFile;
	protected long thumbnailSize;
	protected String ownerId;
	protected String regIp;
	protected String regDate;
	protected String lastUpdateDate;

	//-----------------------------------------------------------
	// 추가된 정보
	
	protected String patRegDate;

	//-----------------------------------------------------------

	public String getKey() { return key; }
	public void setKey(String value) { key = value; }

	public String getSid() { return sid; }
	public void setSid(String value) { sid = value; }

	public String getTitle() { return title; }
	public void setTitle(String value) { title = value; }

	public MDMediaType getMediaType() { return mediaType; }
	public void setMediaType(String value) { mediaType = MDMediaType.find(value); }
	
	public String getMimeType() { return mimeType; }
	public void setMimeType(String value) { mimeType = value; }

	public double getDuration() { return duration; }
	public void setDuration(double value) { duration = value; }

	public int getMediaWidth() { return mediaWidth; }
	public void setMediaWidth(int value) { mediaWidth = value; }

	public int getMediaHeight() { return mediaHeight; }
	public void setMediaHeight(int value) { mediaHeight = value; }

	public String getMediaFile() { return mediaFile; }
	public void setMediaFile(String value) { mediaFile = value; }

	public long getMediaSize() { return mediaSize; }
	public void setMediaSize(long value) { mediaSize = value; }

	public int getScreenWidth() { return screenWidth; }
	public void setScreenWidth(int value) { screenWidth = value; }

	public int getScreenHeight() { return screenHeight; }
	public void setScreenHeight(int value) { screenHeight = value; }

	public String getScreenFile() { return screenFile; }
	public void setScreenFile(String value) { screenFile = value; }

	public long getScreenSize() { return screenSize; }
	public void setScreenSize(long value) { screenSize = value; }

	public int getThumbnailWidth() { return thumbnailWidth; }
	public void setThumbnailWidth(int value) { thumbnailWidth = value; }

	public int getThumbnailHeight() { return thumbnailHeight; }
	public void setThumbnailHeight(int value) { thumbnailHeight = value; }

	public String getThumbnailFile() { return thumbnailFile; }
	public void setThumbnailFile(String value) { thumbnailFile = value; }

	public long getThumbnailSize() { return thumbnailSize; }
	public void setThumbnailSize(long value) { thumbnailSize = value; }

	public String getOwnerId() { return ownerId; }
	public void setOwnerId(String value) { ownerId = value; }

	public String getRegIp() { return regIp; }
	public void setRegIp(String value) { regIp = value; }

	public String getRegDate() { return regDate; }
	public void setRegDate(String value) { regDate = value; }

	public String getLastUpdateDate() { return lastUpdateDate; }
	public void setLastUpdateDate(String value) { lastUpdateDate = value; }

	//-----------------------------------------------------------

	public String getPatRegDate() { return patRegDate; }
	public void setPatRegDate(String value) { patRegDate = value; }
	

	//-----------------------------------------------------------
	//-----------------------------------------------------------
	//-----------------------------------------------------------

	public void copyFrom(MediaData d) {
		key = d.key;
		sid = d.sid;
		title = d.title;
		mediaType = d.mediaType;
		mimeType = d.mimeType;
		duration = d.duration;
		mediaWidth = d.mediaWidth;
		mediaHeight = d.mediaHeight;
		mediaFile = d.mediaFile;
		mediaSize = d.mediaSize;
		screenWidth = d.screenWidth;
		screenHeight = d.screenHeight;
		screenFile = d.screenFile;
		screenSize = d.screenSize;
		thumbnailWidth = d.thumbnailWidth;
		thumbnailHeight = d.thumbnailHeight;
		thumbnailFile = d.thumbnailFile;
		thumbnailSize = d.thumbnailSize;
		ownerId = d.ownerId;
		regIp = d.regIp;
		regDate = d.regDate;
		lastUpdateDate = d.lastUpdateDate;
		patRegDate = d.patRegDate;
	}
}
