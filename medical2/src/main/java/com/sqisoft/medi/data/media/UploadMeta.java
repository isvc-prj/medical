package com.sqisoft.medi.data.media;

import java.util.List;

import com.sqisoft.medi.enums.MDMediaType;
import com.sqisoft.medi.enums.MDYesNo;

public class UploadMeta {
	public MDMediaType type;
	public String title;
	public Double duration;
	
	public UploadPatient patient;
	public UploadOperation operation;
	public List<UploadMask> masks;
}
