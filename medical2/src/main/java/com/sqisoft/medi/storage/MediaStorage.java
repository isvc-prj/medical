package com.sqisoft.medi.storage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileDeleteStrategy;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;

import com.sqisoft.medi.dao.MediaStorageDao;
import com.sqisoft.medi.data.storage.MediaStorageAttribute;
import com.sqisoft.medi.utils.FileUtil;

/**
 * 미디어 파일 스토로지
 * @author Administrator
 *
 */
public class MediaStorage {
	protected MediaStorage() {
	}
	
	/**
	 * 
	 * @param dao
	 * @param uid MM_MEDIA의 MM_UID
	 */
	public MediaStorage(MediaStorageDao dao, String uid) {
		this.dao = dao;
		this.uid = uid;
		
		storageAttr = dao.get(uid);
		
		//storageAttr = new MediaStorageAttribute("DEF", "e:\\medi_storage", true);
	}
	
	//-----------------------------------------------------------------------------------
	
	public static final int MS_MEDIA = 0;
	public static final int MS_SCREEN = 1;
	public static final int MS_SNAPSHOT = 2;
	
	public static final String[] MS_DIRS = new String[] {"", "screen", "snapshot"};
	
	/**
	 * 미디어 파일 버퍼 크기 (1MB)
	 */
	public static final int BUFSIZ = 1048576;
	
	//-----------------------------------------------------------------------------------

	MediaStorageDao dao;
	
	//-----------------------------------------------------------------------------------

	private String uid;
	
	private MediaStorageAttribute storageAttr;
	
	//-----------------------------------------------------------------------------------
	
	public String storageID() { return storageAttr.getId(); }
	
	//-----------------------------------------------------------------------------------
	
	private String prepareDir(String ...arg) {
		String rootPath = storageAttr.getPath();
		
		String path = FileUtil.combinePath(rootPath, uid);
		File f = new File(path);
		
		if (!f.exists()) f.mkdir();
		
		for (String s : arg) {
			path = FileUtil.combinePath(path, s);
			f = new File(path);
			if (!f.exists()) f.mkdir();
		}
		return path;
	}
	
	private String prepareDir(int type) {
		return prepareDir(type, null);
	}
	
	private String prepareDir(int type, String fileName){
		String dir = MS_DIRS[type];
		if (dir != null && !dir.isEmpty()) {
			dir = prepareDir(dir);
		}else {
			dir = prepareDir();
		}
		
		if (fileName != null && !fileName.isEmpty())
			return FileUtil.combinePath(dir, fileName);
		return dir;
	}
	
	//-----------------------------------------------------------------------------------
	
//	public void clear() {
//		String rootPath = storageAttr.getPath();
//		clearFiles(new File(rootPath));
//	}
//	
//	private void clearFiles(File file) {
//		if (file.isDirectory()) {
//			String[] childPaths = file.list();
//			
//			for (String childPath : childPaths) {
//				File child = new File(childPath);
//				clearFiles(child);
//			}
//		}
//		
//		file.delete();
//	}
	
	//-----------------------------------------------------------------------------------
	
	public void clear() throws Exception {
		String rootPath = storageAttr.getPath();
		
		String path = FileUtil.combinePath(rootPath, uid);
		
		int numTry = 5;
		boolean retry = false;

		do
		{
			try
			{
				retry = false;
				
				//FileUtils.deleteDirectory(new File(path));
				FileUtil.forceDelete(new File(path));
			}
			catch (Exception e)
			{
				System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
				System.out.println(e);
				
				numTry--;
				if (numTry > 0) {
					retry = true;
					
					TimeUnit.SECONDS.sleep(1);
					
				}else {
					retry = false;
					
					throw e;
				}
			}
			
		}
		while (retry);
		
	}

	public void clear(int type) throws Exception {
		String path = prepareDir(type);
		
		//FileUtils.deleteDirectory(new File(path));
		FileUtil.forceDelete(new File(path));
	}
	
	//-----------------------------------------------------------------------------------
	
	/**
	 * 할당된 폴더 존재 하는 지 확인
	 * @return
	 */
	public boolean exists() {
		String rootPath = storageAttr.getPath();
		
		String path = FileUtil.combinePath(rootPath, uid);
		File f = new File(path);
		
		return f.exists();
	}
	
	//-----------------------------------------------------------------------------------
	
	/**
	 * 파일이 존재 하는 지 확인
	 * @param type
	 * @param fileName
	 * @return
	 */
	public boolean existsFile(int type, String fileName) {
		String filePath = prepareDir(type, fileName);
		return new File(filePath).exists();
	}
	
	//-----------------------------------------------------------------------------------
	
	public File file(int type, String fileName) {
		String filePath = prepareDir(type, fileName);
		return new File(filePath);
	}
	
	//-----------------------------------------------------------------------------------
	
	public File move(int type, MultipartFile mfile) throws Exception {
		return move(type, mfile, mfile.getOriginalFilename());
	}
	
	public File move(int type, MultipartFile mfile, boolean overrideFile) throws Exception {
		return move(type, mfile, mfile.getOriginalFilename(), overrideFile);
	}
	
	public File move(int type, MultipartFile mfile, String fileName) throws Exception {
		return move(type, mfile, fileName, false);
	}
	
	public File move(int type, MultipartFile mfile, String fileName, boolean overrideFile) throws Exception {
		if (mfile != null && mfile.getSize() > 0) {
			String filePath = prepareDir(type, fileName);
			
			File destFile = new File(filePath);
			if (overrideFile && destFile.exists())
				destFile.delete();
			
			mfile.transferTo(destFile);
			
			return destFile;
		}
		return null;
	}
	
	//-----------------------------------------------------------------------------------
	
	public void remove(int type, String fileName) throws Exception {
		String filePath = prepareDir(type, fileName);
		
		File destFile = new File(filePath);
		destFile.delete();
	}
	
	//-----------------------------------------------------------------------------------
	
	public void write(int type, String fileName, InputStream in) throws Exception {
		String filePath = prepareDir(type, fileName);
		
		FileOutputStream out = null;
		File f = new File(filePath);
		
		try
		{
			f.createNewFile();
		
			out = new FileOutputStream(f);
			FileUtil.write(in, out, BUFSIZ);
		}
		catch (Exception e)
		{
			throw e;
		}
		finally
		{
			if (out != null)
				out.close();
			
			try { in.close(); }
			catch(Exception e) {}
		}
	}
	
	//-----------------------------------------------------------------------------------

	public void read(int type, String fileName, OutputStream out) throws Exception {
		String filePath = prepareDir(type, fileName);
		
		FileInputStream in = null;
		File f = new File(filePath);
		
		try
		{
			in = new FileInputStream(f);
			
			FileUtil.write(in, out, BUFSIZ);
		}
		catch (Exception e)
		{
			throw e;
		}
		finally
		{
			if (in != null)
				in.close();
			
			try { out.close(); }
			catch(Exception e) {}
		}
	}
	
}
