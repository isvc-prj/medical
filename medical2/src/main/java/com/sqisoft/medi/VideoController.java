package com.sqisoft.medi;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.sqisoft.medi.utils.SearchFilterUtil;

@Controller
public class VideoController {

	@RequestMapping(value="/video/{id}", method = RequestMethod.GET)
	public String view(
			@PathVariable(value="id") String id,
			@RequestParam(required=false, defaultValue="") String q,
			Model model) throws Exception {
		
		SearchFilterUtil.delivery(q, model);
		
		model.addAttribute("key", id);
		
		return "video/view";
	}

	@RequestMapping(value="/video/edit/{id}", method = RequestMethod.GET)
	public String edit(
			@PathVariable(value="id") String id,
			@RequestParam(required=false, defaultValue="") String q,
			Model model) throws Exception {
		
		SearchFilterUtil.delivery(q, model);
		
		model.addAttribute("key", id);
		
		return "video/edit";
	}

	@RequestMapping(value="/video/upload", method = RequestMethod.GET)
	public String upload(@RequestParam(required=false, defaultValue="") String q,
			Model model) throws Exception {
		
		SearchFilterUtil.delivery(q, model);
		
		return "video/upload";
	}

	@RequestMapping(value="/video/upload1", method = RequestMethod.GET)
	public String upload1(@RequestParam(required=false, defaultValue="") String q,
			Model model) throws Exception {
		
		SearchFilterUtil.delivery(q, model);
		
		return "video/upload1";
	}

	@RequestMapping(value="/video/edit1/{id}", method = RequestMethod.GET)
	public String edit1(
			@PathVariable(value="id") String id,
			@RequestParam(required=false, defaultValue="") String q,
			Model model) throws Exception {
		
		SearchFilterUtil.delivery(q, model);
		
		model.addAttribute("key", id);
		
		return "video/edit1";
	}

	//	@RequestMapping(value="/a.mp4", method = RequestMethod.GET)
	//	@ResponseBody
	//	public byte[] mp4file() throws IOException {
	//		InputStream in = getClass().getResourceAsStream("/com/sqisoft/medi/data/a.mp4");
	//		return IOUtils.toByteArray(in);
	//	}
}
