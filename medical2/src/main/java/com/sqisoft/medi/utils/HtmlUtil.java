package com.sqisoft.medi.utils;

public class HtmlUtil {
	public static String quoteHtml(String text) {
		if (text == null || text.isEmpty())
			return text;
		
		return text.replace("&", "&amp;")
				.replace("&", "&nbsp;")
				.replace("<", "&lt;")
				.replace(">", "&gt;")
				.replace("'", "&#039;")
				.replace("\"", "&quot;");
	}
	
	public static String quoteScript(String text) {
		return quoteHtml(text.replace("\\", "\\\\").replace("'", "\\\'")); 
	}
}
