package com.sqisoft.medi.utils;

import java.io.IOException;
import java.util.ArrayList;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.type.TypeFactory;

public class JsonUtil {
	/**
	 * JSON 문자열에서 객체로 변환합니다.
	 * @param jsonData JSON 인코딩 문장열
	 * @return 이미지 전송 정보 중 이미지 정보
	 * @throws Exception 예외
	 */
	public static <T> T decode (Class<T> cls, String jsonData) {
		ObjectMapper om = new ObjectMapper();
		
		try
		{
			return om.readValue(jsonData, cls);
		}
		catch (JsonParseException jpe)
		{
			om = null;
			return null;
		}
		catch (JsonMappingException jme)
		{
			om = null;
			return null;
		}
		catch (IOException ioe)
		{
			om = null;
			return null;
		}		
	}
	
	/**
	 * JSON 문자열에서 객체로 변환합니다.
	 * @param jsonData JSON 인코딩 문장열
	 * @return 이미지 전송 정보 중 이미지 정보
	 * @throws Exception 예외
	 */
	public static <T> ArrayList<T> decodeList (Class<T> cls, String jsonData) {
		ObjectMapper om = new ObjectMapper();
		
		try
		{
			return om.readValue(jsonData, TypeFactory.defaultInstance().constructCollectionType(ArrayList.class, cls));
		}
		catch (JsonParseException jpe)
		{
			om = null;
			return null;
		}
		catch (JsonMappingException jme)
		{
			om = null;
			return null;
		}
		catch (IOException ioe)
		{
			om = null;
			return null;
		}		
	}

	/**
	 * 객체를 JSON 문자열로 변환합니다.
	 * @param value 객체
	 * @return JSON 문자열
	 */
	public static String encode(Object value){
		ObjectMapper om = new ObjectMapper();
		
		try
		{
			return om.writeValueAsString(value);
		}
		catch (JsonGenerationException jge)
		{
			return null;
		}
		catch (JsonMappingException jme)
		{
			return null;
		}
		catch (IOException ioe)
		{
			return null;
		}		
	}


}
