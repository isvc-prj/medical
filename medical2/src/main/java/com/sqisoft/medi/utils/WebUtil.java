package com.sqisoft.medi.utils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;

/**
 * 웹 관련 도구
 * @author Administrator
 *
 */
public class WebUtil {
	
	public static String uid() {
		UUID uid = UUID.randomUUID();
		return uid.toString().replaceAll("-", "").toUpperCase();
	}
	
	/**
	 * 아이피를 가져옵니다.
	 * @return 아이피
	 */
	public static String getRemoteIP(){
		HttpServletRequest req = ((ServletRequestAttributes)RequestContextHolder.currentRequestAttributes()).getRequest();
		return getRemoteIP(req);
	}
	
	/**
	 * 아이피를 가져옵니다.
	 * @param req HTTP 요청 정보
	 * @return 아이피
	 */
	public static String getRemoteIP(HttpServletRequest req){
		String ip = req.getHeader("X-FORWARDED-FOR");
		if (ip == null)
			ip = req.getRemoteAddr();
		
		return ip;
	}
	
	//-----------------------------------------------------------
	
	/**
	 * 파일명으로 업로드된 multipart 파일 목록에서 찾는다
	 * @param files multipart 파일 목록
	 * @param filename 찾을 파일명
	 * @return MultipartFile 인스턴스
	 */
	public static MultipartFile findFile(MultipartFile[] files, String filename) {
		int len = files != null ? files.length : 0;
		for (int i=0; i < len; i++) {
			MultipartFile file = files[i];
			String ufilename = file.getOriginalFilename();
			
			if (filename.equalsIgnoreCase(ufilename))
				return file;
		}
		return null;
	}

	//-----------------------------------------------------------
	
	/**
	 * 파일을 다운로드합니다.
	 * @param response HTTP 응답 정보
	 * @param file 다운로드할 파일 객체
	 * @return null이면 정상, null이 아니면 발생한 예외의 객체입니다.
	 */
	public static Exception download(HttpServletResponse response, File file) {
		return download(response, file, null);
	}
	
	/**
	 * 파일을 다운로드합니다.
	 * @param response HTTP 응답 정보
	 * @param file 다운로드할 파일 객체
	 * @param filename 파일명
	 * @return null이면 정상, null이 아니면 발생한 예외의 객체입니다.
	 */
	public static Exception download(HttpServletResponse response, File file, String filename) {
		long length = file.length();
		FileInputStream in = null;
		Exception r = null;
		
		try
		{
			in = new FileInputStream(file);
		}
		catch (FileNotFoundException fnfe)
		{
			DebugUtil.print(fnfe);
			
			r = fnfe;
			System.out.println("[WEB-DOWNLOAD] in open FileNotFoundException");
		}
		catch (Exception e)
		{
			DebugUtil.print(e);
			
			r = e;
			System.out.println("[WEB-DOWNLOAD] in open IOException");
		}
		finally
		{
		}
		
		if (r != null)
			return r;
		
		if (filename == null){
			filename = file.getName();
		}
		
		r = download(response, filename, in, length);

		return r;
	}
	
	/**
	 * 바이너리 데이터를 다운로드합니다.
	 * @param response HTTP 응답 정보
	 * @param filename 파일명
	 * @param bytes 바이너리 데이터
	 * @return null이면 정상, null이 아니면 발생한 예외의 객체입니다.
	 */
	public static Exception download(HttpServletResponse response, String filename, byte[] bytes) {
		ByteArrayInputStream in = new ByteArrayInputStream(bytes);
		
		Exception r = download(response, filename, in, bytes.length);

		return r;
	}
	
	/**
	 * 입력 스트림에서 바리너리 데이터를 읽어 다운로드합니다.
	 * @param response HTTP 응답 정보
	 * @param filename 파일명
	 * @param in 입력 스트림
	 * @param length 읽을 길이
	 * @return null이면 정상, null이 아니면 발생한 예외의 객체입니다.
	 */
	public static Exception download(HttpServletResponse response, String filename, InputStream in, long length) {
		OutputStream out = null;
		Exception ex = null;
		
		try
		{
			out = response.getOutputStream();
						
			String baseName = FileUtil.safeBaseName(filename);
			String encFileName = FileUtil.encodeFileName(baseName);
			
	        response.setHeader("Content-Disposition", "attachment;filename=\""+encFileName+"\";");
			response.setHeader("Content-Transfer-Encoding", "binary");
			
			if (length > 0)
				response.setHeader("Content-Length", "" + length);
			
			FileUtil.write(in, out);
		}
		catch (IOException ioe)
		{
			DebugUtil.print(ioe);
			
			System.out.println("[WEB-DOWNLOAD] IOException");
			ex = ioe;
		}
		catch (Exception e)
		{
			DebugUtil.print(e);
			
			System.out.println("[WEB-DOWNLOAD] Exception");
			ex = e;
		}
		finally
		{
			
			if (in != null){
				try
				{
					in.close();
				}
				catch (IOException ioe)
				{
					System.out.println("[WEB-DOWNLOAD] in close IOException");
				}
				catch (Exception e)
				{
					System.out.println("[WEB-DOWNLOAD] in close Exception");
				}
			}
			
			if (out != null){
				try
				{
					out.close();
				}
				catch (IOException ioe)
				{
					System.out.println("[WEB-DOWNLOAD] out close IOException");
				}
				catch (Exception e)
				{
					System.out.println("[WEB-DOWNLOAD] out close Exception");
				}
			}
		}
		
		return ex;
	}

	//-----------------------------------------------------------
	//-----------------------------------------------------------
	//-----------------------------------------------------------
	
	/**
	 * 파일을 다운로드합니다.
	 * @param response HTTP 응답 정보
	 * @param file 다운로드할 파일 객체
	 * @return null이면 정상, null이 아니면 발생한 예외의 객체입니다.
	 */
	public static Exception contentDownload(HttpServletResponse response, File file) {
		return contentDownload(response, file, null);
	}
	
	/**
	 * 파일을 다운로드합니다.
	 * @param response HTTP 응답 정보
	 * @param file 다운로드할 파일 객체
	 * @param filename 파일명
	 * @return null이면 정상, null이 아니면 발생한 예외의 객체입니다.
	 */
	public static Exception contentDownload(HttpServletResponse response, File file, String contentType) {
		long length = file.length();
		FileInputStream in = null;
		Exception r = null;
		
		try
		{
			in = new FileInputStream(file);
		}
		catch (FileNotFoundException fnfe)
		{
			DebugUtil.print(fnfe);
			
			r = fnfe;
			System.out.println("[WEB-DOWNLOAD] in open FileNotFoundException");
		}
		catch (Exception e)
		{
			DebugUtil.print(e);
			
			r = e;
			System.out.println("[WEB-DOWNLOAD] in open IOException");
		}
		finally
		{
		}
		
		if (r != null)
			return r;
		
		if (contentType == null){
			contentType = FileUtil.contentType(file);
		}
		
		r = contentDownload(response, contentType, in, length);

		return r;
	}
	
	/**
	 * 바이너리 데이터를 다운로드합니다.
	 * @param response HTTP 응답 정보
	 * @param filename 파일명
	 * @param bytes 바이너리 데이터
	 * @return null이면 정상, null이 아니면 발생한 예외의 객체입니다.
	 */
	public static Exception contentDownload(HttpServletResponse response, String contentType, byte[] bytes) {
		ByteArrayInputStream in = new ByteArrayInputStream(bytes);
		
		Exception r = contentDownload(response, contentType, in, bytes.length);

		return r;
	}
	
	/**
	 * 입력 스트림에서 바리너리 데이터를 읽어 다운로드합니다.
	 * @param response HTTP 응답 정보
	 * @param filename 파일명
	 * @param in 입력 스트림
	 * @param length 읽을 길이
	 * @return null이면 정상, null이 아니면 발생한 예외의 객체입니다.
	 */
	public static Exception contentDownload(HttpServletResponse response, String contentType, InputStream in, long length) {
		OutputStream out = null;
		Exception ex = null;
		
		try
		{
			out = response.getOutputStream();
						
	        response.setHeader("Content-Type", contentType);
			
			if (length > 0)
				response.setHeader("Content-Length", "" + length);
			
			FileUtil.write(in, out);
		}
		catch (IOException ioe)
		{
			DebugUtil.print(ioe);
			
			System.out.println("[WEB-DOWNLOAD] IOException");
			ex = ioe;
		}
		catch (Exception e)
		{
			DebugUtil.print(e);
			
			System.out.println("[WEB-DOWNLOAD] Exception");
			ex = e;
		}
		finally
		{
			
			if (in != null){
				try
				{
					in.close();
				}
				catch (IOException ioe)
				{
					System.out.println("[WEB-DOWNLOAD] in close IOException");
				}
				catch (Exception e)
				{
					System.out.println("[WEB-DOWNLOAD] in close Exception");
				}
			}
			
			if (out != null){
				try
				{
					out.close();
				}
				catch (IOException ioe)
				{
					System.out.println("[WEB-DOWNLOAD] out close IOException");
				}
				catch (Exception e)
				{
					System.out.println("[WEB-DOWNLOAD] out close Exception");
				}
			}
		}
		
		return ex;
	}

}
