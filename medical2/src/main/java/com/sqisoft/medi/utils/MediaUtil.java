package com.sqisoft.medi.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 날짜 관련 도구
 * @author Administrator
 *
 */
public class MediaUtil {
	public static class TimeSpan {
		public TimeSpan() {}
		public TimeSpan(double origin, long value, int hours, int minutes, int seconds) {
			this.origin = origin;
			this.value = value;
			this.hours = hours;
			this.minutes = minutes;
			this.seconds = seconds;
		}
		
		public double origin;
		
		public long value;
		public int hours;
		public int minutes;
		public int seconds;
		
		public String toString(boolean useHour) {
			
			String h = MediaUtil.tf(hours);
			String m = MediaUtil.tf(minutes);
			String s = MediaUtil.tf(seconds);
			
			if (useHour)
				return h + ":" + m + ":" + s;
			return h + ":" + s;
		}
	
		@Override
		public String toString() {
			return toString(false);
		}
	}

	//-----------------------------------------------------------
	//-----------------------------------------------------------
	//-----------------------------------------------------------

	
	public static TimeSpan toTimeSpan(String duration) {
		return toTimeSpan(Double.parseDouble(duration));
	}
	
	public static TimeSpan toTimeSpan(double duration) {
		
		double value = Math.round(duration);
		
		double h = value / 60 / 60;
		double m = (value / 60) % 60;
		double s = value % 60;

		h = Math.floor(h);
		m = Math.floor(m);
		s = Math.ceil(s);
		
		return new TimeSpan(duration, (long)value, (int)h, (int)m, (int)s);
	}
	
	public static String toTimeSpanFormat(String duration, boolean useHour) {
		return toTimeSpanFormat(Double.parseDouble(duration), useHour);
	}
	
	public static String toTimeSpanFormat(double duration, boolean useHour) {
		TimeSpan ts = toTimeSpan(duration);
		return ts.toString(useHour);
	}
	
	//-----------------------------------------------------------
	
	private static String tf(int value) {
		return value < 10 ? "0" + value : "" + value;
	}

}
