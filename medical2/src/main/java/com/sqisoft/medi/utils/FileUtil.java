package com.sqisoft.medi.utils;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.io.FileDeleteStrategy;


/**
 * 파일 관련 도구
 * @author Administrator
 *
 */
public class FileUtil {
	/**
	 * 기본 버퍼 크기
	 */
	public static final int BUFSIZ = 20480;
	
	//-----------------------------------------------------------------------------------

	/**
	 * 안전한 파일명을 가져옵니다.
	 * @param basename 파일명
	 * @return 안전한 파일명
	 */
	public static String safeBaseName(String basename){
		return basename.replace("\\", "").replace("/", "").replace(":", "").replace("*", "").replace("?", "")
				.replace("\"", "").replace("<", "").replace(">", "").replace("|", "");
	}
	
	/**
	 * 파일명을 URL 형식으로 인코딩합니다.
	 * @param filename 파일명
	 * @return 인코딩된 파일명
	 * @throws Exception 예외
	 */
	public static String encodeFileName(String filename)
			throws Exception {
		return java.net.URLEncoder.encode(filename, "UTF-8");
	}

	//-----------------------------------------------------------------------------------
	
	/**
	 * 경로와 인자들을 경로 구분자로 구분하여 가져옵니다.
	 * @param path 경로
	 * @param arg 인자1, 인자2, ... 인자N
	 * @return 합쳐진 경로
	 */
	public static String combinePath(String path, String ...arg) {
		java.nio.file.Path oPath = java.nio.file.Paths.get(path, arg);
		return oPath.toString();
	}

	//-----------------------------------------------------------------------------------
	
	/**
	 * 파일의 mime-type을 가져옵니다.
	 * @param file 파일 객체
	 * @return mime-type
	 */
	public static String contentType(File file) {
		Path path = Paths.get(file.getAbsolutePath());
		
		String mimeType = null;
		try
		{
			mimeType = Files.probeContentType(path);
		}
		catch (Exception e)
		{
			DebugUtil.print(e);
		}
		return mimeType;
	}

	//-----------------------------------------------------------------------------------
	
	/**
	 * 입력 스트림을 읽어 출력 스트림에 씁니다. 
	 * @param in 입력 스트림
	 * @param out 출력 스트림
	 * @throws Exception 예외
	 */
	public static void write(InputStream in, OutputStream out) throws Exception{
		write(in, out, BUFSIZ);
	}
	
	/**
	 * 입력 스트림을 읽어 출력 스트림에 씁니다.
	 * @param in 입력 스트림
	 * @param out 출력 스트림
	 * @param bufsiz 버퍼 크기
	 * @throws Exception 예외
	 */
	public static void write(InputStream in, OutputStream out, int bufsiz) throws Exception{
        byte[] buf = new byte[bufsiz];
        int len = -1;
		
        while((len = in.read(buf, 0, buf.length)) != -1) {
        	out.write(buf, 0, len);
        }
	}

	//-----------------------------------------------------------------------------------
	
	public static void forceDelete(File file) throws Exception {
		if (file.isDirectory()) {
			String[] paths = file.list();
			for (String path : paths) {
				forceDelete(new File(path));
			}
			
			FileDeleteStrategy.FORCE.delete(file);
		}else {
			FileDeleteStrategy.FORCE.delete(file);
		}
	}
}
