package com.sqisoft.medi.utils;

import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;

import org.springframework.ui.Model;

import com.sqisoft.medi.data.filters.SearchMediaFilter;

public class SearchFilterUtil {
	public static SearchMediaFilter get(HttpServletRequest request, Model model) throws Exception {
		String q = request.getParameter("q");
		
		return get(q, model);
	}
	
	public static SearchMediaFilter get(String q, Model model) throws Exception {
		if (q != null && !q.isEmpty()) {
			SearchMediaFilter filter = JsonUtil.decode(SearchMediaFilter.class, q);
			
//			System.out.println("filter" + filter);
//			
//			System.out.println("q=" + q);
//			System.out.println("encode q=" + HtmlUtil.quoteHtml(q));
			
			if (model != null) {
				String qEnc = URLEncoder.encode(q, "UTF-8");
				
				model.addAttribute("q", q);
				model.addAttribute("q$enc", qEnc);
				model.addAttribute("q$filter", filter);
			}
			
			return filter;
		}
		return null;
	}
	
	public static void delivery(HttpServletRequest request, Model model) throws Exception {
		String q = request.getParameter("q");
		
		delivery(q, model);
	}
	
	public static void delivery(String q, Model model) throws Exception {
		if (q != null && !q.isEmpty()) {
//			System.out.println("q=" + q);
//			System.out.println("encode q=" + HtmlUtil.quoteHtml(q));
			
			if (model != null) {
				String qEnc = URLEncoder.encode(q, "UTF-8");
				
				model.addAttribute("q", q);
				model.addAttribute("q$enc", qEnc);
			}
		}
	}
}
