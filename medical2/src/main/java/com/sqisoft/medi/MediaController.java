package com.sqisoft.medi;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.channels.WritableByteChannel;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.sqisoft.medi.dao.MediaStorageDao;
import com.sqisoft.medi.data.media.MediaData;
import com.sqisoft.medi.data.media.MediaLabelData;
import com.sqisoft.medi.data.media.MediaSnapshotData;
import com.sqisoft.medi.enums.MDMediaType;
import com.sqisoft.medi.services.MediaService;
import com.sqisoft.medi.storage.MediaStorage;
import com.sqisoft.medi.utils.FileUtil;
import com.sqisoft.medi.utils.WebUtil;

@Controller
public class MediaController {
	
	@Autowired
	MediaService svc;
	
	@Autowired
	private MediaStorageDao storageDao;
	
	//-----------------------------------------------------------

	@RequestMapping(value="/media/screen/{id}", method = RequestMethod.GET)
	public void mediaScreen(
			@PathVariable(value="id") String id,
			HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		MediaData data = svc.get(id);
		
		MediaStorage ms = new MediaStorage(storageDao, id);
		File file = ms.file(MediaStorage.MS_SCREEN, data.getScreenFile());
		
		Exception ex = WebUtil.contentDownload(response, file, "image/jpeg");
		
		if (ex != null)
			throw ex;
	}

	@RequestMapping(value="/media/thumbnail/{id}", method = RequestMethod.GET)
	public void mediaScreenThumbnail(
			@PathVariable(value="id") String id,
			HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		MediaData data = svc.get(id);
		
		MediaStorage ms = new MediaStorage(storageDao, id);
		File file = ms.file(MediaStorage.MS_SCREEN, data.getThumbnailFile());
		
		Exception ex = WebUtil.contentDownload(response, file, "image/jpeg");
		
		if (ex != null)
			throw ex;
	}

	@RequestMapping(value="/media/snapshot/{id}/{time:.+}", method = RequestMethod.GET)
	public void mediaSnapshot(
			@PathVariable(value="id") String id,
			@PathVariable(value="time") String time,
			HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		double itime = Double.parseDouble(time);
		MediaSnapshotData data = svc.getSnapshot(id, itime);
		
		MediaStorage ms = new MediaStorage(storageDao, id);
		File file = ms.file(MediaStorage.MS_SNAPSHOT, data.getSnapFile());
		
		Exception ex = WebUtil.contentDownload(response, file, "image/jpeg");
		
		if (ex != null)
			throw ex;
	}

	@RequestMapping(value="/media/snapshot/thumbnail/{id}/{time:.+}", method = RequestMethod.GET)
	public void mediaSnapshotThumbnail(
			@PathVariable(value="id") String id,
			@PathVariable(value="time") String time,
			HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		double itime = Double.parseDouble(time);
		MediaSnapshotData data = svc.getSnapshot(id, itime);
		
		MediaStorage ms = new MediaStorage(storageDao, id);
		File file = ms.file(MediaStorage.MS_SNAPSHOT, data.getThumbnailFile());
		
		Exception ex = WebUtil.contentDownload(response, file, "image/jpeg");
		
		if (ex != null)
			throw ex;
	}

	@RequestMapping(value="/media/snapshot-label/{id}/{time:.+}/{seq:.+}", method = RequestMethod.GET)
	public void mediaSnapshotLabel(
			@PathVariable(value="id") String id,
			@PathVariable(value="time") String time,
			@PathVariable(value="seq") String seq,
			HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		double itime = Double.parseDouble(time);
		int iseq = Integer.parseInt(seq);
		MediaLabelData data = svc.getSnapshotLabel(id, itime, iseq);
		
		MediaStorage ms = new MediaStorage(storageDao, id);
		File file = ms.file(MediaStorage.MS_SNAPSHOT, data.getFileNm());
		
		Exception ex = WebUtil.contentDownload(response, file, "image/png");
		
		if (ex != null)
			throw ex;
	}
	
	//-----------------------------------------------------------
	
	@RequestMapping(value="/media/{id}", method = RequestMethod.GET)
	public void media(
			@PathVariable(value="id") String id,
			HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		MediaData data = svc.get(id);
		
		MDMediaType mediaType = data.getMediaType();
		
		Exception ex = null;
		if (mediaType == MDMediaType.MDMTYPE_VIDEO)
			ex = mediaStream(id, data, request, response);
		else if (mediaType == MDMediaType.MDMTYPE_IMAGE)
			ex = imageStream(id, data, request, response);
		
		if (ex != null)
			throw ex;
	}
	
	//-----------------------------------------------------------
	
	private Exception imageStream(
			String id,
			MediaData data,
			HttpServletRequest request,
			HttpServletResponse response) throws Exception {
	
		String mimeType = data.getMimeType();
		boolean existMimeType = mimeType != null && !mimeType.isEmpty();
		
		MediaStorage ms = new MediaStorage(storageDao, id);
		File file = ms.file(MediaStorage.MS_MEDIA, data.getMediaFile());
		
		if (!existMimeType)
			mimeType = FileUtil.contentType(file);
		
		return WebUtil.contentDownload(response, file, mimeType);
	}
	
	//-----------------------------------------------------------
	
	private Exception mediaStream(
			String id,
			MediaData data,
			HttpServletRequest request,
			HttpServletResponse response) {
		
		String mimeType = data.getMimeType();
		boolean existMimeType = mimeType != null && !mimeType.isEmpty();
		
		MediaStorage ms = new MediaStorage(storageDao, id);
		File file = ms.file(MediaStorage.MS_MEDIA, data.getMediaFile());
		
		if( !file.exists() )
			return new FileNotFoundException();

		int bufsiz = 1 * 1024 * 1024; // 1MB
		
		String range = request.getHeader("range");
		
		RandomAccessFile inFile = null;
		
		try
		{
			inFile = new RandomAccessFile(file, "r");
		}
		catch (FileNotFoundException ex)
		{
			return ex;
		}
		
		
		FileChannel in = inFile.getChannel();
		WritableByteChannel out = null;
		
		//ByteBuffer buffer = ByteBuffer.allocate(bufsiz);

		long rangeStart = 0, rangeEnd = 0;
		boolean isPart = false;
		Exception rex = null;

		try
		{
			long length = in.size();
			
			//System.out.println("<<<MEDIA REQUEST>>> range=" + range + ", length=" + length);

			// FORMAT: bytes={start}-{end}
			if(range != null && !range.isEmpty()) {
				if(range.endsWith("-"))
					range = range+(length - 1);
				
				int irange = range.indexOf("=");
				int itoken = range.trim().indexOf("-");
				
				rangeStart = Long.parseLong(range.substring(irange + 1, itoken));
				rangeEnd = Long.parseLong(range.substring(itoken + 1));
				
				if(rangeStart > 0)
					isPart = true;
			}else{
				rangeStart = 0;
				rangeEnd = length - 1;
			}
	
			long partSize = rangeEnd - rangeStart + 1;
	
			response.reset();
			response.setStatus(isPart ? HttpServletResponse.SC_PARTIAL_CONTENT : HttpServletResponse.SC_OK);
			response.setContentType(existMimeType ? mimeType : FileUtil.contentType(file));

			response.setHeader("Content-Range", "bytes " + rangeStart + "-" + rangeEnd + "/" + length);
			response.setHeader("Accept-Ranges", "bytes");
			response.setHeader("Content-Length", "" + partSize);
	
			out = Channels.newChannel(response.getOutputStream());

			//in.position(rangeStart);
			
			in.transferTo(rangeStart, in.size(), out);
			
//			while(in.read(buffer) > 0) {
//				buffer.flip();
//				out.write(buffer);
//				buffer.clear();
//			}
		}
		catch(IOException e)
		{
			rex = e;
		}
		finally
		{
			System.out.println("<<<MEDIA::STREAM::CLOSING::IN-CHANNEL>>>");
			
			try
			{
				in.close();
				
				System.out.println("<<<MEDIA::STREAM::CLOSED::IN-CHANNEL>>>");
			}
			catch(Exception ex)
			{
				System.out.println("<<<MEDIA::STREAM::CLOSING::IN-CHANNEL::EXCEPTION>>>");
				System.out.println(ex);
			}
			
			//-----------------------------------------------------------

			System.out.println("<<<MEDIA::STREAM::CLOSING::IN-FILE>>>");
			
			try
			{
				inFile.close();
				
				System.out.println("<<<MEDIA::STREAM::CLOSED::IN-FILE>>>");
			}
			catch(Exception ex)
			{
				System.out.println("<<<MEDIA::STREAM::CLOSING::IN-FILE::EXCEPTION>>>");
				System.out.println(ex);
			}
			
			//-----------------------------------------------------------
		
			System.out.println("<<<MEDIA::STREAM::CLOSING::OUT-CHANNEL>>>");
			
			try
			{
				out.close();
				
				System.out.println("<<<MEDIA::STREAM::CLOSED::OUT-CHANNEL>>>");
			}
			catch(Exception ex)
			{
				System.out.println("<<<MEDIA::STREAM::CLOSING::OUT-CHANNEL::EXCEPTION>>>");
				System.out.println(ex);
			}
			
			System.out.println("---------------------------------------------------------------------");
		}
		
		return rex;
	}

	//-----------------------------------------------------------

	@RequestMapping(value="/media-debug/{id}", method = RequestMethod.GET)
	public void media_by_RESOURCE(
			@PathVariable(value="id") String id,
			HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		URL url = getClass().getResource("/com/sqisoft/medi/data/"+id+".mp4");
		File file = new File(url.toURI());
		if( !file.exists() )
			throw new FileNotFoundException();
		
		String range = request.getHeader("range");
		
		OutputStream out = null;	
		RandomAccessFile in = new RandomAccessFile(file, "r");

		long rangeStart = 0, rangeEnd = 0;
		boolean isPart = false;

		try{
			long length = in.length(); 
			
			//System.out.println("<<<MEDIA REQUEST>>> range=" + range + ", length=" + length);

			// FORMAT: bytes={start}-{end}
			if(range != null && !range.isEmpty()) {
				if(range.endsWith("-"))
					range = range+(length - 1);
				
				int irange = range.indexOf("=");
				int itoken = range.trim().indexOf("-");
				
				rangeStart = Long.parseLong(range.substring(irange + 1, itoken));
				rangeEnd = Long.parseLong(range.substring(itoken + 1));
				
				if(rangeStart > 0)
					isPart = true;
			}else{
				rangeStart = 0;
				rangeEnd = length - 1;
			}
	
			long partSize = rangeEnd - rangeStart + 1;
	
			response.reset();
			response.setStatus(isPart ? HttpServletResponse.SC_PARTIAL_CONTENT : HttpServletResponse.SC_OK);
			response.setContentType(FileUtil.contentType(file));

			response.setHeader("Content-Range", "bytes " + rangeStart + "-" + rangeEnd + "/" + length);
			response.setHeader("Accept-Ranges", "bytes");
			response.setHeader("Content-Length", "" + partSize);
	
			out = response.getOutputStream();

			in.seek(rangeStart);
	
			int bufsiz = 1 * 1024 * 1024; // 1MB
			byte[] buf = new byte[bufsiz];
			
			do{
				int block = partSize > bufsiz ? bufsiz : (int)partSize;
				int len = in.read(buf, 0, block);
				
				out.write(buf, 0, len);
				
				partSize -= block;
			}while(partSize > 0);
		}catch(IOException e){
		}finally{
			try { in.close(); }
			catch(Exception ex) {}
			
			try { out.close(); }
			catch(Exception ex) {}
		}
	}

}
