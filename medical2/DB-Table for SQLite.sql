﻿--
-- 메디컬 미디어 테이블
--

-- 미디어 저장소 정보
CREATE TABLE MM_STORAGE
(
	MM_STORAGE_ID VARCHAR2(20) PRIMARY KEY,	-- 미디어 저장소 아이디
	PATH VARCHAR2(250),						-- 저장소 경로
	USE_YN CHAR(1) DEFAULT 'N',				-- 현재 사용하는 저장소로 설정여부 (Y면 등록 시 해당 저장소를 사용한다)
	REG_ID VARCHAR(20),						-- 등록자 아이디
	REG_DT VARCHAR(14)						-- 등록일
);

INSERT INTO MM_STORAGE(MM_STORAGE_ID,PATH,USE_YN,REG_ID,REG_DT) VALUES('DEF','c:\medi_storage','Y','dev',strftime("%Y%m%d%H%M%S",'now'));

-- 미디어 메타정보
CREATE TABLE MM_MEDIA
(
	MM_UID VARCHAR(32) PRIMARY KEY,			-- 미디어 키
	--
	MM_STORAGE_ID VARCHAR2(20),				-- 미디어 저장소 아이디
	--
	MM_TITLE VARCHAR2(200),					-- 미디어 제목
	MM_TP CHAR(1),							-- 미디어 타입 (V=VIDEO, I=IMAGE)
	MM_MIME VARCHAR(20),					-- 미디어 MIME 타입
	-- 미디어 정보
	MM_DURATION NUMBER,						-- 비디오 시간
	MM_W NUMBER,							-- 미디어 폭
	MM_H NUMBER,							-- 미디어 높이
	MM_FILE VARCHAR2(250),					-- 미디어 파일명
	MM_SIZ NUMBER,							-- 미디어 파일크기
	-- 미디어 스크린샷 정보
	MM_SCR_W NUMBER,						-- 미디어 스크린샷 폭
	MM_SCR_H NUMBER,						-- 미디어 스크린샷 높이
	MM_SCR_FILE VARCHAR2(250),				-- 미디어 스크린샷 파일명
	MM_SCR_SIZ NUMBER,						-- 미디어 스크린샷 파일크기
	-- 미디어 섬네일 정보
	MM_THUMB_W NUMBER,						-- 미디어 섬네일 폭
	MM_THUMB_H NUMBER,						-- 미디어 섬네일 높이
	MM_THUMB_FILE VARCHAR2(250),			-- 미디어 섬네일 파일명
	MM_THUMB_SIZ NUMBER,					-- 미디어 섬네일 파일크기
	--
	REG_STS	CHAR(1) DEFAULT 'R',			-- 등록 상태 (R=RECORDING, C=COMPLETE, D=DELETED)
	REG_ID VARCHAR(20),						-- 등록자 아이디
	IP VARCHAR(15),							-- 등록자 아이피
	REG_DT VARCHAR(14),						-- 등록일
	LAST_UPDT_DT VARCHAR(14)				-- 최종 수정일
);

-- 미디어 환자 및 수슬정보
CREATE TABLE MM_PAT_OP
(
	MM_UID VARCHAR(32),						-- 미디어 키
	PAT_NM VARCHAR2(80),					-- 환자정보, 이름 이니셜
	PAT_NO VARCHAR2(80),					-- 환자정보, 번호
	PAT_ID VARCHAR2(80),					-- 환자정보, 아이디
	PAT_REG_DT VARCHAR(8),					-- 환자정보, 연구등록일
	PAT_GEN CHAR(1),						-- 환자정보, 성별 (M/W)
	PAT_AGE NUMBER(3),						-- 환자정보, 나이(year)
	PAT_WGT NUMBER(3),						-- 환자정보, 체중(kg)
	PAT_HGT NUMBER(3),						-- 환자정보, 신장(cm)
	PAT_CFS_IX NUMBER(2),					-- 환자정보, CFX Ix
	PAT_EXC NUMBER(1),						-- 환자정보, Exclusion
	PAT_HIS NUMBER(1),						-- 환자정보, 수술력
	PAT_EOC_STAT NUMBER(1),					-- 환자정보, 이전 일차 내시경 시행여부
	OP_APPX_REACH NUMBER(1),				-- 수술정보, 맹장 도달
	OP_APPX_REACH_TM NUMBER,				-- 수술정보, 맹장 도달 시간(초)
	OP_APPX_REACH_REM_TM NUMBER,			-- 수술정보, 맹장 회수 시간(초)
	OP_APPX_TOTAL_TM NUMBER,				-- 수술정보, 전체 검사 시간(초)
	OP_RIGHT_COL NUMBER(1),					-- 수술정보, 장정결도 우측
	OP_TRANS_COL NUMBER(1),					-- 수술정보, 장정결도 횡행
	OP_LEFT_COL NUMBER(1),					-- 수술정보, 장정결도 좌측
	OP_BBPS NUMBER,							-- 수술정보, 장정결도 합산
	OP_BBPS_FIT NUMBER(1),					-- 수술정보, 장정결도 (1=적합, 2=부적합)
	OP_CFX_OPI NUMBER(1),					-- 수술정보, CFX 소견
	OP_POLYP_CNT NUMBER						-- 수술정보, 발견한 총 용종개수 (CFX 소견)
);

-- 미디어 마스크
-- MK_SEQ가 0이면 인버트
CREATE TABLE MM_MASK
(
	MM_UID VARCHAR(32),						-- 미디어 키
	MK_SEQ NUMBER,							-- 일련번호
	MK_X NUMBER,							-- X 좌표
	MK_Y NUMBER,							-- Y 좌표
	MK_W NUMBER,							-- 폭
	MK_H NUMBER								-- 높이
);

-- 미디어 스냅샷
CREATE TABLE MM_SNAPSHOT
(
	MM_UID VARCHAR(32),						-- 미디어 키
	SNAP_TM NUMBER,							-- 캡쳐시간(초)
	SNAP_W NUMBER,							-- 캡쳐 이미지 폭
	SNAP_H NUMBER,							-- 캡쳐 이미지 높이
	SNAP_FILE VARCHAR2(250),				-- 캡쳐 이미지 파일명
	SNAP_SIZ NUMBER,						-- 캡쳐 이미지 파일크기
	SNAP_THUMB_W NUMBER,					-- 캡쳐 이미지 섬네일 폭
	SNAP_THUMB_H NUMBER,					-- 캡쳐 이미지 섬네일 높이
	SNAP_THUMB_FILE VARCHAR2(250),			-- 캡쳐 이미지 섬네일 파일명
	SNAP_THUMB_SIZ NUMBER					-- 캡쳐 이미지 섬네일 파일크기
);

-- 미디어 라벨
CREATE TABLE MM_LABEL
(
	MM_UID VARCHAR(32),						-- 미디어 키
	SNAP_TM NUMBER,							-- 캡쳐시간(초)
	LABEL_SEQ NUMBER,						-- 일련번호
	LABEL_X NUMBER,							-- X 좌표
	LABEL_Y NUMBER,							-- Y 좌표
	LABEL_W NUMBER,							-- 폭
	LABEL_H NUMBER,							-- 높이
	LABEL_DAT CLOB,							-- 라벨 도형 데이터
	LABEL_POLYP_POS NUMBER(1),				-- 용종정보, 위치
	LABEL_POLYP_SIZ NUMBER,					-- 용종정보, 크기(mm)
	LABEL_POLYP_SHAPE NUMBER(1),			-- 용종정보, 모양(Yamada type)
	LABEL_POLYP_BIO NUMBER(1),				-- 용종정보, 조직검사
	LABEL_FILE VARCHAR(250)					-- 용종 라벨 이미지 파일명
);
